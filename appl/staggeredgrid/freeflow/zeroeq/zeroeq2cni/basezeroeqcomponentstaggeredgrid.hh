/**
 * \file
 * \ingroup StaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 *        component transport zeroeq equation
 *
 * \copydoc BaseComponentStaggeredGrid
 */

#ifndef DUMUX_BASE_ZEROEQ_COMPONENT_STAGGERED_GRID_HH
#define DUMUX_BASE_ZEROEQ_COMPONENT_STAGGERED_GRID_HH

#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/fmatrix.hh>

#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<appl/staggeredgrid/freeflow/eddyviscosity2cni/eddyviscosity2cniindices.hh>
#include<appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/basecomponentstaggeredgrid.hh>

#include"zeroeq2cnipropertydefaults.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     *        the steady-state component transport zeroeq equation.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class BaseZeroEqComponentStaggeredGrid
    : public BaseComponentStaggeredGrid<TypeTag>
    {
    public:
      typedef BaseComponentStaggeredGrid<TypeTag> ParentTypeComponent;

      typedef typename GET_PROP_TYPE(TypeTag, LocalOperator) Implementation;
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

      typedef typename GET_PROP_TYPE(TypeTag, BCType) BC;
      typedef typename GET_PROP_TYPE(TypeTag, SourceComponentBalance) SourceComponentBalance;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletMassMoleFrac) DirichletMassMoleFrac;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannMassMoleFrac) NeumannMassMoleFrac;

      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      enum { dim = GridView::dimension };

      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;

      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { massMoleFracIdx = Indices::massMoleFracIdx };
      enum { numComponents = Indices::numComponents,
             phaseIdx = Indices::phaseIdx,
             transportCompIdx = Indices::transportCompIdx,
             phaseCompIdx = Indices::phaseCompIdx };

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      // Types to store the velocities and coordinates
      typedef std::vector<double> StoredScalar;
      StoredScalar storedEddyDiffusivity;

      //! \brief Constructor
      BaseZeroEqComponentStaggeredGrid(const BC& bc_,
        const SourceComponentBalance& sourceComponentBalance_,
        const DirichletMassMoleFrac& dirichletMassMoleFrac_,
        const NeumannMassMoleFrac& neumannMassMoleFrac_,
        GridView gridView_)
        : ParentTypeComponent(bc_, sourceComponentBalance_,
              dirichletMassMoleFrac_, neumannMassMoleFrac_,
              gridView_),
          bc(bc_),
          sourceComponentBalance(sourceComponentBalance_),
          dirichletMassMoleFrac(dirichletMassMoleFrac_),
          neumannMassMoleFrac(neumannMassMoleFrac_),
          gridView(gridView_), mapperElement(gridView_)
      {
        karmanConstant_ = GET_PROP_VALUE(TypeTag, KarmanConstant);
        eddyDiffusivityModel_ = GET_PARAM_FROM_GROUP(TypeTag, int, ZeroEq, EddyDiffusivityModel);
        turbulentSchmidtNumber_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, TurbulentSchmidtNumber);

        initialize();
      }


      /**
       * \copydoc BaseComponentStaggeredGrid::alpha_volume
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume_component(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r,
                                  std::vector<DimVector> velocityFaces, Scalar pressure,
                                  Scalar massMoleFrac, Scalar temperature) const
      {
        ParentTypeComponent::alpha_volume_component(eg, lfsu, x, lfsv, r,
                                                       velocityFaces, pressure, massMoleFrac, temperature);
      }


      /**
       * \copydoc BaseComponentStaggeredGrid::alpha_skeleton
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton_component(const IG& ig,
                                    const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                                    const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                                    R& r_s, R& r_n,
                                    std::vector<DimVector> velocities_s, Scalar pressure_s,
                                    Scalar massMoleFrac_s, Scalar temperature_s,
                                    std::vector<DimVector> velocities_n, Scalar pressure_n,
                                    Scalar massMoleFrac_n, Scalar temperature_n) const
      {
        ParentTypeComponent::alpha_skeleton_component(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n, lfsv_n, r_s, r_n,
                                                       velocities_s, pressure_s, massMoleFrac_s, temperature_s,
                                                       velocities_n, pressure_n, massMoleFrac_n, temperature_n);
      }

      /**
       * \copydoc BaseComponentStaggeredGrid::alpha_boundary
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary_component(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s,
                                    std::vector<DimVector> velocityFaces, Scalar pressure_s,
                                    Scalar massMoleFrac_s, Scalar temperature_s,
                                    Scalar pressure_boundary,
                                    Scalar massMoleFrac_boundary, Scalar temperature_boundary) const
      {
        ParentTypeComponent::alpha_boundary_component(ig, lfsu_s, x_s, lfsv_s, r_s,
                                                      velocityFaces, pressure_s, massMoleFrac_s, temperature_s,
                                                      pressure_boundary, massMoleFrac_boundary, temperature_boundary);
      }

      /**
       * \copydoc BaseComponentStaggeredGrid::initialize
       */
      void initialize()
      {
        storedEddyDiffusivity.resize(mapperElement.size());
        for (unsigned int i = 0; i < mapperElement.size(); ++i)
        {
          storedEddyDiffusivity[i] = 0.0;
        }

        ParentTypeComponent::initialize();

        std::cout << "Used eddy diffusivity model = ";
        switch (eddyDiffusivityModel_)
        {
          case EddyDiffusivityIndices::noEddyDiffusivityModel:
            std::cout << "no eddy diffusivity model" << std::endl;
            break;
          case EddyDiffusivityIndices::reynoldsAnalogy:
            std::cout << "reynoldsAnalogy" << std::endl;
            break;
          case EddyDiffusivityIndices::modifiedVanDriest:
            std::cout << "modifiedVanDriest" << std::endl;
            break;
          case EddyDiffusivityIndices::deissler:
            std::cout << "deissler" << std::endl;
            break;
          case EddyDiffusivityIndices::meier:
            std::cout << "meier" << std::endl;
            break;
          default:
            DUNE_THROW(Dune::NotImplemented, "This eddy diffusivity method is not implemented.");
        }
      }

      /**
       * \copydoc BaseComponentStaggeredGrid::updateStoredValues
       */
#if IS_STAGGERED_MULTIDOMAIN_MODEL
      template<typename SubDomainGridView, typename MDGFS, typename X, int stokesDomainIdx>
      void updateStoredValues(const SubDomainGridView& sdgv, const MDGFS& mdgfs, X& lastSolution)
      {
        ParentTypeComponent::template updateStoredValues
                             <SubDomainGridView, MDGFS, X, stokesDomainIdx>
                             (sdgv, mdgfs, lastSolution);
#else
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
        ParentTypeComponent::updateStoredValues(gfs, lastSolution);
#endif
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;

        switch (eddyDiffusivityModel_)
        {
          case EddyDiffusivityIndices::noEddyDiffusivityModel: // 0
            for (ElementIterator eit = gridView.template begin<0>();
                  eit != gridView.template end<0>(); ++eit)
            {
              const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
              asImp_().storedEddyDiffusivity[elementInsideID] = 0.0;
            }
            break;
          case EddyDiffusivityIndices::reynoldsAnalogy: // 1
            reynoldsAnalogy();
            break;
          case EddyDiffusivityIndices::modifiedVanDriest: // 2
            modifiedVanDriest();
            break;
          case EddyDiffusivityIndices::deissler: // 3
            deissler();
            break;
          case EddyDiffusivityIndices::meier: // 4
            meier();
            break;
          default:
            DUNE_THROW(Dune::NotImplemented, "This eddy diffusivity method is not implemented.");
        }
      }

      /**
       * \brief Converts the eddy diffusivity based on the kinematic eddy viscosity
       *
       * \f$ D_\text{t} = \frac{\nu_\text{t}}{\text{Sc}_\text{t}} \f$
       */
      void reynoldsAnalogy()
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        for (ElementIterator eit = gridView.template begin<0>();
              eit != gridView.template end<0>(); ++eit)
        {
          asImp_().storedEddyDiffusivity[mapperElement.index(*eit)]
            = asImp_().storedKinematicEddyViscosity[mapperElement.index(*eit)]
              / turbulentSchmidtNumber_;
        }
      }

      /**
       * \brief Converts the eddy diffusivity based on the kinematic eddy viscosity
       *
       * \f[ D_\textrm{t} = l_\textrm{mix}^2 \left| \frac{\partial u}{\partial y} \right| \f]
       * with \f$ l_\textrm{mix} = \kappa y \frac{1 - \exp \left( - y^+ / 26 \right)}{\sqrt{1.0 - \exp \left( -0.26 y^+ \right)}}] \f$
       * and \f$ \kappa = 0.4 \f$
       */
      void modifiedVanDriest()
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        for (ElementIterator eit = gridView.template begin<0>();
             eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

          double wallDistance = asImp_().storedDistanceToWall[elementInsideID]
                                + asImp_().storedAdditionalRoughnessLength[elementInsideID];
          unsigned int wallNormalAxis = asImp_().storedCorrespondingWallNormalAxis[elementInsideID];
          unsigned int flowNormalAxis = asImp_().storedCorrespondingFlowNormalAxis[elementInsideID];

          double velocityGradient = asImp_().storedVelocityGradientTensor[elementInsideID][flowNormalAxis][wallNormalAxis];
          // conversion to yPlusRough
          double yPlusRough = asImp_().storedDistanceInWallCoordinates[elementInsideID]
                              / asImp_().storedDistanceToWall[elementInsideID] * wallDistance;
          double mixingLength = 0.0;
          if (wallDistance > 0.0 && yPlusRough > 0.0)
            mixingLength = karmanConstant_ * wallDistance
                           * (1.0 - std::exp(-yPlusRough / 26.0 ))
                           / std::sqrt(1.0 - std::exp(-0.26 * yPlusRough));

          asImp_().storedKinematicEddyViscosity[elementInsideID]
            = mixingLength * mixingLength * std::abs(velocityGradient);
        }
      }

      /**
       * \brief Calculates the eddy diffusivity based on Deissler's formula
       *
       * \f[ D_\textrm{t} = \beta \left( 1.0 - \exp \left( \nicefrac{-\beta}{\nu} \right) \right) \f]
       * with \f$ \beta =  0.124^2 v_\text{x} y \f$ and 0.124 as the Deissler constant
       */
      void deissler()
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        for (ElementIterator eit = gridView.template begin<0>();
              eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

          double wallDistance = asImp_().storedDistanceToWall[elementInsideID]
                                + asImp_().storedAdditionalRoughnessLength[elementInsideID];
          unsigned int flowNormalAxis = asImp_().storedCorrespondingFlowNormalAxis[elementInsideID];

          double deisslerConstant = 0.124;
          double beta = deisslerConstant * deisslerConstant
                        * asImp_().storedVelocitiesAtElementCenter[elementInsideID][flowNormalAxis]
                        * wallDistance;
          asImp_().storedEddyDiffusivity[elementInsideID]
            = beta * (1.0 - exp(-beta / asImp_().storedKinematicViscosity[elementInsideID]));
        }
      }

      /**
       * \brief Calculates the eddy diffusivity based on Meier's formula
       *
       * \f[ D_\textrm{t} = l_\textrm{mix}^2 \left| \frac{\partial u}{\partial y} \right| \f]
       * with \f$ l_\textrm{mix} = k_\text{m} y \left( 1.0 - \exp \left( \nicefrac{y^+}{32.45} \right) \right) \f$
       * and the Maier constant \f$ k_\text{m} = 0.44 \f$
       */
      void meier()
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        for (ElementIterator eit = gridView.template begin<0>();
              eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

          double wallDistance = asImp_().storedDistanceToWall[elementInsideID]
                                + asImp_().storedAdditionalRoughnessLength[elementInsideID];
          unsigned int wallNormalAxis = asImp_().storedCorrespondingWallNormalAxis[elementInsideID];
          unsigned int flowNormalAxis = asImp_().storedCorrespondingFlowNormalAxis[elementInsideID];

          double velocityGradient = asImp_().storedVelocityGradientTensor[elementInsideID][flowNormalAxis][wallNormalAxis];
          // conversion to yPlusRough
          double yPlusRough = asImp_().storedDistanceInWallCoordinates[elementInsideID]
                              / asImp_().storedDistanceToWall[elementInsideID] * wallDistance;
          double kConstantMeier = 0.44;
          double aPlusMeier = 32.45;
          double mixingLength = 0.0;
          if (wallDistance > 0.0 && yPlusRough > 0.0)
            mixingLength = kConstantMeier * wallDistance * (1.0 - exp(-yPlusRough / aPlusMeier));
          asImp_().storedEddyDiffusivity[elementInsideID]
            = mixingLength * mixingLength * std::abs(velocityGradient);
        }
      }

      /**
       * \brief Returns the eddy diffusivity for a given element [m^2/s]
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar eddyDiffusivity(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(eg);
        return asImp_().storedEddyDiffusivity[elementInsideID];
      }

private:
      const BC& bc;
      const SourceComponentBalance& sourceComponentBalance;
      const DirichletMassMoleFrac& dirichletMassMoleFrac;
      const NeumannMassMoleFrac& neumannMassMoleFrac;
      GridView gridView;
      MapperElement mapperElement;

      // properties
      unsigned int eddyDiffusivityModel_;
      Scalar turbulentSchmidtNumber_;
      Scalar karmanConstant_;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_BASE_ZEROEQ_COMPONENT_STAGGERED_GRID_HH