/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup BaseZeroEqStaggeredGrid
 *
 * \copydoc EddyViscosityStaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 * Navier-Stokes equation, modeled with algebraic turbulence models.
 *
 * The eddy viscosity depends on the chosen model, but looks in general like this:
 * \f[
 *    \nu_{\alpha,\textrm{t}}
 *    = \nu_{\alpha,\textrm{t}} \left( y, \left| \frac{\partial u}{\partial y} \right| \right)
 *    > 0
 * \f]
 *
 * The following models are available and can be chosen via setting <tt>ZeroEq.EddyViscosityModel</tt>,
 * for detailed description have look at the individual functions in the local operator.<br>
 * (0) No eddy viscosity model<br>
 * (1) Prandtl's mixing length<br>
 * (2) Modified Van Driest<br>
 * (3) Baldwin-Lomax model<br>
 * (4) Cebeci-Smith model<br>
 *
 * Be aware that the storage terms (the time dependent terms)
 * are not included here but are implemented in the transient part
 * of the local operator.
 */

#ifndef DUMUX_BASEZEROEQ_STAGGERED_GRID_HH
#define DUMUX_BASEZEROEQ_STAGGERED_GRID_HH

#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/fmatrix.hh>

#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/subspace.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<appl/staggeredgrid/freeflow/eddyviscosity2cni/eddyviscosityindices.hh>
#include<appl/staggeredgrid/freeflow/eddyviscosity2cni/baseeddyviscositystaggeredgrid.hh>

#include"zeroeqpropertydefaults.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     * the steady-state zeroeq Navier-Stokes turbulence models.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class BaseZeroEqStaggeredGrid
    : public BaseEddyViscosityStaggeredGrid<TypeTag>
    {
    public:
      typedef BaseEddyViscosityStaggeredGrid<TypeTag> ParentTypeMassMomentum;

      typedef typename GET_PROP_TYPE(TypeTag, LocalOperator) Implementation;

      typedef typename GET_PROP_TYPE(TypeTag, BCType) BC;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletVelocity) DirichletVelocity;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletPressure) DirichletPressure;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannVelocity) NeumannVelocity;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannPressure) NeumannPressure;
      typedef typename GET_PROP_TYPE(TypeTag, SourceMomentumBalance) SourceMomentumBalance;
      typedef typename GET_PROP_TYPE(TypeTag, SourceMassBalance) SourceMassBalance;

      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      enum { dim = GridView::dimension };

      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;

      //! additional stored quantities
      std::vector<double> storedVelocityThickness;
      std::vector<double> storedBoundaryLayerThickness;
      std::vector<double> storedSwitchingPosition;
      std::vector<double> storedFMax;
      std::vector<double> storedYFMax;
      std::vector<double> storedKinematicEddyViscosityInner;
      std::vector<double> storedKinematicEddyViscosityOuter;
      std::vector<double> storedKinematicEddyViscosityDifference;
      std::vector<Dune::FieldVector<double, GridView::dimension> > storedPressureGradient;

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      //! \brief Index of unknowns
      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { velocityIdx = Indices::velocityIdx,
             pressureIdx = Indices::pressureIdx };

      //! \brief Constructor
      BaseZeroEqStaggeredGrid(const BC& bc_,
                          const SourceMomentumBalance& sourceMomentumBalance_, const SourceMassBalance& sourceMassBalance_,
                          const DirichletVelocity& dirichletVelocity_, const DirichletPressure& dirichletPressure_,
                          const NeumannVelocity& neumannVelocity_, const NeumannPressure& neumannPressure_,
                          GridView gridView_)
        : ParentTypeMassMomentum(bc_,
              sourceMomentumBalance_, sourceMassBalance_,
              dirichletVelocity_, dirichletPressure_,
              neumannVelocity_, neumannPressure_,
              gridView_),
          bc(bc_),
          sourceMomentumBalance(sourceMomentumBalance_), sourceMassBalance(sourceMassBalance_),
          dirichletVelocity(dirichletVelocity_), dirichletPressure(dirichletPressure_),
          neumannVelocity(neumannVelocity_), neumannPressure(neumannPressure_),
          gridView(gridView_), mapperElement(gridView_)
      {
        eddyViscosityModel_ = GET_PARAM_FROM_GROUP(TypeTag, int, ZeroEq, EddyViscosityModel);
        karmanConstant_ = GET_PROP_VALUE(TypeTag, KarmanConstant);
        initialize();
      }


      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_volume
       * \param velocity phase velocity vector of the velocity on the inside the element faces
       * \param pressure phase pressure inside the element
       * \param massMoleFrac phase composition inside the element
       * \param temperature phase temperature inside the element
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume_massmomentum(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r,
                                     std::vector<DimVector> velocityFaces, Scalar pressure,
                                     Scalar massMoleFrac, Scalar temperature) const
      {
        ParentTypeMassMomentum::alpha_volume_massmomentum(eg, lfsu, x, lfsv, r,
                                                          velocityFaces, pressure, massMoleFrac, temperature);
      }


      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_skeleton
       * \param massMoleFrac_s phase composition of self/inside
       * \param temperature_s phase composition of neighbor/outside
       * \param massMoleFrac_n phase temperature of self/inside
       * \param temperature_n phase temperature of neighbor/outside
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton_massmomentum(const IG& ig,
                                       const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                                       const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                                       R& r_s, R& r_n,
                                       std::vector<DimVector> velocities_s, Scalar pressure_s,
                                       Scalar massMoleFrac_s, Scalar temperature_s,
                                       std::vector<DimVector> velocities_n, Scalar pressure_n,
                                       Scalar massMoleFrac_n, Scalar temperature_n) const
      {
        ParentTypeMassMomentum::alpha_skeleton_massmomentum(ig, lfsu_s, x_s, lfsv_s, lfsu_n, x_n, lfsv_n, r_s, r_n,
                                                            velocities_s, pressure_s, massMoleFrac_s, temperature_s,
                                                            velocities_n, pressure_n, massMoleFrac_n, temperature_n);
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_boundary
       * \param velocity phase velocity vector of the velocity on the inside the element faces
       * \param pressure_s phase pressure inside the element
       * \param massMoleFrac_s phase composition inside the element
       * \param temperature_s phase temperature inside the element
       * \param pressure_boundary phase pressure at the boundary face
       * \param massMoleFrac_boundary phase composition at the boundary face
       * \param temperature_boundary phase temperature at the boundary face
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary_massmomentum(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s,
                                       std::vector<DimVector> velocityFaces, Scalar pressure_s,
                                       Scalar massMoleFrac_s, Scalar temperature_s,
                                       Scalar pressure_boundary,
                                       Scalar massMoleFrac_boundary, Scalar temperature_boundary) const
      {
        ParentTypeMassMomentum::alpha_boundary_massmomentum(ig, lfsu_s, x_s, lfsv_s, r_s,
                                                            velocityFaces, pressure_s, massMoleFrac_s, temperature_s,
                                                            pressure_boundary, massMoleFrac_boundary, temperature_boundary);
      }

      /**
       * \brief Initialize vector-stored values used for alpha_* routines.
       *
       * Travers grid and store values like relation to next wall.<br>
       * In a first step, this method writes the ID and coordinates of all elements
       * having a boundary face into a vector. In the second traversing grid step,
       * to each individual elements the relation to it's shortest corresponding
       * wall element is found and stored.
       *
       * \tparam GridView GridView type
       */
      void initialize()
      {
        ParentTypeMassMomentum::initialize();
//         std::cout << "initialize zeroeq" << std::endl;

        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;

        storedVelocityThickness.resize(mapperElement.size());
        storedBoundaryLayerThickness.resize(mapperElement.size());
        storedSwitchingPosition.resize(mapperElement.size());
        storedFMax.resize(mapperElement.size());
        storedYFMax.resize(mapperElement.size());
        storedKinematicEddyViscosityInner.resize(mapperElement.size());
        storedKinematicEddyViscosityOuter.resize(mapperElement.size());
        storedKinematicEddyViscosityDifference.resize(mapperElement.size());
        storedPressureGradient.resize(mapperElement.size());

        for (unsigned int i = 0; i < mapperElement.size(); ++i)
        {
          storedVelocityThickness[i] = 0.0;
          storedBoundaryLayerThickness[i] = 0.001;
          storedSwitchingPosition[i] = 0.0;
          storedFMax[i] = 0.0;
          storedYFMax[i] = 0.0;
          storedKinematicEddyViscosityInner[i] = 0.0;
          storedKinematicEddyViscosityOuter[i] = 0.0;
          storedKinematicEddyViscosityDifference[i] = 0.0;
          for (unsigned int j = 0; j < dim; ++j)
          {
            storedPressureGradient[i][j] = 0.0;
          }
        }

        // loop over grid view
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const Dune::FieldVector<double, dim>& cellCenterLocal =
            Dune::ReferenceElements<double, dim>::general(eit->geometry().type()).position(0, 0);
          Dune::FieldVector<double, dim> cellCenterGlobal = eit->geometry().global(cellCenterLocal);
          asImp_().storedRoughness[mapperElement.index(*eit)] = dirichletVelocity.roughness(*eit, cellCenterGlobal);
        }

        std::cout << "Used eddy viscosity model = ";
        switch (eddyViscosityModel_)
        {
          case EddyViscosityIndices::noEddyViscosityModel: // 0
            std::cout << "no eddy viscosity model" << std::endl;
            break;
          case EddyViscosityIndices::prandtl: // 1
            std::cout << "prandtl" << std::endl;
            break;
          case EddyViscosityIndices::modifiedVanDriest: // 2
            std::cout << "modifiedVanDriest" << std::endl;
            break;
          case EddyViscosityIndices::baldwinLomax: // 3
            std::cout << "baldwinLomax" << std::endl;
            break;
          case EddyViscosityIndices::cebeciSmith: // 4
            std::cout << "cebeciSmith" << std::endl;
            break;
          default:
            DUNE_THROW(Dune::NotImplemented, "This eddy viscosity method is not implemented.");
        }
      }

      /**
       * \brief Update vector-stored values used for alpha_* routines.
       *
       * In a first step, this method writes the ID and coordinates of all elements
       * having a boundary face into a vector. In the second traversing grid step,
       * to each individual elements the relation to it's shortest corresponding
       * wall element is found and stored.
       * The velocities are copied across each intersection to be available on the other
       * side adjacent face. This is only necessary for the tangential case of the viscous
       * term, which means <tt>dim > 1</tt>.
       * For the eddy viscosity models also the velocity gradient tensor containing
       * the gradient of each velocity component in all spatial dimension is stored
       * per element.
       *
       * \tparam GFS GridFunctionSpace type
       * \tparam X Coefficient vector
       * \param gfs Grid function space
       * \param lastSolution Coefficient vector from last Newton step or time step
       */
#if IS_STAGGERED_MULTIDOMAIN_MODEL
      template<typename SubDomainGridView, typename MDGFS, typename X, int stokesDomainIdx>
      void updateStoredValues(const SubDomainGridView& sdgv, const MDGFS& mdgfs, X& lastSolution)
      {
        // call eddyViscosity updateStoredValues
        ParentTypeMassMomentum::template updateStoredValues
                                <SubDomainGridView, MDGFS, X, stokesDomainIdx>
                                (sdgv, mdgfs, lastSolution);

        // grid function sub spaces
        using SubGfsPressure = Dune::PDELab::GridFunctionSubSpace
            <MDGFS, Dune::TypeTree::TreePath<stokesDomainIdx, pressureIdx> >;
        SubGfsPressure subGfsPressure(mdgfs);
#else
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
        // call eddyViscosity updateStoredValues
        ParentTypeMassMomentum::updateStoredValues(gfs, lastSolution);

        // grid function sub spaces
        using SubGfsPressure = Dune::PDELab::GridFunctionSubSpace
            <GFS, Dune::TypeTree::TreePath<pressureIdx> >;
        SubGfsPressure subGfsPressure(gfs);
#endif

        // discrete function objects
        using DgfPressure = Dune::PDELab::DiscreteGridFunction<SubGfsPressure, X>;
        DgfPressure dgfPressure(subGfsPressure, lastSolution);

        // constants and types
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;

        // loop over grid view
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

          // reset values
          storedBoundaryLayerThickness[elementInsideID] = std::numeric_limits<Scalar>::min();
          storedVelocityThickness[elementInsideID] = std::numeric_limits<Scalar>::min();
          storedSwitchingPosition[elementInsideID] = std::numeric_limits<Scalar>::max();
          storedFMax[elementInsideID] = std::numeric_limits<Scalar>::min();

          // recalculate u+, could be done in a finer way, but works
          Scalar uStarDividedByViscosity = asImp_().storedDistanceInWallCoordinates[elementInsideID]
                                           / asImp_().storedDistanceToWall[elementInsideID];
          if (uStarDividedByViscosity > 0.0)
          {
            Scalar dimensionlessEquivalentSandGrainRoughness
                    = asImp_().storedRoughness[asImp_().storedCorrespondingWallElementID[elementInsideID]]
                      * uStarDividedByViscosity;
            asImp_().storedAdditionalRoughnessLength[elementInsideID]
              = 0.9 / uStarDividedByViscosity
                * (std::sqrt(dimensionlessEquivalentSandGrainRoughness)
                  - dimensionlessEquivalentSandGrainRoughness * std::exp(-dimensionlessEquivalentSandGrainRoughness / 6.0));
          }

          for (unsigned int i = 0; i < dim; ++i)
          {
            storedPressureGradient[elementInsideID][i] = 0.0;
          }
          for (IntersectionIterator is = gridView.ibegin(*eit);
              is != gridView.iend(*eit); ++is)
          {
            if (is->neighbor())
            {
              // evaluate orientation of intersection
              unsigned int normDim = 0;
              for (unsigned int curDim = 0; curDim < dim; ++curDim)
              {
                if (std::abs(is->centerUnitOuterNormal()[curDim]) > 1e-10)
                {
                  normDim = curDim;
                }
              }

              // local position of cell and face centers
              const Dune::FieldVector<Scalar, dim>& insideCellCenterLocal =
                Dune::ReferenceElements<Scalar, dim>::general(is->inside().type()).position(0, 0);
              const Dune::FieldVector<Scalar, dim>& outsideCellCenterLocal =
                Dune::ReferenceElements<Scalar, dim>::general(is->outside().type()).position(0, 0);

              // global position of cell and face centers
              Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
                is->inside().geometry().global(insideCellCenterLocal);
              Dune::FieldVector<Scalar, dim> outsideCellCenterGlobal =
                is->outside().geometry().global(outsideCellCenterLocal);


#if IS_STAGGERED_MULTIDOMAIN_MODEL
              if (!gridView.indexSet().contains(stokesDomainIdx, is->inside())
                  || !gridView.indexSet().contains(stokesDomainIdx, is->outside()))
                  continue;
              auto element_s = sdgv.grid().subDomainEntityPointer(is->inside());
              auto element_n = sdgv.grid().subDomainEntityPointer(is->outside());
#else
              auto element_s = is->inside();
              auto element_n = is->outside();
#endif
              // get the pressure in the cell centers
              Dune::FieldVector<Scalar, 1> pressure_s(0.0);
              Dune::FieldVector<Scalar, 1> pressure_n(0.0);
              dgfPressure.evaluate(element_s, insideCellCenterLocal, pressure_s);
              dgfPressure.evaluate(element_n, outsideCellCenterLocal, pressure_n);

              storedPressureGradient[elementInsideID][normDim] += (pressure_n - pressure_s)
                                                                  / (insideCellCenterGlobal[normDim] - outsideCellCenterGlobal[normDim]);
            }
          }

          // Perform correction for values for elements without a boundary, whyever
          // two values have been summed up. It only works this strange procedure.
          for (IntersectionIterator is = gridView.ibegin(*eit);
              is != gridView.iend(*eit); ++is)
          {
            if (is->neighbor())
            {
              // evaluate orientation of intersection
              unsigned int normDim = 0;
              for (unsigned int curDim = 0; curDim < dim; ++curDim)
              {
                if (std::abs(is->centerUnitOuterNormal()[curDim]) > 1e-10)
                {
                  normDim = curDim;
                }
              }
              storedPressureGradient[elementInsideID][normDim] /= 2.0;
            }
          }
          // strange behavior, but this line is needed
          for (unsigned int i = 0; i < dim; ++i)
          {
            storedPressureGradient[elementInsideID][i] *= 2.0;
          }
        }

        // get maximum and minimum velocities
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = asImp_().storedCorrespondingWallElementID[mapperElement.index(*eit)];

          for (unsigned int curDim = 0; curDim < dim; ++curDim)
          {
            // maximum velocity
            if (asImp_().storedVelocityMaximum[wallElementID][curDim] < asImp_().storedVelocitiesAtElementCenter[elementInsideID][curDim])
            {
              asImp_().storedVelocityMaximum[wallElementID][curDim] = asImp_().storedVelocitiesAtElementCenter[elementInsideID][curDim];
            }

            // minimum velocity
            if (asImp_().storedVelocityMinimum[wallElementID][curDim] > asImp_().storedVelocitiesAtElementCenter[elementInsideID][curDim])
            {
              asImp_().storedVelocityMinimum[wallElementID][curDim] = asImp_().storedVelocitiesAtElementCenter[elementInsideID][curDim];
            }
          }
        }

        // calculate boundary layer thickness
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = asImp_().storedCorrespondingWallElementID[mapperElement.index(*eit)];

          unsigned int flowNormalAxis = asImp_().storedCorrespondingFlowNormalAxis[elementInsideID];
          if (storedBoundaryLayerThickness[wallElementID] < asImp_().storedDistanceToWall[elementInsideID]
              && asImp_().storedVelocitiesAtElementCenter[elementInsideID][flowNormalAxis]
                 < 0.99 * asImp_().storedVelocityMaximum[wallElementID][flowNormalAxis])
          {
            storedBoundaryLayerThickness[wallElementID] = asImp_().storedDistanceToWall[elementInsideID];
          }
        }

        switch (eddyViscosityModel_)
        {
          case EddyViscosityIndices::noEddyViscosityModel: // 0
            for (ElementIterator eit = gridView.template begin<0>();
                  eit != gridView.template end<0>(); ++eit)
            {
              const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
              asImp_().storedKinematicEddyViscosity[elementInsideID] = 0.0;
            }
            break;
          case EddyViscosityIndices::prandtl: // 1
            prandtl();
            break;
          case EddyViscosityIndices::modifiedVanDriest: // 2
            modifiedVanDriest();
            break;
          case EddyViscosityIndices::baldwinLomax: // 3
            baldwinLomax();
            break;
          case EddyViscosityIndices::cebeciSmith: // 4
            cebeciSmith();
            break;
          default:
            DUNE_THROW(Dune::NotImplemented, "This eddy viscosity method is not implemented.");
        }
      }

      /**
       * \brief Calculates the eddy viscosity based on Prandtl's mixing length approach
       *
       * \f[ \nu_\textrm{t} = l_\textrm{mix}^2 \left| \frac{\partial u}{\partial y} \right| \f]
       * with \f$ l_\textrm{mix} = \kappa y \f$ and \f$ \kappa = 0.4 \f$.
       */
      void prandtl()
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        for (ElementIterator eit = gridView.template begin<0>();
             eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

          double wallDistance = asImp_().storedDistanceToWall[elementInsideID]
                                + asImp_().storedAdditionalRoughnessLength[elementInsideID];
          unsigned int wallNormalAxis = asImp_().storedCorrespondingWallNormalAxis[elementInsideID];
          unsigned int flowNormalAxis = asImp_().storedCorrespondingFlowNormalAxis[elementInsideID];

          double velocityGradient = asImp_().storedVelocityGradientTensor[elementInsideID][flowNormalAxis][wallNormalAxis];
          double mixingLength = karmanConstant_ * wallDistance;
          asImp_().storedKinematicEddyViscosity[elementInsideID]
            = mixingLength * mixingLength * std::abs(velocityGradient);
        }
      }

      /**
       * \brief Calculates the eddy viscosity based on the modified van Driest's formula
       *
       * \f[ \nu_\textrm{t} = l_\textrm{mix}^2 \left| \frac{\partial u}{\partial y} \right| \f]
       * with \f$ l_\textrm{mix} = \kappa y \frac{1 - \exp \left( - y^+ / 26 \right)}{\sqrt{1.0 - \exp \left( -0.26 y^+ \right)}}] \f$
       * and \f$ \kappa = 0.4 \f$
       */
      void modifiedVanDriest()
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        for (ElementIterator eit = gridView.template begin<0>();
             eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

          double wallDistance = asImp_().storedDistanceToWall[elementInsideID]
                                + asImp_().storedAdditionalRoughnessLength[elementInsideID];
          unsigned int wallNormalAxis = asImp_().storedCorrespondingWallNormalAxis[elementInsideID];
          unsigned int flowNormalAxis = asImp_().storedCorrespondingFlowNormalAxis[elementInsideID];

          double velocityGradient = asImp_().storedVelocityGradientTensor[elementInsideID][flowNormalAxis][wallNormalAxis];
          // conversion to yPlusRough
          double yPlusRough = asImp_().storedDistanceInWallCoordinates[elementInsideID]
                              / asImp_().storedDistanceToWall[elementInsideID] * wallDistance;
          double mixingLength = 0.0;
          if (wallDistance > 0.0 && yPlusRough > 0.0)
            mixingLength = karmanConstant_ * wallDistance
                           * (1.0 - std::exp(-yPlusRough / 26.0 ))
                           / std::sqrt(1.0 - std::exp(-0.26 * yPlusRough));

          asImp_().storedKinematicEddyViscosity[elementInsideID]
            = mixingLength * mixingLength * std::abs(velocityGradient);
        }
      }

      /**
       * \brief Calculates the eddy viscosity based on Cebeci Smith's model
       */
      void cebeciSmith()
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;

        // (1) calculate velocity thickness
        for (ElementIterator eit = gridView.template begin<0>();
             eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = asImp_().storedCorrespondingWallElementID[mapperElement.index(*eit)];
          unsigned int flowNormalAxis = asImp_().storedCorrespondingFlowNormalAxis[elementInsideID];

          // calculate velocity DoF positions
          const unsigned int numControlVolumeFaces =
            Dune::ReferenceElements<double, dim>::general(eit->geometry().type()).size(1);
          std::vector<Dune::FieldVector<double, dim> > faceCentersLocal(numControlVolumeFaces);
          std::vector<Dune::FieldVector<double, dim> > faceCentersGlobal(numControlVolumeFaces);
          for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
          {
            std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
            faceCentersLocal[curFace][curFace/2] = curFace % 2;
            faceCentersGlobal[curFace] = eit->geometry().global(faceCentersLocal[curFace]);
          }

          // distance between two face mid points
          Dune::FieldVector<Scalar, dim> distancesFaceCenters(0.0);
          for (unsigned int curDim = 0; curDim < dim; ++curDim)
          {
            distancesFaceCenters[curDim] =
              std::abs(faceCentersGlobal[2*curDim+1][curDim] - faceCentersGlobal[2*curDim][curDim]);
          }

          // staggered face volume (goes through cell center) perpendicular to each direction
          Dune::FieldVector<Scalar, dim> orthogonalFaceVolumes(1.0);
          for (unsigned int curDim = 0; curDim < dim; ++curDim)
          {
            for (unsigned int normDim = 0; normDim < dim; ++normDim)
            {
              if (curDim != normDim)
              {
                orthogonalFaceVolumes[curDim] *= distancesFaceCenters[normDim];
              }
            }
          }

          // evaluate over total height, because if boundary layer edge is reached, 0 is summed up
          asImp_().storedVelocityThickness[wallElementID] += (1.0 - asImp_().storedVelocitiesAtElementCenter[elementInsideID][flowNormalAxis]
                                                                 / asImp_().storedVelocityMaximum[wallElementID][flowNormalAxis])
                                                          * orthogonalFaceVolumes[flowNormalAxis];
        }

        // (2) calculate inner and outer viscosity
        for (ElementIterator eit = gridView.template begin<0>();
             eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = asImp_().storedCorrespondingWallElementID[mapperElement.index(*eit)];

          double wallDistance = asImp_().storedDistanceToWall[elementInsideID]
                                + asImp_().storedAdditionalRoughnessLength[elementInsideID];
          unsigned int wallNormalAxis = asImp_().storedCorrespondingWallNormalAxis[elementInsideID];
          unsigned int flowNormalAxis = asImp_().storedCorrespondingFlowNormalAxis[elementInsideID];

          double yPlus = asImp_().storedDistanceInWallCoordinates[elementInsideID];
          double kinematicViscosityAtWall = asImp_().storedKinematicViscosity[wallElementID];
          double densityAtWall = asImp_().storedDensity[wallElementID];
          double density = asImp_().storedDensity[elementInsideID];
          double uStar = std::sqrt(kinematicViscosityAtWall * densityAtWall
                                   * std::abs(asImp_().storedVelocityGradientTensor[wallElementID][flowNormalAxis][wallNormalAxis]))
                         / densityAtWall;
          double aPlus = 26.0;
          double sqrtPart = 1.0 + wallDistance * asImp_().storedPressureGradient[elementInsideID][flowNormalAxis]
                                  / (density * uStar * uStar);
          if (!std::isnan(sqrtPart) && sqrtPart > 0.0)
          {
            aPlus = 26.0 / std::sqrt(sqrtPart);
          }
          double mixingLength = karmanConstant_ * wallDistance * (1.0 - std::exp(-yPlus / aPlus));
          asImp_().storedKinematicEddyViscosityInner[elementInsideID]
            = mixingLength * mixingLength
              * std::sqrt(std::pow(asImp_().storedVelocityGradientTensor[elementInsideID][1][0], 2.0)
                          + std::pow(asImp_().storedVelocityGradientTensor[elementInsideID][0][1], 2.0));

          const double alpha = 0.0168;
          double fKleb = 1.0 / (1.0 + 5.5 * std::pow(wallDistance / asImp_().storedBoundaryLayerThickness[wallElementID], 6.0));
          asImp_().storedKinematicEddyViscosityOuter[elementInsideID]
            = alpha * asImp_().storedVelocityMaximum[wallElementID][flowNormalAxis]
              * asImp_().storedVelocityThickness[wallElementID] * fKleb;

          asImp_().storedKinematicEddyViscosityDifference[elementInsideID]
            = asImp_().storedKinematicEddyViscosityInner[elementInsideID]
              - asImp_().storedKinematicEddyViscosityOuter[elementInsideID];
        }

        // (3) switching point
        for (ElementIterator eit = gridView.template begin<0>();
             eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = asImp_().storedCorrespondingWallElementID[mapperElement.index(*eit)];

          // checks if sign switches, by multiplication
          double check = asImp_().storedKinematicEddyViscosityDifference[wallElementID] * asImp_().storedKinematicEddyViscosityDifference[elementInsideID];
          if (check < 0 // means sign has switched
              && storedSwitchingPosition[wallElementID] > asImp_().storedDistanceToWall[elementInsideID])
          {
            storedSwitchingPosition[wallElementID] = asImp_().storedDistanceToWall[elementInsideID];
          }
        }

        // (4) finally determine eddy viscosity
        for (ElementIterator eit = gridView.template begin<0>();
             eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = asImp_().storedCorrespondingWallElementID[mapperElement.index(*eit)];

          if (asImp_().storedDistanceToWall[elementInsideID] >= storedSwitchingPosition[wallElementID])
          {
            asImp_().storedKinematicEddyViscosity[elementInsideID] = storedKinematicEddyViscosityOuter[elementInsideID];
          }
          else
          {
            asImp_().storedKinematicEddyViscosity[elementInsideID] = storedKinematicEddyViscosityInner[elementInsideID];
          }
        }
      }

      /**
       * \brief Calculates the eddy viscosity based on Baldwin-Lomax's model
       */
      void baldwinLomax()
      {
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        const double aPlus = 26.0;
        const double k = 0.0168;
        const double cCP = 1.6;
        const double cWake = 0.25;
        const double cKleb = 0.3;

        // (1) calculate inner viscosity and Klebanoff function
        for (ElementIterator eit = gridView.template begin<0>();
             eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = asImp_().storedCorrespondingWallElementID[mapperElement.index(*eit)];

          double wallDistance = asImp_().storedDistanceToWall[elementInsideID]
                                + asImp_().storedAdditionalRoughnessLength[elementInsideID];
          unsigned int wallNormalAxis = asImp_().storedCorrespondingWallNormalAxis[elementInsideID];
          unsigned int flowNormalAxis = asImp_().storedCorrespondingFlowNormalAxis[elementInsideID];

          double omegaAbs = std::abs(asImp_().storedVelocityGradientTensor[elementInsideID][flowNormalAxis][wallNormalAxis]
                                     - asImp_().storedVelocityGradientTensor[elementInsideID][wallNormalAxis][flowNormalAxis]);
          double yPlus = asImp_().storedDistanceInWallCoordinates[elementInsideID];
          double mixingLength = karmanConstant_ * wallDistance * (1.0 - exp(-yPlus / aPlus));
          asImp_().storedKinematicEddyViscosityInner[elementInsideID]
            = mixingLength * mixingLength * omegaAbs;

          double f = wallDistance * omegaAbs * (1.0 - exp(-yPlus / aPlus));
          if (f > storedFMax[wallElementID])
          {
            storedFMax[wallElementID] = f;
            storedYFMax[wallElementID] = wallDistance;
          }
        }

        // (2) calculate outer viscosity
        for (ElementIterator eit = gridView.template begin<0>();
             eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = asImp_().storedCorrespondingWallElementID[mapperElement.index(*eit)];

          double wallDistance = asImp_().storedDistanceToWall[elementInsideID];

          double max = 0.0;
          double min = 0.0;
          for (unsigned dimIdx = 0; dimIdx < dim; ++dimIdx)
          {
              max += asImp_().storedVelocityMaximum[wallElementID][dimIdx] * asImp_().storedVelocityMaximum[wallElementID][dimIdx];
              min += asImp_().storedVelocityMinimum[wallElementID][dimIdx] * asImp_().storedVelocityMinimum[wallElementID][dimIdx];
          }
          double deltaU = std::sqrt(max) - std::sqrt(min);
          double yFMax = storedYFMax[wallElementID];
          double fMax = storedFMax[wallElementID];
          double fWake = std::min(yFMax * fMax, cWake * yFMax * deltaU * deltaU / fMax);
          double fKleb = 1.0 / (1.0 + 5.5 * std::pow(cKleb * wallDistance / yFMax, 6.0));
          asImp_().storedKinematicEddyViscosityOuter[elementInsideID]
            = k * cCP * fWake * fKleb;

          asImp_().storedKinematicEddyViscosityDifference[elementInsideID]
            = asImp_().storedKinematicEddyViscosityInner[elementInsideID]
              - asImp_().storedKinematicEddyViscosityOuter[elementInsideID];
        }

        // (3) switching point
        for (ElementIterator eit = gridView.template begin<0>();
             eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = asImp_().storedCorrespondingWallElementID[mapperElement.index(*eit)];

          // checks if sign switches, by multiplication
          double check = asImp_().storedKinematicEddyViscosityDifference[wallElementID] * asImp_().storedKinematicEddyViscosityDifference[elementInsideID];
          if (check < 0 // means sign has switched
              && storedSwitchingPosition[wallElementID] > asImp_().storedDistanceToWall[elementInsideID])
          {
            storedSwitchingPosition[wallElementID] = asImp_().storedDistanceToWall[elementInsideID];
          }
        }

        // (4) finally determine eddy viscosity
        for (ElementIterator eit = gridView.template begin<0>();
             eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
          const typename GridView::IndexSet::IndexType wallElementID = asImp_().storedCorrespondingWallElementID[mapperElement.index(*eit)];

          if (asImp_().storedDistanceToWall[elementInsideID] >= storedSwitchingPosition[wallElementID])
          {
            asImp_().storedKinematicEddyViscosity[elementInsideID] = storedKinematicEddyViscosityOuter[elementInsideID];
          }
          else
          {
            asImp_().storedKinematicEddyViscosity[elementInsideID] = storedKinematicEddyViscosityInner[elementInsideID];
          }
        }
      }

private:
      const BC& bc;
      const SourceMomentumBalance& sourceMomentumBalance;
      const SourceMassBalance& sourceMassBalance;
      const DirichletVelocity& dirichletVelocity;
      const DirichletPressure& dirichletPressure;
      const NeumannVelocity& neumannVelocity;
      const NeumannPressure& neumannPressure;
      GridView gridView;
      MapperElement mapperElement;

      // Properties
      unsigned int eddyViscosityModel_;
      Scalar karmanConstant_;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_BASEZEROEQ_STAGGERED_GRID_HH
