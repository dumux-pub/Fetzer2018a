/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup BaseSpalartAllmarasStaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 * Navier-Stokes equation, modeled with the Spalart-Allmaras turbulence model
 *
 * after (without trip correction, default):
 * Wilcox, Turbulence Modeling for CFD
 *
 * after (with trip correction):
 * Spalart, P. R. & Allmaras, S. R. A One-Equation Turbulence Model for Aerodynamic Flows
 * Recherche Aerospatiale, 1994, 1, 5-21 , doi:<br>
 * or<br>
 * Allmaras, S. R.; Johnson, F. T. & Spalart, P. R. Modifications and Clarifications for the Implementation of the Spalart-Allmaras Turbulence Model
 * Seventh International Conference on Computational Fluid Dynamics (ICCFD7), 2012, doi:<br>
 * or<br>
 * http://turbmodels.larc.nasa.gov/spalart.html
 *
 * \copydoc EddyViscosityStaggeredGrid
 *
 * Be aware that the storage terms (the time dependent terms)
 * are not included here but are implemented in the transient part
 * of the local operator.
 */

#ifndef DUMUX_BASE_SPALARTALLMARAS_STAGGERED_GRID_HH
#define DUMUX_BASE_SPALARTALLMARAS_STAGGERED_GRID_HH

#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/fmatrix.hh>

#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/gridfunctionspace/subspace.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<appl/staggeredgrid/common/evaluatefluxes.hh>

#include"spalartallmaraspropertydefaults.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     * the steady-state Spalart-Allmaras equations.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class BaseSpalartAllmarasStaggeredGrid
    {
    public:
      typedef typename GET_PROP_TYPE(TypeTag, LocalOperator) Implementation;
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

      typedef typename GET_PROP_TYPE(TypeTag, BCType) BC;
      typedef typename GET_PROP_TYPE(TypeTag, SourceViscosityTildeBalance) SourceViscosityTildeBalance;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletViscosityTilde) DirichletViscosityTilde;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannViscosityTilde) NeumannViscosityTilde;

      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      enum { dim = GridView::dimension };

      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;
      typedef typename GridView::template Codim<0>::Entity Element;
      typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
      typedef typename GridView::IntersectionIterator IntersectionIterator;

      typedef typename Dumux::EvaluateFluxes<TypeTag> eval;

      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { viscosityTildeIdx = Indices::viscosityTildeIdx };

      enum { phaseIdx = Indices::phaseIdx };

      //! element stored data
      typedef std::vector<double> StoredScalar;
      mutable StoredScalar storedViscosityTilde;

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      //! \brief Constructor
      BaseSpalartAllmarasStaggeredGrid(const BC& bc_,
                          const SourceViscosityTildeBalance& sourceViscosityTildeBalance_,
                          const DirichletViscosityTilde& dirichletViscosityTilde_,
                          const NeumannViscosityTilde& neumannViscosityTilde_,
                          GridView gridView_, Problem& problem)
        : bc(bc_),
          sourceViscosityTildeBalance(sourceViscosityTildeBalance_),
          dirichletViscosityTilde(dirichletViscosityTilde_),
          neumannViscosityTilde(neumannViscosityTilde_),
          gridView(gridView_), mapperElement(gridView), problemPtr_(0)
      {
        // properties
        enableAdvectionAveraging_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableAdvectionAveraging);
        enableTripCorrcetion_ = GET_PARAM_FROM_GROUP(TypeTag, bool, SpalartAllmaras, EnableTripCorrection);
        karmanConstant_ = GET_PROP_VALUE(TypeTag, KarmanConstant);

        problemPtr_ = &problem;
        initialize();
      }


      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_volume_navierstokes
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume_spalartallmaras(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r,
                                 std::vector<DimVector> velocityFaces, Scalar pressure,
                                 Scalar massMoleFrac, Scalar temperature) const
      {
        // select the components from the subspaces
        typedef typename LFSU::template Child<viscosityTildeIdx>::Type LFSU_N;
        const LFSU_N& lfsu_n = lfsu.template child<viscosityTildeIdx>();

        Scalar elementVolume = eg.geometry().volume();

        // /////////////////////
        // evaluation of unknown, upwinding and averaging
        Scalar viscosityTilde = x(lfsu_n, 0);

        // scalar product of velocity gradient tensor is the stress tenosr S_ij
        Dune::FieldMatrix<Scalar, GridView::dimension, GridView::dimension> vorticity(0.0);
        for (unsigned int i = 0; i < dim; ++i)
        {
          for (unsigned int j = 0; j < dim; ++j)
          {
            vorticity[i][j] = 0.5 * asImp_().storedVelocityGradientTensor[mapperElement.index(eg.entity())][i][j];
            vorticity[i][j] -= 0.5 * asImp_().storedVelocityGradientTensor[mapperElement.index(eg.entity())][j][i];
          }
        }

        Scalar vorticityMagnitude = 0.0;
        for (unsigned int i = 0; i < dim; ++i)
        {
          for (unsigned int j = 0; j < dim; ++j)
          {
            vorticityMagnitude += vorticity[i][j] * vorticity[i][j];
          }
        }

        Scalar distanceToWall = asImp_().storedDistanceToWall[mapperElement.index(eg.entity())];
        Scalar shearStressTilde = std::sqrt(2.0 * vorticityMagnitude)
                                  + viscosityTilde * fv2(eg.entity())
                                    / (karmanConstant_ * karmanConstant_
                                       * distanceToWall * distanceToWall);
        // /////////////////////
        // contribution to residual from element
        // calculate flux over staggered interface in each dimension
        /**
         * (1) \b Production term of <b> viscosityTilde </b> balance equation
         */
        r.accumulate(lfsu_n, 0,
                     -cb1() * (1.0 - ft2(eg.entity()))
                     * shearStressTilde
                     * viscosityTilde
                     * elementVolume);
        /**
         * (2) \b Trip term of <b> viscosityTilde </b> balance equation
         */
        r.accumulate(lfsu_n, 0,
                     -ft1(eg.entity()) * deltaU(eg.entity()) * deltaU(eg.entity())
                     * elementVolume);

        Scalar rFunction = viscosityTilde / (shearStressTilde * karmanConstant_ * karmanConstant_
                                              * distanceToWall * distanceToWall);
        rFunction = std::min(rFunction, 10.0);
        Scalar gFunction = rFunction + cw2() * (std::pow(rFunction, 6.0) - rFunction);
        Scalar poweredTerm = (1.0 + std::pow(cw3(), 6.0))
                             / (std::pow(gFunction, 6.0) + std::pow(cw3(), 6.0));
        Scalar wFunction = gFunction * std::pow(poweredTerm, 1.0/6.0);
        /**
         * (3) \b WallDestruction term of <b> viscosityTilde </b> balance equation
         */
        r.accumulate(lfsu_n, 0,
                     (cw1() * wFunction
                        - cb1() * ft2(eg.entity()) / (karmanConstant_ * karmanConstant_))
                     * viscosityTilde * viscosityTilde / (distanceToWall * distanceToWall)
                     * elementVolume);
      }


      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_skeleton_navierstokes
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton_spalartallmaras(const IG& ig,
                                   const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                                   const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                                   R& r_s, R& r_n,
                                   std::vector<DimVector> velocities_s, Scalar pressure_s,
                                   Scalar massMoleFrac_s, Scalar temperature_s,
                                   std::vector<DimVector> velocities_n, Scalar pressure_n,
                                   Scalar massMoleFrac_n, Scalar temperature_n) const
      {
        // select the components from the subspaces
        typedef typename LFSU::template Child<viscosityTildeIdx>::Type LFSU_N;
        const LFSU_N& lfsu_n_s = lfsu_s.template child<viscosityTildeIdx>();
        const LFSU_N& lfsu_n_n = lfsu_n.template child<viscosityTildeIdx>();

        // local position of cell and face centers
        const Dune::FieldVector<Scalar, dim>& insideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& outsideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.outside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).position(0, 0);

        // global position of cell and face centers
        Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside().geometry().global(insideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> outsideCellCenterGlobal =
          ig.outside().geometry().global(outsideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face normal
        const Dune::FieldVector<Scalar, dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
          {
            normDim = curDim;
          }
        }

        // face midpoints of all faces
        const unsigned int numFaces =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).size(1);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal_s(numFaces);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal_s(numFaces);
        for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
        {
          faceCentersLocal_s[curFace] =
            Dune::ReferenceElements<Scalar, dim>::general(ig.inside().geometry().type()).position(curFace, 1);
          faceCentersGlobal_s[curFace] = ig.inside().geometry().global(faceCentersLocal_s[curFace]);
        }

        // face volume for integration
        Scalar faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                            * Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).volume();
        Scalar elementVolume_s = ig.inside().geometry().volume();
        Scalar elementVolume_n = ig.outside().geometry().volume();

        // distances between face center and cell centers for rectangular shapes
        Scalar distanceInsideToFace = faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim];
        Scalar distanceOutsideToFace = outsideCellCenterGlobal[normDim] - faceCenterGlobal[normDim];

        // /////////////////////
        // evaluation of unknown, upwinding and averaging

        // evaluate cell values and constants
        Scalar viscosityTilde_s = x_s(lfsu_n_s, 0);
        Scalar viscosityTilde_n = x_n(lfsu_n_n, 0);
        Scalar kinematicViscosity_s = asImp_().kinematicViscosity(pressure_s, temperature_s, massMoleFrac_s);
        Scalar kinematicViscosity_n = asImp_().kinematicViscosity(pressure_n, temperature_n, massMoleFrac_n);
        Scalar viscosityTildeGradient = (viscosityTilde_n - viscosityTilde_s)
                                        / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim]);

        // perform averaging for diffusive terms
        Scalar kinematicViscosity_avg = eval::average(kinematicViscosity_s, kinematicViscosity_n,
                                                      distanceInsideToFace, distanceOutsideToFace);
        Scalar viscosityTilde_avg = eval::average(viscosityTilde_s, viscosityTilde_n,
                                                  distanceInsideToFace, distanceOutsideToFace);

        // upwinding: advection term
        Scalar velocityNormal = (velocities_s[ig.indexInInside()] * faceUnitOuterNormal);
        Scalar viscosityTilde_up = viscosityTilde_s;
        if (velocityNormal < 0)
        {
          viscosityTilde_up = viscosityTilde_n;
        }

        if (enableAdvectionAveraging_)
        {
          viscosityTilde_up = (distanceInsideToFace * viscosityTilde_n
                                        + distanceOutsideToFace * viscosityTilde_s)
                                      / (distanceInsideToFace + distanceOutsideToFace);
        }

        // /////////////////////
        // contribution to residual on inside element, other residual is computed by symmetric call
        /**
         * (1) \b Flux term of <b> viscosityTilde </b> balance equation
         *
         * The default value is \b upwinding for the advective part, by
         * using the macro <tt>ENABLE_ADVECTION_AVERAGING 1</tt> you can do an
         * averaging instead of upwinding for the viscosityTilde.
         */
        r_s.accumulate(lfsu_n_s, 0,
                       1.0 * viscosityTilde_up
                       * velocityNormal
                       * faceVolume);
        r_n.accumulate(lfsu_n_n, 0,
                       -1.0 * viscosityTilde_up
                       * velocityNormal
                       * faceVolume);

        /**
         * (2) \b Diffusion term of <b> viscosityTilde </b> balance equation
         */
        r_s.accumulate(lfsu_n_s, 0,
                       -1.0 / sigma()
                       * (kinematicViscosity_avg + viscosityTilde_avg) * viscosityTildeGradient
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);
        r_n.accumulate(lfsu_n_n, 0,
                       1.0 / sigma()
                       * (kinematicViscosity_avg + viscosityTilde_avg) * viscosityTildeGradient
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);

        /**
         * (3) \b "Diffusion" term of <b> viscosityTilde </b> balance equation
         */
        r_s.accumulate(lfsu_n_s, 0,
                       -1.0 / sigma()
                       * cb2() * viscosityTildeGradient * viscosityTildeGradient
                       * 0.5 * elementVolume_s);
        r_n.accumulate(lfsu_n_n, 0,
                       -1.0 / sigma()
                       * cb2() * viscosityTildeGradient * viscosityTildeGradient
                       * 0.5 * elementVolume_n);
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_boundary_navierstokes
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary_spalartallmaras(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s,
                                   std::vector<DimVector> velocityFaces, Scalar pressure_s,
                                   Scalar massMoleFrac_s, Scalar temperature_s,
                                   Scalar pressure_boundary,
                                   Scalar massMoleFrac_boundary, Scalar temperature_boundary) const
      {
        // select the components from the subspaces
        typedef typename LFSU::template Child<viscosityTildeIdx>::Type LFSU_N;
        const LFSU_N& lfsu_n_s = lfsu_s.template child<viscosityTildeIdx>();
        typedef typename BC::template Child<viscosityTildeIdx>::Type BCViscosityTilde;
        const BCViscosityTilde& bcViscosityTilde = bc.template child<viscosityTildeIdx>();

        // center in face's reference element
        const Dune::FieldVector<Scalar, IG::dimension-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, IG::dimension-1>::general(ig.geometry().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // center in inside element
        const Dune::FieldVector<Scalar, dim>& insideFaceCenterLocal =
          ig.geometryInInside().global(faceCenterLocal);

        // face volume for integration
        Scalar faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                            * Dune::ReferenceElements<Scalar,IG::dimension-1>::general(ig.geometry().type()).volume();
        Scalar elementVolume_s = ig.inside().geometry().volume();

        // calculate velocity DoF positions
        const unsigned int numControlVolumeFaces =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().geometry().type()).size(1);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal(numControlVolumeFaces);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
          faceCentersLocal[curFace][curFace / 2] = curFace % 2;
          faceCentersGlobal[curFace] =
            ig.inside().geometry().global(faceCentersLocal[curFace]);
        }

        // cell center in reference element
        const Dune::FieldVector<Scalar,IG::dimension>&
          insideCellCenterLocal = Dune::ReferenceElements<Scalar,IG::dimension>::general(ig.inside().type()).position(0, 0);
        Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside().geometry().global(insideCellCenterLocal);
        const Dune::FieldVector<Scalar,dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
          {
            normDim = curDim;
          }
        }

        // evaluation of cell values
        Scalar viscosityTilde_s = x_s(lfsu_n_s, 0);

        // /////////////////////
        // call ParentType
        // viscosityTilde at the boundary is given
        typename DirichletViscosityTilde::Traits::RangeType  viscosityTilde_boundary(0.0);
        dirichletViscosityTilde.evaluate(ig.inside(), insideFaceCenterLocal, viscosityTilde_boundary);

        Scalar kinematicViscosity_avg = asImp_().kinematicViscosity(pressure_s, temperature_s, massMoleFrac_s);
        Scalar viscosityTilde_avg = viscosityTilde_s;
        Scalar viscosityTildeGradient = (viscosityTilde_boundary - viscosityTilde_s)
                                        / (faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim]);

        // Inflow boundary for viscosityTilde
        if (bcViscosityTilde.isInflow(ig, faceCenterLocal))
        {

          /**
           * Inflow boundary handling for viscosityTilde balance<br>
           * (1) \b Flux term of <b> viscosityTilde </b> balance equation
           */
          r_s.accumulate(lfsu_n_s, 0,
                         1.0 * viscosityTilde_boundary
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);
          /**
           * (2) \b Diffusion term of <b> viscosityTilde </b> balance equation
           *
           * At the boundary the values from the inner cell are taken and <b>no averaging</b>
           * is performed.
           */
          r_s.accumulate(lfsu_n_s, 0,
                         -1.0 / sigma()
                         * (kinematicViscosity_avg + viscosityTilde_avg) * viscosityTildeGradient
                         * faceUnitOuterNormal[normDim]
                         * faceVolume);

          /**
           * (3) \b "Diffusion" term of <b> viscosityTilde </b> balance equation
           *
           * At the boundary the values from the inner cell are taken and <b>no averaging</b>
           * is performed.
           */
          r_s.accumulate(lfsu_n_s, 0,
                         -1.0 / sigma()
                         * cb2() * viscosityTildeGradient * viscosityTildeGradient
                         * 0.5 * elementVolume_s);
        }
        // Wall boundary for viscosityTilde
        else if (bcViscosityTilde.isWall(ig, faceCenterLocal))
        {
          /**
           * (1) \b Diffusion term of <b> viscosityTilde </b> balance equation
           *
           * At the boundary the values from the inner cell are taken and <b>no averaging</b>
           * is performed.
           */
          r_s.accumulate(lfsu_n_s, 0,
                         -1.0 / sigma()
                         * (kinematicViscosity_avg + viscosityTilde_avg) * viscosityTildeGradient
                         * faceUnitOuterNormal[normDim]
                         * faceVolume);

          /**
           * (2) \b "Diffusion" term of <b> viscosityTilde </b> balance equation
           *
           * At the boundary the values from the inner cell are taken and <b>no averaging</b>
           * is performed.
           */
          r_s.accumulate(lfsu_n_s, 0,
                         -1.0 / sigma()
                         * cb2() * viscosityTildeGradient * viscosityTildeGradient
                         * 0.5 * elementVolume_s);
        }
        // Outflow boundary for viscosityTilde
        else if (bcViscosityTilde.isOutflow(ig, faceCenterLocal))
        {
          /**
           * Outflow boundary handling for viscosityTilde balance<br>
           * (1) \b Flux term of <b> viscosityTilde </b> balance equation
           */
          r_s.accumulate(lfsu_n_s, 0,
                         1.0 * viscosityTilde_s
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);

          /**
           * (2) \b "Diffusion" term of <b> viscosityTilde </b> balance equation
           *
           * At the boundary the values from the inner cell are taken and <b>no averaging</b>
           * is performed.
           */
          r_s.accumulate(lfsu_n_s, 0,
                         -1.0 / sigma()
                         * cb2() * viscosityTildeGradient * viscosityTildeGradient
                         * 0.5 * elementVolume_s);
        }
        // Symmetry boundary for viscosityTilde
        else if (bcViscosityTilde.isSymmetry(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Symmetry Condition for viscosityTilde.
          //! Ensure normal velocity = 0 is done by the parent function.
        }
        else
        {
          DUNE_THROW(Dune::NotImplemented, "Wrong boundary condition type for viscosityTilde.");
        }
      }

      /**
       * \brief Initialize vector-stored values used for alpha_* routines.
       */
      void initialize()
      {
        storedViscosityTilde.resize(mapperElement.size());

        // initialize some of the values
        for (unsigned int i = 0; i < mapperElement.size(); ++i)
        {
          storedViscosityTilde[i] = 1e-10;
        }

        // select the components from the subspaces
        typedef typename BC::template Child<viscosityTildeIdx>::Type BCViscosityTilde;
        const BCViscosityTilde& bcViscosityTilde = bc.template child<viscosityTildeIdx>();

        // loop over grid view to get elements with a wall intersection
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          for (IntersectionIterator ig = gridView.ibegin(*eit);
              ig != gridView.iend(*eit); ++ig)
          {
            // local and global position of face centers
            const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
              Dune::ReferenceElements<Scalar, dim-1>::general(ig->geometry().type()).position(0, 0);
            Dune::FieldVector<Scalar, dim> faceCenterGlobal = ig->geometry().global(faceCenterLocal);

            // check for multiple defined boundary conditions
            unsigned int numberOfBCTypesAtPos = 0;
            if (bcViscosityTilde.isWall(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcViscosityTilde.isInflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcViscosityTilde.isOutflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcViscosityTilde.isSymmetry(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (numberOfBCTypesAtPos > 1)
            {
              std::cout << "BCViscosityTilde at faceCenterGlobal " << faceCenterGlobal << std::endl;
              std::cout << "Wall      " << bcViscosityTilde.isWall(*ig, faceCenterLocal) << std::endl;
              std::cout << "Inflow    " << bcViscosityTilde.isInflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Outflow   " << bcViscosityTilde.isOutflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Symmetry  " << bcViscosityTilde.isSymmetry(*ig, faceCenterLocal) << std::endl;
              DUNE_THROW(Dune::NotImplemented, "Multiple boundary conditions for viscosityTilde at one point.");
            }
          }
        }
      }

      /**
       * \brief Update vector-stored values used for alpha_* routines.
       *
       * This function just calls the ParentType.
       *
       * \tparam GFS GridFunctionSpace type
       * \tparam X Coefficient vector
       * \param gfs Grid function space
       * \param lastSolution Coefficient vector from last Newton step or time step
       */
#if IS_STAGGERED_MULTIDOMAIN_MODEL
      template<typename SubDomainGridView, typename MDGFS, typename X, int stokesDomainIdx>
      void updateStoredValues(const SubDomainGridView& sdgv, const MDGFS& mdgfs, X& lastSolution)
      {
        // grid function sub spaces
        using SubGfsViscosityTilde = Dune::PDELab::GridFunctionSubSpace
            <MDGFS, Dune::TypeTree::TreePath<stokesDomainIdx, viscosityTildeIdx> >;
        SubGfsViscosityTilde subGfsViscosityTilde(mdgfs);
#else
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
        // grid function sub spaces
        using SubGfsViscosityTilde = Dune::PDELab::GridFunctionSubSpace
            <GFS, Dune::TypeTree::TreePath<viscosityTildeIdx> >;
        SubGfsViscosityTilde subGfsViscosityTilde(gfs);
#endif

        // discrete function objects
        using DgfViscosityTilde = Dune::PDELab::DiscreteGridFunction<SubGfsViscosityTilde, X>;
        DgfViscosityTilde dgfViscosityTilde(subGfsViscosityTilde, lastSolution);

        // loop over grid view
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          // NOTE use eit also for multidomain models
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);

#if IS_STAGGERED_MULTIDOMAIN_MODEL
          if (!gridView.indexSet().contains(stokesDomainIdx, *eit))
              continue;
          auto element = sdgv.grid().subDomainEntityPointer(*eit);
#else
          auto element = *eit;
#endif

          // evaluate solution
          Dune::FieldVector<Scalar, dim> cellCenterLocal(0.5);
          Dune::FieldVector<Scalar, 1> viscosityTilde(0.0);
          dgfViscosityTilde.evaluate(element, cellCenterLocal, viscosityTilde);

          storedViscosityTilde[elementInsideID] = viscosityTilde;
          asImp_().storedKinematicEddyViscosity[elementInsideID]
            = calculateKinematicEddyViscosity(*eit);
        }
      }

      /**
       * \brief Returns the viscosityTilde for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar viscosityTilde(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        typedef typename LFSU::template Child<viscosityTildeIdx>::Type LFSU_N;
        const LFSU_N& lfsu_n = lfsu.template child<viscosityTildeIdx>();
        return x(lfsu_n, 0);
      }

      //! \brief Calculates the kinematic eddy viscosity
      double calculateKinematicEddyViscosity(const Element& e) const
      { return asImp_().storedViscosityTilde[mapperElement.index(e)] * fv1(e); }

      //! \brief Returns viscosity ratio for an element
      const Scalar chi(const Element& e) const
      {
          return asImp_().storedViscosityTilde[mapperElement.index(e)]
                / asImp_().storedKinematicViscosity[mapperElement.index(e)];
      }

      //! \brief Returns the norm of the velocity difference
      const Scalar deltaU(const Element& e) const
      {
        unsigned int elementInsideID = mapperElement.index(e);
        unsigned int wallElementID = asImp_().storedCorrespondingWallElementID[elementInsideID];
        Scalar cur = 0.0;
        Scalar min = 0.0;
        for (unsigned dimIdx = 0; dimIdx < dim; ++dimIdx)
        {
            cur += asImp_().storedVelocitiesAtElementCenter[elementInsideID][dimIdx] * asImp_().storedVelocitiesAtElementCenter[elementInsideID][dimIdx];
            min += asImp_().storedVelocityMinimum[wallElementID][dimIdx] * asImp_().storedVelocityMinimum[wallElementID][dimIdx];
        }
        return std::sqrt(cur) - std::sqrt(min);
      }

      //! \brief Returns ft1-function value for an element
      const Scalar ft1(const Element& e) const
      {
        if (enableTripCorrcetion_)
        {
          Scalar wallDistance = asImp_().storedDistanceToWall[mapperElement.index(e)];
          return ct1() * gt(e)
                * std::exp(-ct2() * wt(e) * wt(e) / deltaU(e) / deltaU(e)
                            * (wallDistance * wallDistance
                              + gt(e) * gt(e) * tripDistance(e) * tripDistance(e)));
        }
        return 0.0;
      }

      //! \brief Returns ft2-function value for an element
      const Scalar ft2(const Element& e) const
      {
        if (enableTripCorrcetion_)
            return ct3() * std::exp(-ct4() * chi(e) * chi(e));
        else
            return 0.0;
      }

      //! \brief Returns fv1-function value for an element
      const Scalar fv1(const Element& e) const
      { return chi(e) * chi(e) * chi(e) / (chi(e) * chi(e) * chi(e) + cv1() * cv1() * cv1()); }

      //! \brief Returns fv2-function value for an element
      const Scalar fv2(const Element& e) const
      { return 1.0 - chi(e) / (1.0 + chi(e) * fv1(e)); }


      //! \brief Returns gt-function value for an element
      const Scalar gt(const Element& e) const
      {
//         Scalar wallDistance = asImp_().storedDistanceToWall[mapperElement.index(e)];
        return std::min(0.1, deltaU(e) / wt(e) * dx(e));
      }

      //! \brief Returns the vorticity at the trip, whether this is ...
      const Scalar wt(const Element& e) const
      {
        // the vorticity at the trip (the transition)
        DUNE_THROW(Dune::NotImplemented, "wt() is not correctly implemented.");

//         unsigned int elementInsideID = mapperElement.index(e);
//         unsigned int wallElementID = asImp_().storedCorrespondingWallElementID[elementInsideID];
//         unsigned int wallNormalAxis = asImp_().storedCorrespondingWallNormalAxis[wallElementID];
//         unsigned int flowNormalAxis = asImp_().storedCorrespondingFlowNormalAxis[wallElementID];
//         return 0.5 * asImp_().storedVelocityGradientTensor[wallElementID][flowNormalAxis][wallNormalAxis]
//                - asImp_().storedVelocityGradientTensor[wallElementID][wallNormalAxis][flowNormalAxis];
      }

      //! \brief Returns the discretization length at the wall
      const Scalar dx(const Element& e) const
      {
        unsigned int elementInsideID = mapperElement.index(e);
        unsigned int wallElementID = asImp_().storedCorrespondingWallElementID[elementInsideID];
        unsigned int flowNormalAxis = asImp_().storedCorrespondingFlowNormalAxis[wallElementID];
        unsigned int neigborElementIDLeft = asImp_().storedNeighborID[wallElementID][flowNormalAxis][0];
        unsigned int neigborElementIDRight = asImp_().storedNeighborID[wallElementID][flowNormalAxis][1];
        return 0.5 * std::abs(asImp_().storedElementCentersGlobal[neigborElementIDRight][flowNormalAxis]
                              - asImp_().storedElementCentersGlobal[neigborElementIDLeft][flowNormalAxis]);
      }

      /*!
       * \brief Returns the trip distance, the distance to the laminar-turbulent transition
       */
      const Scalar tripDistance(const Element& e) const
      {
        // trip distance is the distance to the transition
        DUNE_THROW(Dune::NotImplemented, "tripDistance() is not implemented.");
//         unsigned int elementInsideID = mapperElement.index(e);
//         unsigned int flowNormalAxis = asImp_().storedCorrespondingFlowNormalAxis[elementInsideID];
//         return std::abs(asImp_().storedElementCentersGlobal[elementInsideID][flowNormalAxis]);
      }


      //! \brief Returns sigma constant
      const Scalar sigma() const
      { return 2.0/3.0; }

      //! \brief Returns cb1 constant
      const Scalar cb1() const
      { return 0.1355; }

      //! \brief Returns cb2 constant
      const Scalar cb2() const
      { return 0.622; }

      //! \brief Returns cv1 constant
      const Scalar cv1() const
      { return 7.1; }

      //! \brief Returns cw1 constant
      const Scalar cw1() const
      { return cb1() / (karmanConstant_ * karmanConstant_) + (1.0 + cb2()) / sigma(); }

      //! \brief Returns cw2 constant
      const Scalar cw2() const
      { return 0.3; }

      //! \brief Returns cw3 constant
      const Scalar cw3() const
      { return 2.0; }

      //! \brief Returns ct1 constant
      const Scalar ct1() const
      { return 1.0; }

      //! \brief Returns ct2 constant
      const Scalar ct2() const
      { return 2.0; }

      //! \brief Returns ct3 constant
      const Scalar ct3() const
      { return 1.2; }

      //! \brief Returns ct4 constant
      const Scalar ct4() const
      { return 0.5; }

private:
      const BC& bc;
      const SourceViscosityTildeBalance& sourceViscosityTildeBalance;
      const DirichletViscosityTilde& dirichletViscosityTilde;
      const NeumannViscosityTilde& neumannViscosityTilde;
      GridView gridView;
      MapperElement mapperElement;

      // properties
      bool enableAdvectionAveraging_;
      bool enableTripCorrcetion_;
      Scalar karmanConstant_;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }

      Problem &problem_()
      { return *problemPtr_; }
      const Problem &problem_() const
      { return *problemPtr_; }

      Problem *problemPtr_;
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_BASE_SPALARTALLMARAS_STAGGERED_GRID_HH
