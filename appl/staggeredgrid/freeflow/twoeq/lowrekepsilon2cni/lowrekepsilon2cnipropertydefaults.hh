// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 *
 * \file
 *
 * \brief Defines default properties for the compositional non-isothermal
 *        low-Re kepsilon staggered grid model.
 *
 * These can be overwritten at a different place or
 * may be replaced by values of the input file.
 */

#ifndef DUMUX_LOWREKEPSILON_TWOCNI_PROPERTY_DEFAULTS_HH
#define DUMUX_LOWREKEPSILON_TWOCNI_PROPERTY_DEFAULTS_HH

#include<appl/staggeredgrid/freeflow/eddyviscosity2cni/eddyviscosity2cnipropertydefaults.hh>

#include"lowrekepsilon2cniindices.hh"
#include"lowrekepsilon2cniproperties.hh"

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Properties
//////////////////////////////////////////////////////////////////

//! Set property value for BCType
SET_PROP(StaggeredGridLowReKEpsilon2cni, BCType)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, BCVelocity) BCVelocity;
    typedef typename GET_PROP_TYPE(TypeTag, BCPressure) BCPressure;
    typedef typename GET_PROP_TYPE(TypeTag, BCMassMoleFrac) BCMassMoleFrac;
    typedef typename GET_PROP_TYPE(TypeTag, BCTemperature) BCTemperature;
    typedef typename GET_PROP_TYPE(TypeTag, BCTurbulentKineticEnergy) BCTurbulentKineticEnergy;
    typedef typename GET_PROP_TYPE(TypeTag, BCDissipation) BCDissipation;
public:
    typedef Dune::PDELab::CompositeConstraintsParameters<BCVelocity, BCPressure,
                                                         BCMassMoleFrac, BCTemperature,
                                                         BCTurbulentKineticEnergy, BCDissipation> type;
};

//! Set the indices used by the model
SET_TYPE_PROP(StaggeredGridLowReKEpsilon2cni, Indices,
              StaggeredGridLowReKEpsilon2cniCommonIndices<TypeTag>);

//! Factor to control the timestep increment (should be smaller for eddy viscosity problems)
SET_SCALAR_PROP(StaggeredGridLowReKEpsilon2cni, NewtonTimeStepIncrementFactor, 0.5);
}
}

#endif // DUMUX_LOWREKEPSILON_TWOCNI_PROPERTY_DEFAULTS_HH
