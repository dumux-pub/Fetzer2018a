// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/** \file
 *  \brief Constraints fixing the dissipation value in cells, having boundaries
 *         with edges in the specified domain.
*/

#ifndef DUMUX_FIX_DISSIPATION_CONSTRAINTS_HH
#define DUMUX_FIX_DISSIPATION_CONSTRAINTS_HH

#include<dune/grid/common/grid.hh>

#include<dune/pdelab/common/geometrywrapper.hh>
#include<dune/pdelab/constraints/common/constraintsparameters.hh>
#include<dune/pdelab/gridfunctionspace/gridfunctionspace.hh>

#include"../kepsilon/kepsilonpropertydefaults.hh"

namespace Dumux {

/**
  * \brief Sets fix values for the specified domain.
  *
  * \tparam TypeTag TypeTag of the problem
  */
template<class TypeTag>
class FixDissipationConstraints
{
public:
  enum { doBoundary = false };
  enum { doProcessor = false };
  enum { doSkeleton = false };
  enum { doVolume = true };

  typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
  typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
  typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
  typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
  typedef typename GridView::IntersectionIterator IntersectionIterator;
  enum { dim = GridView::dimension };

  //! \brief Index of unknowns
  typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
  enum { dissipationIdx = Indices::dissipationIdx };

  //! \brief Constructor
  //! \tparam GridView GridView type
  FixDissipationConstraints(GridView gridView, Problem& problem)
    : gridView_(gridView), problemPtr_(&problem)
  { }

  /**
    * \brief Volume constraints to fix specified elements to a given value.
    *
    * \tparam EG  element geometry
    * \tparam LFS local function space
    * \tparam T   transformation type
    */
  template<typename EG, typename LFS, typename T>
  void volume(const EG& eg, const LFS& lfs, T& trafo) const
  {
    if (problem_().kEpsilonWallFunctions().useWallFunctionDissipation(eg.entity()))
    {
      // empty map means Dirichlet constraint
      typename T::RowType empty;

      // set first (and only because we have p0 elements) degrees of freedom
      trafo[lfs.dofIndex(0)] = empty;
    }
  }

private:
    Problem &problem_()
    { return *problemPtr_; }
    const Problem &problem_() const
    { return *problemPtr_; }

    GridView gridView_;
    Problem *problemPtr_;
};
} // end namespace Dumux

#endif // DUMUX_FIX_DISSIPATION_CONSTRAINTS_HH
