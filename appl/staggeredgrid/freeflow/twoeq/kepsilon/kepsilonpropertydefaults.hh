// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup BoxStokesModel
 *
 * \file
 *
 * \brief Defines default properties for the kepsilon staggered grid model.
 *
 * These can be overwritten at a different place or
 * may be replaced by values of the input file.
 */

#ifndef DUMUX_KEPSILON_PROPERTY_DEFAULTS_HH
#define DUMUX_KEPSILON_PROPERTY_DEFAULTS_HH

#include<appl/staggeredgrid/freeflow/eddyviscosity2cni/eddyviscositypropertydefaults.hh>

#include"kepsilonboundaryconditions.hh"
#include"kepsilonproperties.hh"
#include"kepsilonindices.hh"
#include"kepsilonwallfunctions.hh"

namespace Dumux
{

template<class TypeTag> class BCTurbulentKineticEnergy;
template<class TypeTag> class BCDissipation;
template<class TypeTag, typename GridView, typename Scalar> class DirichletTurbulentKineticEnergy;
template<class TypeTag, typename GridView, typename Scalar> class DirichletDissipation;
template<class TypeTag, typename GridView, typename Scalar> class NeumannTurbulentKineticEnergy;
template<class TypeTag, typename GridView, typename Scalar> class NeumannDissipation;
template<class TypeTag, typename GridView, typename Scalar> class SourceTurbulentKineticEnergyBalance;
template<class TypeTag, typename GridView, typename Scalar> class SourceDissipationBalance;

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Properties
//////////////////////////////////////////////////////////////////

//! Set the indices used by the model
SET_TYPE_PROP(StaggeredGridKEpsilon, Indices,
              StaggeredGridKEpsilonCommonIndices<TypeTag>);

//! Set property value for the turbulentKineticEnergy boundary condition
SET_TYPE_PROP(StaggeredGridKEpsilon, BCTurbulentKineticEnergy, BCTurbulentKineticEnergy<TypeTag>);

//! Set property value for the dissipation boundary condition
SET_TYPE_PROP(StaggeredGridKEpsilon, BCDissipation, BCDissipation<TypeTag>);

//! Set property value for BCType
SET_PROP(StaggeredGridKEpsilon, BCType)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, BCVelocity) BCVelocity;
    typedef typename GET_PROP_TYPE(TypeTag, BCPressure) BCPressure;
    typedef typename GET_PROP_TYPE(TypeTag, BCTurbulentKineticEnergy) BCTurbulentKineticEnergy;
    typedef typename GET_PROP_TYPE(TypeTag, BCDissipation) BCDissipation;
public:
    typedef Dune::PDELab::CompositeConstraintsParameters<BCVelocity, BCPressure,
                                                         BCTurbulentKineticEnergy, BCDissipation> type;
};

//! Set property value for DirichletTurbulentKineticEnergy
SET_PROP(StaggeredGridKEpsilon, DirichletTurbulentKineticEnergy)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef DirichletTurbulentKineticEnergy<TypeTag, GridView, Scalar> type;
};

//! Set property value for DirichletDissipation
SET_PROP(StaggeredGridKEpsilon, DirichletDissipation)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef DirichletDissipation<TypeTag, GridView, Scalar> type;
};

//! Set property value for NeumannTurbulentKineticEnergy
SET_PROP(StaggeredGridKEpsilon, NeumannTurbulentKineticEnergy)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef NeumannTurbulentKineticEnergy<TypeTag, GridView, Scalar> type;
};

//! Set property value for NeumannDissipation
SET_PROP(StaggeredGridKEpsilon, NeumannDissipation)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef NeumannDissipation<TypeTag, GridView, Scalar> type;
};

//! Set property value for SourceTurbulentKineticEnergyBalance
SET_PROP(StaggeredGridKEpsilon, SourceTurbulentKineticEnergyBalance)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef SourceTurbulentKineticEnergyBalance<TypeTag, GridView, Scalar> type;
};

//! Set property value for SourceDissipationBalance
SET_PROP(StaggeredGridKEpsilon, SourceDissipationBalance)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef SourceDissipationBalance<TypeTag, GridView, Scalar> type;
};

//! Use Launder and Sharma constant by default
SET_INT_PROP(StaggeredGridKEpsilon, KEpsilonModelConstants, 0);

//! Set property value for SourceDissipationBalance
SET_TYPE_PROP(StaggeredGridKEpsilon, KEpsilonWallFunctions, Dumux::KEpsilonWallFunctions<TypeTag>);

//! Use constant value wall function as default
SET_INT_PROP(StaggeredGridKEpsilon, KEpsilonWallFunctionModel, 2);

//! Use wall functions below a y^+ value of 30
SET_SCALAR_PROP(StaggeredGridKEpsilon, KEpsilonWallFunctionYPlusThreshold, 50);

//! Use wall functions below a u^+ value of infinity
SET_SCALAR_PROP(StaggeredGridKEpsilon, KEpsilonWallFunctionUPlusThreshold,
                std::numeric_limits<typename GET_PROP_TYPE(TypeTag, Scalar)>::max());

//! Replace laminar and turbulent diffusive fluxes
SET_INT_PROP(StaggeredGridKEpsilon, KEpsilonWallFunctionReplaceFluxesType, 2);

//! Do not use wall functions for components on default
SET_SCALAR_PROP(StaggeredGridKEpsilon, KEpsilonWallFunctionComponentMinimum, 30);

//! Do not use wall functions for temperature on default
SET_SCALAR_PROP(StaggeredGridKEpsilon, KEpsilonWallFunctionTemperatureMinimum, 30);

//! Do not scale the ZeroEq model for the two-layer approach (may lead to worse convergence)
SET_BOOL_PROP(StaggeredGridKEpsilon, KEpsilonEnableZeroEqScaling, false);

//! Use kinematic viscosity for diffusion term of the kepsilon equation
SET_BOOL_PROP(StaggeredGridKEpsilon, KEpsilonEnableKinematicViscosity, true);

//! Use the stored eddy viscosity
SET_BOOL_PROP(StaggeredGridKEpsilon, KEpsilonUseStoredEddyViscosity, true);

//! Always update the stored variables (might lead to convergence problems)
SET_SCALAR_PROP(StaggeredGridKEpsilon, KEpsilonUpdateStoredVariablesUntil,
                std::numeric_limits<typename GET_PROP_TYPE(TypeTag, Scalar)>::max());

//! Use maximum suggested time step
SET_SCALAR_PROP(StaggeredGridKEpsilon, KEpsilonUpdateStoredVariablesMaxTimeStep,
                std::numeric_limits<typename GET_PROP_TYPE(TypeTag, Scalar)>::max());

//! The axis to calculate wall distances (negative means determine automatically)
SET_INT_PROP(StaggeredGridKEpsilon, ProblemWallNormalAxis, -1);

//! The main flow axis for calculating the eddy viscosity (negative means determine automatically)
SET_INT_PROP(StaggeredGridKEpsilon, ProblemFlowNormalAxis, -1);

//! Factor to control the timestep increment (should be smaller for eddy viscosity problems)
SET_SCALAR_PROP(StaggeredGridKEpsilon, NewtonTimeStepIncrementFactor, 5.0/6.0);
}
}

#endif // DUMUX_KEPSILON_PROPERTY_DEFAULTS_HH
