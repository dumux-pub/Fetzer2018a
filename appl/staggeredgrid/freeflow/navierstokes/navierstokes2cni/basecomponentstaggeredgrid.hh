/**
 * \file
 * \ingroup StaggeredGrid
 * \ingroup BaseComponentStaggeredGrid
 *
 * \brief Local operator for staggered grid discretization for steady-state
 *        component transport equation
 *
 * \copydoc NavierStokesStaggeredGrid
 *
 * The component transport equation for mole fractions:<br>
 *
 * \f[
 *    \frac{\partial}{\partial t}  \left( \varrho_{\textrm{mol},\alpha} x^\kappa_\alpha \right)
 *    + \nabla \cdot \left( \varrho_{\textrm{mol},\alpha} v_\alpha x^\kappa_\alpha \right)
 *    - \nabla \cdot \left( D^\kappa_\alpha \frac{\varrho_{\alpha}}{M_\alpha} \nabla x^\kappa_\alpha \right)
 *    - q^\kappa_\alpha
 *    = 0
 * \f]
 *
 * The component transport equation for mass fractions:<br>
 *
 * \f[
 *    \frac{\partial}{\partial t}  \left( \varrho_\alpha X^\kappa_\alpha \right)
 *    + \nabla \cdot \left( \varrho_\alpha v_\alpha X^\kappa_\alpha \right)
 *    - \nabla \cdot \left( D^\kappa_\alpha \varrho_{\alpha} \frac{M^\kappa}{M_\alpha} \nabla x^\kappa_\alpha \right)
 *    - q^\kappa_\alpha
 *    = 0
 * \f]
 *
 * The conversion from mass to mole fraction is given by:<br>
 * \f[
 *    X^\kappa_\alpha \frac{M_\alpha}{M^\kappa} = \nabla x^\kappa_\alpha
 * \f]
 *
 * And from density to molar density:<br>
 * \f[
 *    \frac{\varrho_\alpha}{M_\alpha} = \varrho_{\textrm{mol},\alpha}
 * \f]
 *
 * \note The default value are <b> mass fractions</b>, by
 *       using the macro <tt>USE_MOLES 1</tt> you can use mole fractions
 *       for the component transport.
 * Be aware that the storage terms (the time dependent terms)
 * are not included here but are implemented in the transient part
 * of the local operator.
 */

#ifndef DUMUX_BASECOMPONENT_STAGGERED_GRID_HH
#define DUMUX_BASECOMPONENT_STAGGERED_GRID_HH

#include<cstddef>
#include<vector>

#include<dune/common/exceptions.hh>
#include<dune/common/fvector.hh>
#include<dune/common/fmatrix.hh>

#include<dune/geometry/quadraturerules.hh>
#include<dune/geometry/referenceelements.hh>
#include<dune/geometry/type.hh>

#include<dune/pdelab/gridfunctionspace/lfsindexcache.hh>
#include<dune/pdelab/gridfunctionspace/localfunctionspace.hh>
#include<dune/pdelab/localoperator/defaultimp.hh>
#include<dune/pdelab/localoperator/flags.hh>
#include<dune/pdelab/localoperator/idefault.hh>
#include<dune/pdelab/localoperator/pattern.hh>

#include<appl/staggeredgrid/common/evaluatefluxes.hh>

#include"navierstokes2cniproperties.hh"

namespace Dune
{
  namespace PDELab
  {
    /**
     * \brief Local operator for staggered grid discretization solving
     *        the steady-state component transport equation.
     *
     * \tparam TypeTag TypeTag of the problem
     */
    template<class TypeTag>
    class BaseComponentStaggeredGrid
    {
    public:
      typedef typename GET_PROP_TYPE(TypeTag, LocalOperator) Implementation;
      typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;

      typedef typename GET_PROP_TYPE(TypeTag, BCType) BC;
      typedef typename GET_PROP_TYPE(TypeTag, SourceComponentBalance) SourceComponentBalance;
      typedef typename GET_PROP_TYPE(TypeTag, DirichletMassMoleFrac) DirichletMassMoleFrac;
      typedef typename GET_PROP_TYPE(TypeTag, NeumannMassMoleFrac) NeumannMassMoleFrac;

      typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
      enum { dim = GridView::dimension };

      typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
      typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;
      typedef typename GET_PROP_TYPE(TypeTag, MapperElement) MapperElement;

      typedef typename Dumux::EvaluateFluxes<TypeTag> eval;

      typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
      enum { pressureIdx = Indices::pressureIdx,
             massMoleFracIdx = Indices::massMoleFracIdx };
      enum { numComponents = Indices::numComponents,
             phaseIdx = Indices::phaseIdx,
             transportCompIdx = Indices::transportCompIdx,
             phaseCompIdx = Indices::phaseCompIdx };

      //! \brief Pattern assembly flags
      enum { doPatternVolume = true,
             doPatternSkeleton = true };

      //! \brief Residual assembly flags
      enum { doAlphaVolume = true,
             doAlphaSkeleton = true,
             doAlphaBoundary = true };

      // Types to store the velocities and coordinates
      typedef std::vector<double> StoredScalar;
      StoredScalar storedDiffusionCoefficient;

      //! \brief Constructor
      BaseComponentStaggeredGrid(const BC& bc_,
        const SourceComponentBalance& sourceComponentBalance_,
        const DirichletMassMoleFrac& dirichletMassMoleFrac_,
        const NeumannMassMoleFrac& neumannMassMoleFrac_,
        GridView gridView_)
        : bc(bc_),
          sourceComponentBalance(sourceComponentBalance_),
          dirichletMassMoleFrac(dirichletMassMoleFrac_),
          neumannMassMoleFrac(neumannMassMoleFrac_),
          gridView(gridView_), mapperElement(gridView_)
      {
        enableAdvectionAveraging_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableAdvectionAveraging);

        useMoles_ = GET_PROP_VALUE(TypeTag, UseMoles);
        if (useMoles_)
        {
          DUNE_THROW(NotImplemented, "The component transport is not implemented for mole fraction formulation.");
        }
        enableDiffusiveFluxesInTotalMassBalance_  = GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableDiffusiveFluxesInTotalMassBalance);

        initialize();
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_volume_navierstokes
       */
      template<typename EG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_volume_component(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, R& r,
                                  std::vector<DimVector> velocityFaces, Scalar pressure,
                                  Scalar massMoleFrac, Scalar temperature) const
      {
        typedef typename LFSU::template Child<massMoleFracIdx>::Type LFSU_C;
        const LFSU_C& lfsu_c = lfsu.template child<massMoleFracIdx>();

        // /////////////////////
        // geometry information

        Scalar elementVolume = eg.geometry().volume();

        /**
          * (1) \b Source term of \b component balance equation<br>
          *
          * \f[
          *    - q^\kappa_\alpha
          *    \Rightarrow - \int_V q^\kappa_\alpha
          * \f]
          * \f[
          *    \alpha = - q^\kappa_\alpha V_e
          * \f]
          */
        Dune::GeometryType gt = eg.geometry().type();
        const int qorder = 4;
        const Dune::QuadratureRule<Scalar,dim>& rule = Dune::QuadratureRules<Scalar,dim>::rule(gt, qorder);

        // loop over quadrature points
        for (typename Dune::QuadratureRule<Scalar,dim>::const_iterator it=rule.begin(); it!=rule.end(); ++it)
        {
          typename SourceComponentBalance::Traits::RangeType sourceComponentBalanceValue;
          sourceComponentBalance.evaluate(eg.entity(), it->position(), sourceComponentBalanceValue);
          r.accumulate(lfsu_c, 0,
                       -1.0 * sourceComponentBalanceValue * elementVolume * it->weight());
        }
      }


      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_skeleton_navierstokes
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_skeleton_component(const IG& ig,
                                    const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s,
                                    const LFSU& lfsu_n, const X& x_n, const LFSV& lfsv_n,
                                    R& r_s, R& r_n,
                                    std::vector<DimVector> velocities_s, Scalar pressure_s,
                                    Scalar massMoleFrac_s, Scalar temperature_s,
                                    std::vector<DimVector> velocities_n, Scalar pressure_n,
                                    Scalar massMoleFrac_n, Scalar temperature_n) const
      {
        // select the components from the subspaces
        typedef typename LFSU::template Child<massMoleFracIdx>::Type LFSU_C;
        typedef typename LFSU::template Child<pressureIdx>::Type LFSU_P;
        const LFSU_C& lfsu_c_s = lfsu_s.template child<massMoleFracIdx>();
        const LFSU_C& lfsu_c_n = lfsu_n.template child<massMoleFracIdx>();
        const LFSU_P& lfsu_p_s = lfsu_s.template child<pressureIdx>();
        const LFSU_P& lfsu_p_n = lfsu_n.template child<pressureIdx>();

        // local position of cell and face centers
        const Dune::FieldVector<Scalar, dim>& insideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& outsideCellCenterLocal =
          Dune::ReferenceElements<Scalar, dim>::general(ig.outside().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).position(0, 0);

        // global position of cell and face centers
        Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside().geometry().global(insideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> outsideCellCenterGlobal =
          ig.outside().geometry().global(outsideCellCenterLocal);
        Dune::FieldVector<Scalar, dim> faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face normal
        const Dune::FieldVector<Scalar, dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
          {
            normDim = curDim;
          }
        }

        // face midpoints of all faces
        const unsigned int numFaces =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().type()).size(1);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal_s(numFaces);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal_s(numFaces);
        for (unsigned int curFace = 0; curFace < numFaces; ++curFace)
        {
          faceCentersLocal_s[curFace] =
            Dune::ReferenceElements<Scalar, dim>::general(ig.inside().geometry().type()).position(curFace, 1);
          faceCentersGlobal_s[curFace] = ig.inside().geometry().global(faceCentersLocal_s[curFace]);
        }

        // face volume for integration
        Scalar faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                            * Dune::ReferenceElements<Scalar, dim-1>::general(ig.geometry().type()).volume();

        // distances between face center and cell centers for rectangular shapes
        Scalar distanceInsideToFace = faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim];
        Scalar distanceOutsideToFace = outsideCellCenterGlobal[normDim] - faceCenterGlobal[normDim];

        // /////////////////////
        // evaluate cell values and constants
        const Scalar density_s = asImp_().density(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar density_n = asImp_().density(pressure_n, temperature_n, massMoleFrac_n);
        const Scalar molarDensity_s = asImp_().molarDensity(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar molarDensity_n = asImp_().molarDensity(pressure_n, temperature_n, massMoleFrac_n);
        Scalar diffusionCoefficient_s = asImp_().diffusionCoefficient(pressure_s, temperature_s, massMoleFrac_s);
        Scalar diffusionCoefficient_n = asImp_().diffusionCoefficient(pressure_n, temperature_n, massMoleFrac_n);
        Scalar eddyDiffusivity_s = asImp_().eddyDiffusivity(ig.inside(), lfsu_s, x_s);
        Scalar eddyDiffusivity_n = asImp_().eddyDiffusivity(ig.outside(), lfsu_n, x_n);
        Scalar molarMassComponent = asImp_().molarMassComponent(transportCompIdx);

        // averaging: distance weighted average for diffusion term
        Scalar molarDensity_avg = eval::average(molarDensity_s, molarDensity_n,
                                                distanceInsideToFace, distanceOutsideToFace);
        Scalar diffusionCoefficient_avg = eval::average(diffusionCoefficient_s, diffusionCoefficient_n,
                                                        distanceInsideToFace, distanceOutsideToFace);
        Scalar eddyDiffusivity_avg = eval::average(eddyDiffusivity_s, eddyDiffusivity_n,
                                                   distanceInsideToFace, distanceOutsideToFace);

        // perform averaging for diffusive terms
        Scalar velocityNormal = (velocities_s[ig.indexInInside()] * faceUnitOuterNormal);
        Scalar density_up = density_s;
        Scalar massMoleFrac_up = massMoleFrac_s;
        if (velocityNormal < 0)
        {
          density_up = density_n;
          massMoleFrac_up = massMoleFrac_n;
        }

        if (enableAdvectionAveraging_)
        {
            // distance weighted average mean
            density_up = (distanceInsideToFace * density_n + distanceOutsideToFace * density_s)
                         / (distanceInsideToFace + distanceOutsideToFace);
            massMoleFrac_up = (distanceInsideToFace * massMoleFrac_n + distanceOutsideToFace * massMoleFrac_s)
                              / (distanceInsideToFace + distanceOutsideToFace);
        }

        /**
         * (1) \b Flux term of \b component balance equation
         *
         * In mole formulation:
         * \f[
         *    \varrho_\alpha x^\kappa_\alpha v
         *    \Rightarrow \int_\gamma \left( \varrho_\alpha x^\kappa_\alpha v \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = |\gamma| \varrho_{\alpha,\textrm{up}} x^\kappa_{\alpha,\textrm{up}} \left( v \cdot n \right)
         * \f]
         *
         * In mass formulation:
         * \f[
         *    \varrho_\alpha X^\kappa_\alpha v
         *    \Rightarrow \int_\gamma \left( \varrho_\alpha X^\kappa_\alpha v \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = |\gamma| \varrho_{\alpha,\textrm{up}} X^\kappa_{\alpha,\textrm{up}} \left( v \cdot n \right)
         * \f]
         *
         * The default value is \b upwinding for the advective part, by
         * using the macro <tt>ENABLE_ADVECTION_AVERAGING 1</tt> you can do an
         * averaging instead of upwinding for the component transport.
         */

        r_s.accumulate(lfsu_c_s, 0,
                       1.0 * massMoleFrac_up
                       * density_up
                       * velocityNormal
                       * faceVolume);
        r_n.accumulate(lfsu_c_n, 0,
                       -1.0 * massMoleFrac_up
                       * density_up
                       * velocityNormal
                       * faceVolume);

        /**
         * (2) \b Diffusion term of \b component balance equation
         *
         * In mole formulation
         * \f[
         *    - D^\kappa_\alpha \frac{\varrho_\alpha}{M_\alpha} \nabla x^\kappa_\alpha
         *    \Rightarrow - \int_\gamma \left( D^\kappa_\alpha \frac{\varrho_\alpha}{M_\alpha} \nabla x^\kappa_\alpha \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = - |\gamma| D^\kappa_{\alpha\textrm{,avg}} \frac{\varrho_{\alpha\textrm{,avg}}}{M_{\alpha\textrm{,avg}}} \nabla x^\kappa_\alpha \cdot n
         * \f]
         *
         * In mass formulation
         * \f[
         *    - D^\kappa_\alpha \varrho_\alpha \frac{M^\kappa}{M_\alpha} \nabla x^\kappa_\alpha
         *    \Rightarrow - \int_\gamma \left( D^\kappa_\alpha \varrho_\alpha \frac{M^\kappa}{M_\alpha} \nabla x^\kappa_\alpha \right) \cdot n
         * \f]
         * \f[
         *    \alpha_\textrm{self}
         *    = - |\gamma| D^\kappa_{\alpha\textrm{,avg}} \varrho_{\alpha\textrm{,avg}} \frac{M^\kappa_\textrm{avg}}{M_{\alpha\textrm{,avg}}}
         *                 \nabla x^\kappa_\alpha \cdot n
         * \f]
         *
         */
        Scalar moleFraction_n = asImp_().convertToMoleFrac(massMoleFrac_n);
        Scalar moleFraction_s = asImp_().convertToMoleFrac(massMoleFrac_s);

        Scalar effectiveGradient = molarDensity_avg
                               * (moleFraction_n - moleFraction_s)
                               / (outsideCellCenterGlobal[normDim] - insideCellCenterGlobal[normDim]);

        Scalar effectiveGradientTotalMassMole = 0.0;
        if (!useMoles_)
        {
          effectiveGradientTotalMassMole = effectiveGradient * molarMassComponent
                                           - effectiveGradient * asImp_().molarMassComponent(phaseIdx);
          effectiveGradient *= molarMassComponent;
        }

        r_s.accumulate(lfsu_c_s, 0,
                       -1.0 * (diffusionCoefficient_avg + eddyDiffusivity_avg)
                       * effectiveGradient
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);
        r_n.accumulate(lfsu_c_n, 0,
                       1.0 * (diffusionCoefficient_avg + eddyDiffusivity_avg)
                       * effectiveGradient
                       * faceUnitOuterNormal[normDim]
                       * faceVolume);

        if (enableDiffusiveFluxesInTotalMassBalance_)
        {
          r_s.accumulate(lfsu_p_s, 0,
                         -1.0 * (diffusionCoefficient_avg + eddyDiffusivity_avg)
                         * effectiveGradientTotalMassMole
                         * faceUnitOuterNormal[normDim]
                         * faceVolume);
          r_n.accumulate(lfsu_p_n, 0,
                         1.0 * (diffusionCoefficient_avg + eddyDiffusivity_avg)
                         * effectiveGradientTotalMassMole
                         * faceUnitOuterNormal[normDim]
                         * faceVolume);
        }
      }

      /**
       * \copydoc BaseNavierStokesStaggeredGrid::alpha_boundary_navierstokes
       */
      template<typename IG, typename LFSU, typename X, typename LFSV, typename R>
      void alpha_boundary_component(const IG& ig, const LFSU& lfsu_s, const X& x_s, const LFSV& lfsv_s, R& r_s,
                                    std::vector<DimVector> velocityFaces, Scalar pressure_s,
                                    Scalar massMoleFrac_s, Scalar temperature_s,
                                    Scalar pressure_boundary,
                                    Scalar massMoleFrac_boundary, Scalar temperature_boundary) const
      {
        // select the components from the subspaces
        typedef typename LFSU::template Child<massMoleFracIdx>::Type LFSU_C;
        typedef typename LFSU::template Child<pressureIdx>::Type LFSU_P;
        const LFSU_C& lfsu_c_s = lfsu_s.template child<massMoleFracIdx>();
        const LFSU_P& lfsu_p_s = lfsu_s.template child<pressureIdx>();
        typedef typename BC::template Child<massMoleFracIdx>::Type BCMassMoleFrac;
        const BCMassMoleFrac& bcMassMoleFrac = bc.template child<massMoleFracIdx>();

        // center in face's reference element
        const Dune::FieldVector<Scalar, IG::dimension-1>& faceCenterLocal =
          Dune::ReferenceElements<Scalar, IG::dimension-1>::general(ig.geometry().type()).position(0, 0);
        const Dune::FieldVector<Scalar, dim>& faceCenterGlobal =
          ig.geometry().global(faceCenterLocal);

        // face volume for integration
        Scalar faceVolume = ig.geometry().integrationElement(faceCenterLocal)
                            * Dune::ReferenceElements<Scalar,IG::dimension-1>::general(ig.geometry().type()).volume();

        // calculate velocity DoF positions
        const unsigned int numControlVolumeFaces =
          Dune::ReferenceElements<Scalar, dim>::general(ig.inside().geometry().type()).size(1);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersLocal(numControlVolumeFaces);
        std::vector<Dune::FieldVector<Scalar, dim> > faceCentersGlobal(numControlVolumeFaces);
        for (unsigned int curFace = 0; curFace < numControlVolumeFaces; ++curFace)
        {
          std::fill(faceCentersLocal[curFace].begin(), faceCentersLocal[curFace].end(), 0.5);
          faceCentersLocal[curFace][curFace / 2] = curFace % 2;
          faceCentersGlobal[curFace] =
            ig.inside().geometry().global(faceCentersLocal[curFace]);
        }

        // cell center in reference element
        const Dune::FieldVector<Scalar,IG::dimension>&
          insideCellCenterLocal = Dune::ReferenceElements<Scalar,IG::dimension>::general(ig.inside().type()).position(0, 0);
        Dune::FieldVector<Scalar, dim> insideCellCenterGlobal =
          ig.inside().geometry().global(insideCellCenterLocal);

        const Dune::FieldVector<Scalar,dim>& faceUnitOuterNormal = ig.centerUnitOuterNormal();

        // evaluate orientation of intersection
        unsigned int normDim = 0;
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
        {
          if (std::abs(faceUnitOuterNormal[curDim]) > 1e-10 && dim > 1)
          {
            normDim = curDim;
          }
        }

        // evaluation of cell values and constants
        const Scalar density_s = asImp_().density(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar density_boundary = asImp_().density(pressure_boundary, temperature_boundary, massMoleFrac_boundary);
        const Scalar molarDensity_s = asImp_().molarDensity(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar diffusionCoefficient_s = asImp_().diffusionCoefficient(pressure_s, temperature_s, massMoleFrac_s);
        const Scalar eddyDiffusivity_s = asImp_().eddyDiffusivity(ig.inside(), lfsu_s, x_s);
        const Scalar molarMassComponent = asImp_().molarMassComponent(transportCompIdx);

        // Inflow boundary for component
        if (bcMassMoleFrac.isInflow(ig, faceCenterLocal))
        {
          /**
           * Inflow boundary handling for component balance<br>
           * (1) \b Flux term of \b component balance equation
           *
           * In mole formulation:
           * \f[
           *    \varrho_\alpha x^\kappa_\alpha v
           *    \Rightarrow \int_\gamma \left( \varrho_\alpha x^\kappa_\alpha v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \varrho_{\alpha,\textrm{s}} x^\kappa_{\alpha,\textrm{boundary}} \left( v \cdot n \right)
           * \f]
           *
           * In mass formulation:
           * \f[
           *    \varrho_\alpha X^\kappa_\alpha v
           *    \Rightarrow \int_\gamma \left( \varrho_\alpha X^\kappa_\alpha v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \varrho_{\alpha,\textrm{s}} X^\kappa_{\alpha,\textrm{boundary}} \left( v \cdot n \right)
           * \f]
           *
           * \note upwinding is assumed by default, because it is inflow
           */
          r_s.accumulate(lfsu_c_s, 0,
                         1.0 * massMoleFrac_boundary
                         * density_boundary
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);

          /**
          * (2) \b Diffusion term of \b component balance equation
          *
          * In mole formulation:
          * \f[
          *    - D^\kappa_\alpha \frac{\varrho_{\textrm{mol},\alpha}}{M_\alpha} \nabla x^\kappa_\alpha
          *    \Rightarrow - \int_\gamma \left( D^\kappa_\alpha \frac{\varrho_\alpha}{M_\alpha} \nabla x^\kappa_\alpha \right) \cdot n
          * \f]
          * \f[
          *    \alpha_\textrm{self}
          *    = - |\gamma| D^\kappa_{\alpha\textrm{,self}} \varrho_{\alpha\textrm{,self}} \frac{M^\kappa_\textrm{self}}{M_{\alpha\textrm{,self}}}
          *                 \nabla x^\kappa_\alpha \cdot n
          * \f]
          *
          * In mass formulation:
          * \f[
          *    - D^\kappa_\alpha \varrho_\alpha \frac{M^\kappa}{M_\alpha} \nabla x^\kappa_\alpha
          *    \Rightarrow - \int_\gamma \left( D^\kappa_\alpha \varrho_\alpha \frac{M^\kappa}{M_\alpha} \nabla x^\kappa_\alpha \right) \cdot n
          * \f]
          * \f[
          *    \alpha_\textrm{self}
          *    = - |\gamma| D^\kappa_{\alpha\textrm{,avg}} \varrho_{\alpha\textrm{,avg}} \frac{M^\kappa_\textrm{avg}}{M_{\alpha\textrm{,avg}}}
          *                 \nabla x^\kappa_\alpha \cdot n
          * \f]
          *
          * At the boundary the values from the inner cell are taken and <b>no averaging</b>
          * is performed.
          */

          Scalar moleFraction_boundary = asImp_().convertToMoleFrac(massMoleFrac_boundary);
          Scalar moleFraction_s = asImp_().convertToMoleFrac(massMoleFrac_s);

          Scalar effectiveGradient = molarDensity_s
                                 * (moleFraction_boundary - moleFraction_s)
                                 / (faceCenterGlobal[normDim] - insideCellCenterGlobal[normDim]);

          Scalar effectiveGradientTotalMassMole = 0.0;
          if (!useMoles_)
          {
            effectiveGradientTotalMassMole = effectiveGradient * molarMassComponent
                                             - effectiveGradient * asImp_().molarMassComponent(phaseIdx);
            effectiveGradient *= molarMassComponent;
          }

          r_s.accumulate(lfsu_c_s, 0,
                         -1.0 * (diffusionCoefficient_s + eddyDiffusivity_s)
                         * effectiveGradient
                         * faceUnitOuterNormal[normDim]
                         * faceVolume);

          if (enableDiffusiveFluxesInTotalMassBalance_)
          {
            r_s.accumulate(lfsu_p_s, 0,
                           -1.0 * (diffusionCoefficient_s + eddyDiffusivity_s)
                           * effectiveGradientTotalMassMole
                           * faceUnitOuterNormal[normDim]
                           * faceVolume);
          }
        }
        // Wall boundary for component
        else if (bcMassMoleFrac.isWall(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Wall Condition for component.
          //! Wall is Neumann no-flow.
        }
        // Outflow boundary for component
        else if (bcMassMoleFrac.isOutflow(ig, faceCenterLocal))
        {
          /**
           * Outflow boundary handling for component balance<br>
           * (1) \b Flux term of \b component balance equation
           *
           * In mole formulation:
           * \f[
           *    \varrho_{\textrm{mol},\alpha} x^\kappa_\alpha v
           *    \Rightarrow \int_\gamma \left( \varrho_{\textrm{mol},\alpha} x^\kappa_\alpha v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \varrho_{mol,\alpha,\textrm{self}} x^\kappa_{\alpha,\textrm{self}} \left( v \cdot n \right)
           * \f]
           *
           * In mass formulation:
           * \f[
           *    \varrho_\alpha X^\kappa_\alpha v
           *    \Rightarrow \int_\gamma \left( \varrho_\alpha X^\kappa_\alpha v \right) \cdot n
           * \f]
           * \f[
           *    \alpha_\textrm{self}
           *    = |\gamma| \varrho_{\alpha,\textrm{self}} X^\kappa_{\alpha,\textrm{self}} \left( v \cdot n \right)
           * \f]
           *
           * \note upwinding is assumed by default, because it is inflow
           */

          r_s.accumulate(lfsu_c_s, 0,
                         1.0 * massMoleFrac_s
                         * density_s
                         * (velocityFaces[ig.indexInInside()] * faceUnitOuterNormal)
                         * faceVolume);
        }
        // Symmetry boundary for component
        else if (bcMassMoleFrac.isSymmetry(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Symmetry Condition for velocity.
          //! Ensure normal velocity = 0 is done by the parent function.
        }
        // Coupling boundary for component
        else if (bcMassMoleFrac.isCoupling(ig, faceCenterLocal))
        {
          //! Nothing has to be done in case of Coupling Condition for component.
        }
        else
        {
          DUNE_THROW(Dune::NotImplemented, "Wrong boundary condition type for component.");
        }
      }

      /**
       * \brief Initialize vector-stored values used for alpha_* routines.
       *
       * \tparam GridView GridView type
       */
      void initialize()
      {
        if (useMoles_) // check if the component balance is formulated with mole fractions
        {
          std::cout << "USE_MOLES = true" << std::endl;
        }

        storedDiffusionCoefficient.resize(mapperElement.size());
        for (unsigned int i = 0; i < mapperElement.size(); ++i)
        {
          storedDiffusionCoefficient[i] = 0.0;
        }

        typedef typename BC::template Child<massMoleFracIdx>::Type BCMassMoleFrac;
        const BCMassMoleFrac& bcMassMoleFrac = bc.template child<massMoleFracIdx>();

        // constants and types
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;
        typedef typename GridView::IntersectionIterator IntersectionIterator;

        // loop over grid view to get elements with a wall intersection
        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          for (IntersectionIterator ig = gridView.ibegin(*eit);
              ig != gridView.iend(*eit); ++ig)
          {
            if (!ig->boundary())
            {
              continue;
            }

            // local and global position of face centers
            const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
              Dune::ReferenceElements<Scalar, dim-1>::general(ig->geometry().type()).position(0, 0);
            Dune::FieldVector<Scalar, dim> faceCenterGlobal = ig->geometry().global(faceCenterLocal);

            // check for multiple defined boundary conditions
            unsigned int numberOfBCTypesAtPos = 0;
            if (bcMassMoleFrac.isWall(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcMassMoleFrac.isInflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcMassMoleFrac.isOutflow(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcMassMoleFrac.isSymmetry(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (bcMassMoleFrac.isCoupling(*ig, faceCenterLocal))
              numberOfBCTypesAtPos += 1;
            if (numberOfBCTypesAtPos != 1)
            {
              std::cout << "BCMassMoleFrac at faceCenterGlobal " << faceCenterGlobal << std::endl;
              std::cout << "Wall      " << bcMassMoleFrac.isWall(*ig, faceCenterLocal) << std::endl;
              std::cout << "Inflow    " << bcMassMoleFrac.isInflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Outflow   " << bcMassMoleFrac.isOutflow(*ig, faceCenterLocal) << std::endl;
              std::cout << "Symmetry  " << bcMassMoleFrac.isSymmetry(*ig, faceCenterLocal) << std::endl;
              std::cout << "Coupling  " << bcMassMoleFrac.isCoupling(*ig, faceCenterLocal) << std::endl;
              DUNE_THROW(Dune::NotImplemented, "Multiple or no boundary conditions for component at one point.");
            }
          }
        }
      }

      /**
       * \copydoc BaseComponentStaggeredGrid::updateStoredValues
       */
#if IS_STAGGERED_MULTIDOMAIN_MODEL
      template<typename SubDomainGridView, typename MDGFS, typename X, int stokesDomainIdx>
      void updateStoredValues(const SubDomainGridView& sdgv, const MDGFS& mdgfs, X& lastSolution)
      {
#else
      template<typename GFS, typename X>
      void updateStoredValues(const GFS& gfs, X& lastSolution)
      {
#endif
        typedef typename GridView::Traits::template Codim<0>::Iterator ElementIterator;

        for (ElementIterator eit = gridView.template begin<0>();
            eit != gridView.template end<0>(); ++eit)
        {
          const typename GridView::IndexSet::IndexType elementInsideID = mapperElement.index(*eit);
          storedDiffusionCoefficient[elementInsideID] = asImp_().diffusionCoefficient(asImp_().storedPressure[elementInsideID],
                                                                                      asImp_().storedTemperature[elementInsideID],
                                                                                      asImp_().storedMassMoleFrac[elementInsideID]);
        }
      }

      /**
       * \brief Returns the massMoleFrac for a given element
       *
       * \param eg element geometry
       * \param lfsu local functions space for ansatz functions
       * \param x coefficient vector
       */
      template<typename EG, typename LFSU, typename X>
      const Scalar massMoleFrac(const EG& eg, const LFSU& lfsu, const X& x) const
      {
        typedef typename LFSU::template Child<massMoleFracIdx>::Type LFSU_C;
        const LFSU_C& lfsu_c = lfsu.template child<massMoleFracIdx>();
        return x(lfsu_c, 0);
      }


private:
      const BC& bc;
      const SourceComponentBalance& sourceComponentBalance;
      const DirichletMassMoleFrac& dirichletMassMoleFrac;
      const NeumannMassMoleFrac& neumannMassMoleFrac;
      GridView gridView;
      MapperElement mapperElement;

      // properties
      bool enableAdvectionAveraging_;
      bool useMoles_;
      bool enableDiffusiveFluxesInTotalMassBalance_;

protected:
      //! Current implementation.
      Implementation &asImp_()
      { return *static_cast<Implementation*>(this); }
      //! Current implementation.
      const Implementation &asImp_() const
      { return *static_cast<const Implementation*>(this); }
    };
  } // namespace PDELab
} // namespace Dune

#endif // DUMUX_BASECOMPONENT_STAGGERED_GRID_HH
