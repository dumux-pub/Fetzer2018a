// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup BoxStokesModel
 *
 * \file
 *
 * \brief Defines default properties for the Navier-Stokes compositional
 *        non-isothermal staggered grid model.
 *
 * These can be overwritten at a different place or
 * may be replaced by values of the input file.
 */

#ifndef DUMUX_NAVIERSSTOKES_TWOCNI_PROPERTY_DEFAULTS_HH
#define DUMUX_NAVIERSSTOKES_TWOCNI_PROPERTY_DEFAULTS_HH

#include <dumux/material/fluidstates/compositional.hh>

#include "../navierstokes/navierstokespropertydefaults.hh"
#include "navierstokes2cniboundaryconditions.hh"
#include "navierstokes2cniindices.hh"
#include "navierstokes2cniproperties.hh"

namespace Dumux
{

template<class TypeTag> class BCMassMoleFrac;
template<class TypeTag> class BCTemperature;
template<class TypeTag, typename GridView, typename Scalar> class DirichletMassMoleFrac;
template<class TypeTag, typename GridView, typename Scalar> class DirichletTemperature;
template<class TypeTag, typename GridView, typename Scalar> class NeumannMassMoleFrac;
template<class TypeTag, typename GridView, typename Scalar> class NeumannTemperature;
template<class TypeTag, typename GridView, typename Scalar> class SourceComponentBalance;
template<class TypeTag, typename GridView, typename Scalar> class SourceEnergyBalance;

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Properties
//////////////////////////////////////////////////////////////////

//! Set the indices used by the model
SET_TYPE_PROP(StaggeredGridNavierStokes2cni, Indices,
              StaggeredGridNavierStokes2cniCommonIndices<TypeTag>);

//! Set property value for the massMoleFrac boundary condition
SET_TYPE_PROP(StaggeredGridNavierStokes2cni, BCMassMoleFrac, BCMassMoleFrac<TypeTag>);

//! Set property value for the temperature boundary condition
SET_TYPE_PROP(StaggeredGridNavierStokes2cni, BCTemperature, BCTemperature<TypeTag>);

//! Set property value for BCType
SET_PROP(StaggeredGridNavierStokes2cni, BCType)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, BCVelocity) BCVelocity;
    typedef typename GET_PROP_TYPE(TypeTag, BCPressure) BCPressure;
    typedef typename GET_PROP_TYPE(TypeTag, BCMassMoleFrac) BCMassMoleFrac;
    typedef typename GET_PROP_TYPE(TypeTag, BCTemperature) BCTemperature;
public:
    typedef Dune::PDELab::CompositeConstraintsParameters<BCVelocity, BCPressure,
                                                         BCMassMoleFrac, BCTemperature> type;
};

//! Set property value for DirichletMassMoleFrac
SET_PROP(StaggeredGridNavierStokes2cni, DirichletMassMoleFrac)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef DirichletMassMoleFrac<TypeTag, GridView, Scalar> type;
};

//! Set property value for DirichletTemperature
SET_PROP(StaggeredGridNavierStokes2cni, DirichletTemperature)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef DirichletTemperature<TypeTag, GridView, Scalar> type;
};

//! Set property value for NeumannMassMoleFrac
SET_PROP(StaggeredGridNavierStokes2cni, NeumannMassMoleFrac)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef NeumannMassMoleFrac<TypeTag, GridView, Scalar> type;
};

//! Set property value for NeumannTemperature
SET_PROP(StaggeredGridNavierStokes2cni, NeumannTemperature)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef NeumannTemperature<TypeTag, GridView, Scalar> type;
};

//! Set property value for SourceComponentBalance
SET_PROP(StaggeredGridNavierStokes2cni, SourceComponentBalance)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef SourceComponentBalance<TypeTag, GridView, Scalar> type;
};

//! Set property value for SourceEnergyBalance
SET_PROP(StaggeredGridNavierStokes2cni, SourceEnergyBalance)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef SourceEnergyBalance<TypeTag, GridView, Scalar> type;
};

//! Set property value for the use of mass formulation for the component balance
SET_BOOL_PROP(StaggeredGridNavierStokes2cni, UseMoles, false);

//! Choose the type of the employed fluid state
SET_PROP(StaggeredGridNavierStokes2cni, FluidState)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
public:
    typedef Dumux::CompositionalFluidState<Scalar, FluidSystem> type;
};

//! Choose the considered phase (single-phase system); the gas phase is used
SET_PROP(StaggeredGridNavierStokes2cni, PhaseIdx)
{
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
public:
    static constexpr int value = FluidSystem::nPhaseIdx;
};

//! set the number of equations
SET_PROP(StaggeredGridNavierStokes2cni, NumEq)
{
    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    static const int dim = Grid::dimension;
public:
    static constexpr int value = FluidSystem::numComponents + dim;
};

//! Property tag to enable or disable enthalpy to be transport with diffusion
SET_BOOL_PROP(StaggeredGridNavierStokes2cni, ProblemEnableDiffusiveEnthalpyTransport, true);

//!< Property tag to check to influence of diffusive fluxes in a total mass balance
SET_BOOL_PROP(StaggeredGridNavierStokes2cni, ProblemEnableDiffusiveFluxesInTotalMassBalance, true);
}
}

#endif // DUMUX_NAVIERSSTOKES_TWOCNI_PROPERTY_DEFAULTS_HH
