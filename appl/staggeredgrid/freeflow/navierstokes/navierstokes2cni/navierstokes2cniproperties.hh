// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup BoxStokesModel
 *
 * \file
 *
 * \brief Defines the properties required for the Navier-Stokes compositional
 *        non-isothermal staggered grid model.
 */

#ifndef DUMUX_NAVIERSTOKES_TWOCNI_PROPERTIES_HH
#define DUMUX_NAVIERSTOKES_TWOCNI_PROPERTIES_HH

#include "../navierstokes/navierstokesproperties.hh"

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////

//! The type tag for the Navier-Stokes problems
NEW_TYPE_TAG(StaggeredGridNavierStokes2cni, INHERITS_FROM(StaggeredGridNavierStokes));

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////

NEW_PROP_TAG(BCMassMoleFrac); //!< Property tag for the used massMoleFrac boundary condition
NEW_PROP_TAG(BCTemperature); //!< Property tag for the used temperature boundary condition
NEW_PROP_TAG(SourceComponentBalance); //!< Property tag for the SourceComponentBalance
NEW_PROP_TAG(SourceEnergyBalance); //!< Property tag for the SourceEnergyBalance
NEW_PROP_TAG(DirichletMassMoleFrac); //!< Property tag for the DirichletMassMoleFrac
NEW_PROP_TAG(DirichletTemperature); //!< Property tag for the DirichletTemperature
NEW_PROP_TAG(NeumannMassMoleFrac); //!< Property tag for the NeumannMassMoleFrac
NEW_PROP_TAG(NeumannTemperature); //!< Property tag for the NeumannTemperature

NEW_PROP_TAG(UseMoles); //!< Property tag to switch from mass to mole fraction formulation
NEW_PROP_TAG(ProblemEnableDiffusiveEnthalpyTransport); //!< Property tag to enable or disable enthalpy to be transport with diffusion
NEW_PROP_TAG(ProblemEnableDiffusiveFluxesInTotalMassBalance); //!< Property tag to check to influence of diffusive fluxes in a total mass balance
}
}

#endif // DUMUX_NAVIERSTOKES_TWOCNI_PROPERTIES_HH
