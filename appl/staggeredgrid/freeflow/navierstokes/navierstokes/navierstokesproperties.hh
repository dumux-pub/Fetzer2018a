// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup BoxStokesModel
 *
 * \file
 *
 * \brief Defines the properties required for the Naviers-Stokes staggered grid model.
 */

#ifndef DUMUX_NAVIERSTOKES_PROPERTIES_HH
#define DUMUX_NAVIERSTOKES_PROPERTIES_HH

#include <dumux/implicit/propertydefaults.hh>

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////

//! The type tag for the Navier-Stokes problems
NEW_TYPE_TAG(StaggeredGridNavierStokes, INHERITS_FROM(ImplicitBase));

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////

NEW_PROP_TAG(Problem); //!< Property tag for the Problem
NEW_PROP_TAG(LocalOperator); //!< Property tag for the used local operator
NEW_PROP_TAG(TransientLocalOperator); //!< Property tag for the used transient local operator

NEW_PROP_TAG(GridView); //!< Property tag for GridView
NEW_PROP_TAG(Indices); //!< Enumerations for the model
NEW_PROP_TAG(DimVector); //!< Property tag for DimVector
NEW_PROP_TAG(MapperElement); //!< Mapper to get indices for elements

NEW_PROP_TAG(BCType); //!< Property tag for the used BoundaryConditionType
NEW_PROP_TAG(BCVelocity); //!< Property tag for the used velocity boundary condition
NEW_PROP_TAG(BCPressure); //!< Property tag for the used pressure boundary condition
NEW_PROP_TAG(VelocityConstraints); //!< Property tag for the used velocity constraints
NEW_PROP_TAG(PressureConstraints); //!< Property tag for the used pressure constraints
NEW_PROP_TAG(SourceMomentumBalance); //!< Property tag for the SourceMomentumBalance
NEW_PROP_TAG(SourceMassBalance); //!< Property tag for the SourceMassBalance
NEW_PROP_TAG(DirichletVelocity); //!< Property tag for the DirichletVelocity
NEW_PROP_TAG(DirichletPressure); //!< Property tag for the DirichletPressure
NEW_PROP_TAG(NeumannVelocity); //!< Property tag for the NeumannVelocity
NEW_PROP_TAG(NeumannPressure); //!< Property tag for the NeumannPressure
NEW_PROP_TAG(FixVelocityConstraints); //!< Property tag to constraint the velocity dof
NEW_PROP_TAG(FixPressureConstraints); //!< Property tag to constraint the pressure dof

NEW_PROP_TAG(FluidSystem); //!< The type of the fluid system to use
NEW_PROP_TAG(FluidState);
NEW_PROP_TAG(Fluid); //!< The fluid used for the default fluid system
NEW_PROP_TAG(PhaseIdx); //!< A phase index in case that a two-phase FluidSystem is used

NEW_PROP_TAG(ProblemEnableNavierStokes); //!< Use Stokes or NavierStokes
NEW_PROP_TAG(ProblemEnableUnsymmetrizedVelocityGradient); //!< Returns whether unsymmetrized velocity gradient for viscous term is used
NEW_PROP_TAG(ProblemEnableGravity); //!< Enable gravity for the simulation

NEW_PROP_TAG(ProblemEnableAdvectionAveraging); //!< Use Upwinding or Averaging for advection terms
NEW_PROP_TAG(ProblemDiffusionAveragingMethod); //!< Decide which averaging method to use for diffusion terms

NEW_PROP_TAG(NewtonMaxTimeStepDivisions); //!< Maximum number timestep divisions for Newton solver
NEW_PROP_TAG(NewtonMaxAbsoluteResidual); //!< Absolute limit for Newton convergence

NEW_PROP_TAG(OutputVtkFrequency); //!< Frequency of vtk output
NEW_PROP_TAG(OutputErrorConvergence); //!< Print the error convergence at the end of an analytic problem
NEW_PROP_TAG(NumEq);
}
}

#endif // DUMUX_NAVIERSTOKES_PROPERTIES_HH
