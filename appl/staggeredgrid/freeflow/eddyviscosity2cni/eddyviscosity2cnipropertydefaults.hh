// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup BoxStokesModel
 *
 * \file
 *
 * \brief Defines default properties for the compositional non-isothermal
 *        eddy viscosity staggered grid model.
 *
 * These can be overwritten at a different place or
 * may be replaced by values of the input file.
 */

#ifndef DUMUX_EDDYVISCOSITY_TWOCNI_PROPERTY_DEFAULTS_HH
#define DUMUX_EDDYVISCOSITY_TWOCNI_PROPERTY_DEFAULTS_HH

#include<appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cnipropertydefaults.hh>

#include"eddyviscosity2cniproperties.hh"

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Properties
//////////////////////////////////////////////////////////////////

//! Use reynolds analogy as default
SET_INT_PROP(StaggeredGridEddyViscosity2cni, ZeroEqEddyDiffusivityModel, 1);

//! Set the turbulent Schmidt number \f$[-]\f$ (Reynolds analogy - component transport)
SET_SCALAR_PROP(StaggeredGridEddyViscosity2cni, FreeFlowTurbulentSchmidtNumber, 1.0);

//! Use reynolds analogy as default
SET_INT_PROP(StaggeredGridEddyViscosity2cni, ZeroEqEddyConductivityModel, 1);

//! Set the turbulent Prandtl number \f$[-]\f$ (Reynolds analogy - energy transport)
SET_SCALAR_PROP(StaggeredGridEddyViscosity2cni, FreeFlowTurbulentPrandtlNumber, 1.0);
}
}

#endif // DUMUX_EDDYVISCOSITY_TWOCNI_PROPERTY_DEFAULTS_HH
