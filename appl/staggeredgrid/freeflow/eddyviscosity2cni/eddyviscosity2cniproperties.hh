// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup BoxStokesModel
 *
 * \file
 *
 * \brief Defines the properties required for the compositional non-isothermal
 *        eddy viscosity staggered grid model.
 */

#ifndef DUMUX_EDDYVISCOSITY_TWOCNI_PROPERTIES_HH
#define DUMUX_EDDYVISCOSITY_TWOCNI_PROPERTIES_HH

#include"eddyviscosityproperties.hh"
#include<appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cniproperties.hh>


namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////

//! The type tag for the compositional non-isothermal eddy viscosity problems
NEW_TYPE_TAG(StaggeredGridEddyViscosity2cni, INHERITS_FROM(StaggeredGridEddyViscosity, StaggeredGridNavierStokes2cni));

//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////
NEW_PROP_TAG(ZeroEqEddyDiffusivityModel); //!< Type of the used zeroeq eddy diffusivity model
NEW_PROP_TAG(FreeFlowTurbulentSchmidtNumber); //!< Returns the used turbulent Schmidt number (Reynolds analogy)
NEW_PROP_TAG(ZeroEqEddyConductivityModel); //!< Type of the used zeroeq eddy conductivity model
NEW_PROP_TAG(FreeFlowTurbulentPrandtlNumber); //!< Returns the used turbulent Prandtl number (Reynolds analogy)
}
}

#endif // DUMUX_EDDYVISCOSITY_TWOCNI_PROPERTIES_HH
