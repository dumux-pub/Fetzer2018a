// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
#ifndef DUMUX_STAGGERED_MULTIDOMAIN_PROPERTIES_HH
#define DUMUX_STAGGERED_MULTIDOMAIN_PROPERTIES_HH

#include <dumux/multidomain/properties.hh>

namespace Dumux
{
namespace Properties
{
NEW_TYPE_TAG(MultiDomainStaggered, INHERITS_FROM(MultiDomain));

// sub-domains type tags
NEW_PROP_TAG(StokesSubProblemTypeTag);
NEW_PROP_TAG(DarcySubProblemTypeTag);

// Grid tags
NEW_PROP_TAG(MultiDomainGridView);
NEW_PROP_TAG(SubDomainGrid);
NEW_PROP_TAG(SubDomainGridView);
NEW_PROP_TAG(DimVector);
NEW_PROP_TAG(CouplingMethod);
NEW_PROP_TAG(CouplingInitTime);
NEW_PROP_TAG(CouplingInitMethod);
NEW_PROP_TAG(UseMoles);

NEW_PROP_TAG(NewtonTimeStepIncrementFactor); //!< Factor to control the timestep increment
NEW_PROP_TAG(NewtonMinAbsoluteResidualRequirement);
} // end namespace Properties
} // end namespace Dumux

#endif // DUMUX_STAGGERED_MULTIDOMAIN_PROPERTIES_HH
