// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
#ifndef DUMUX_STAGGERED_MULTIDOMAIN_PROPERTYDEFAULTS_HH
#define DUMUX_STAGGERED_MULTIDOMAIN_PROPERTYDEFAULTS_HH

#include <limits>

#include <dune/grid/yaspgrid.hh>
#include <dune/pdelab/multidomain/gridoperator.hh>

#include "multidomainindices.hh"
#include "multidomaincclocalresidual.hh"
#include "multidomainccfvelementgeometry.hh"
#include "multidomainproperties.hh"

namespace Dumux
{

// forward declarations
template <class TypeTag> class MultidomainCCLocalResidual;

namespace Properties
{
// grid type
SET_PROP(MultiDomainStaggered, Grid)
{
private:
#ifndef DUMUX_MULTIDOMAIN_DIM
#define DUMUX_MULTIDOMAIN_DIM 2
#endif
    static const int dim = DUMUX_MULTIDOMAIN_DIM;
public:
    using type = Dune::YaspGrid<dim, Dune::TensorProductCoordinates<typename GET_PROP_TYPE(TypeTag, Scalar), dim> >;
};

SET_PROP(MultiDomainStaggered, MultiDomainGrid)
{
private:
    using BaseGrid = typename GET_PROP_TYPE(TypeTag, Grid);
public:
    typedef Dune::MultiDomainGrid
        <BaseGrid, Dune::mdgrid::FewSubDomainsTraits<BaseGrid::dimension, 2> > type;
};

SET_TYPE_PROP(MultiDomainStaggered, GridView,
              typename GET_PROP_TYPE(TypeTag, Grid)::LeafGridView);
SET_TYPE_PROP(MultiDomainStaggered, MultiDomainGridView,
              typename GET_PROP_TYPE(TypeTag, MultiDomainGrid)::LeafGridView);
SET_TYPE_PROP(MultiDomainStaggered, SubDomainGrid,
              typename GET_PROP_TYPE(TypeTag, MultiDomainGrid)::SubDomainGrid);
SET_TYPE_PROP(MultiDomainStaggered, SubDomainGridView,
              typename GET_PROP_TYPE(TypeTag, SubDomainGrid)::LeafGridView);

// modified base local resiudal
SET_TYPE_PROP(MultiDomainStaggered, BaseLocalResidual, MultidomainCCLocalResidual<TypeTag>);

// set the multidomain time manager
SET_TYPE_PROP(MultiDomainStaggered, TimeManager, TimeManager<TypeTag>);

// set the maximum time step divisions
SET_INT_PROP(MultiDomainStaggered, NewtonMaxTimeStepDivisions, 15);
SET_INT_PROP(MultiDomainStaggered, NumEq, 1);

//! Factor to control the timestep increment (should be smaller for eddy viscosity problems)
SET_SCALAR_PROP(MultiDomainStaggered, NewtonTimeStepIncrementFactor, 5.0/6.0);

//! the value for the absolute residual to accept any kind of solution
SET_SCALAR_PROP(MultiDomainStaggered, NewtonMinAbsoluteResidualRequirement, 1e5);

//! the value for the maximum absolute residual below which convergence is declared
SET_SCALAR_PROP(MultiDomainStaggered, NewtonMaxAbsoluteResidual, 1e-15);

// Set property value for the DimVector
SET_PROP(MultiDomainStaggered, DimVector)
{ private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
public:
    typedef Dune::FieldVector<Scalar, GridView::dimension> type;
};

// set which formulation is used to couple the subdomains
SET_INT_PROP(MultiDomainStaggered, CouplingMethod, 1);
SET_SCALAR_PROP(MultiDomainStaggered, CouplingInitTime, -1e9);
SET_INT_PROP(MultiDomainStaggered, CouplingInitMethod, 1);

// Set the indices used by the model
SET_TYPE_PROP(MultiDomainStaggered, Indices, MultiDomainCommonIndices<TypeTag>);
} // end namespace Properties
} // end namespace Dumux

#endif // DUMUX_STAGGERED_MULTIDOMAIN_PROPERTYDEFAULTS_HH
