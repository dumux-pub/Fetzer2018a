// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.    *
 *****************************************************************************/
#ifndef DUMUX_MULTIDOMAIN_NAVIERSTOKES_DARCYTWOP_PROPERTYDEFAULTS_HH
#define DUMUX_MULTIDOMAIN_NAVIERSTOKES_DARCYTWOP_PROPERTYDEFAULTS_HH

#include <dune/pdelab/gridoperator/onestep.hh>

#include <dune/pdelab/multidomain/coupling.hh>
#include <dune/pdelab/multidomain/gridoperator.hh>
#include <dune/pdelab/multidomain/multidomaingridfunctionspace.hh>
#include <dune/pdelab/multidomain/subproblem.hh>

#include <dumux/porousmediumflow/2p/implicit/properties.hh>
#include <dumux/porousmediumflow/2p/implicit/propertydefaults.hh>
#include <dumux/porousmediumflow/2p/implicit/model.hh>
#include <dumux/porousmediumflow/implicit/problem.hh>

#include <appl/staggeredgrid/common/fixpressureconstraints.hh>
#include <appl/staggeredgrid/common/fixvelocityconstraints.hh>
#if COUPLING_KEPSILON
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon/fixturbulentkineticenergyconstraints.hh>
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon/fixdissipationconstraints.hh>
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon/kepsilonproperties.hh>
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon/kepsilonpropertydefaults.hh>
#elif COUPLING_LOWREKEPSILON
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon/lowrekepsilonproperties.hh>
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon/lowrekepsilonpropertydefaults.hh>
#elif COUPLING_ONEEQ
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras/spalartallmarasproperties.hh>
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras/spalartallmaraspropertydefaults.hh>
#elif COUPLING_ZEROEQ
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq/zeroeqproperties.hh>
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq/zeroeqpropertydefaults.hh>
#else
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes/navierstokesproperties.hh>
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes/navierstokespropertydefaults.hh>
#endif

#include <appl/staggeredgrid/multidomain/common/multidomainproperties.hh>
#include <appl/staggeredgrid/multidomain/common/multidomainpropertydefaults.hh>
#include <appl/staggeredgrid/multidomain/common/multidomainlocaloperator.hh>
#include <appl/staggeredgrid/multidomain/common/multidomainnewtonmethod.hh>

#include "boundaryconditions.hh"
#include "couplingoperator.hh"
#include "indices.hh"
#include "properties.hh"

namespace Dune {
namespace PDELab {
// forward declarations
#if COUPLING_KEPSILON
template <class TypeTag> class KEpsilonStaggeredGrid;
template <class TypeTag> class KEpsilonTransientStaggeredGrid;
#elif COUPLING_LOWREKEPSILON
template <class TypeTag> class LowReKEpsilonStaggeredGrid;
template <class TypeTag> class LowReKEpsilonTransientStaggeredGrid;
#elif COUPLING_ONEEQ
template <class TypeTag> class SpalartAllmarasStaggeredGrid;
template <class TypeTag> class SpalartAllmarasTransientStaggeredGrid;
#elif COUPLING_ZEROEQ
template <class TypeTag> class ZeroEqStaggeredGrid;
template <class TypeTag> class NavierStokesTransientStaggeredGrid; // no own transient lop
#else
template <class TypeTag> class NavierStokesStaggeredGrid;
template <class TypeTag> class NavierStokesTransientStaggeredGrid;
#endif
}
}

namespace Dumux
{
namespace Properties
{

// sub-domains type tags
SET_TYPE_PROP(MDSNavierStokesDarcyTwoP, StokesSubProblemTypeTag, TTAG(StokesSubProblem));
SET_TYPE_PROP(MDSNavierStokesDarcyTwoP, DarcySubProblemTypeTag, TTAG(DarcySubProblem));

// multi-domain type tag
SET_TYPE_PROP(StokesSubProblem, MultiDomainTypeTag, TTAG(MDSNavierStokesDarcyTwoP));
SET_TYPE_PROP(DarcySubProblem, MultiDomainTypeTag, TTAG(MDSNavierStokesDarcyTwoP));

// modified class for FVElementGeometry
SET_TYPE_PROP(MDSNavierStokesDarcyTwoP, FVElementGeometry,
              MultidomainCCFVElementGeometry<TTAG(DarcySubProblem)>);
SET_TYPE_PROP(DarcySubProblem, FVElementGeometry,
              MultidomainCCFVElementGeometry<TTAG(DarcySubProblem)>);

// single grid function spaces
SET_TYPE_PROP(StokesSubProblem, PressureConstraints,
              Dumux::FixPressureConstraints<TTAG(StokesSubProblem)>);
SET_TYPE_PROP(StokesSubProblem, VelocityConstraints,
              Dumux::FixVelocityConstraints<TTAG(StokesSubProblem)>);
SET_PROP(StokesSubProblem, SubDomainScalarGridFunctionSpaceP0)
{
private:
    using SDGV = GET_PROP_TYPE(TTAG(MDSNavierStokesDarcyTwoP), SubDomainGridView);
    using P0FEM = GET_PROP_TYPE(TTAG(StokesSubProblem), FiniteElementMapP0);
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
public:
    typedef Dune::PDELab::GridFunctionSpace<SDGV, P0FEM, Dune::PDELab::NoConstraints, VectorBackend> type;
};
SET_PROP(StokesSubProblem, SubDomainScalarGridFunctionSpaceP0Constrained)
{
private:
    using SDGV = GET_PROP_TYPE(TTAG(MDSNavierStokesDarcyTwoP), SubDomainGridView);
    using P0FEM = GET_PROP_TYPE(TTAG(StokesSubProblem), FiniteElementMapP0);
    using PressureConstraints = GET_PROP_TYPE(TTAG(StokesSubProblem), PressureConstraints);
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
public:
    typedef Dune::PDELab::GridFunctionSpace<SDGV, P0FEM, PressureConstraints, VectorBackend> type;
};
#if COUPLING_KEPSILON
SET_TYPE_PROP(StokesSubProblem, TurbulentKineticEnergyConstraints,
              Dumux::FixTurbulentKineticEnergyConstraints<TTAG(StokesSubProblem)>);
SET_TYPE_PROP(StokesSubProblem, DissipationConstraints,
              Dumux::FixDissipationConstraints<TTAG(StokesSubProblem)>);
SET_PROP(StokesSubProblem, SubDomainScalarGridFunctionSpaceP0TurbulentKineticEnergyConstrained)
{
private:
    using SDGV = GET_PROP_TYPE(TTAG(MDSNavierStokesDarcyTwoP), SubDomainGridView);
    using P0FEM = GET_PROP_TYPE(TTAG(StokesSubProblem), FiniteElementMapP0);
    using TurbulentKineticEnergyConstraints = GET_PROP_TYPE(TTAG(StokesSubProblem), TurbulentKineticEnergyConstraints);
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
public:
    typedef Dune::PDELab::GridFunctionSpace<SDGV, P0FEM, TurbulentKineticEnergyConstraints, VectorBackend> type;
};
SET_PROP(StokesSubProblem, SubDomainScalarGridFunctionSpaceP0DissipationConstrained)
{
private:
    using SDGV = GET_PROP_TYPE(TTAG(MDSNavierStokesDarcyTwoP), SubDomainGridView);
    using P0FEM = GET_PROP_TYPE(TTAG(StokesSubProblem), FiniteElementMapP0);
    using DissipationConstraints = GET_PROP_TYPE(TTAG(StokesSubProblem), DissipationConstraints);
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
public:
    typedef Dune::PDELab::GridFunctionSpace<SDGV, P0FEM, DissipationConstraints, VectorBackend> type;
};
#endif
SET_PROP(StokesSubProblem, SubDomainScalarGridFunctionSpaceStaggered)
{
private:
    using SDGV = GET_PROP_TYPE(TTAG(MDSNavierStokesDarcyTwoP), SubDomainGridView);
    using StaggeredQ0FEM = GET_PROP_TYPE(TTAG(StokesSubProblem), FiniteElementMapStaggered);
    using VelocityConstraints = GET_PROP_TYPE(TTAG(StokesSubProblem), VelocityConstraints);
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
public:
    typedef Dune::PDELab::GridFunctionSpace<SDGV, StaggeredQ0FEM, VelocityConstraints, VectorBackend> type;
};
SET_PROP(DarcySubProblem, SubDomainScalarGridFunctionSpaceP0)
{
private:
    using SDGV = GET_PROP_TYPE(TTAG(MDSNavierStokesDarcyTwoP), SubDomainGridView);
    using P0FEM = GET_PROP_TYPE(TTAG(DarcySubProblem), FiniteElementMapP0);
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
public:
    typedef Dune::PDELab::GridFunctionSpace<SDGV, P0FEM, Dune::PDELab::NoConstraints, VectorBackend> type;
};

// composed grid function spaces
SET_PROP(StokesSubProblem, SubDomainComposedGridFunctionSpace)
{
private:
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
    using StaggeredGFS = GET_PROP_TYPE(TTAG(StokesSubProblem), SubDomainScalarGridFunctionSpaceStaggered);
    using P0GFSConstrained = GET_PROP_TYPE(TTAG(StokesSubProblem), SubDomainScalarGridFunctionSpaceP0Constrained);
    using P0GFS = GET_PROP_TYPE(TTAG(StokesSubProblem), SubDomainScalarGridFunctionSpaceP0);
#if COUPLING_KEPSILON
    using TurbulentKineticEnergyConstrainted = GET_PROP_TYPE(TTAG(StokesSubProblem), SubDomainScalarGridFunctionSpaceP0TurbulentKineticEnergyConstrained);
    using DissipationConstrainted = GET_PROP_TYPE(TTAG(StokesSubProblem), SubDomainScalarGridFunctionSpaceP0DissipationConstrained);
#endif
public:
    typedef Dune::PDELab::CompositeGridFunctionSpace<
        VectorBackend, Dune::PDELab::LexicographicOrderingTag,
        StaggeredGFS, P0GFSConstrained
#if COUPLING_LOWREKEPSILON
        , P0GFS, P0GFS
#elif COUPLING_KEPSILON
        , TurbulentKineticEnergyConstrainted, DissipationConstrainted
#elif COUPLING_ONEEQ
        , P0GFS
#endif
        > type;
};
SET_PROP(DarcySubProblem, SubDomainComposedGridFunctionSpace)
{
private:
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
    using P0GFS = GET_PROP_TYPE(TTAG(StokesSubProblem), SubDomainScalarGridFunctionSpaceP0);
public:
    typedef Dune::PDELab::CompositeGridFunctionSpace<
        VectorBackend, Dune::PDELab::LexicographicOrderingTag,
        P0GFS, P0GFS> type;
};

// multidomain grid function space
SET_PROP(MDSNavierStokesDarcyTwoP, MultiDomainGridFunctionSpace)
{
private:
    using MultiDomainGrid = GET_PROP_TYPE(TTAG(MDSNavierStokesDarcyTwoP), MultiDomainGrid);
    using VectorBackend = Dune::PDELab::ISTLVectorBackend<>;
    using StokesGFS = GET_PROP_TYPE(TTAG(StokesSubProblem), SubDomainComposedGridFunctionSpace);
    using DarcyGFS = GET_PROP_TYPE(TTAG(DarcySubProblem), SubDomainComposedGridFunctionSpace);
public:
    typedef Dune::PDELab::MultiDomain::MultiDomainGridFunctionSpace<
        MultiDomainGrid, VectorBackend,
        Dune::PDELab::LexicographicOrderingTag,
        StokesGFS, DarcyGFS> type;
};

// local operator for stationary part
#if COUPLING_KEPSILON
SET_TYPE_PROP(StokesSubProblem, LocalOperator, Dune::PDELab::KEpsilonStaggeredGrid<TTAG(StokesSubProblem)>);
#elif COUPLING_LOWREKEPSILON
SET_TYPE_PROP(StokesSubProblem, LocalOperator, Dune::PDELab::LowReKEpsilonStaggeredGrid<TTAG(StokesSubProblem)>);
#elif COUPLING_ONEEQ
SET_TYPE_PROP(StokesSubProblem, LocalOperator, Dune::PDELab::SpalartAllmarasStaggeredGrid<TTAG(StokesSubProblem)>);
#elif COUPLING_ZEROEQ
SET_TYPE_PROP(StokesSubProblem, LocalOperator, Dune::PDELab::ZeroEqStaggeredGrid<TTAG(StokesSubProblem)>);
#else
SET_TYPE_PROP(StokesSubProblem, LocalOperator, Dune::PDELab::NavierStokesStaggeredGrid<TTAG(StokesSubProblem)>);
#endif
SET_TYPE_PROP(DarcySubProblem, LocalOperator,
              Dumux::PDELab::PDELabLocalOperator<TTAG(MDSNavierStokesDarcyTwoP)>);

// local operator for transient part
#if COUPLING_KEPSILON
SET_TYPE_PROP(StokesSubProblem, TransientLocalOperator, Dune::PDELab::KEpsilonTransientStaggeredGrid<TTAG(StokesSubProblem)>);
#elif COUPLING_LOWREKEPSILON
SET_TYPE_PROP(StokesSubProblem, TransientLocalOperator, Dune::PDELab::LowReKEpsilonTransientStaggeredGrid<TTAG(StokesSubProblem)>);
#elif COUPLING_ONEEQ
SET_TYPE_PROP(StokesSubProblem, TransientLocalOperator, Dune::PDELab::SpalartAllmarasTransientStaggeredGrid<TTAG(StokesSubProblem)>);
#elif COUPLING_ZEROEQ // NOTE: does not have an own transient lop
SET_TYPE_PROP(StokesSubProblem, TransientLocalOperator, Dune::PDELab::NavierStokesTransientStaggeredGrid<TTAG(StokesSubProblem)>);
#else
SET_TYPE_PROP(StokesSubProblem, TransientLocalOperator, Dune::PDELab::NavierStokesTransientStaggeredGrid<TTAG(StokesSubProblem)>);
#endif
SET_TYPE_PROP(DarcySubProblem, TransientLocalOperator,
              Dumux::PDELab::PDELabInstationaryLocalOperator);

// stationary PDELab sub problem
SET_PROP(MDSNavierStokesDarcyTwoP, SubProblemStationaryStokes)
{
private:
    using MultiDomainGrid = typename GET_PROP_TYPE(TypeTag, MultiDomainGrid);
    using MultiGFS = typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace);
    using LOpStokes = typename GET_PROP_TYPE(typename GET_PROP_TYPE(TypeTag, StokesSubProblemTypeTag), LocalOperator);
    using Condition = Dune::PDELab::MultiDomain::SubDomainEqualityCondition<MultiDomainGrid>;
public:
    typedef Dune::PDELab::MultiDomain::SubProblem<
        MultiGFS, MultiGFS, LOpStokes, Condition, 0> type;
};
SET_PROP(MDSNavierStokesDarcyTwoP, SubProblemStationaryDarcy)
{
private:
    using MultiDomainGrid = typename GET_PROP_TYPE(TypeTag, MultiDomainGrid);
    using MultiGFS = typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace);
    using LOpDarcy = typename GET_PROP_TYPE(typename GET_PROP_TYPE(TypeTag, DarcySubProblemTypeTag), LocalOperator);
    using Condition = Dune::PDELab::MultiDomain::SubDomainEqualityCondition<MultiDomainGrid>;
public:
    typedef Dune::PDELab::MultiDomain::SubProblem<
        MultiGFS, MultiGFS, LOpDarcy, Condition, 1> type;
};

// transient PDELab sub problem
SET_PROP(MDSNavierStokesDarcyTwoP, SubProblemTransientStokes)
{
private:
    using MultiDomainGrid = typename GET_PROP_TYPE(TypeTag, MultiDomainGrid);
    using MultiGFS = typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace);
    using TLopStokes = typename GET_PROP_TYPE(typename GET_PROP_TYPE(TypeTag, StokesSubProblemTypeTag), TransientLocalOperator);
    using Condition = Dune::PDELab::MultiDomain::SubDomainEqualityCondition<MultiDomainGrid>;
public:
    typedef Dune::PDELab::MultiDomain::SubProblem<
        MultiGFS, MultiGFS, TLopStokes, Condition, 0> type;
};
SET_PROP(MDSNavierStokesDarcyTwoP, SubProblemTransientDarcy)
{
private:
    using MultiDomainGrid = typename GET_PROP_TYPE(TypeTag, MultiDomainGrid);
    using MultiGFS = typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace);
    using TLopDarcy = typename GET_PROP_TYPE(typename GET_PROP_TYPE(TypeTag, DarcySubProblemTypeTag), TransientLocalOperator);
    using Condition = Dune::PDELab::MultiDomain::SubDomainEqualityCondition<MultiDomainGrid>;
public:
    typedef Dune::PDELab::MultiDomain::SubProblem<
        MultiGFS, MultiGFS, TLopDarcy, Condition, 1> type;
};

// coupling operator
SET_TYPE_PROP(MDSNavierStokesDarcyTwoP, CouplingOperator, Dumux::CouplingStokesDarcy2p<TypeTag>);

SET_PROP(MDSNavierStokesDarcyTwoP, Coupling)
{
private:
    using Scalar = GET_PROP_TYPE(TTAG(MDSNavierStokesDarcyTwoP), Scalar);
    using SubProblemStationaryStokes = typename GET_PROP_TYPE(TypeTag, SubProblemStationaryStokes);
    using SubProblemStationaryDarcy = typename GET_PROP_TYPE(TypeTag, SubProblemStationaryDarcy);
    using CouplingOperator = typename GET_PROP_TYPE(TypeTag, CouplingOperator);
public:
    typedef Dune::PDELab::MultiDomain::Coupling<
      SubProblemStationaryStokes, SubProblemStationaryDarcy, CouplingOperator> type;
};

// stationary multi domain grid operator
SET_PROP(MDSNavierStokesDarcyTwoP, MultiDomainStationaryGridOperator)
{
private:
    using MultiGFS = typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace);
    using MatrixBackend = Dune::PDELab::ISTLMatrixBackend;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using ConstraintsContainer = typename MultiGFS::template ConstraintsContainer<Scalar>::Type;
    using SubProblemStationaryStokes = typename GET_PROP_TYPE(TypeTag, SubProblemStationaryStokes);
    using SubProblemStationaryDarcy = typename GET_PROP_TYPE(TypeTag, SubProblemStationaryDarcy);
    using Coupling = typename GET_PROP_TYPE(TypeTag, Coupling);
public:
    typedef Dune::PDELab::MultiDomain::GridOperator<
        MultiGFS, MultiGFS,
        MatrixBackend, Scalar, Scalar, Scalar,
        ConstraintsContainer, ConstraintsContainer,
        SubProblemStationaryStokes,
        SubProblemStationaryDarcy,
        Coupling> type;
};

// transient multi domain grid operator
SET_PROP(MDSNavierStokesDarcyTwoP, MultiDomainTransientGridOperator)
{
private:
    using MultiGFS = typename GET_PROP_TYPE(TypeTag, MultiDomainGridFunctionSpace);
    using MatrixBackend = Dune::PDELab::ISTLMatrixBackend;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using ConstraintsContainer = typename MultiGFS::template ConstraintsContainer<Scalar>::Type;
    using SubProblemTransientStokes = typename GET_PROP_TYPE(TypeTag, SubProblemTransientStokes);
    using SubProblemTransientDarcy = typename GET_PROP_TYPE(TypeTag, SubProblemTransientDarcy);
public:
    typedef Dune::PDELab::MultiDomain::GridOperator<
        MultiGFS, MultiGFS,
        MatrixBackend, Scalar, Scalar, Scalar,
        ConstraintsContainer, ConstraintsContainer,
        SubProblemTransientStokes,
        SubProblemTransientDarcy> type;
};

// full multidomain grid operator
SET_PROP(MDSNavierStokesDarcyTwoP, MultiDomainGridOperator)
{
private:
    using StationaryGridOperator = typename GET_PROP_TYPE(TypeTag, MultiDomainStationaryGridOperator);
    using TransientGridOperator = typename GET_PROP_TYPE(TypeTag, MultiDomainTransientGridOperator);
public:
    typedef Dune::PDELab::OneStepGridOperator<
        StationaryGridOperator, TransientGridOperator> type;
};

// copy runtime parameters
SET_PROP(StokesSubProblem, ParameterTree)
{
private:
    using ParameterTree = typename GET_PROP(TTAG(MDSNavierStokesDarcyTwoP), ParameterTree);
public:
    typedef typename ParameterTree::type type;

    static type &tree()
    { return ParameterTree::tree(); }

    static type &compileTimeParams()
    { return ParameterTree::compileTimeParams(); }

    static type &runTimeParams()
    { return ParameterTree::runTimeParams(); }

    static type &deprecatedRunTimeParams()
    { return ParameterTree::deprecatedRunTimeParams(); }

    static type &unusedNewRunTimeParams()
    { return ParameterTree::unusedNewRunTimeParams(); }
};
SET_PROP(DarcySubProblem, ParameterTree)
{
private:
    using ParameterTree = typename GET_PROP(TTAG(MDSNavierStokesDarcyTwoP), ParameterTree);
public:
    typedef typename ParameterTree::type type;

    static type &tree()
    { return ParameterTree::tree(); }

    static type &compileTimeParams()
    { return ParameterTree::compileTimeParams(); }

    static type &runTimeParams()
    { return ParameterTree::runTimeParams(); }

    static type &deprecatedRunTimeParams()
    { return ParameterTree::deprecatedRunTimeParams(); }

    static type &unusedNewRunTimeParams()
    { return ParameterTree::unusedNewRunTimeParams(); }
};

// modified Newton method
SET_TYPE_PROP(DarcySubProblem, NewtonMethod, PDELabNewtonMethod<TTAG(MDSNavierStokesDarcyTwoP)>);
SET_TYPE_PROP(DarcySubProblem, NewtonController, NewtonController<TypeTag>);

// set TimeManager for sub-problems
SET_TYPE_PROP(StokesSubProblem, TimeManager, typename GET_PROP_TYPE(TTAG(MDSNavierStokesDarcyTwoP), TimeManager));
SET_TYPE_PROP(DarcySubProblem, TimeManager, typename GET_PROP_TYPE(TTAG(MDSNavierStokesDarcyTwoP), TimeManager));

// some Darcy things
SET_TYPE_PROP(MDSNavierStokesDarcyTwoP, Model, GET_PROP_TYPE(TTAG(DarcySubProblem), Model));
SET_TYPE_PROP(MDSNavierStokesDarcyTwoP, SolutionVector, GET_PROP_TYPE(TTAG(DarcySubProblem), SolutionVector));
SET_TYPE_PROP(MDSNavierStokesDarcyTwoP, JacobianAssembler, GET_PROP_TYPE(TTAG(DarcySubProblem), JacobianAssembler));
SET_TYPE_PROP(MDSNavierStokesDarcyTwoP, JacobianMatrix, GET_PROP_TYPE(TTAG(DarcySubProblem), JacobianMatrix));
SET_TYPE_PROP(MDSNavierStokesDarcyTwoP, VertexMapper, GET_PROP_TYPE(TTAG(DarcySubProblem), VertexMapper));

// set the used linear solver
SET_PROP(MDSNavierStokesDarcyTwoP, LinearSolver)
{
private:
    using MultiDomainGridOperator = typename GET_PROP_TYPE(TypeTag, MultiDomainGridOperator);
    using MatrixBase = typename MultiDomainGridOperator::Traits::Jacobian::BaseT;
public:
    typedef Dune::UMFPack<MatrixBase> type;
};

// Set the indices used by the model
SET_TYPE_PROP(MDSNavierStokesDarcyTwoP, Indices, NavierStokesDarcyTwoPIndices<TypeTag>);

// Use Beavers Joseph as solution dependent Neumann on default (is more stable than solDependetDirichlet)
SET_BOOL_PROP(MDSNavierStokesDarcyTwoP, CouplingBeaversJosephAsSolDependentDirichlet, false);

// Minimum accepted time step size to proceed simulation
SET_SCALAR_PROP(MDSNavierStokesDarcyTwoP, NewtonAbortTimeStepSize, 1e-8);

// Factor to adapt the time step, if Newton failed
SET_SCALAR_PROP(MDSNavierStokesDarcyTwoP, NewtonTimeStepReduction, 0.5);

// Limit range of k and epsilon to positive values
SET_BOOL_PROP(MDSNavierStokesDarcyTwoP, NewtonKEpsilonLimiter, false);

// Use the non-wetting phase as fluid
SET_INT_PROP(StokesSubProblem, PhaseIdx, GET_PROP_TYPE(TypeTag, FluidSystem)::nPhaseIdx);
} // end namespace Properties
} // end namespace Dumux

#endif // DUMUX_MULTIDOMAIN_NAVIERSTOKES_DARCYTWOP_PROPERTYDEFAULTS_HH
