// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 * \brief Functions required to evaluate fluxes in the staggeredgrid models
 */
#ifndef DUMUX_EVALUATE_FLUXES_HH
#define DUMUX_EVALUATE_FLUXES_HH

#include <dune/common/float_cmp.hh>

#include <dumux/common/basicproperties.hh>
#include <dumux/common/propertysystem.hh>

namespace Dumux
{
namespace Properties
{
// Forward the necessary properties
NEW_PROP_TAG(ProblemDiffusionAveragingMethod);
}

/*!
 * \brief Functions required to evaluate fluxes in the staggeredgrid models
 */
template <class TypeTag>
class EvaluateFluxes
{
public:
  typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;

  /*!
    * \brief Returns the averaged value
    */
  template<typename T>
  static const T average(T i, T j, Scalar di, Scalar dj)
  {
    if (Dune::FloatCmp::eq<Scalar, Dune::FloatCmp::absolute>(i, 0.0, 1.0e-30)
        && Dune::FloatCmp::eq<Scalar, Dune::FloatCmp::absolute>(j, 0.0, 1.0e-30))
      return T(0);

    // arithmetic mean
    if (GET_PROP_VALUE(TypeTag, ProblemDiffusionAveragingMethod) == 0)
      return (i + j) / 2.0;
    // harmonic mean
    else if (GET_PROP_VALUE(TypeTag, ProblemDiffusionAveragingMethod) == 1)
      return 2.0 * i * j / (i + j);
    // distance weighted arithmetic mean
    else if (GET_PROP_VALUE(TypeTag, ProblemDiffusionAveragingMethod) == 2)
      return (i * di + j * dj) / (di + dj);
    // distance weighted harmonic mean
    else if (GET_PROP_VALUE(TypeTag, ProblemDiffusionAveragingMethod) == 3)
      return (i * j) * (di + dj) / (j * di + i * dj);
    else
        DUNE_THROW(Dune::NotImplemented, "This averaging method is not implemented.");
  }
};
} // end namespace

#endif // DUMUX_EVALUATE_FLUXES_HH
