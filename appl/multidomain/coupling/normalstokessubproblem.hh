// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_WINDTUNNEL_STOKES_SUBPROBLEM_HH
#define DUMUX_WINDTUNNEL_STOKES_SUBPROBLEM_HH

#if COUPLING_ZEROEQ
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq2cni/zeroeq2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq2cni/zeroeq2cnipropertydefaults.hh>
#else
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cnipropertydefaults.hh>
#endif

#include <appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/properties.hh>

namespace Dumux
{
template <class TypeTag>
class WindtunnelStokesSubProblem;

namespace Properties
{
#if COUPLING_ZEROEQ
// Use Navier-Stokes equation without turbulence models
// Do not use a turbulence model, just use zeroeq for symmetrized velocitygradient
SET_BOOL_PROP(StokesSubProblem, ProblemEnableUnsymmetrizedVelocityGradient, false);
SET_INT_PROP(StokesSubProblem, ZeroEqEddyViscosityModel, 0);
SET_INT_PROP(StokesSubProblem, ProblemFlowNormalAxis, 1);
SET_INT_PROP(StokesSubProblem, ProblemWallNormalAxis, -1);
#endif

// Use either velocity or Reynolds number to define the flow
NEW_PROP_TAG(FreeFlowVelocity);
NEW_PROP_TAG(FreeFlowReynoldsNumber);
SET_SCALAR_PROP(StokesSubProblem, FreeFlowVelocity, 0.0);
SET_SCALAR_PROP(StokesSubProblem, FreeFlowReynoldsNumber, 0.0);

// Use either velocity or Reynolds number to define the flow
NEW_PROP_TAG(FreeFlowVelocityProfileType);
SET_STRING_PROP(StokesSubProblem, FreeFlowVelocityProfileType, "half-parabola");
}

template <class TypeTag>
class WindtunnelStokesSubProblem
#if COUPLING_ZEROEQ
  : public ZeroEqTwoCNIProblem<TypeTag>
#else
  : public NavierStokesTwoCNIProblem<TypeTag>
#endif
{
#if COUPLING_ZEROEQ
    using ParentType = ZeroEqTwoCNIProblem<TypeTag>;
#else
    using ParentType = NavierStokesTwoCNIProblem<TypeTag>;
#endif
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Implementation;
    using MultiDomainTypeTag = typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag);

    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    enum { dim = GridView::dimension };
    using DimVector = typename GET_PROP_TYPE(TypeTag, DimVector);
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);
    using GlobalPosition = Dune::FieldVector<Scalar, dim>;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum { phaseIdx = Indices::phaseIdx,
           wCompIdx = FluidSystem::wCompIdx };

public:
    WindtunnelStokesSubProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView), gridView_(gridView)
    {
        velocity_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Velocity);
        velocityProfileType_ = GET_PARAM_FROM_GROUP(TypeTag, std::string, FreeFlow, VelocityProfileType);
        pressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Pressure);
        massMoleFrac_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, MassMoleFrac);
        temperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Temperature);
        Scalar reynoldsNumber = GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, ReynoldsNumber);
        if (reynoldsNumber > eps_ && std::abs(velocity_) > 1e-10)
        {
            std::cout << "Please specify either the Reynolds number or the velocity" << std::endl;
            exit(2300);
        }

        darcyLowerLeft_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, GlobalPosition, Grid, DarcyLowerLeft);
        darcyUpperRight_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, GlobalPosition, Grid, DarcyUpperRight);

        FluidState fluidState;
        fluidState.setPressure(phaseIdx, pressure_);
        fluidState.setTemperature(temperature_);
        fluidState.setMassFraction(phaseIdx, wCompIdx, massMoleFrac_);
        Scalar density = FluidSystem::density(fluidState, phaseIdx);
        Scalar kinematicViscosity = FluidSystem::viscosity(fluidState, phaseIdx) / density;
        Scalar diameter = this->bBoxMax()[0] - this->bBoxMin()[0];
        if (reynoldsNumber > eps_)
        {
            velocity_ = -reynoldsNumber * kinematicViscosity / diameter;
        }
        Scalar velocityAbs = std::abs(velocity_);
//         Scalar diameterSideLeft = std::max(0.0, darcyLowerLeft_[0] - this->bBoxMin()[0]);
//         Scalar diameterSideRight = std::max(0.0, this->bBoxMax()[0] - darcyUpperRight_[0]);
//         Scalar velocityAbsLeft = 2.0 * velocityAbs * diameter * diameterSideLeft
//                                  / std::pow(diameterSideLeft + diameterSideRight, 2.0);
//         Scalar velocityAbsRight = 2.0 * velocityAbs * diameter * diameterSideRight
//                                   / std::pow(diameterSideLeft + diameterSideRight, 2.0);
        std::cout << " density: " <<  density << std::endl;
        std::cout << " kinematicViscosity: " <<  kinematicViscosity << std::endl;
        std::cout << " Pipe   - "
                  << " d: " << diameter
                  << " v: " << velocityAbs
                  << " Re: " << diameter * velocityAbs / kinematicViscosity
                  << std::endl;
//         std::cout << " SideL  - "
//                   << " d: " << diameterSideLeft
//                   << " v: " << velocityAbsLeft
//                   << " Re: " << diameter * velocityAbsLeft / kinematicViscosity
//                   << std::endl;
//         std::cout << " SideR  - "
//                   << " d: " << diameterSideRight
//                   << " v: " << velocityAbsRight
//                   << " Re: " << diameter * velocityAbsRight / kinematicViscosity
//                   << std::endl;

        Scalar permeability = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, SpatialParams, Permeability);
        Scalar diameterPore = std::sqrt(permeability * 9.81/ kinematicViscosity / 0.0116) / 1000;
        std::cout << " PM     - "
                  << " d: " << diameterPore
                  << " v: " << velocityAbs
                  << " Re: " << diameterPore * velocityAbs / kinematicViscosity
                  << std::endl;
    }

    //! \copydoc Dumux::ImplicitProblem::init()
    void init()
    {
        // overwrite the parent function
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        std::string string = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Output, Name);
        return string.c_str();
    }

    //! \brief Velocity boundary condition types
    bool bcVelocityIsWall(const DimVector& global) const
    {
        return (global[0] < this->bBoxMin()[0] + eps_)
               && !isOnCouplingFace(global);
    }
    bool bcVelocityIsInflow(const DimVector& global) const
    {
        return !bcVelocityIsWall(global)
               && !bcVelocityIsOutflow(global)
               && !bcVelocityIsCoupling(global)
               && !bcVelocityIsSymmetry(global);
    }
    bool bcVelocityIsOutflow(const DimVector& global) const
    {
        return (global[dim-1] < this->bBoxMin()[dim-1] + eps_)
               && !isOnCouplingFace(global);
    }
    bool bcVelocityIsCoupling(const DimVector& global) const
    { return isOnCouplingFace(global); }
    bool bcVelocityIsSymmetry(const DimVector& global) const
    {
      return global[0] > this->bBoxMax()[0] - eps_
             && !isOnCouplingFace(global);
    }

    //! \brief Pressure boundary condition types
    bool bcPressureIsDirichlet(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }
    bool bcPressureIsOutflow(const DimVector& global) const
    { return !bcPressureIsDirichlet(global) && !bcPressureIsCoupling(global); }
    bool bcPressureIsCoupling(const DimVector& global) const
    { return isOnCouplingFace(global); }

    //! \brief MassMoleFrac boundary condition types
    bool bcMassMoleFracIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global); }
    bool bcMassMoleFracIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }
    bool bcMassMoleFracIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }
    bool bcMassMoleFracIsSymmetry(const DimVector& global) const
    { return bcVelocityIsSymmetry(global); }
    bool bcMassMoleFracIsCoupling(const DimVector& global) const
    { return isOnCouplingFace(global); }

    //! \brief Temperature boundary condition types
    bool bcTemperatureIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global); }
    bool bcTemperatureIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }
    bool bcTemperatureIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }
    bool bcTemperatureIsSymmetry(const DimVector& global) const
    { return bcVelocityIsSymmetry(global); }
    bool bcTemperatureIsCoupling(const DimVector& global) const
    { return isOnCouplingFace(global); }

    //! \brief Dirichlet values for velocity field
    DimVector dirichletVelocityAtPos(const Element& e, const DimVector& global) const
    {
        DimVector y(0.0);
        if (std::strcmp(velocityProfileType_.c_str(), "parabola") == 0)
        {
            y[1] = 4.0 * velocity_ * global[0] * (this->bBoxMax()[0] - global[0])
                   / (this->bBoxMax()[0] - this->bBoxMin()[0])
                   / (this->bBoxMax()[0] - this->bBoxMin()[0]);
        }
        else if (std::strcmp(velocityProfileType_.c_str(), "half-parabola") == 0)
        {
            y[1] = 4.0 * velocity_ * (global[0] / 2) * (this->bBoxMax()[0] - (global[0] / 2))
                   / (this->bBoxMax()[0] - this->bBoxMin()[0])
                   / (this->bBoxMax()[0] - this->bBoxMin()[0]);
        }
        else if (std::strcmp(velocityProfileType_.c_str(), "block") == 0)
        {
            y[1] = velocity_;
        }
        else
        {
            DUNE_THROW(Dune::NotImplemented, "Velocity profile type is unkown " << velocityProfileType_.c_str());
        }
        if (bcVelocityIsWall(global))
            y[1] = 0.0;
        return y;
    }
    Scalar dirichletPressureAtPos(const DimVector& global) const
    {
        return pressure_;
    }
    Scalar dirichletMassMoleFracAtPos(const DimVector& global) const
    {
        return massMoleFrac_;
    }
    Scalar dirichletTemperatureAtPos(const DimVector& global) const
    {
        return temperature_;
    }

    //! \brief Returns whether we are on a coupling face
    const bool isOnCouplingFace(const DimVector& global) const
    {
        bool isOnCouplingFace = false;

        for (unsigned int dimIdx = 0; dimIdx < dim; ++dimIdx)
            if (((global[dimIdx] < darcyLowerLeft_[dimIdx] + eps_ && global[dimIdx] > darcyLowerLeft_[dimIdx] - eps_)
                  || (global[dimIdx] < darcyUpperRight_[dimIdx] + eps_ && global[dimIdx] > darcyUpperRight_[dimIdx] - eps_))
                && global[1-dimIdx] > darcyLowerLeft_[1-dimIdx] - eps_ && global[1-dimIdx] < darcyUpperRight_[1-dimIdx] + eps_)
            {
                isOnCouplingFace = true;
            }

        return isOnCouplingFace;
    }

    //! \brief Return whether a point is located on the boundary of the domain
    bool isOnBoundary(const DimVector& global) const
    {
        return global[0] < this->bBoxMin()[0] + eps_
               || global[0] > this->bBoxMax()[0] - eps_
               || global[1] < this->bBoxMin()[1] + eps_
               || global[1] > this->bBoxMax()[1] - eps_
               || isOnCouplingFace(global);
    }

private:
    std::string name_;
    const GridView gridView_;
    static constexpr Scalar eps_ = 1e-6;

    GlobalPosition darcyLowerLeft_;
    GlobalPosition darcyUpperRight_;

    Scalar velocity_;
    std::string velocityProfileType_;
    Scalar pressure_;
    Scalar temperature_;
    Scalar massMoleFrac_;
};

} //end namespace

#endif // DUMUX_WINDTUNNEL_STOKES_SUBPROBLEM_HH
