// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief \todo please doc me
 */
#ifndef DUMUX_WINDTUNNEL_DARCY_SUBPROBLEM_HH
#define DUMUX_WINDTUNNEL_DARCY_SUBPROBLEM_HH

#include <dumux/io/gnuplotinterface.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivityjohansen.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivitysomerton.hh>
#include <dumux/material/fluidmatrixinteractions/diffusivityconstant.hh>
#include <dumux/porousmediumflow/2p2c/implicit/model.hh>
#include <dumux/porousmediumflow/implicit/darcyfluxvariables.hh>
#include <dumux/porousmediumflow/implicit/forchheimerfluxvariables.hh>

#include <appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/properties.hh>

#include "normaldarcyspatialparams.hh"

#define ISOTHERMAL 0

namespace Dumux
{
template <class TypeTag>
class WindtunnelDarcySubProblem;

namespace Properties
{
// choose pn and Sw as primary variables
SET_INT_PROP(DarcySubProblem, Formulation, TwoPTwoCFormulation::pnsw);

// the gas component balance (air) is replaced by the total mass balance
SET_INT_PROP(DarcySubProblem, ReplaceCompEqIdx, GET_PROP_TYPE(TypeTag, Indices)::contiNEqIdx);

// Use the Darcy or Forchheimer equation
#if FORCHHEIMER
SET_TYPE_PROP(DarcySubProblem, BaseFluxVariables, ImplicitForchheimerFluxVariables<TypeTag>);
#else
SET_TYPE_PROP(DarcySubProblem, BaseFluxVariables, ImplicitDarcyFluxVariables<TypeTag>);
#endif

// Somerton is used as model to compute the effective thermal heat conductivity
SET_TYPE_PROP(DarcySubProblem, ThermalConductivityModel,
              ThermalConductivitySomerton<typename GET_PROP_TYPE(TypeTag, Scalar)>);

// Live plot of the evaporation rates
NEW_PROP_TAG(OutputPlotMassLossRate);
SET_BOOL_PROP(DarcySubProblem, OutputPlotMassLossRate, false);

// Frequency of writing storage files
NEW_PROP_TAG(OutputFreqMassOutput);
SET_INT_PROP(DarcySubProblem, OutputFreqMassOutput, 5);
}


/*!
 * \ingroup TwoPTwoCModel
 * \ingroup ImplicitTestProblems
 * \brief \todo please doc me
 */
template <class TypeTag >
class WindtunnelDarcySubProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag) MultiDomainTypeTag;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(MultiDomainTypeTag, SubDomainGridView) SubDomainGridView;

    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(MultiDomainTypeTag, Indices) MultiDomainIndices;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    // the equation indices
    enum { contiTotalMassIdx = Indices::contiNEqIdx,
           contiWEqIdx = Indices::contiWEqIdx,
           energyEqIdx = Indices::energyEqIdx };
    // the indices of the primary variables
    enum { pressureIdx = Indices::pressureIdx,
           switchIdx = Indices::switchIdx,
           temperatureIdx = Indices::temperatureIdx };
    // the indices of the phase presence
    enum { bothPhases = Indices::bothPhases,
           nPhaseOnly = Indices::nPhaseOnly };
    // Grid and world dimension
    enum { dim = GridView::dimension,
           dimWorld = GridView::dimensionworld };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, LocalJacobian) LocalJacobian;
    typedef typename GET_PROP_TYPE(TypeTag, LocalResidual) LocalResidual;

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    // property that defines whether mole or mass fractions are used
    static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);
    static const unsigned int darcySubDomainIdx = MultiDomainIndices::darcySubDomainIdx;

public:
    /*!
     * \brief The constructor.
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     */
    WindtunnelDarcySubProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
        , gridView_(gridView)
    {
        pressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, Pressure);
        switch_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, Switch);
        temperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, Temperature);
        name_ = GET_RUNTIME_PARAM(TypeTag, std::string, Output.Name);
        initialPhasePresence_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, PorousMedium, InitialPhasePresence);

        freqMassOutput_ = GET_PARAM_FROM_GROUP(TypeTag, int, Output, FreqMassOutput);

        storageLastTimestep_ = Scalar(0);
        lastMassOutputTime_ = Scalar(0);

        std::string storageFile = name() + "-storage.csv";
        outfile_.open(storageFile);
        outfile_ << "Time[s]" << ";"
                 << "WaterMass[kg/mDepth]" << ";"
                 << "WaterMassChange[kg/(s*mDepth))]"
                 << std::endl;
    }

    /*!
     * \name Problem parameters
     */
    // \{

    //! \brief The destructor
    ~WindtunnelDarcySubProblem()
    {
        outfile_.close();
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    { return name_; }

    /*!
     * \brief Called by the Dumux::TimeManager in order to
     *        initialize the problem.
     */
    void init()
    {
        ParentType::init();
        globalStorage(storageLastTimestep_);
    }

    // suppress output from DuMuX
    bool shouldWriteOutput() const
    {
        return false;
    }

    /*!
     * \brief Returns the source term at specific position in the domain.
     *
     * \param values The source values for the primary variables
     * \param globalPos The position
     *
     * The units must be according to either using mole or mass fractions. (mole/(m^3*s) or kg/(m^3*s))
     */
    void sourceAtPos(PrimaryVariables &values,
                     const GlobalPosition &globalPos) const
    {
        values = 0;
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param globalPos The position for which the bc type should be evaluated
     */
    void boundaryTypesAtPos(BoundaryTypes &values,
                            const GlobalPosition &globalPos) const
    {
        values.setAllNeumann();
        if (globalPos[dim-1] < eps_)
        {
            values.setDirichlet(pressureIdx);
            values.setOutflow(switchIdx);
            values.setOutflow(temperatureIdx);
        }
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        boundary segment.
     *
     * \param values The dirichlet values for the primary variables
     * \param globalPos The position for which the bc type should be evaluated
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        initialAtPos(values, globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * \param values The neumann values for the conservation equations
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry in the box scheme
     * \param intersection The intersection between element and boundary
     * \param scvIdx The local vertex index
     * \param boundaryFaceIdx The index of the boundary face
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     *
     * The units must be according to either using mole or mass fractions. (mole/(m^2*s) or kg/(m^2*s))
     */
    void neumannAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        values = 0;
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * \param values The initial values for the primary variables
     * \param globalPos The position for which the initial condition should be evaluated
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        values[pressureIdx] = pressure_;
        values[switchIdx] = switch_;
        values[temperatureIdx] = temperature_;
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     *
     * \param globalPos The global position
     */
    int initialPhasePresenceAtPos(const GlobalPosition &globalPos) const
    {
        if (std::strcmp(initialPhasePresence_.c_str(), "bothPhases") == 0)
        {
            return bothPhases;
        }
        else if (std::strcmp(initialPhasePresence_.c_str(), "nPhaseOnly") == 0)
        {
            return nPhaseOnly;
        }
        else
        {
            DUNE_THROW(Dune::NotImplemented, "Initial phases presence is unkown " << initialPhasePresence_.c_str());
        }
    }

    /*!
     * \brief Called by the time manager after the time integration to
     *        do some post processing on the solution.
     */
    void postTimeStep()
    {
        // Calculate masses
        PrimaryVariables storage;
        globalStorage(storage);

        const Scalar time = this->timeManager().time() +  this->timeManager().timeStepSize();

        if (this->timeManager().timeStepIndex() % freqMassOutput_ == 0
            || this->timeManager().episodeWillBeFinished())
        {
            PrimaryVariables storageChange(0.);
            storageChange = storageLastTimestep_ - storage;

            assert(time - lastMassOutputTime_ != 0);
            storageChange /= (time - lastMassOutputTime_);

            std::cout << "-----" << std::endl;
            std::cout << "Time[s]: " << time
                      << " WaterMass[kg]: " << storage[contiWEqIdx]
                      << " WaterMassChange[kg/s]: " << storageChange[contiWEqIdx]
                      << std::endl;
            std::cout << "-----" << std::endl;

            if (this->timeManager().time() != 0.)
                outfile_ << time << ";"
                         << storage[contiWEqIdx] << ";"
                         << storageChange[contiWEqIdx] << ";"
                         << std::endl;

            static std::vector<double> x;
            static std::vector<double> y;

            x.push_back(time / 86400.0); // d
            y.push_back(storageChange[contiWEqIdx] * 1000.0); // g/s

            gnuplot_.resetPlot();
            gnuplot_.setXlabel("time [d]");
            gnuplot_.setYlabel("mass loss rate [g/s]");
            gnuplot_.addDataSetToPlot(x, y, "#t[d];massLossRate[g/s]",  "massLossRate.dat");
            if (GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Output, PlotMassLossRate))
            {
                gnuplot_.plot("massLossRate");
            }
        }
    }

    /*!
     * \brief Compute the integral over the domain of the storage
     *        terms of all conservation quantities.
     *
     * \param storage Stores the result
     */
    void globalStorage(PrimaryVariables &storage)
    {
        localJacobian_.init(*this);
        storage = 0;
        for (const auto& element : elements(gridView_))
        {
            if(element.partitionType() == Dune::InteriorEntity
               && gridView_.indexSet().contains(darcySubDomainIdx, element))
            {
                localJacobian_.localResidual().evalStorage(element);
                storage += localJacobian_.localResidual().storageTerm()[0];
            }
        }
    }

private:
    static constexpr Scalar eps_ = 1e-8;

    Scalar pressure_;
    Scalar switch_;
    Scalar temperature_;
    std::string name_;
    std::string initialPhasePresence_;

    int freqMassOutput_;
    PrimaryVariables storageLastTimestep_;
    Scalar lastMassOutputTime_;
    std::ofstream outfile_;
    Dumux::GnuplotInterface<double> gnuplot_;

    LocalJacobian localJacobian_;
    GridView gridView_;
};

} //end namespace

#endif // DUMUX_WINDTUNNEL_DARCY_SUBPROBLEM_HH
