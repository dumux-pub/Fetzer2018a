// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \brief Non-isothermal two-component ZeroEq subproblem with air flowing
 *        from the left to the right and coupling at the bottom.
 */
#ifndef DUMUX_ZEROEQTWOCNI_SUBPROBLEM_HH
#define DUMUX_ZEROEQTWOCNI_SUBPROBLEM_HH

#include <dumux/freeflow/zeroeqncni/model.hh>
#include <dumux/multidomain/subdomainpropertydefaults.hh>
#include <dumux/multidomain/2cnistokes2p2cni/stokesncnicouplinglocalresidual.hh>

#include <dumux/io/readwritedatafile.hh>

namespace Dumux
{

template <class TypeTag>
class ZeroEq2cniSubProblem;

namespace Properties
{
NEW_TYPE_TAG(ZeroEq2cniSubProblem,
             INHERITS_FROM(BoxZeroEqncni, SubDomain));

// Set the problem property
SET_TYPE_PROP(ZeroEq2cniSubProblem, Problem, Dumux::ZeroEq2cniSubProblem<TypeTag>);

// Use the StokesncniCouplingLocalResidual for the computation of the local residual in the ZeroEq domain
SET_TYPE_PROP(ZeroEq2cniSubProblem, LocalResidual,
              StokesncniCouplingLocalResidual<TypeTag>);

// Set the property for the material parameters by extracting it from the material law.
SET_TYPE_PROP(ZeroEq2cniSubProblem, MaterialLawParams,
              typename GET_PROP_TYPE(TypeTag, MaterialLaw)::Params);

// Used the fluid system from the coupled problem
SET_TYPE_PROP(ZeroEq2cniSubProblem, FluidSystem,
              typename GET_PROP_TYPE(typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag), FluidSystem));

// Set number of intervals for the evaluation of flow properties at the wall
SET_INT_PROP(ZeroEq2cniSubProblem, NumberOfIntervals, 250);

// Disable use of mole formulation
SET_BOOL_PROP(ZeroEq2cniSubProblem, UseMoles, false);

// Disable gravity
SET_BOOL_PROP(ZeroEq2cniSubProblem, ProblemEnableGravity, false);

// Enable Navier-Stokes
SET_BOOL_PROP(ZeroEq2cniSubProblem, EnableNavierStokes, true);
}

/*!
 * \ingroup ImplicitTestProblems
 * \ingroup MultidomainProblems
 * \brief ZeroEq2cni problem with air flowing from the left to the right.
 *
 * \todo update test description
 * This sub problem uses the \ref ZeroEq2cniModel. It is part of the 2cnizeroeq2p2cni model and
 * is combined with the 2p2csubproblem for the Darcy domain.
 */
template <class TypeTag>
class ZeroEq2cniSubProblem : public ZeroEqProblem<TypeTag>
{
    typedef ZeroEq2cniSubProblem<TypeTag> ThisType;
    typedef ZeroEqProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum {
        dim = GridView::dimension
    };
    enum {
        // equation indices
        massBalanceIdx = Indices::massBalanceIdx,
        momentumXIdx = Indices::momentumXIdx, // Index of the x-component of the momentum balance
        momentumYIdx = Indices::momentumYIdx, // Index of the y-component of the momentum balance
        momentumZIdx = Indices::momentumZIdx, // Index of the z-component of the momentum balance
        transportEqIdx = Indices::transportEqIdx, // Index of the transport equation (massfraction)
        energyEqIdx =    Indices::energyEqIdx     // Index of the energy equation (temperature)
    };
    enum { // primary variable indices
        pressureIdx = Indices::pressureIdx,
        velocityXIdx = Indices::velocityXIdx,
        velocityYIdx = Indices::velocityYIdx,
        velocityZIdx = Indices::velocityZIdx,
        massOrMoleFracIdx = Indices::massOrMoleFracIdx,
        temperatureIdx = Indices::temperatureIdx
    };
    enum { phaseIdx = Indices::phaseIdx };
    enum { numComponents = Indices::numComponents };
    enum {
        transportCompIdx = Indices::transportCompIdx, // water component index
        phaseCompIdx = Indices::phaseCompIdx          // air component index
    };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::ctype CoordScalar;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<CoordScalar, dim> GlobalPosition;


public:
    /*!
     * \brief The sub-problem for the ZeroEq subdomain
     *
     * \param timeManager The TimeManager which is used by the simulation
     * \param gridView The simulation's idea about physical space
     */
    ZeroEq2cniSubProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {

        // get velocity variations
        try {
            sinusVAmplitude_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusVelAmplitude);
            sinusVPeriod_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusVelPeriod);
            sinusVPhaseShift_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusVelPhaseShift);
        }
        catch (...) {
            sinusVAmplitude_ = 0.0;
            sinusVPeriod_ = 3600.0;
            sinusVPhaseShift_ = 0.0;
        }
        try {
            velDataFile_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, FreeFlow, VelDataFile);
            useVelDataFile_ = true;
            readData(velDataFile_, velocityData_);
//             checkData(velocityData_, "velocity");
        }
        catch (...) {
            useVelDataFile_ = false;
        }

        // get pressure variations
        try {
            sinusPAmplitude_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusPressureAmplitude);
            sinusPPeriod_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusPressurePeriod);
            sinusPPhaseShift_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusPressurePhaseShift);
        }
        catch (...) {
            sinusPAmplitude_ = 0.0;
            sinusPPeriod_ = 3600.0;
            sinusPPhaseShift_ = 0.0;
        }

        // get concentration variations
        try {
            sinusXAmplitude_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusConcentrationAmplitude);
            sinusXPeriod_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusConcentrationPeriod);
            sinusXPhaseShift_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusConcentrationPhaseShift);
        }
        catch (...) {
            sinusXAmplitude_ = 0.0;
            sinusXPeriod_ = 3600.0;
            sinusXPhaseShift_ = 0.0;
        }
        try {
            massFractionDataFile_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, FreeFlow, MassFractionDataFile);
            useMassFractionDataFile_ = true;
            readData(massFractionDataFile_, massFractionData_);
        }
        catch (...) {
            useMassFractionDataFile_ = false;
        }

        // get temperature variations
        try {
            sinusTAmplitude_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusTemperatureAmplitude);
            sinusTPeriod_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusTemperaturePeriod);
            sinusTPhaseShift_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusTemperaturePhaseShift);
        }
        catch (...) {
            sinusTAmplitude_ = 0.0;
            sinusTPeriod_ = 3600.0;
            sinusTPhaseShift_ = 0.0;
        }
        try {
            temperatureDataFile_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, FreeFlow, TemperatureDataFile);
            useTemperatureDataFile_ = true;
            readData(temperatureDataFile_, temperatureData_);
        }
        catch (...) {
            useTemperatureDataFile_ = false;
        }

        std::vector<Scalar> positions0 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions0);
        std::vector<Scalar> positions1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions1);

        bBoxMin_[0] = positions0.front();
        bBoxMax_[0] = positions0.back();
        bBoxMin_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePosY);
        bBoxMax_[1] = positions1.back();
        // parts of the interface without coupling
        try { runUpDistanceX1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, RunUpDistanceX1); }
        catch (...) { runUpDistanceX1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1); }
        try { runUpDistanceX2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, RunUpDistanceX2); }
        catch (...) { runUpDistanceX2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2); }
        runUpDistanceY_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, RunUpDistanceY);

        enableNeumannInflow_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, FreeFlow, EnableNeumannInflow);
        refVelocity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefVelocity);
        refPressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefPressure);
        refMassfrac_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefMassfrac);
        refTemperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefTemperature);

        initializationTime_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, InitTime);
    }

    /*! 
     * \todo bBox functions have to be overwritten, otherwise they remain uninitialised
     */
    //! \copydoc BoxProblem::&bBoxMin()
    const GlobalPosition &bBoxMin() const
    { return bBoxMin_; }

    //! \copydoc BoxProblem::&bBoxMax()
    const GlobalPosition &bBoxMax() const
    { return bBoxMax_; }

    /*!
     * \name Problem parameters
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::name()
    const std::string &name() const
    {
        return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Output, NameFF);
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::boundaryTypes()
    void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
        const GlobalPosition &globalPos = vertex.geometry().center();
        const Scalar time = this->timeManager().time();

        values.setAllDirichlet();


        if (onUpperBoundary_(globalPos))
        {
            values.setNeumann(transportEqIdx);
            values.setNeumann(temperatureIdx);
        }

        if (onLowerBoundary_(globalPos))
        {
            values.setNeumann(transportEqIdx);
            values.setDirichlet(temperatureIdx);

            if (globalPos[0] > runUpDistanceX1_ + eps_
                && globalPos[0] < runUpDistanceX2_ - eps_
                && time >= initializationTime_)
            {
                values.setAllCouplingDirichlet();
                values.setCouplingNeumann(momentumXIdx);
                values.setCouplingNeumann(momentumYIdx);
            }
        }

        if (onRightBoundary_(globalPos))
        {
            values.setAllOutflow();

            values.setDirichlet(temperatureIdx);
            if (onUpperBoundary_(globalPos) || onLowerBoundary_(globalPos)) // corner point
            {
                values.setAllDirichlet();
            }
        }

        // Left inflow boundaries should be Neumann, otherwise the
        // evaporative fluxes are much more grid dependent
        if (onLeftBoundary_(globalPos))
        {
            values.setAllDirichlet();
            if (enableNeumannInflow_)
            {
                values.setNeumann(transportEqIdx);
                values.setNeumann(temperatureIdx);
            }
        }

        // the mass balance has to be of type outflow
        // it does not get a coupling condition, since pn is a condition for stokes
        values.setOutflow(massBalanceIdx);

        // set pressure at one point, do NOT specify this
        // if the Darcy domain has a Dirichlet condition for pressure
        if (onRightBoundary_(globalPos))
        {
            if (time > initializationTime_)
            {
                values.setAllOutflow();
                values.setDirichlet(pressureIdx, massBalanceIdx);
                if (globalPos[1] < runUpDistanceY_
                    || onLowerBoundary_(globalPos))
                {
                    values.setAllDirichlet();
                    values.setOutflow(massBalanceIdx);
                    values.setNeumann(transportEqIdx);
                    values.setDirichlet(temperatureIdx);
                }
            }
            else
                if (!onLowerBoundary_(globalPos) && !onUpperBoundary_(globalPos))
                    values.setDirichlet(pressureIdx, massBalanceIdx);
        }
    }

    //! \copydoc Dumux::ImplicitProblem::dirichlet()
    void dirichlet(PrimaryVariables &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();
        values = 0.0;
        initial_(values, globalPos);
    }

    //! \copydoc Dumux::ImplicitProblem::neumann()
    void neumann(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const Intersection &is,
                 const int scvIdx,
                 const int boundaryFaceIdx) const
    {
        const GlobalPosition &globalPos =
                fvGeometry.boundaryFace[boundaryFaceIdx].ipGlobal;

        values = 0.;

        FluidState fluidState;
        if (useTemperatureDataFile_)
            fluidState.setTemperature(evaluateData(temperatureData_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize()));
        else
            fluidState.setTemperature(refTemperature());

        fluidState.setPressure(phaseIdx, refPressure());

        Scalar massFraction[numComponents];
        if (useMassFractionDataFile_)
            massFraction[transportCompIdx] = evaluateData(massFractionData_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize());
        else
            massFraction[transportCompIdx] = refMassfrac();
        massFraction[phaseCompIdx] = 1 - massFraction[transportCompIdx];

        // calculate average molar mass of the gas phase
        Scalar M1 = FluidSystem::molarMass(transportCompIdx);
        Scalar M2 = FluidSystem::molarMass(phaseCompIdx);
        Scalar X2 = massFraction[phaseCompIdx];
        Scalar massToMoleDenominator = M2 + X2 * (M1 - M2);

        fluidState.setMoleFraction(phaseIdx, transportCompIdx, massFraction[transportCompIdx]*M2/massToMoleDenominator);
        fluidState.setMoleFraction(phaseIdx, phaseCompIdx, massFraction[phaseCompIdx]*M1/massToMoleDenominator);

        const Scalar density = FluidSystem::density(fluidState, phaseIdx);
        const Scalar enthalpy = FluidSystem::enthalpy(fluidState, phaseIdx);
        const Scalar xVelocity = xVelocity_(globalPos);

        if (onLeftBoundary_(globalPos)
            && globalPos[1] > bBoxMin_[1] && globalPos[1] < bBoxMax_[1])
        {
            values[transportEqIdx] = -xVelocity * density * refMassfrac();
            values[energyEqIdx] = -xVelocity * density * enthalpy;
        }
    }

    // \}

    //! \copydoc Dumux::ImplicitProblem::source()
    void source(PrimaryVariables &values,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                const int scvIdx) const
    {
        // ATTENTION: The source term of the mass balance has to be chosen as
        // div (q_momentum) in the problem file
        values = Scalar(0);
    }

    //! \copydoc Dumux::ImplicitProblem::initial()
    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvGeometry,
                 const int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);

        initial_(values, globalPos);
    }
    // \}

    /*!
     * \brief Determine if we are on a corner of the grid
     *
     * \param global Pos The global position
     *
     */
    bool isCornerPoint(const GlobalPosition &globalPos)
    {
        if ((onLeftBoundary_(globalPos) && onLowerBoundary_(globalPos)) ||
            (onLeftBoundary_(globalPos) && onUpperBoundary_(globalPos)) ||
            (onRightBoundary_(globalPos) && onLowerBoundary_(globalPos)) ||
            (onRightBoundary_(globalPos) && onUpperBoundary_(globalPos)))
            return true;
        else
            return false;
    }

    /*!
     * \brief Auxiliary function used for the mortar coupling, if mortar coupling,
     *        this should return true
     *
     * \param globalPos The global position
     */
    bool isInterfaceCornerPoint(const GlobalPosition &globalPos) const
    { return false; }

    //! \brief Returns the reference velocity.
    const Scalar refVelocity() const
    { return refVelocity_ + variation_(sinusVAmplitude_, sinusVPeriod_, sinusVPhaseShift_); }

    //! \brief Returns the reference pressure.
    const Scalar refPressure() const
    { return refPressure_ + variation_(sinusPAmplitude_, sinusPPeriod_, sinusPPhaseShift_); }

    //! \brief Returns the reference mass fraction.
    const Scalar refMassfrac() const
    { return refMassfrac_ + variation_(sinusXAmplitude_, sinusXPeriod_, sinusXPhaseShift_); }

    //! \brief Returns the reference temperature.
    const Scalar refTemperature() const
    { return refTemperature_+ variation_(sinusTAmplitude_, sinusTPeriod_, sinusTPhaseShift_); }

    /*!
     * \brief Decide whether to use open top or not
     */
    bool bBoxMaxIsWall()
    { return false; }

private:
    /*!
     * \brief Internal method for the initial condition
     */
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        values[velocityXIdx] = xVelocity_(globalPos);
        values[velocityYIdx] = 0.;

        values[pressureIdx] = refPressure()
            + 1.189 * this->gravity()[1] * (globalPos[1] - bBoxMin_[1]);

        if (useMassFractionDataFile_)
          values[massOrMoleFracIdx] = evaluateData(massFractionData_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize());
        else
          values[massOrMoleFracIdx] = refMassfrac();

        if (useTemperatureDataFile_)
          values[temperatureIdx] = evaluateData(temperatureData_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize());
        else
          values[temperatureIdx] = refTemperature();
    }

    const Scalar xVelocity_(const GlobalPosition &globalPos) const
    {
        if (globalPos[1] < runUpDistanceY_ + eps_)
            return 0.0;

        if (useVelDataFile_)
          return evaluateData(velocityData_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize());

        return refVelocity();
    }

    // can be used for the variation of a boundary condition
    const Scalar variation_(const Scalar amplitude, const Scalar period, const Scalar phaseShift) const
    { return sin(2*M_PI*this->timeManager().time()/period+phaseShift) * amplitude; }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < bBoxMin_[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > bBoxMax_[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < bBoxMin_[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > bBoxMax_[1] - eps_; }

    bool onBoundary_(const GlobalPosition &globalPos) const
    {
        return (onLeftBoundary_(globalPos) || onRightBoundary_(globalPos)
                || onLowerBoundary_(globalPos) || onUpperBoundary_(globalPos));
    }

    static constexpr Scalar eps_ = 1e-8;
    GlobalPosition bBoxMin_;
    GlobalPosition bBoxMax_;

    bool enableNeumannInflow_;

    Scalar refVelocity_;
    Scalar refPressure_;
    Scalar refMassfrac_;
    Scalar refTemperature_;

    Scalar sinusVAmplitude_;
    Scalar sinusVPeriod_;
    Scalar sinusVPhaseShift_;
    bool useVelDataFile_;
    std::string velDataFile_;
    std::vector<double> velocityData_[2];
    Scalar sinusPAmplitude_;
    Scalar sinusPPeriod_;
    Scalar sinusPPhaseShift_;
    Scalar sinusXAmplitude_;
    Scalar sinusXPeriod_;
    Scalar sinusXPhaseShift_;
    bool useMassFractionDataFile_;
    std::string relativeHumidityDataFile_;
    std::string massFractionDataFile_;
    std::vector<double> relativeHumidityData_[2];
    std::vector<double> massFractionData_[2];
    Scalar sinusTAmplitude_;
    Scalar sinusTPeriod_;
    Scalar sinusTPhaseShift_;
    bool useTemperatureDataFile_;
    std::string temperatureDataFile_;
    std::vector<double> temperatureData_[2];

    Scalar runUpDistanceX1_;
    Scalar runUpDistanceX2_;
    Scalar runUpDistanceY_;
    Scalar initializationTime_;
};
} //end namespace

#endif // DUMUX_ZEROEQTWOCNI_SUBPROBLEM_HH
