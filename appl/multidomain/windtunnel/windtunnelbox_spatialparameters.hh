// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Spatial parameters for evaporation problems.
 */
#ifndef DUMUX_WINDTUNNELSPATIALPARAMETERS_HH
#define DUMUX_WINDTUNNELSPATIALPARAMETERS_HH

#include <dune/grid/io/file/vtk/common.hh>

#include <dumux/material/spatialparams/implicit.hh>
#include "dumux/material/spatialparams/gstatrandomfield.hh"
#include <dumux/material/fluidmatrixinteractions/2p/efftoabslaw.hh>
#include <dumux/material/fluidmatrixinteractions/2p/linearmaterial.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedbrookscorey.hh>
#include <dumux/material/fluidmatrixinteractions/2p/regularizedvangenuchten.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivityjohansen.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivitysomerton.hh>
#include <dumux/io/plotmateriallaw.hh>
#include <dumux/io/plotthermalconductivitymodel.hh>

namespace Dumux
{

//forward declaration
template<class TypeTag>
class WindTunnelSpatialParams;

namespace Properties
{
// The spatial parameters TypeTag
NEW_TYPE_TAG(WindTunnelSpatialParams);

// Set the spatial parameters
SET_TYPE_PROP(WindTunnelSpatialParams, SpatialParams, WindTunnelSpatialParams<TypeTag>);

// Johansen is used as model to compute the effective thermal heat conductivity
SET_TYPE_PROP(WindTunnelSpatialParams, ThermalConductivityModel,
              ThermalConductivityJohansen<typename GET_PROP_TYPE(TypeTag, Scalar)>);

// Set the material Law
NEW_PROP_TAG(EffMaterialLaw);
SET_PROP(WindTunnelSpatialParams, EffMaterialLaw)
{
private:
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
public:
//     typedef VanGenuchten<Scalar> type;
//     typedef RegularizedBrooksCorey<Scalar> type;
    typedef RegularizedVanGenuchten<Scalar> type;
//     typedef RegularizedVanGenuchten<Scalar, LinearizedRegVanGenuchtenParams<Scalar, TypeTag> > type;
};

// Set the material Law
SET_TYPE_PROP(WindTunnelSpatialParams, MaterialLaw, EffToAbsLaw<typename GET_PROP_TYPE(TypeTag, EffMaterialLaw)>);

// Decide whether to plot the porous-medium properties or not
NEW_PROP_TAG(OutputPlotMaterialLaw);
SET_BOOL_PROP(WindTunnelSpatialParams, OutputPlotMaterialLaw, true);

// Set properties of the porous medium
NEW_PROP_TAG(Soil1SolidDensity);
NEW_PROP_TAG(Soil1SolidHeatCapacity);
NEW_PROP_TAG(Soil2SolidDensity);
NEW_PROP_TAG(Soil2SolidHeatCapacity);
SET_SCALAR_PROP(WindTunnelSpatialParams, Soil1SolidDensity, 2700.0);
SET_SCALAR_PROP(WindTunnelSpatialParams, Soil1SolidHeatCapacity, 790.0);
SET_SCALAR_PROP(WindTunnelSpatialParams, Soil2SolidDensity, 2700.0);
SET_SCALAR_PROP(WindTunnelSpatialParams, Soil2SolidHeatCapacity, 790.0);

// Set properties of the porous medium
NEW_PROP_TAG(SpatialParamsRandomField);
NEW_PROP_TAG(SpatialParamsVerticalLayers);
NEW_PROP_TAG(Soil1RegularizationLowSw);
NEW_PROP_TAG(Soil1RegularizationHighSw);
NEW_PROP_TAG(Soil2RegularizationLowSw);
NEW_PROP_TAG(Soil2RegularizationHighSw);
SET_BOOL_PROP(WindTunnelSpatialParams, SpatialParamsRandomField, false);
SET_BOOL_PROP(WindTunnelSpatialParams, SpatialParamsVerticalLayers, false);
SET_SCALAR_PROP(WindTunnelSpatialParams, Soil1RegularizationLowSw, 0.0);
SET_SCALAR_PROP(WindTunnelSpatialParams, Soil1RegularizationHighSw, 1.0);
SET_SCALAR_PROP(WindTunnelSpatialParams, Soil2RegularizationLowSw, 0.0);
SET_SCALAR_PROP(WindTunnelSpatialParams, Soil2RegularizationHighSw, 1.0);
}


/*!
 * \ingroup TwoPTwoCNIModel
 * \ingroup ZeroEqTwoCNIModel
 * \ingroup ImplicitTestProblems
 * \brief Definition of the spatial parameters for
 *        the coupling of a non-isothermal two-component ZeroEq
 *        and a non-isothermal two-phase two-component Darcy model.
 */
template<class TypeTag>
class WindTunnelSpatialParams : public ImplicitSpatialParams<TypeTag>
{
    typedef ImplicitSpatialParams<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView::ctype CoordScalar;

    enum {
        dim=GridView::dimension,
        dimWorld=GridView::dimensionworld
    };

    typedef Dune::FieldVector<CoordScalar,dim> LocalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> GlobalPosition;
    typedef Dune::FieldVector<CoordScalar,dimWorld> DimVector;
    typedef Dune::FieldVector<int,dimWorld> IntVector;
    typedef std::vector<Scalar> ScalarVector;

    typedef typename GridView::IndexSet IndexSet;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<0>::Entity Element;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

    typedef typename GET_PROP(TypeTag, ParameterTree) ParameterTree;
    typedef typename GridView::template Codim<dim>::Iterator VertexIterator;

    typedef typename GET_PROP_TYPE(TypeTag, EffMaterialLaw) EffMaterialLaw;
    typedef EffToAbsLaw<EffMaterialLaw> MaterialLaw;
    typedef typename MaterialLaw::Params MaterialLawParams;
    typedef std::vector<MaterialLawParams> MaterialLawParamsVector;

public:
    /*!
     * \brief Spatial parameters for the
     *        coupling of a non-isothermal two-component ZeroEq
     *        and a non-isothermal two-phase two-component Darcy model.
     *
     * \param gridView The GridView which is used by the problem
     */
    WindTunnelSpatialParams(const GridView& gridView)
        : ParentType(gridView),
          randomPermeability_(gridView.size(dim), 0.0),
          randomPorosity_(gridView.size(dim), 0.0),
          randomSolidThermalConductivity_(gridView.size(dim), 0.0),
          randomSpatialParams_(gridView.size(dim)),
          indexSet_(gridView.indexSet())
    {
        if (dim > 1)
        {
            // domain extents
            Scalar noDarcyX1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1);
            Scalar noDarcyX2 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2);
            std::vector<Scalar> positions0 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions0);
            std::vector<Scalar> positions1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions1);

            bBoxMin_[0] = std::max(positions0.front(),noDarcyX1);
            bBoxMax_[0] = std::min(positions0.back(),noDarcyX2);
            bBoxMin_[1] = positions1.front();
            bBoxMax_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePosY);
            lengthPM_ = bBoxMax_[0] - bBoxMin_[0];
            heightPM_ = bBoxMax_[1] - bBoxMin_[1];
        }

        // soil properties
        plotMaterialLaw_ = GET_PARAM_FROM_GROUP(TypeTag, bool, Output, PlotMaterialLaw);
        randomField_ = GET_PARAM_FROM_GROUP(TypeTag, bool, SpatialParams, RandomField);
        verticalLayers_ = GET_PARAM_FROM_GROUP(TypeTag, bool, SpatialParams, VerticalLayers);
        numberOfSoilLayers_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, SpatialParams, NumberOfSoilLayers);
        firstSoilLayerIdx_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, SpatialParams, SoilLayerIdx1);
        secondSoilLayerIdx_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, SpatialParams, SoilLayerIdx2);

        if (firstSoilLayerIdx_ == 1 || secondSoilLayerIdx_ == 1)
        {
            permeability1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, Permeability);
            porosity1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, Porosity);
            alphaBJ1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, AlphaBJ);
            solidDensity1_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, SolidDensity);
            solidHeatCapacity1_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, SolidHeatCapacity);
            solidThermalConductivity1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, ThermalConductivitySolid);
            spatialParams1_.setSwr(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, Swr));
            spatialParams1_.setSnr(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, Snr));
            spatialParams1_.setVgAlpha(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, VgAlpha));
            spatialParams1_.setVgn(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, VgN));
            spatialParams1_.setVgn(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, VgN));
            Scalar threshold = 0.01 * (1.0 - GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, Swr)
                                           - GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, Snr));
            spatialParams1_.setPcLowSw(std::max(threshold,GET_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, RegularizationLowSw)));
            spatialParams1_.setPcHighSw(std::min(1.0-threshold,GET_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, RegularizationHighSw)));
        }

        if (firstSoilLayerIdx_ == 2 || secondSoilLayerIdx_ == 2)
        {
            permeability2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil2, Permeability);
            porosity2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil2, Porosity);
            alphaBJ2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil2, AlphaBJ);
            solidDensity2_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Soil2, SolidDensity);
            solidHeatCapacity2_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Soil2, SolidHeatCapacity);
            solidThermalConductivity2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil2, ThermalConductivitySolid);
            spatialParams2_.setSwr(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil2, Swr));
            spatialParams2_.setSnr(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil2, Snr));
            spatialParams2_.setVgAlpha(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil2, VgAlpha));
            spatialParams2_.setVgn(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil2, VgN));
            Scalar threshold = 0.01 * (1.0 - GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil2, Swr)
                                           - GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil2, Snr));
            spatialParams2_.setPcLowSw(std::max(threshold,GET_PARAM_FROM_GROUP(TypeTag, Scalar, Soil2, RegularizationLowSw)));
            spatialParams2_.setPcHighSw(std::min(1.0-threshold,GET_PARAM_FROM_GROUP(TypeTag, Scalar, Soil2, RegularizationHighSw)));
        }
    }

    /*!
     * \brief Returns the intrinsic permeability tensor \f$[m^2]\f$
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar intrinsicPermeability(const Element &element,
                                 const FVElementGeometry &fvGeometry,
                                 const int scvIdx) const
    {
        int curSoilType = soilType(element.geometry().corner(scvIdx));
        if (curSoilType == 0)
            return randomPermeability_[indexSet_.index(element.template subEntity<dim> (scvIdx))];
        else if (curSoilType == 1)
            return permeability1_;
        else if (curSoilType == 2)
            return permeability2_;
        else
            DUNE_THROW(Dune::NotImplemented, "This soil type is not implemented");
    }

    /*!
     * \brief Returns the porosity \f$[-]\f$
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar porosity(const Element &element,
                    const FVElementGeometry &fvGeometry,
                    const int scvIdx) const
    {
        int curSoilType = soilType(element.geometry().corner(scvIdx));
        if (curSoilType == 0)
            return randomPorosity_[indexSet_.index(element.template subEntity<dim> (scvIdx))];
        else if (curSoilType == 1)
            return porosity1_;
        else if (curSoilType == 2)
            return porosity2_;
        else
            DUNE_THROW(Dune::NotImplemented, "This soil type is not implemented");
    }


    /*!
     * \brief Returns the parameter object for the material law
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    const MaterialLawParams& materialLawParams(const Element &element,
                                               const FVElementGeometry &fvGeometry,
                                               const int scvIdx) const
    {
        int curSoilType = soilType(element.geometry().corner(scvIdx));
        if (curSoilType == 0)
            return randomSpatialParams_[indexSet_.index(element.template subEntity<dim> (scvIdx))];
        else if (curSoilType == 1)
            return spatialParams1_;
        else if (curSoilType == 2)
            return spatialParams2_;
        else
            DUNE_THROW(Dune::NotImplemented, "This soil type is not implemented");
    }


    /*!
     * \brief Evaluate the Beavers-Joseph coefficient
     *        at the center of a given intersection
     *
     * \param GlobalPosition global Position
     *
     * \return Beavers-Joseph coefficient
     */
    Scalar beaversJosephCoeffAtPos(const GlobalPosition globalPos) const
    {
        int curSoilType = soilType(globalPos);
        if (curSoilType == 0)
            return 1.0;
        else if (curSoilType == 1)
            return alphaBJ1_;
        else if (curSoilType == 2)
            return alphaBJ2_;
        else
            DUNE_THROW(Dune::NotImplemented, "This soil type is not implemented");
    }



    /*!
     * \brief Returns the heat capacity \f$[J / (kg K)]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar solidHeatCapacity(const Element &element,
                             const FVElementGeometry &fvGeometry,
                             const int scvIdx) const
    {
        int curSoilType = soilType(element.geometry().corner(scvIdx));
        if (curSoilType == 0)
            DUNE_THROW(Dune::NotImplemented, "This soil type is not implemented");//randomSolidThermalConductivity_[indexSet_.index(element.template subEntity<dim> (scvIdx))];
        else if (curSoilType == 1)
            return solidHeatCapacity1_;
        else if (curSoilType == 2)
            return solidHeatCapacity2_;
        else
            DUNE_THROW(Dune::NotImplemented, "This soil type is not implemented");
    }

    /*!
     * \brief Returns the mass density \f$[kg / m^3]\f$ of the rock matrix.
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar solidDensity(const Element &element,
                        const FVElementGeometry &fvGeometry,
                        const int scvIdx) const
    {
        int curSoilType = soilType(element.geometry().corner(scvIdx));
        if (curSoilType == 0)
            DUNE_THROW(Dune::NotImplemented, "This soil type is not implemented");//randomSolidThermalConductivity_[indexSet_.index(element.template subEntity<dim> (scvIdx))];
        else if (curSoilType == 1)
            return solidDensity1_;
        else if (curSoilType == 2)
            return solidDensity2_;
        else
            DUNE_THROW(Dune::NotImplemented, "This soil type is not implemented");
    }

    /*!
     * \brief Returns the thermal conductivity \f$[W/(m*K)]\f$ of the solid
     *
     * This is only required for non-isothermal models.
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param scvIdx The local index of the sub-control volume
     */
    Scalar solidThermalConductivity(const Element &element,
                                    const FVElementGeometry &fvGeometry,
                                    const int scvIdx) const
    {
        int curSoilType = soilType(element.geometry().corner(scvIdx));
        if (curSoilType == 0)
            return randomSolidThermalConductivity_[indexSet_.index(element.template subEntity<dim> (scvIdx))];
        else if (curSoilType == 1)
            return solidThermalConductivity1_;
        else if (curSoilType == 2)
            return solidThermalConductivity2_;
        else
            DUNE_THROW(Dune::NotImplemented, "This soil type is not implemented");
    }

    /*!
     * \brief Returns the relative position between 0 and 1 of a global coordinate
     */
    GlobalPosition relativePosition(const GlobalPosition &globalPos) const
    {
        GlobalPosition relativePosition(0.0);
        relativePosition[0] = (globalPos[0] - bBoxMin_[0]) / lengthPM_;
        relativePosition[1] = (globalPos[1] - bBoxMin_[1]) / heightPM_;
        return relativePosition;
    }

    /*!
     * \brief Returns the layerIdx of a global coordinate
     */
    IntVector layerIdx(const GlobalPosition &globalPos) const
    {
        if (dim == 1)
            return IntVector(0);

        GlobalPosition curRelativePosition = relativePosition(globalPos);
        curRelativePosition *= numberOfSoilLayers_ * 1.0;

        IntVector layerIdx(0);
        layerIdx[0] = int(floor(curRelativePosition[0]));
        layerIdx[0] = std::max(layerIdx[0], 0);
        layerIdx[0] = std::min(layerIdx[0], numberOfSoilLayers_-1);
        layerIdx[1] = int(floor(curRelativePosition[1]));
        layerIdx[1] = std::max(layerIdx[1], 0);
        layerIdx[1] = std::min(layerIdx[1], numberOfSoilLayers_-1);
        return layerIdx;
    }

    /*!
     * \brief Returns global position of the heterogeneity locations
     */
    std::vector<Scalar> heterogeneityPos() const
    {
        std::vector<Scalar> heterogeneityPos(numberOfSoilLayers_-1, bBoxMin_[0]);
        heterogeneityPos[0] = bBoxMin_[0] + lengthPM_ / numberOfSoilLayers_;
        for (int idx = 1; idx < heterogeneityPos.size(); idx++)
        {
            heterogeneityPos[idx] = heterogeneityPos[idx-1] + lengthPM_ / numberOfSoilLayers_;
        }
        return heterogeneityPos;
    }

    /*!
     * \brief Returns the the soil typ at a global coordinate
     */
    int soilType(const GlobalPosition &globalPos) const
    {
        if (randomField_)
            return 0;

        if (dim == 1)
            return firstSoilLayerIdx_;

        int curLayerID = layerIdx(globalPos)[0];
        if (verticalLayers_)
            curLayerID = layerIdx(globalPos)[1];
        if (curLayerID % 2 == 0)
            return firstSoilLayerIdx_;
        return secondSoilLayerIdx_;
    }

    /*!
     * \brief This method allows the generation of a statistical field using GStat
     *
     * Because gstat is not open source and has to be installed manually,
     * the content of this function is deactivated (commented) by
     * default. If you have gstat installed, please uncomment
     * the lines between the curly brackets.
     *
     * \param gridView The GridView which is used by the problem
     */
    void initRandomField(const GridView& gridView)
    {
        if (!randomField_)
            return;

        bool create = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool , Gstat, GenerateNewPermeability);
        bool setOnlyTopLayer = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, Gstat, SetOnlyTopLayer);
        std::string permeabilityFileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Gstat, PermeabilityInputFileName);
        std::string gStatControlFileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Gstat, ControlFileName);
        std::string gStatInputFileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Gstat, InputFileName);
        Scalar interfacePosY = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePosY);

        // create random permeability object
        GstatRandomField<GridView, Scalar> randomField(gridView);
        randomField.create(gStatControlFileName,
                           gStatInputFileName,
                           permeabilityFileName,
                           GstatRandomField<GridView, Scalar>::FieldType::log10,
                           create);

        Scalar totalVolume = 0;
        Scalar meanPermeability = 0;

        // have to be resized because in multidomain problems, grid is set up after constructor
        randomPermeability_.resize(gridView.size(dim), 0.0);
        randomPorosity_.resize(gridView.size(dim), 0.0);
        randomSolidThermalConductivity_.resize(gridView.size(dim), 0.0);
        randomSpatialParams_.resize(gridView.size(dim));

        //TODO: only for 2D elements and rectangular grids
        const unsigned numVertices = 4;
        for (unsigned vertexIdx=0; vertexIdx<numVertices; ++vertexIdx)
        {
            ElementIterator eItEnd = gridView.template end<0> ();
            for (ElementIterator eIt = gridView.template begin<0> (); eIt
                     != eItEnd; ++eIt)
            {
                // for isotropic media
                const Scalar perm = randomField.data(*eIt);
                randomPermeability_[indexSet_.index(eIt->template subEntity<dim> (vertexIdx))]
                    = perm;

                const Scalar volume = eIt->geometry().volume();
                meanPermeability += volume / perm;
                totalVolume += volume;
            }
        }
        meanPermeability /= totalVolume;
        meanPermeability = 1.0 / meanPermeability;

        ElementIterator eItEnd = gridView.template end<0> ();
        for (ElementIterator eIt = gridView.template begin<0> (); eIt
                  != eItEnd; ++eIt)
        {
            for (unsigned vertexIdx=0; vertexIdx<numVertices; ++vertexIdx)
            {
                GlobalPosition global = eIt->geometry().corner(vertexIdx);
                if (global[1] < interfacePosY - 1e-6 && setOnlyTopLayer)
                {
                    randomPermeability_[indexSet_.index(eIt->template subEntity<dim> (vertexIdx))]
                        = meanPermeability;
                }
                else
                {
                    randomPermeability_[indexSet_.index(eIt->template subEntity<dim> (vertexIdx))]
                        = randomField.data(*eIt);
                }
            }
        }

        //Iterate over elements
        VertexIterator vItEnd = gridView.template end<dim> ();
        for (VertexIterator vIt = gridView.template begin<dim> (); vIt
                 != vItEnd; ++vIt)
        {
            int globalIdx = indexSet_.index(*vIt);
            randomPorosity_[globalIdx] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, Porosity);
            randomSolidThermalConductivity_[globalIdx] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, ThermalConductivitySolid);

            // heterogeneous media with Van Genuchten
            // assumes Van-Genuchten alpha to be the inverse entry pressure
            Scalar elementEntryPressure = 1./GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, VgAlpha);
            elementEntryPressure *= sqrt(meanPermeability / randomPermeability_[globalIdx]);
            randomSpatialParams_[globalIdx].setVgAlpha(1./elementEntryPressure);
            randomSpatialParams_[globalIdx].setVgn(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, VgN));
            randomSpatialParams_[globalIdx].setSwr(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, Swr));
            randomSpatialParams_[globalIdx].setSnr(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Soil1, Snr));
        }

        randomField.writeVtk("permeability", "absolute permeability");
    }

    /*!
     * \brief This is called from the coupled problem and creates
     *        a gnuplot output of the Pc-Sw curve
     */
    void plotMaterialLaw()
    {
        if (plotMaterialLaw_ && !randomField_)
        {
            GnuplotInterface<Scalar> gnuplot;
            PlotMaterialLaw<TypeTag> plotMaterialLaw;
            PlotThermalConductivityModel<TypeTag> plotThermalConductivityModel(293.0, 1e5);

            if (firstSoilLayerIdx_ == 1 || secondSoilLayerIdx_ == 1)
                plotMaterialLaw.addpcswcurve(gnuplot, spatialParams1_, 0.0, 1.0, "pcsw_soil1.dat");
            if (firstSoilLayerIdx_ == 2 || secondSoilLayerIdx_ == 2)
                plotMaterialLaw.addpcswcurve(gnuplot, spatialParams2_, 0.0, 1.0, "pcsw_soil2.dat");
            gnuplot.plot("pc-Sw");

            gnuplot.resetAll();
            if (firstSoilLayerIdx_ == 1 || secondSoilLayerIdx_ == 1)
            {
                plotMaterialLaw.addkrcurves(gnuplot, spatialParams1_, 0.0, 1.0, "soil1.dat");
            }
            if (firstSoilLayerIdx_ == 2 || secondSoilLayerIdx_ == 2)
            {
                plotMaterialLaw.addkrcurves(gnuplot, spatialParams2_, 0.0, 1.0, "soil2.dat");
            }
            gnuplot.plot("kr");

//             gnuplot.resetAll();
//             if (firstSoilLayerIdx_ == 1 || secondSoilLayerIdx_ == 1)
//                 plotThermalConductivityModel.addlambdaeffcurve(gnuplot, porosity1_, 2700.0, solidThermalConductivity1_, 0.0, 1.0, "lambda_eff_soil1");
//             if (firstSoilLayerIdx_ == 2 || secondSoilLayerIdx_ == 2)
//                 plotThermalConductivityModel.addlambdaeffcurve(gnuplot, porosity2_, 2700.0, solidThermalConductivity2_, 0.0, 1.0, "lambda_eff_soil2");
//             gnuplot.plot("lambdaeff");
        }
    }

private:
    GlobalPosition bBoxMin_;
    GlobalPosition bBoxMax_;
    Scalar lengthPM_;
    Scalar heightPM_;

    bool plotMaterialLaw_;
    bool randomField_;
    int numberOfSoilLayers_;
    int firstSoilLayerIdx_;
    int secondSoilLayerIdx_;
    bool verticalLayers_;

    Scalar permeability1_;
    Scalar porosity1_;
    Scalar alphaBJ1_;
    Scalar solidDensity1_;
    Scalar solidHeatCapacity1_;
    Scalar solidThermalConductivity1_;
    MaterialLawParams spatialParams1_;

    Scalar permeability2_;
    Scalar porosity2_;
    Scalar alphaBJ2_;
    Scalar solidDensity2_;
    Scalar solidHeatCapacity2_;
    Scalar solidThermalConductivity2_;
    MaterialLawParams spatialParams2_;

    ScalarVector randomPermeability_;
    ScalarVector randomPorosity_;
    ScalarVector randomSolidThermalConductivity_;
    MaterialLawParamsVector randomSpatialParams_;
    const IndexSet& indexSet_;
};
} // end namespace

#endif // DUMUX_WINDTUNNELSPATIALPARAMETERS_HH
