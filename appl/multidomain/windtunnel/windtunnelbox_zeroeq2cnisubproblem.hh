// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \brief Non-isothermal two-component ZeroEq subproblem with air flowing
 *        from the left to the right and coupling at the bottom.
 */
#ifndef DUMUX_WINDTUNNELZEROEQTWOCNISUBPROBLEM_HH
#define DUMUX_WINDTUNNELZEROEQTWOCNISUBPROBLEM_HH

#include <dumux/freeflow/zeroeqncni/model.hh>
#include <dumux/multidomain/subdomainpropertydefaults.hh>
#include <dumux/multidomain/2cnistokes2p2cni/stokesncnicouplinglocalresidual.hh>

#include <dumux/io/readwritedatafile.hh>

namespace Dumux
{

template <class TypeTag>
class FreeFlowSubProblem;

namespace Properties
{
NEW_TYPE_TAG(FreeFlowSubProblem,
             INHERITS_FROM(BoxZeroEqncni, SubDomain));

// Set the problem property
SET_TYPE_PROP(FreeFlowSubProblem, Problem, Dumux::FreeFlowSubProblem<TypeTag>);

// Use the StokesncniCouplingLocalResidual for the computation of the local residual in the free-flow domain
SET_TYPE_PROP(FreeFlowSubProblem, LocalResidual, StokesncniCouplingLocalResidual<TypeTag>);

// Used the fluid system from the coupled problem
SET_TYPE_PROP(FreeFlowSubProblem, FluidSystem,
              typename GET_PROP_TYPE(typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag), FluidSystem));

// Set number of intervals for the evaluation of flow properties at the wall
SET_INT_PROP(FreeFlowSubProblem, NumberOfIntervals, 250);

// Disable use of mole formulation
SET_BOOL_PROP(FreeFlowSubProblem, UseMoles, false);

// Disable gravity
SET_BOOL_PROP(FreeFlowSubProblem, ProblemEnableGravity, false);

// switch inertia term on or off
#if STOKES
SET_BOOL_PROP(FreeFlowSubProblem, EnableNavierStokes, false);
#else
SET_BOOL_PROP(FreeFlowSubProblem, EnableNavierStokes, true);
#endif

// Ensures that we are on the correct dumux branch
SET_SCALAR_PROP(FreeFlowSubProblem, FreeFlowTurbulentSchmidtNumber, 1.0);
SET_SCALAR_PROP(FreeFlowSubProblem, FreeFlowTurbulentPrandtlNumber, 1.0);

// Use symmetry on top
NEW_PROP_TAG(FreeFlowTopIsSymmetry);
SET_BOOL_PROP(FreeFlowSubProblem, FreeFlowTopIsSymmetry, true);

// Depth in third dimension
NEW_PROP_TAG(GridExtrusionFactor);
SET_SCALAR_PROP(FreeFlowSubProblem, GridExtrusionFactor, 1.0);

// Smallest grid cells for turbulence properties
NEW_PROP_TAG(GridSmallestGridCellY);
NEW_PROP_TAG(GridSmallestGridCellZ);
SET_SCALAR_PROP(FreeFlowSubProblem, GridSmallestGridCellY, 0.0);
SET_SCALAR_PROP(FreeFlowSubProblem, GridSmallestGridCellZ, 0.0);

// Set the properties for variable inflow BC
NEW_PROP_TAG(FreeFlowSinusVelocityAmplitude);
NEW_PROP_TAG(FreeFlowSinusVelocityPeriod);
NEW_PROP_TAG(FreeFlowSinusVelocityPhaseShift);
SET_SCALAR_PROP(FreeFlowSubProblem, FreeFlowSinusVelocityAmplitude, 0.0);
SET_SCALAR_PROP(FreeFlowSubProblem, FreeFlowSinusVelocityPeriod, 3600.0);
SET_SCALAR_PROP(FreeFlowSubProblem, FreeFlowSinusVelocityPhaseShift, 0.0);
NEW_PROP_TAG(FreeFlowSinusPressureAmplitude);
NEW_PROP_TAG(FreeFlowSinusPressurePeriod);
NEW_PROP_TAG(FreeFlowSinusPressurePhaseShift);
SET_SCALAR_PROP(FreeFlowSubProblem, FreeFlowSinusPressureAmplitude, 0.0);
SET_SCALAR_PROP(FreeFlowSubProblem, FreeFlowSinusPressurePeriod, 3600.0);
SET_SCALAR_PROP(FreeFlowSubProblem, FreeFlowSinusPressurePhaseShift, 0.0);
NEW_PROP_TAG(FreeFlowSinusConcentrationAmplitude);
NEW_PROP_TAG(FreeFlowSinusConcentrationPeriod);
NEW_PROP_TAG(FreeFlowSinusConcentrationPhaseShift);
SET_SCALAR_PROP(FreeFlowSubProblem, FreeFlowSinusConcentrationAmplitude, 0.0);
SET_SCALAR_PROP(FreeFlowSubProblem, FreeFlowSinusConcentrationPeriod, 3600.0);
SET_SCALAR_PROP(FreeFlowSubProblem, FreeFlowSinusConcentrationPhaseShift, 0.0);
NEW_PROP_TAG(FreeFlowSinusTemperatureAmplitude);
NEW_PROP_TAG(FreeFlowSinusTemperaturePeriod);
NEW_PROP_TAG(FreeFlowSinusTemperaturePhaseShift);
SET_SCALAR_PROP(FreeFlowSubProblem, FreeFlowSinusTemperatureAmplitude, 0.0);
SET_SCALAR_PROP(FreeFlowSubProblem, FreeFlowSinusTemperaturePeriod, 3600.0);
SET_SCALAR_PROP(FreeFlowSubProblem, FreeFlowSinusTemperaturePhaseShift, 3600.0);

// Forward declarations
NEW_PROP_TAG(TimeManagerInitTime);
}

/*!
 * \ingroup ImplicitTestProblems
 * \ingroup MultidomainProblems
 * \brief ZeroEq2cni problem with air flowing from the left to the right.
 *
 * \todo update test description
 * This sub problem uses the \ref ZeroEqNCNIModel. It is part of the 2cnizeroeq2p2cni model and
 * is combined with the 2p2csubproblem for the Darcy domain.
 */
template <class TypeTag>
class FreeFlowSubProblem : public ZeroEqProblem<TypeTag>
{
    typedef FreeFlowSubProblem<TypeTag> ThisType;
    typedef ZeroEqProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum {
        dim = GridView::dimension
    };
    enum {
        // equation indices
        massBalanceIdx = Indices::massBalanceIdx,
        momentumXIdx = Indices::momentumXIdx, // Index of the x-component of the momentum balance
        momentumYIdx = Indices::momentumYIdx, // Index of the y-component of the momentum balance
        momentumZIdx = Indices::momentumZIdx, // Index of the z-component of the momentum balance
        transportEqIdx = Indices::transportEqIdx, // Index of the transport equation (massfraction)
        energyEqIdx =    Indices::energyEqIdx     // Index of the energy equation (temperature)
    };
    enum { // primary variable indices
        pressureIdx = Indices::pressureIdx,
        velocityXIdx = Indices::velocityXIdx,
        velocityYIdx = Indices::velocityYIdx,
        velocityZIdx = Indices::velocityZIdx,
        massOrMoleFracIdx = Indices::massOrMoleFracIdx,
        temperatureIdx = Indices::temperatureIdx
    };
    enum { phaseIdx = Indices::phaseIdx };
    enum { numComponents = Indices::numComponents };
    enum {
        transportCompIdx = Indices::transportCompIdx, // water component index
        phaseCompIdx = Indices::phaseCompIdx          // air component index
    };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::ctype CoordScalar;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<CoordScalar, dim> GlobalPosition;


public:
    /*!
     * \brief The sub-problem for the ZeroEq subdomain
     *
     * \param timeManager The TimeManager which is used by the simulation
     * \param gridView The simulation's idea about physical space
     */
    FreeFlowSubProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        try {
            velDataFile_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, FreeFlow, VelocityDataFile);
            useVelDataFile_ = true;
            readData(velDataFile_, velocityData_);
//             checkData(velocityData_, "velocity");
        }
        catch (...) {
            useVelDataFile_ = false;
        }

        try {
            massFractionDataFile_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, FreeFlow, MassFractionDataFile);
            useMassFractionDataFile_ = true;
            readData(massFractionDataFile_, massFractionData_);
        }
        catch (...) {
            useMassFractionDataFile_ = false;
        }

        try {
            temperatureDataFile_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, FreeFlow, TemperatureDataFile);
            useTemperatureDataFile_ = true;
            readData(temperatureDataFile_, temperatureData_);
        }
        catch (...) {
            useTemperatureDataFile_ = false;
        }

        std::vector<Scalar> positions0 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions0);
        std::vector<Scalar> positions1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions1);

        bBoxMin_[0] = positions0.front();
        bBoxMax_[0] = positions0.back();
        bBoxMin_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePosY);
        bBoxMax_[1] = positions1.back();
        extrusionFactor_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, ExtrusionFactor);
        topIsSymmetry_ = GET_PARAM_FROM_GROUP(TypeTag, bool, FreeFlow, TopIsSymmetry);
#if STOKES
        topIsSymmetry_ = true;
#endif

        // parts of the interface without coupling
        try { runUpDistanceX1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, RunUpDistanceX1); }
        catch (...) { runUpDistanceX1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1); }
        try { runUpDistanceX2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, RunUpDistanceX2); }
        catch (...) { runUpDistanceX2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2); }

        refVelocity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefVelocity);
        refPressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefPressure);
        refTemperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefTemperature);
        try {
            refMassFrac_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefMassFraction);
        }
        catch (...) {
            FluidState fluidState;
            fluidState.setPressure(phaseIdx, refPressure_);
            fluidState.setTemperature(refTemperature_);
            Scalar relativeHumidity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefRelativeHumidity);
            fluidState.template setRelativeHumidity<FluidState>(fluidState, phaseIdx, transportCompIdx, relativeHumidity);
            refMassFrac_ = fluidState.massFraction(phaseIdx, transportCompIdx);
        }

        initializationTime_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, InitTime);

        if (useTemperatureDataFile_ && !useMassFractionDataFile_)
        {
            try {
                relativeHumidityDataFile_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, FreeFlow, RelativeHumidityDataFile);
                useMassFractionDataFile_ = true;
                readData(relativeHumidityDataFile_, relativeHumidityData_);

                FluidState fluidState;
                fluidState.setPressure(phaseIdx, refPressure_);
                for (unsigned int i = 0; i < relativeHumidityData_[0].size()
                                         && i < temperatureData_[0].size(); ++i)
                {
                    fluidState.setTemperature(temperatureData_[1][i]);
                    fluidState.template setRelativeHumidity<FluidState>(fluidState, phaseIdx, transportCompIdx, relativeHumidityData_[1][i]);
                    massFractionData_[0].push_back(relativeHumidityData_[0][i]/*the time*/);
                    massFractionData_[1].push_back(fluidState.massFraction(phaseIdx, transportCompIdx));
                }
                checkData(massFractionData_, "massFraction");
                std::string dataName[2] = {"Time[s]", "massFraction[-]"};
                writeDataFile(massFractionData_, dataName, "massFraction.dat");
            }
            catch (...) {
                useMassFractionDataFile_ = false;
            }
        }
    }

    /*!
     * \todo bBox functions have to be overwritten, otherwise they remain uninitialised
     */
    //! \copydoc BoxProblem::&bBoxMin()
    const GlobalPosition &bBoxMin() const
    { return bBoxMin_; }

    //! \copydoc BoxProblem::&bBoxMax()
    const GlobalPosition &bBoxMax() const
    { return bBoxMax_; }

    /*!
     * \name Problem parameters
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::name()
    const std::string name() const
    {
        return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Output, Name)
               + std::string("_box-ff");
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::boundaryTypesAtPos()
   void boundaryTypesAtPos(BoundaryTypes &values, const GlobalPosition &globalPos) const
    {
        const Scalar time = this->timeManager().time();

        // Boundary conditions for:
        // momentum, component, and energy
        values.setAllDirichlet();

        if (onUpperBoundary_(globalPos))
        {
            values.setNeumann(transportEqIdx);
            values.setNeumann(energyEqIdx);
            if (topIsSymmetry_)
            {
                values.setNeumann(transportEqIdx);
                values.setNeumann(energyEqIdx);
                values.setNeumann(velocityXIdx);
                values.setDirichlet(velocityYIdx, momentumYIdx);
            }
        }

        if (onLowerBoundary_(globalPos))
        {
            values.setNeumann(transportEqIdx);
            values.setNeumann(energyEqIdx);
            if (onCouplingInterface(globalPos) && time >= initializationTime_)
            {
                values.setAllCouplingDirichlet();
                values.setCouplingNeumann(momentumXIdx);
                values.setCouplingNeumann(momentumYIdx);
            }
        }

        if (onRightBoundary_(globalPos))
        {
            values.setAllOutflow();
            if (onUpperBoundary_(globalPos) || onLowerBoundary_(globalPos)) // corner point
            {
                values.setAllDirichlet();
            }
        }

        // if inflow boundaries are Neumann evaporative fluxes are less grid dependent
        if (onLeftBoundary_(globalPos))
        {
            values.setAllDirichlet();
        }


        // Boundary conditions for:
        // mass
        values.setOutflow(massBalanceIdx);

        // set pressure at one point, do NOT specify this
        // if the Darcy domain has a Dirichlet condition for pressure
        if (onRightBoundary_(globalPos))
        {
            if (time > initializationTime_)
                values.setDirichlet(pressureIdx, massBalanceIdx);
            else
                if (!onLowerBoundary_(globalPos) && !onUpperBoundary_(globalPos))
                    values.setDirichlet(pressureIdx, massBalanceIdx);
        }
    }

    //! \copydoc Dumux::ImplicitProblem::dirichletAtPos()
     void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        values = 0.0;

        initial_(values, globalPos);
    }

    //! \copydoc Dumux::ImplicitProblem::neumannAtPos()
    void neumannAtPos(PrimaryVariables &values,const GlobalPosition &globalPos) const
    {

        values = 0.;

        FluidState fluidState;
        if (useTemperatureDataFile_)
            fluidState.setTemperature(evaluateData(temperatureData_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize()));
        else
            fluidState.setTemperature(refTemperature());

        fluidState.setPressure(phaseIdx, refPressure());

        Scalar massFraction[numComponents];
        if (useMassFractionDataFile_)
            massFraction[transportCompIdx] = evaluateData(massFractionData_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize());
        else
            massFraction[transportCompIdx] = refMassfrac();
        massFraction[phaseCompIdx] = 1 - massFraction[transportCompIdx];

        fluidState.setMassFraction(phaseIdx, transportCompIdx, massFraction[transportCompIdx]);

        const Scalar density = FluidSystem::density(fluidState, phaseIdx);
        const Scalar enthalpy = FluidSystem::enthalpy(fluidState, phaseIdx);
        const Scalar xVelocity = xVelocity_(globalPos);

        if (onLeftBoundary_(globalPos)
            && globalPos[1] > bBoxMin_[1] && globalPos[1] < bBoxMax_[1])
        {
            values[transportEqIdx] = -xVelocity * density * massFraction[transportCompIdx];
            values[energyEqIdx] = -xVelocity * density * enthalpy;
        }
    }

    // \}

    //! \copydoc Dumux::ImplicitProblem::sourceAtPos()
     void sourceAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        // The source term of the mass balance has to be chosen as
        // div (q_momentum) in the problem file
        values = Scalar(0);
    }

    //! \copydoc Dumux::ImplicitProblem::initialAtPos()
    void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        initial_(values, globalPos);
    }
    // \}

    //! \brief Returns the velocity at the inflow.
    const Scalar refVelocity() const
    {
        return refVelocity_ + variation_(GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusVelocityAmplitude),
                                         GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusVelocityPeriod),
                                         GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusVelocityPhaseShift));
    }

    //! \brief Returns the pressure at the inflow.
    const Scalar refPressure() const
    {
        return refPressure_ + variation_(GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusPressureAmplitude),
                                         GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusPressurePeriod),
                                         GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusPressurePhaseShift));
    }

    //! \brief Returns the mass fraction at the inflow.
    const Scalar refMassfrac() const
    {
        return refMassFrac_ + variation_(GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusConcentrationAmplitude),
                                         GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusConcentrationPeriod),
                                         GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusConcentrationPhaseShift));
    }

    //! \brief Returns the temperature at the inflow.
    const Scalar refTemperature() const
    {
        return refTemperature_ + variation_(GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusTemperatureAmplitude),
                                            GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusTemperaturePeriod),
                                            GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, SinusTemperaturePhaseShift));
    }

    /*!
     * \brief The depth of the problem in third dimension
     */
    Scalar extrusionFactorAtPos(const GlobalPosition &globalPos) const
    { return extrusionFactor_; }

    /*!
     * \brief Decide whether to use open top or not
     */
    bool bBoxMaxIsWall()
    {
        return !GET_PARAM_FROM_GROUP(TypeTag, bool, FreeFlow, TopIsSymmetry);
    }

private:
    /*!
     * \brief Internal method for the initial condition
     */
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        values[velocityXIdx] = xVelocity_(globalPos);
        values[velocityYIdx] = 0.;

        values[pressureIdx] = refPressure()
            + 1.189 * this->gravity()[1] * (globalPos[1] - bBoxMin_[1]);

        if (useMassFractionDataFile_)
          values[massOrMoleFracIdx] = evaluateData(massFractionData_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize());
        else
          values[massOrMoleFracIdx] = refMassfrac();

        if (useTemperatureDataFile_)
          values[temperatureIdx] = evaluateData(temperatureData_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize());
        else
          values[temperatureIdx] = refTemperature();
    }

    const Scalar xVelocity_(const GlobalPosition &globalPos) const
    {
        if (onUpperBoundary_(globalPos) && !topIsSymmetry_)
            return 0.0;

        if (onLowerBoundary_(globalPos))
            return 0.0;

        if (useVelDataFile_)
          return evaluateData(velocityData_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize());

        return refVelocity();
    }

    // can be used for the variation of a boundary condition
    const Scalar variation_(const Scalar amplitude, const Scalar period, const Scalar phaseShift) const
    { return sin(2*M_PI*this->timeManager().time()/period+phaseShift) * amplitude; }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < bBoxMin_[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > bBoxMax_[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < bBoxMin_[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > bBoxMax_[1] - eps_; }

    bool onCouplingInterface(const GlobalPosition &globalPos) const
    {
        return globalPos[1] < bBoxMin_[1] + eps_
               && globalPos[0] > runUpDistanceX1_ + eps_
               && globalPos[0] < runUpDistanceX2_ - eps_;
    }

    static constexpr Scalar eps_ = 1e-8;
    GlobalPosition bBoxMin_;
    GlobalPosition bBoxMax_;
    Scalar extrusionFactor_;
    bool topIsSymmetry_;

    Scalar refVelocity_;
    Scalar refPressure_;
    Scalar refMassFrac_;
    Scalar refTemperature_;

    bool useVelDataFile_;
    std::string velDataFile_;
    std::vector<double> velocityData_[2];
    bool useMassFractionDataFile_;
    std::string relativeHumidityDataFile_;
    std::string massFractionDataFile_;
    std::vector<double> relativeHumidityData_[2];
    std::vector<double> massFractionData_[2];
    bool useTemperatureDataFile_;
    std::string temperatureDataFile_;
    std::vector<double> temperatureData_[2];

    Scalar runUpDistanceX1_;
    Scalar runUpDistanceX2_;
    Scalar initializationTime_;
};
} //end namespace

#endif // DUMUX_WINDTUNNELZEROEQTWOCNISUBPROBLEM_HH
