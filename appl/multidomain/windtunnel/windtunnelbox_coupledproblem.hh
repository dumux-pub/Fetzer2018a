// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The problem class for the coupling of a non-isothermal two-component
 *        free flow and and a non-isothermal two-phase two-component
 *        porous medium model.
 */
#ifndef DUMUX_WINDTUNNELCOUPLEDPROBLEM_HH
#define DUMUX_WINDTUNNELCOUPLEDPROBLEM_HH

#include <dune/grid/multidomaingrid.hh>
#include <dune/grid/common/gridinfo.hh>
#include <dune/grid/io/file/dgfparser.hh>

#include <dumux/material/fluidsystems/h2oair.hh>
#include <dumux/multidomain/problem.hh>
#include <dumux/multidomain/2cstokes2p2c/newtoncontroller.hh>
#if !TRANSPORT
#include <dumux/multidomain/2cnistokes2p2cni/localoperator.hh>
#include <dumux/multidomain/2cnistokes2p2cni/problem.hh>
#include <dumux/multidomain/2cnistokes2p2cni/propertydefaults.hh>
#else
#include <dumux/multidomain/2cnitransport2p2cni/localoperator.hh>
#include <dumux/multidomain/2cnitransport2p2cni/propertydefaults.hh>
#endif

#include "windtunnelbox_spatialparameters.hh"
#include "windtunnelbox_2p2cnisubproblem.hh"
#if !TRANSPORT
#include "windtunnelbox_zeroeq2cnisubproblem.hh"
#else
#include "windtunnelbox_transport2cnisubproblem.hh"
#endif

namespace Dumux
{
template <class TypeTag>
class WindTunnelCoupledProblem;

namespace Properties
{
#if !TRANSPORT
NEW_TYPE_TAG(WindTunnelCoupledProblem, INHERITS_FROM(TwoCNIStokesTwoPTwoCNI));

// Set the local coupling operator
SET_TYPE_PROP(WindTunnelCoupledProblem, MultiDomainCouplingLocalOperator,
              Dumux::TwoCNIStokesTwoPTwoCNILocalOperator<TypeTag>);
#else
NEW_TYPE_TAG(WindTunnelCoupledProblem, INHERITS_FROM(TwoCNITransportTwoPTwoCNI));

// Set the local coupling operator
SET_TYPE_PROP(WindTunnelCoupledProblem, MultiDomainCouplingLocalOperator,
              Dumux::TwoCNITransportTwoPTwoCNILocalOperator<TypeTag>);
#endif

// Set the grid type
SET_TYPE_PROP(WindTunnelCoupledProblem, Grid, Dune::YaspGrid<2, Dune::TensorProductCoordinates<typename GET_PROP_TYPE(TypeTag, Scalar), 2> >);

// Set the global problem
SET_TYPE_PROP(WindTunnelCoupledProblem, Problem, WindTunnelCoupledProblem<TypeTag>);

// Set the two sub-problems of the global problem
SET_TYPE_PROP(WindTunnelCoupledProblem, SubDomain1TypeTag, TTAG(FreeFlowSubProblem));
SET_TYPE_PROP(WindTunnelCoupledProblem, SubDomain2TypeTag, TTAG(PorousMediumSubProblem));

// Set the global problem in the context of the two sub-problems
SET_TYPE_PROP(FreeFlowSubProblem, MultiDomainTypeTag, TTAG(WindTunnelCoupledProblem));
SET_TYPE_PROP(PorousMediumSubProblem, MultiDomainTypeTag, TTAG(WindTunnelCoupledProblem));

// Set the other sub-problem for each of the two sub-problems
SET_TYPE_PROP(FreeFlowSubProblem, OtherSubDomainTypeTag, TTAG(PorousMediumSubProblem));
SET_TYPE_PROP(PorousMediumSubProblem, OtherSubDomainTypeTag, TTAG(FreeFlowSubProblem));

// Set the same spatial parameters for both sub-problems
SET_TYPE_PROP(FreeFlowSubProblem, SpatialParams, Dumux::WindTunnelSpatialParams<TypeTag>);
SET_TYPE_PROP(PorousMediumSubProblem, SpatialParams, Dumux::WindTunnelSpatialParams<TypeTag>);

// Set the fluid system
SET_TYPE_PROP(WindTunnelCoupledProblem, FluidSystem,
              FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar)>);

// Use a smaller FluidSystem table
NEW_PROP_TAG(ProblemUseSmallFluidSystemTable);
SET_BOOL_PROP(WindTunnelCoupledProblem, ProblemUseSmallFluidSystemTable, false);

// If UMFPack is not available, SuperLU solver is used:
#ifdef HAVE_UMFPACK
SET_TYPE_PROP(WindTunnelCoupledProblem, LinearSolver, UMFPackBackend<TypeTag>);
#else
SET_TYPE_PROP(WindTunnelCoupledProblem, LinearSolver, SuperLUBackend<TypeTag>);
#endif

// Set the output frequencies
NEW_PROP_TAG(OutputFreqRestart);
SET_INT_PROP(WindTunnelCoupledProblem, OutputFreqRestart, 10);
NEW_PROP_TAG(OutputFreqFluxOutput);
SET_INT_PROP(WindTunnelCoupledProblem, OutputFreqFluxOutput, 100000);
SET_INT_PROP(PorousMediumSubProblem, OutputFreqFluxOutput, 100000);
NEW_PROP_TAG(OutputFreqOutput);
SET_INT_PROP(WindTunnelCoupledProblem, OutputFreqOutput, 5);

// Set time manager properties
NEW_PROP_TAG(TimeManagerEpisodeLength);
SET_INT_PROP(WindTunnelCoupledProblem, TimeManagerEpisodeLength, 43200);
NEW_PROP_TAG(TimeManagerInitTime);
SET_SCALAR_PROP(WindTunnelCoupledProblem, TimeManagerInitTime, 0.0);
SET_SCALAR_PROP(PorousMediumSubProblem, TimeManagerInitTime, 0.0);
SET_SCALAR_PROP(FreeFlowSubProblem, TimeManagerInitTime, 0.0);
}

/*!
 * \brief please doc me
 */
template <class TypeTag = TTAG(WindTunnelCoupledProblem) >
#if !TRANSPORT
class WindTunnelCoupledProblem : public TwoCNIStokesTwoPTwoCNIProblem<TypeTag>
{
    typedef TwoCNIStokesTwoPTwoCNIProblem<TypeTag> ParentType;
#else
class WindTunnelCoupledProblem : public MultiDomainProblem<TypeTag>
{
    typedef MultiDomainProblem<TypeTag> ParentType;
#endif
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, SubDomain1TypeTag) FreeFlowTypeTag;
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain2TypeTag) PorousMediumTypeTag;

    typedef typename GET_PROP_TYPE(FreeFlowTypeTag, Problem) FreeFlowSubProblem;
    typedef typename GET_PROP_TYPE(PorousMediumTypeTag, Problem) PorousMediumSubProblem;

    typedef typename GET_PROP_TYPE(FreeFlowTypeTag, GridView) FreeFlowGridView;
    typedef typename GET_PROP_TYPE(PorousMediumTypeTag, GridView) PorousMediumGridView;

    typedef typename GET_PROP_TYPE(FreeFlowTypeTag, ElementSolutionVector) ElementSolutionVector1;
    typedef typename GET_PROP_TYPE(PorousMediumTypeTag, ElementSolutionVector) ElementSolutionVector2;

    typedef typename GET_PROP_TYPE(FreeFlowTypeTag, ElementVolumeVariables) ElementVolumeVariables1;
    typedef typename GET_PROP_TYPE(PorousMediumTypeTag, ElementVolumeVariables) ElementVolumeVariables2;

    typedef typename GET_PROP_TYPE(FreeFlowTypeTag, ElementBoundaryTypes) ElementBoundaryTypes;
    typedef typename GET_PROP_TYPE(FreeFlowTypeTag, FluxVariables) FluxVariables1;

    typedef typename GET_PROP_TYPE(FreeFlowTypeTag, FVElementGeometry) FVElementGeometry1;
    typedef typename GET_PROP_TYPE(PorousMediumTypeTag, FVElementGeometry) FVElementGeometry2;

    typedef typename GET_PROP_TYPE(TypeTag, Grid) HostGrid;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MDGrid;
    typedef typename MDGrid::SubDomainGrid SDGrid;

    typedef typename MDGrid::Traits::template Codim<0>::Entity MDElement;
    typedef typename MDGrid::Traits::template Codim<0>::EntityPointer MDElementPointer;
    typedef typename FreeFlowGridView::template Codim<0>::Entity SDElement1;
    typedef typename PorousMediumGridView::template Codim<0>::Entity SDElement2;
    typedef typename SDGrid::Traits::template Codim<0>::EntityPointer SDElementPointer;
    typedef typename MDGrid::template Codim<0>::LeafIterator ElementIterator;
    typedef typename MDGrid::LeafSubDomainInterfaceIterator SDInterfaceIterator;

    typedef typename GET_PROP_TYPE(FreeFlowTypeTag, Indices) FreeFlowIndices;
    typedef typename GET_PROP_TYPE(PorousMediumTypeTag, Indices) PorousMediumIndices;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    enum { dim = FreeFlowGridView::dimension };
    enum { // indices in the free flow domain
#if !TRANSPORT
        massBalanceIdx1 = FreeFlowIndices::massBalanceIdx, // Index of the mass balance
#endif
        transportEqIdx1 = FreeFlowIndices::transportEqIdx, // Index of the transport equation
        energyEqIdx1 = FreeFlowIndices::energyEqIdx // Index of the transport equation
    };
    enum { // indices in the porous medium domain
        contiTotalMassIdx2 = PorousMediumIndices::contiNEqIdx,
        contiWEqIdx2 = PorousMediumIndices::contiWEqIdx,
        energyEqIdx2 = PorousMediumIndices::energyEqIdx
    };
    enum { transportCompIdx1 = FreeFlowIndices::transportCompIdx };
    enum { phaseIdx = GET_PROP_VALUE(FreeFlowTypeTag, PhaseIdx) };
    enum {
        numEq1 = GET_PROP_VALUE(FreeFlowTypeTag, NumEq),
        numEq2 = GET_PROP_VALUE(PorousMediumTypeTag, NumEq)
    };

    typedef Dune::FieldVector<Scalar, dim> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dim> FieldVector;

public:
    /*!
     * \brief The problem for the coupling of the free-flow transport and Darcy flow
     *
     * \param timeManager The time manager
     * \param gridView The grid view
     */
    template<class GridView>
    WindTunnelCoupledProblem(TimeManager &timeManager,
                             GridView gridView)
    : ParentType(timeManager, gridView)
    {
        eps_ = 1e-6;

        // define location of the interface
        interfacePosY_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePosY);
        noDarcyX1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1);
        noDarcyX2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2);
        episodeLength_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, EpisodeLength);
        initializationTime_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, InitTime);
        dtInit_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, DtInitial);
        std::vector<Scalar> positions0 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions0);
        std::vector<Scalar> positions1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions1);
        Scalar bBoxMin = std::max(positions0.front(),noDarcyX1_);
        Scalar bBoxMax = std::min(positions0.back(),noDarcyX2_);
        lengthPM_ = bBoxMax - bBoxMin;

        // define output options
        freqRestart_ = GET_PARAM_FROM_GROUP(TypeTag, int, Output, FreqRestart);
        freqOutput_ = GET_PARAM_FROM_GROUP(TypeTag, int, Output, FreqOutput);
        freqFluxOutput_ = GET_PARAM_FROM_GROUP(TypeTag, int, Output, FreqFluxOutput);
        vtkInternalIdx_ = 0;

        numberOfSoilLayers_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, SpatialParams, NumberOfSoilLayers);
        freeFlow_ = this->sdID1();
        porousMedium_ = this->sdID2();

        initializeGrid();

        // initialize the tables of the fluid system
        if (GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, UseSmallFluidSystemTable))
        {
            FluidSystem::init(/*tempMin=*/273.15, /*tempMax=*/323.15, /*numTemp=*/50,
                              /*pMin=*/5e4, /*pMax=*/1.5e5, /*numP=*/100);
        }
        else
        {
            FluidSystem::init(/*tempMin=*/273.15, /*tempMax=*/343.15, /*numTemp=*/140,
                              /*pMin=*/5e4, /*pMax=*/1e7, /*numP=*/995);
        }

        if (initializationTime_ > 0.0)
            this->timeManager().startNextEpisode(initializationTime_);
        else
            this->timeManager().startNextEpisode(episodeLength_);

        // spatial parameter stuff, if requested
        this->sdProblem2().spatialParams().initRandomField(this->sdGridView2());
        this->sdProblem2().spatialParams().plotMaterialLaw();

        for (unsigned int i = 0; i < numberOfSoilLayers_+1; ++i)
        {
            char outputname[20];
            if (i < numberOfSoilLayers_)
            {
                sprintf(outputname, "%s%02d%s","fluxes", i, ".csv");
            }
            else
            {
                sprintf(outputname, "%s%s","fluxes", ".csv");
            }
            fluxFileNames_.push_back(outputname);
            std::ofstream outfile(outputname, std::ios_base::out);
            outfile << "Time;"
                    << "EvapRate[mm/d];"
                    << "TotalWaterVaporFluxFF;" // from residuum
                    << "AdvWaterVaporFluxFF;" // from gradients (imprecise)
                    << "DiffWaterVaporFluxFF;" // from gradients (imprecise)
                    << "EnergyFluxFF;"
//                     << "TotalWaterComponentFluxPM;"
//                     << "EnergyFluxPM"
                    << std::endl;
            outfile.close();
        }
     }


    /*!
     * \brief Initialization of the grids
     *
     * This function splits the multidomain grid in the two
     * individual subdomain grids and takes care of parallelization.
     */
    void initializeGrid()
    {
        MDGrid& mdGrid = this->mdGrid();
        mdGrid.startSubDomainMarking();

        // subdivide grid in two subdomains
        for (const auto& element : Dune::elements(mdGrid.leafGridView()))
        {
            // this is required for parallelization
            // checks if element is within a partition
            if (element.partitionType() != Dune::InteriorEntity)
                continue;

            GlobalPosition globalPos = element.geometry().center();

            if (globalPos[1] > interfacePosY_)
                mdGrid.addToSubDomain(freeFlow_, element);
            else
                if(globalPos[0] > noDarcyX1_ && globalPos[0] < noDarcyX2_)
                    mdGrid.addToSubDomain(porousMedium_, element);
        }
        mdGrid.preUpdateSubDomains();
        mdGrid.updateSubDomains();
        mdGrid.postUpdateSubDomains();

        gridinfo(this->sdGrid1());
        gridinfo(this->sdGrid2());
    }

    //! \copydoc Dumux::CoupledProblem::postTimeStep()
    void postTimeStep()
    {
        // call the postTimeStep function of the subproblems
        this->sdProblem1().postTimeStep();
        this->sdProblem2().postTimeStep();

        if (shouldWriteFluxFile())
        {
            counter_ = this->sdProblem1().currentVTKFileNumber() + 1;

            calculateFirstInterfaceFluxes();
//             calculateSecondInterfaceFluxes();
        }
    }

    //! \copydoc Dumux::CoupledProblem::episodeEnd()
    void episodeEnd()
    { this->timeManager().startNextEpisode(episodeLength_); }

    /*!
     * \brief Calculates fluxes and coupling terms at the interface for the ZeroEq model.
     *        Flux output files are created and the summarized flux is written to a file.
     */
    void calculateFirstInterfaceFluxes()
    {
        const MDGrid& mdGrid = this->mdGrid();
        ElementVolumeVariables1 elemVolVarsPrev1, elemVolVarsCur1;
        FVElementGeometry1 fvGeometry1;
        Scalar sumTotalWaterVaporFlux = 0.;
        Scalar sumAdvectiveWaterVaporFlux = 0.;
        Scalar sumDiffusiveWaterVaporFlux = 0.;
        Scalar sumEnergyFlux = 0.;
        Scalar sumArea = 0.;
        std::vector<Scalar> totalWaterVaporFlux(numberOfSoilLayers_, 0.);
        std::vector<Scalar> advectiveWaterVaporFlux(numberOfSoilLayers_, 0.);
        std::vector<Scalar> diffusiveWaterVaporFlux(numberOfSoilLayers_, 0.);
        std::vector<Scalar> energyFlux(numberOfSoilLayers_, 0.);
        std::vector<Scalar> area(numberOfSoilLayers_, 0.);

        // loop over the element faces on the interface
        const SDInterfaceIterator endIfIt = mdGrid.leafSubDomainInterfaceEnd(freeFlow_, porousMedium_);
        for (SDInterfaceIterator ifIt =
                 mdGrid.leafSubDomainInterfaceBegin(freeFlow_, porousMedium_); ifIt != endIfIt; ++ifIt)
        {
            const int firstFaceIdx = ifIt->indexInFirstCell();
            const std::shared_ptr<MDElement> mdElement1
                = std::make_shared<MDElement>(ifIt->firstCell());
            const std::shared_ptr<SDElement1> sdElement1
                = std::make_shared<SDElement1>(this->sdElementPointer1(*mdElement1));
            fvGeometry1.update(this->sdGridView1(), *sdElement1);

            const Dune::ReferenceElement<typename MDGrid::ctype,dim>& referenceElement1 =
                Dune::ReferenceElements<typename MDGrid::ctype,dim>::general((*mdElement1).type());
            const int numVerticesOfFace = referenceElement1.size(firstFaceIdx, 1, dim);

            elemVolVarsPrev1.update(this->sdProblem1(),
                                    *sdElement1,
                                    fvGeometry1,
                                    true /* oldSol? */);
            elemVolVarsCur1.update(this->sdProblem1(),
                                   *sdElement1,
                                   fvGeometry1,
                                   false /* oldSol? */);

            ElementBoundaryTypes bcTypes;
            bcTypes.update(this->sdProblem1(), *sdElement1, fvGeometry1);
            this->localResidual1().evalPDELab(*sdElement1, fvGeometry1, elemVolVarsPrev1, elemVolVarsCur1, bcTypes);

            const int vertInElem0 = referenceElement1.subEntity(firstFaceIdx, 1, 0, dim);
            const int vertInElem1 = referenceElement1.subEntity(firstFaceIdx, 1, 1, dim);
            const FieldVector& vertexGlobal0 = (*mdElement1).geometry().corner(vertInElem0);
            const FieldVector& vertexGlobal1 = (*mdElement1).geometry().corner(vertInElem1);
            GlobalPosition middle(0.0);
            for (int idx = 0; idx < dim; idx++)
            {
                middle[idx] = (vertexGlobal0[idx] + vertexGlobal1[idx]) / 2.0;
            }
            for (int nodeInFace = 0; nodeInFace < numVerticesOfFace; nodeInFace++)
            {
                const int boundaryFaceIdx = fvGeometry1.boundaryFaceIndex(firstFaceIdx, nodeInFace);
                const int vertInElem = referenceElement1.subEntity(firstFaceIdx, 1, nodeInFace, dim);
                const FieldVector& vertexGlobal = (*mdElement1).geometry().corner(vertInElem);
                const ElementSolutionVector1& firstVertexResidual = this->localResidual1().residual();

                FluxVariables1 boundaryVars1;
                boundaryVars1.update(this->sdProblem1(),
                                     *sdElement1,
                                     fvGeometry1,
                                     boundaryFaceIdx,
                                     elemVolVarsCur1,
                                     /*onBoundary=*/true);

                GlobalPosition globalPos(0.0);
                Scalar extrusionFactor = this->sdProblem1().extrusionFactorAtPos(globalPos);
                sumTotalWaterVaporFlux += firstVertexResidual[vertInElem][transportEqIdx1];
                sumAdvectiveWaterVaporFlux += computeAdvectiveVaporFluxes1(elemVolVarsCur1, boundaryVars1, vertInElem);
                sumDiffusiveWaterVaporFlux += computeDiffusiveVaporFluxes1(elemVolVarsCur1, boundaryVars1, vertInElem);
                sumEnergyFlux += firstVertexResidual[vertInElem][energyEqIdx1];
                sumArea += boundaryVars1.face().area * extrusionFactor;

                // Split the fluxes to different layer if the middle of the interface and the corner
                // belong to different layers
                std::vector<Scalar> weight(numVerticesOfFace, 0.5);
                int layerCornerIdx = this->sdProblem2().spatialParams().layerIdx(vertexGlobal)[0];
                int layerMiddleIdx = this->sdProblem2().spatialParams().layerIdx(middle)[0];
                if (layerCornerIdx != layerMiddleIdx)
                {
                    std::vector<Scalar> heterogeneityPositions(this->sdProblem2().spatialParams().heterogeneityPos());
                    weight[0] = std::abs(vertexGlobal[0] - heterogeneityPositions[std::min(layerCornerIdx,layerMiddleIdx)])
                                / std::abs(vertexGlobal[0] - middle[0]);
                    weight[1] = 1.0 - weight[0];
                }

                totalWaterVaporFlux[layerCornerIdx] += weight[0] * firstVertexResidual[vertInElem][transportEqIdx1];
                advectiveWaterVaporFlux[layerCornerIdx] += weight[0] * computeAdvectiveVaporFluxes1(elemVolVarsCur1, boundaryVars1, vertInElem);
                diffusiveWaterVaporFlux[layerCornerIdx] += weight[0] * computeDiffusiveVaporFluxes1(elemVolVarsCur1, boundaryVars1, vertInElem);
                energyFlux[layerCornerIdx] += weight[0] * firstVertexResidual[vertInElem][energyEqIdx1];
                area[layerCornerIdx] += weight[0] * boundaryVars1.face().area * extrusionFactor;

                totalWaterVaporFlux[layerMiddleIdx] += weight[1] * firstVertexResidual[vertInElem][transportEqIdx1];
                advectiveWaterVaporFlux[layerMiddleIdx] += weight[1] * computeAdvectiveVaporFluxes1(elemVolVarsCur1, boundaryVars1, vertInElem);
                diffusiveWaterVaporFlux[layerMiddleIdx] += weight[1] * computeDiffusiveVaporFluxes1(elemVolVarsCur1, boundaryVars1, vertInElem);
                energyFlux[layerMiddleIdx] += weight[1] * firstVertexResidual[vertInElem][energyEqIdx1];
                area[layerMiddleIdx] += weight[1] * boundaryVars1.face().area * extrusionFactor;
            }
        } // end loop over element faces on interface

        std::cout << "Writing fluxes.csv" << std::endl;
        std::ofstream outfile(fluxFileNames_.back(), std::ios_base::app);
        outfile << this->timeManager().time() + this->timeManager().timeStepSize() << ";"
                << sumTotalWaterVaporFlux * 86400.0 / sumArea << ";"
                << sumTotalWaterVaporFlux << ";"
                << sumAdvectiveWaterVaporFlux << ";"
                << sumDiffusiveWaterVaporFlux << ";"
                << sumEnergyFlux << ";"
                << std::endl;
        outfile.close();
        for (int curLayerIdx = 0; curLayerIdx < numberOfSoilLayers_; curLayerIdx++)
        {
            outfile.open(fluxFileNames_[curLayerIdx], std::ios_base::app);
            outfile << this->timeManager().time() + this->timeManager().timeStepSize() << ";"
                    << totalWaterVaporFlux[curLayerIdx] * 86400.0 / area[curLayerIdx] << ";"
                    << totalWaterVaporFlux[curLayerIdx] << ";"
                    << advectiveWaterVaporFlux[curLayerIdx] << ";"
                    << diffusiveWaterVaporFlux[curLayerIdx] << ";"
                    << energyFlux[curLayerIdx] << ";"
                    << std::endl;
            outfile.close();
        }

        std::cout << "-------Evaporation Rates and Fluxes---------" << std::endl;
        std::cout << " Total: " << sumTotalWaterVaporFlux * 86400.0 / sumArea << "[mm/d]"
                  << " Adv: " << sumAdvectiveWaterVaporFlux
                  << " Diff: " << sumDiffusiveWaterVaporFlux
                  << " Sum: " << sumAdvectiveWaterVaporFlux + sumDiffusiveWaterVaporFlux
                  << "   Area: " << sumArea
                  << std::endl;
        for (int curLayerIdx = 0; curLayerIdx < numberOfSoilLayers_; curLayerIdx++)
        {
            std::cout << " Layer" << curLayerIdx
                      << ": " << totalWaterVaporFlux[curLayerIdx] * 86400.0 / area[curLayerIdx] << "[mm/d]"
                      << " Adv: " << advectiveWaterVaporFlux[curLayerIdx]
                      << " Diff: " << diffusiveWaterVaporFlux[curLayerIdx]
                      << " Sum: " << advectiveWaterVaporFlux[curLayerIdx] + diffusiveWaterVaporFlux[curLayerIdx]
                      << "   Area: " << area[curLayerIdx]
                      << std::endl;
        }
        std::cout << "--------------------------------------------" << std::endl;
    }

    /*!
     * \brief Calculates fluxes and coupling terms at the interface for the Darcy model.
     *        Flux output files are created and the summarized flux is written to a file.
     */
    void calculateSecondInterfaceFluxes()
    {
        DUNE_THROW(Dune::NotImplemented, "Implement the division by area first");

        const MDGrid& mdGrid = this->mdGrid();
        ElementVolumeVariables2 elemVolVarsPrev2, elemVolVarsCur2;
        FVElementGeometry2 fvGeometry2;

        std::vector<Scalar> totalWaterComponentFlux(numberOfSoilLayers_, 0.);
        std::vector<Scalar> energyFlux(numberOfSoilLayers_, 0.);
        std::vector<Scalar> waterFluxGasPhase(numberOfSoilLayers_, 0.);

        // loop over the element faces on the interface
        const SDInterfaceIterator endIfIt = mdGrid.leafSubDomainInterfaceEnd(freeFlow_, porousMedium_);
        for (SDInterfaceIterator ifIt =
                 mdGrid.leafSubDomainInterfaceBegin(freeFlow_, porousMedium_); ifIt != endIfIt; ++ifIt)
        {
            const int secondFaceIdx = ifIt->indexInSecondCell();
            const std::shared_ptr<MDElement> mdElement2
                = std::make_shared<MDElement>(ifIt->secondCell());
            const std::shared_ptr<SDElement2> sdElement2
                = std::make_shared<SDElement2>(this->sdElementPointer2(*mdElement2));
            fvGeometry2.update(this->sdGridView2(), *sdElement2);

            const Dune::ReferenceElement<typename MDGrid::ctype,dim>& referenceElement2 =
                Dune::ReferenceElements<typename MDGrid::ctype,dim>::general((*mdElement2).type());
            const int numVerticesOfFace = referenceElement2.size(secondFaceIdx, 1, dim);

            elemVolVarsPrev2.update(this->sdProblem2(),
                                    *sdElement2,
                                    fvGeometry2,
                                    true /* oldSol? */);
            elemVolVarsCur2.update(this->sdProblem2(),
                                   *sdElement2,
                                   fvGeometry2,
                                   false /* oldSol? */);

            typedef typename GET_PROP_TYPE(PorousMediumTypeTag, ElementBoundaryTypes) ElementBoundaryTypes;
            ElementBoundaryTypes bcTypes;
            bcTypes.update(this->sdProblem2(), *sdElement2, fvGeometry2);

            this->localResidual2().evalPDELab(*sdElement2, fvGeometry2, elemVolVarsPrev2, elemVolVarsCur2, bcTypes);

            for (int nodeInFace = 0; nodeInFace < numVerticesOfFace; nodeInFace++)
            {
                const int vertInElem2 = referenceElement2.subEntity(secondFaceIdx, 1, nodeInFace, dim);
                const FieldVector& vertexGlobal = *mdElement2.geometry().corner(vertInElem2);
                const ElementSolutionVector2& secondVertexResidual = this->localResidual2().residual();

                // evaluate the vapor fluxes within each phase
//                 this->localResidual2().evalComponentFluxes(vertInElem2);

                int curLayerIdx = this->sdProblem2().spatialParams().layerIdx(vertexGlobal)[0];
                totalWaterComponentFlux[curLayerIdx] += secondVertexResidual[vertInElem2][contiWEqIdx2];
                energyFlux[curLayerIdx] += secondVertexResidual[vertInElem2][energyEqIdx2];
            }
        }

        for (unsigned int curLayerIdx = 0; curLayerIdx < numberOfSoilLayers_; ++curLayerIdx)
        {
            std::ofstream outfile(fluxFileNames_[curLayerIdx], std::ios_base::app);
            outfile << totalWaterComponentFlux[curLayerIdx] / (lengthPM_ / numberOfSoilLayers_) << ";"
                    << energyFlux[curLayerIdx] / (lengthPM_ / numberOfSoilLayers_) << ";"
                    << std::endl;
            outfile.close();
        }
    }

    Scalar computeAdvectiveVaporFluxes1(const ElementVolumeVariables1& elemVolVars1,
                                        const FluxVariables1& boundaryVars1,
                                        int vertInElem1)
    {
        return elemVolVars1[vertInElem1].density()
               * elemVolVars1[vertInElem1].fluidState().massFraction(phaseIdx, transportCompIdx1)
               * boundaryVars1.normalVelocity();
    }

    Scalar computeDiffusiveVaporFluxes1(const ElementVolumeVariables1& elemVolVars1,
                                        const FluxVariables1& boundaryVars1,
                                        int vertInElem1)
    {
        return (boundaryVars1.moleFractionGrad(transportCompIdx1)
                * boundaryVars1.face().normal)
                * (boundaryVars1.diffusionCoeff(transportCompIdx1)
                   + boundaryVars1.eddyDiffusivity())
                * boundaryVars1.molarDensity()
                * FluidSystem::molarMass(transportCompIdx1);
    }

    /*!
     * \brief Returns the concentration gradient through the boundary layer
     *
     * \param cParams a parameter container
     * \param scvIdx1 The local index of the sub-control volume of the Stokes domain
     */
    template<typename CParams>
    Scalar evalBoundaryLayerConcentrationGradient(CParams cParams, const int scvIdx1) const
    {
        Scalar massFractionOut = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefMassFraction);
        Scalar M1 = FluidSystem::molarMass(transportCompIdx1);
        Scalar M2 = FluidSystem::molarMass(phaseIdx);
        Scalar X2 = 1.0 - massFractionOut;
        Scalar massToMoleDenominator = M2 + X2*(M1 - M2);
        Scalar moleFractionOut = massFractionOut * M2 /massToMoleDenominator;

        Scalar normalMoleFracGrad = cParams.elemVolVarsCur1[scvIdx1].moleFraction(transportCompIdx1)
                                    - moleFractionOut;
        return normalMoleFracGrad / this->evalBoundaryLayerModel(cParams, scvIdx1).massBoundaryLayerThickness();
    }

    //! \copydoc Dumux::CoupledProblem::shouldWriteRestartFile()
    bool shouldWriteRestartFile() const
    {
        return (vtkInternalIdx_ % freqRestart_ == 0
                && this->timeManager().episodeWillBeFinished())
               || this->timeManager().willBeFinished();
    }

    //! \copydoc Dumux::shouldWriteOutput()
    bool shouldWriteOutput()
    {
        if (this->timeManager().timeStepIndex() % freqOutput_ == 0
            || this->timeManager().episodeWillBeFinished()
            || this->timeManager().willBeFinished())
        {
            vtkInternalIdx_++;
            return true;
        }
        return false;
    }

    /*!
     * \brief Returns true if a file with the fluxes across the
     *        free-flow -- porous-medium interface should be written.
     */
    bool shouldWriteFluxFile() const
    {
        return this->timeManager().timeStepIndex() % freqFluxOutput_ == 0
                || this->timeManager().episodeWillBeFinished()
                || this->timeManager().willBeFinished();
    }

    FreeFlowSubProblem& freeFlowProblem()
    { return this->sdProblem1(); }
    const FreeFlowSubProblem& freeFlowProblem() const
    { return this->sdProblem1(); }

    PorousMediumSubProblem& porousMediumProblem()
    { return this->sdProblem2(); }
    const PorousMediumSubProblem& porousMediumProblem() const
    { return this->sdProblem2(); }

private:
    typename MDGrid::SubDomainType freeFlow_;
    typename MDGrid::SubDomainType porousMedium_;

    unsigned counter_;
    unsigned freqRestart_;
    unsigned freqOutput_;
    unsigned freqFluxOutput_;
    unsigned int vtkInternalIdx_;

    Scalar interfacePosY_;
    Scalar noDarcyX1_;
    Scalar noDarcyX2_;
    Scalar lengthPM_;
    Scalar episodeLength_;
    Scalar initializationTime_;
    Scalar dtInit_;
    Scalar eps_;

    int numberOfSoilLayers_;
    std::vector<std::string> fluxFileNames_;
};

} //end namespace

#endif // DUMUX_WINDTUNNELCOUPLEDPROBLEM_HH
