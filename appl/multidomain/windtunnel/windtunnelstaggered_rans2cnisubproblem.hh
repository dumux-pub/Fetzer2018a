// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_WINDTUNNEL_STOKES_SUBPROBLEM_HH
#define DUMUX_WINDTUNNEL_STOKES_SUBPROBLEM_HH

#if COUPLING_KEPSILON
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon2cni/kepsilon2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon2cni/kepsilon2cnipropertydefaults.hh>
#elif COUPLING_LOWREKEPSILON
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon2cni/lowrekepsilon2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon2cni/lowrekepsilon2cnipropertydefaults.hh>
#elif COUPLING_KOMEGA
#include <appl/staggeredgrid/freeflow/twoeq/komega2cni/komega2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/komega2cni/komega2cnipropertydefaults.hh>
#elif COUPLING_ONEEQ
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras2cni/spalartallmaras2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras2cni/spalartallmaras2cnipropertydefaults.hh>
#elif COUPLING_ZEROEQ
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq2cni/zeroeq2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq2cni/zeroeq2cnipropertydefaults.hh>
#else
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cnipropertydefaults.hh>
#endif

#include <dumux/freeflow/turbulenceproperties.hh>
#include <appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/properties.hh>

#include <dumux/io/readwritedatafile.hh>

namespace Dumux
{
template <class TypeTag>
class WindtunnelStokesSubProblem;

namespace Properties
{
// Do not use pressure constraints
SET_BOOL_PROP(StokesSubProblem, FixPressureConstraints, false);

// Use velocity constraints
SET_BOOL_PROP(StokesSubProblem, FixVelocityConstraints, true);

// Use complete Navier-Stokes equation
SET_BOOL_PROP(StokesSubProblem, ProblemEnableNavierStokes, true);

// Disable gravity field
SET_BOOL_PROP(StokesSubProblem, ProblemEnableGravity, false);

// Use symmetrized velocity gradient
SET_BOOL_PROP(StokesSubProblem, ProblemEnableUnsymmetrizedVelocityGradient, false);

#if TURBULENT
// Predefine flow axis and automatically search for wall normal axises
SET_INT_PROP(StokesSubProblem, ProblemFlowNormalAxis, 0);
SET_INT_PROP(StokesSubProblem, ProblemWallNormalAxis, -1);
#endif

#if COUPLING_KEPSILON
// Use Pope model on default
SET_INT_PROP(StokesSubProblem, KEpsilonWallFunctionModel, 2);
#endif

// Use symmetry on top
NEW_PROP_TAG(FreeFlowTopIsSymmetry);
SET_BOOL_PROP(StokesSubProblem, FreeFlowTopIsSymmetry, true);

// Smallest grid cells for turbulence properties
NEW_PROP_TAG(GridSmallestGridCellY);
NEW_PROP_TAG(GridSmallestGridCellZ);
SET_SCALAR_PROP(StokesSubProblem, GridSmallestGridCellY, 0.0);
SET_SCALAR_PROP(StokesSubProblem, GridSmallestGridCellZ, 0.0);

// Set the sand grain roughness at the lower boundary
NEW_PROP_TAG(ZeroEqBBoxMinSandGrainRoughness);
SET_SCALAR_PROP(StokesSubProblem, ZeroEqBBoxMinSandGrainRoughness, 0.0);

// Initialization time for the velocity
NEW_PROP_TAG(FreeFlowIncreaseVelocityUntil);
NEW_PROP_TAG(FreeFlowIncreaseVelocityPower);
SET_SCALAR_PROP(StokesSubProblem, FreeFlowIncreaseVelocityUntil, 0.0);
SET_SCALAR_PROP(StokesSubProblem, FreeFlowIncreaseVelocityPower, 1.0);

// Decide which interface profile type to use
NEW_PROP_TAG(GridInterfaceProfile);
SET_STRING_PROP(StokesSubProblem, GridInterfaceProfile, "flat");

// The use porous medium box type
NEW_PROP_TAG(GridPorousMediumBoxType);
SET_STRING_PROP(StokesSubProblem, GridPorousMediumBoxType, "rectangular");

// Use either velocity or Reynolds number to define the flow
NEW_PROP_TAG(FreeFlowVelocityProfileType);
SET_STRING_PROP(StokesSubProblem, FreeFlowVelocityProfileType, "block");
}

template <class TypeTag>
class WindtunnelStokesSubProblem
#if COUPLING_KEPSILON
  : public KEpsilonTwoCNIProblem<TypeTag>
#elif COUPLING_LOWREKEPSILON
  : public LowReKEpsilonTwoCNIProblem<TypeTag>
#elif COUPLING_KOMEGA
  : public KOmegaTwoCNIProblem<TypeTag>
#elif COUPLING_ONEEQ
  : public SpalartAllmarasTwoCNIProblem<TypeTag>
#elif COUPLING_ZEROEQ
  : public ZeroEqTwoCNIProblem<TypeTag>
#else
  : public NavierStokesTwoCNIProblem<TypeTag>
#endif
{
#if COUPLING_KEPSILON
    using ParentType = KEpsilonTwoCNIProblem<TypeTag>;
    using KEpsilonWallFunctions = typename GET_PROP_TYPE(TypeTag, KEpsilonWallFunctions);
#elif COUPLING_LOWREKEPSILON
    using ParentType = LowReKEpsilonTwoCNIProblem<TypeTag>;
#elif COUPLING_KOMEGA
    using ParentType = KOmegaTwoCNIProblem<TypeTag>;
#elif COUPLING_ONEEQ
    using ParentType = SpalartAllmarasTwoCNIProblem<TypeTag>;
#elif COUPLING_ZEROEQ
    using ParentType = ZeroEqTwoCNIProblem<TypeTag>;
#else
    using ParentType = NavierStokesTwoCNIProblem<TypeTag>;
#endif
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Implementation;
    using MultiDomainTypeTag = typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag);

    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    enum { dim = GridView::dimension };
    using DimVector = typename GET_PROP_TYPE(TypeTag, DimVector);
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum { phaseIdx = Indices::phaseIdx,
           transportCompIdx = Indices::transportCompIdx };

public:
    WindtunnelStokesSubProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView), gridView_(gridView)
      , gravity_(0.0)
#if COUPLING_KEPSILON
      , kEpsilonWallFunctions_(gridView_, asImp_())
#endif
    {
        if (GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableGravity))
            gravity_[1]  = -9.81;

        velocityProfileType_ = GET_PARAM_FROM_GROUP(TypeTag, std::string, FreeFlow, VelocityProfileType);
        velocity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefVelocity);
        pressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefPressure);
        temperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefTemperature);
        try {
            massMoleFrac_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefMassFraction);
        }
        catch (...) {
            FluidState fluidState;
            fluidState.setPressure(phaseIdx, pressure_);
            fluidState.setTemperature(temperature_);
            Scalar relativeHumidity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefRelativeHumidity);
            fluidState.template setRelativeHumidity<FluidState>(fluidState, phaseIdx, transportCompIdx, relativeHumidity);
            massMoleFrac_ = fluidState.massFraction(phaseIdx, transportCompIdx);
        }

        // grid properties
        bBoxMinFiltered_ = ParentType::bBoxMin();
        interfacePos_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePosY);
        bBoxMinFiltered_[1] = interfacePos_;
        interfaceProfile_ = GET_PARAM_FROM_GROUP(TypeTag, std::string, Grid, InterfaceProfile);
        amplitude_ = 0.0;
        baseline_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePosY);
        if (std::strcmp(interfaceProfile_.c_str(), "flat") != 0)
        {
            amplitude_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, ObstacleAmplitude);
            baseline_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, ObstacleBaseline);
        }

        topIsSymmetry_ = GET_PARAM_FROM_GROUP(TypeTag, bool, FreeFlow, TopIsSymmetry);

        try {
            velDataFile_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, FreeFlow, VelocityDataFile);
            useVelDataFile_ = true;
            readData(velDataFile_, velocityData_);
        }
        catch (...) {
            useVelDataFile_ = false;
        }

        try {
            massFractionDataFile_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, FreeFlow, MassFractionDataFile);
            useMassFractionDataFile_ = true;
            readData(massFractionDataFile_, massFractionData_);
        }
        catch (...) {
            useMassFractionDataFile_ = false;
        }

        try {
            temperatureDataFile_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, FreeFlow, TemperatureDataFile);
            useTemperatureDataFile_ = true;
            readData(temperatureDataFile_, temperatureData_);
        }
        catch (...) {
            useTemperatureDataFile_ = false;
        }

        roughness_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, ZeroEq, BBoxMinSandGrainRoughness);

#if TURBULENT
        Dumux::TurbulenceProperties<Scalar, dim, true> turbulenceProperties;
        Scalar smallestGridCellY = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, SmallestGridCellY);
        FluidSystem::init(/*tempMin=*/273.15, /*tempMax=*/343.15, /*numTemp=*/140,
                          /*pMin=*/5e4, /*pMax=*/1.5e5, /*numP=*/100);
        FluidState fluidState;
        fluidState.setPressure(phaseIdx, 1e5);
        fluidState.setTemperature(temperature_);
        fluidState.setMassFraction(phaseIdx, transportCompIdx, massMoleFrac_);
        Scalar density = FluidSystem::density(fluidState, phaseIdx);
        kinematicViscosity_ = FluidSystem::viscosity(fluidState, phaseIdx) / density;
        std::cout << " density: " << density << " [kg/m^3]" << std::endl;
        std::cout << " kinematicViscosity: " << kinematicViscosity_ << " [m^2/s]" << std::endl;
        std::cout << std::endl;
        Dune::FieldVector<double, dim> halfWay;
        halfWay[0] = 0.5 * (this->bBoxMax()[0] - this->bBoxMin()[0]) + this->bBoxMin()[0];
        halfWay[1] = 0.5 * smallestGridCellY;
        Dune::FieldVector<double, dim> end;
        end[0] = this->bBoxMax()[0];
        end[1] = 0.5 * smallestGridCellY;
        turbulenceProperties.yPlusEstimation(velocity_, halfWay, kinematicViscosity_, density, 1);
        turbulenceProperties.yPlusEstimation(velocity_, end, kinematicViscosity_, density, 1);
        Scalar diameter = this->bBoxMax()[1] - this->bBoxMin()[1];
        turbulenceProperties.entranceLength(velocity_, diameter, kinematicViscosity_);
        viscosityTilde_ = turbulenceProperties.viscosityTilde(velocity_, diameter, kinematicViscosity_);
        turbulentKineticEnergy_ = turbulenceProperties.turbulentKineticEnergy(velocity_, diameter, kinematicViscosity_);
        dissipation_ = turbulenceProperties.dissipation(velocity_, diameter, kinematicViscosity_);
        dissipationRate_ = turbulenceProperties.dissipationRate(velocity_, diameter, kinematicViscosity_);
        std::cout << std::endl;
#endif
    }

    //! \copydoc Dumux::ImplicitProblem::init()
    void init()
    {
        // overwrite the parent function
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string name() const
    { return "unused"; }

    //! \brief Velocity boundary condition types
    bool bcVelocityIsWall(const DimVector& global) const
    {
      return (global[1] < this->bBoxMin()[1] + eps_
              || (global[1] > this->bBoxMax()[1] - eps_ && !topIsSymmetry_)
#if DUMUX_MULTIDOMAIN_DIM > 2
              || global[2] < this->bBoxMin()[2] + eps_
              || global[2] > this->bBoxMax()[2] - eps_
#endif
             )
             && !bcVelocityIsCoupling(global)
             && !bcVelocityIsSymmetry(global);
    }
    bool bcVelocityIsInflow(const DimVector& global) const
    {
      return !bcVelocityIsCoupling(global)
             && !bcVelocityIsSymmetry(global)
             && !bcVelocityIsWall(global)
             && !bcVelocityIsOutflow(global);
    }
    bool bcVelocityIsOutflow(const DimVector& global) const
    {
      return global[0] > this->bBoxMax()[0] - eps_
             && !bcVelocityIsCoupling(global)
             && !bcVelocityIsSymmetry(global)
             && !bcVelocityIsWall(global);
    }
    bool bcVelocityIsSymmetry(const DimVector& global) const
    {
      return (global[1] > this->bBoxMax()[1] - eps_ && topIsSymmetry_)
             && !bcVelocityIsCoupling(global);
    }
    bool bcVelocityIsCoupling(const DimVector& global) const
    { return isOnCouplingFace(global); }

    //! \brief Pressure boundary condition types
    bool bcPressureIsDirichlet(const DimVector& global) const
    { return global[0] > this->bBoxMax()[0] - eps_; }
    bool bcPressureIsOutflow(const DimVector& global) const
    { return !bcPressureIsDirichlet(global) && !bcPressureIsCoupling(global); }
    bool bcPressureIsCoupling(const DimVector& global) const
    { return isOnCouplingFace(global); }

    //! \brief MassMoleFrac boundary condition types
    bool bcMassMoleFracIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global); }
    bool bcMassMoleFracIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }
    bool bcMassMoleFracIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }
    bool bcMassMoleFracIsSymmetry(const DimVector& global) const
    { return bcVelocityIsSymmetry(global); }
    bool bcMassMoleFracIsCoupling(const DimVector& global) const
    { return bcVelocityIsCoupling(global); }

    //! \brief Temperature boundary condition types
    bool bcTemperatureIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global); }
    bool bcTemperatureIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }
    bool bcTemperatureIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }
    bool bcTemperatureIsSymmetry(const DimVector& global) const
    { return bcVelocityIsSymmetry(global); }
    bool bcTemperatureIsCoupling(const DimVector& global) const
    { return bcVelocityIsCoupling(global); }

    //! \brief Dirichlet values for velocity field
    DimVector dirichletVelocityAtPos(const Element& e, const DimVector& global) const
    {
        DimVector y(0.0);
        if (!bcVelocityIsWall(global) && !bcVelocityIsCoupling(global))
        {
            y[0] = useVelDataFile_
                   ? evaluateData(velocityData_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize())
                   : velocity_;

            Scalar height = this->bBoxMax()[1] - this->bBoxMin()[1];
            Scalar relativePosition = (global[1] - this->bBoxMin()[1]) / height;
            if (std::strcmp(velocityProfileType_.c_str(), "parabola") == 0)
            {
                y[0] *= 4.0 * relativePosition * (1.0 - relativePosition);
            }
            else if (std::strcmp(velocityProfileType_.c_str(), "half-parabola") == 0)
            {
                y[0] *= 4.0 * relativePosition / 2.0 * (1.0 - relativePosition / 2.0);
            }
            else if (std::strcmp(velocityProfileType_.c_str(), "block") == 0)
            {
                y[1] *= 1.0;
            }
            else
            {
                DUNE_THROW(Dune::NotImplemented, "Velocity profile type is unkown " << velocityProfileType_.c_str());
            }

//             y[1] = 1e-15; // debugging the boundary conditions
//             y[2] = 1e-15; // debugging the boundary conditions
        }

        Scalar increaseVelocityUntil = GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, IncreaseVelocityUntil);
        Scalar increaseVelocityPower = GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, IncreaseVelocityPower);
        if (increaseVelocityUntil > this->timeManager().time())
            y *= std::pow(this->timeManager().time() / increaseVelocityUntil, increaseVelocityPower);

        return y;
    }
    Scalar dirichletPressureAtPos(const DimVector& global) const
    { return pressure_; }
    Scalar dirichletMassMoleFracAtPos(const DimVector& global) const
    {
        return useMassFractionDataFile_
               ? evaluateData(massFractionData_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize())
               : massMoleFrac_;
    }
    Scalar dirichletTemperatureAtPos(const DimVector& global) const
    {
        return useTemperatureDataFile_
               ? evaluateData(temperatureData_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize())
               : temperature_;
    }

#if (COUPLING_KEPSILON || COUPLING_LOWREKEPSILON || COUPLING_KOMEGA)
    //! \brief TurbulentKineticEnergy boundary condition types
    bool bcTurbulentKineticEnergyIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global) || bcVelocityIsCoupling(global); }
    bool bcTurbulentKineticEnergyIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }
    bool bcTurbulentKineticEnergyIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }
    bool bcTurbulentKineticEnergyIsSymmetry(const DimVector& global) const
    { return bcVelocityIsSymmetry(global); }

    //! \brief TurbulentKineticEnergy boundary condition values
    Scalar dirichletTurbulentKineticEnergyAtPos(const Element& e, const DimVector& global) const
    {
#if COUPLING_KEPSILON
        if (this->kEpsilonWallFunctions().useWallFunctionTurbulentKineticEnergy(e)
            && this->timeManager().time() > eps_)
        {
            return this->kEpsilonWallFunctions().wallFunctionTurbulentKineticEnergy(e);
        }
        return turbulentKineticEnergy_;
#elif COUPLING_LOWREKEPSILON
        if (bcTurbulentKineticEnergyIsWall(global)
            && this->timeManager().time() > 0.0)
        {
            return 0.0;
        }
        return turbulentKineticEnergy_;
#else // COUPLING_KOMEGA
        if (bcTurbulentKineticEnergyIsWall(global)
            && this->timeManager().time() > 0.0)
        {
            return 0.0;
        }
        return turbulentKineticEnergy_;
#endif
    }

    //! \brief Dissipation boundary condition types
    bool bcDissipationIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global) || bcVelocityIsCoupling(global); }
    bool bcDissipationIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }
    bool bcDissipationIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }
    bool bcDissipationIsSymmetry(const DimVector& global) const
    { return bcVelocityIsSymmetry(global); }

    //! \brief Dissipation boundary condition values
    Scalar dirichletDissipationAtPos(const Element& e, const DimVector& global) const
    {
#if COUPLING_KEPSILON
        if (this->kEpsilonWallFunctions().useWallFunctionDissipation(e)
            && this->timeManager().time() > eps_)
        {
            return kEpsilonWallFunctions().wallFunctionDissipation(e);
        }
        return dissipation_;
#elif COUPLING_LOWREKEPSILON
        if (bcTurbulentKineticEnergyIsWall(global)
            && this->timeManager().time() > 0.0)
        {
            return 0.0;
        }
        return dissipation_;
#else // COUPLING_KOMEGA
        Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGElementLayout> dofMapper(gridView_);
        if ((bcDissipationIsInflow(global) && isOnBoundary(global))
            || this->timeManager().time() < eps_)
        {
            return dissipationRate_;
        }
        return 6.0 * kinematicViscosity_
                / (this->betaOmega() * std::pow(this->wallDistance_(dofMapper.index(e)), 2));
#endif
    }
#endif

#if COUPLING_ONEEQ
    //! \brief ViscosityTilde boundary condition types
    bool bcViscosityTildeIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global) || bcVelocityIsCoupling(global); }
    bool bcViscosityTildeIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }
    bool bcViscosityTildeIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }
    bool bcViscosityTildeIsSymmetry(const DimVector& global) const
    { return bcVelocityIsSymmetry(global); }

    //! \brief ViscosityTilde boundary condition values
    Scalar dirichletViscosityTildeAtPos(const Element& e, const DimVector& global) const
    {
        if (bcViscosityTildeIsWall(global) && isOnBoundary(global))
            return 0.0;

        if (this->timeManager().time() < 10.0)
            return viscosityTilde_ * (0.01 + this->timeManager().time() / 10);

        return viscosityTilde_;// [-]
    }
#endif

    /*!
     * \brief The coordinate of the corner of the GridView's bounding
     *        box with the smallest values.
     *
     * Filter out Darcy part
     */
    const DimVector &bBoxMin() const
    {
        return bBoxMinFiltered_;
    }

    //! \brief Returns whether we are on a coupling face
    const bool isOnCouplingFace(const DimVector& global) const
    {
        std::string porousMediumBoxType = GET_PARAM_FROM_GROUP(TypeTag, std::string, Grid, PorousMediumBoxType);
        if (std::strcmp(porousMediumBoxType.c_str(), "cylindrical") == 0 && dim == 3)
        {
            Scalar radius = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, Radius);
            DimVector center = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, DimVector, Grid, CircleCenter);
            using std::pow;
            return ((pow(global[0] - center[0], 2) + pow(global[2] - center[2], 2)) < pow(radius, 2))
                    && (global[1] < amplitude_ + baseline_ + eps_);
        }
        else
        {
            return (global[0] > GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1) + eps_
                    && global[0] < GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2) - eps_
#if DUMUX_MULTIDOMAIN_DIM > 2
                    && global[2] > GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyZ1) + eps_
                    && global[2] < GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyZ2) - eps_
#endif
                    && global[1] < amplitude_ + baseline_ + eps_)
                  || (global[0] > GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1) - eps_
                      && global[0] < GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1) + eps_
#if DUMUX_MULTIDOMAIN_DIM > 2
                      && global[2] > GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyZ1)
                      && global[2] < GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyZ2)
#endif
                      && global[1] > interfacePos_ && global[1] < amplitude_ + baseline_ + eps_)
                  || (global[0] > GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2) - eps_
                      && global[0] < GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2) + eps_
#if DUMUX_MULTIDOMAIN_DIM > 2
                      && global[2] > GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyZ1)
                      && global[2] < GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyZ2)
#endif
                      && global[1] > interfacePos_ && global[1] < amplitude_ + baseline_ + eps_)
#if DUMUX_MULTIDOMAIN_DIM > 2
                  || (global[2] > GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyZ1) - eps_
                      && global[2] < GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyZ1) + eps_
                      && global[0] > GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1)
                      && global[0] < GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2)
                      && global[1] > interfacePos_ && global[1] < amplitude_ + baseline_ + eps_)
                  || (global[2] > GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyZ2) - eps_
                      && global[2] < GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyZ2) + eps_
                      && global[0] > GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1)
                      && global[0] < GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2)
                      && global[1] > interfacePos_ && global[1] < amplitude_ + baseline_ + eps_)
#endif
                ;
        }
    }

    //! \brief Return whether a point is located on the boundary of the domain
    bool isOnBoundary(const DimVector& global) const
    {
        return global[0] < this->bBoxMin()[0] + eps_
               || global[0] > this->bBoxMax()[0] - eps_
               || global[1] < this->bBoxMin()[1] + eps_
               || global[1] > this->bBoxMax()[1] - eps_
#if DUMUX_MULTIDOMAIN_DIM > 2
               || global[2] < this->bBoxMin()[2] + eps_
               || global[2] > this->bBoxMax()[2] - eps_
#endif
               || isOnCouplingFace(global);
    }

#if COUPLING_KEPSILON
    //! \brief Returns the KEpsilonWallFunctions object used by the simulation
    const KEpsilonWallFunctions &kEpsilonWallFunctions() const
    { return kEpsilonWallFunctions_; }
#endif

    //! \brief Returns a constant value for the turbulentKineticEnergy wall function
    const Scalar constantWallFunctionTurbulentKineticEnergy() const
    { return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, WallTurbulentKineticEnergy); }

    //! \brief Returns a constant value for the dissipation wall function
    const Scalar constantWallFunctionDissipation() const
    { return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, WallDissipation); }

    //! \brief Return the roughness at the cell center in global coordinates
    Scalar roughness(const Element& e, const DimVector& cellCenterGlobal) const
    {
        return roughness_;
    }

    //! \brief Returns the acceleration due to gravity
    const DimVector &gravity() const
    { return gravity_; }

private:
    //! Returns the implementation of the problem (i.e. static polymorphism)
    Implementation &asImp_()
    { return *static_cast<Implementation *>(this); }

    //! \copydoc asImp_()
    const Implementation &asImp_() const
    { return *static_cast<const Implementation *>(this); }

    const GridView gridView_;
    static constexpr Scalar eps_ = 1e-6;
    DimVector bBoxMinFiltered_;
    Scalar interfacePos_;
    std::string interfaceProfile_;
    Scalar amplitude_;
    Scalar baseline_;

    bool topIsSymmetry_;

    DimVector gravity_;
    Scalar velocity_;
    std::string velocityProfileType_;
    Scalar pressure_;
    Scalar temperature_;
    Scalar massMoleFrac_;
    Scalar kinematicViscosity_;
    Scalar viscosityTilde_;
    Scalar turbulentKineticEnergy_;
    Scalar dissipation_;
    Scalar dissipationRate_;
#if COUPLING_KEPSILON
    KEpsilonWallFunctions kEpsilonWallFunctions_;
#endif

    bool useVelDataFile_;
    std::string velDataFile_;
    std::vector<double> velocityData_[2];
    bool useMassFractionDataFile_;
    std::string massFractionDataFile_;
    std::vector<double> massFractionData_[2];
    bool useTemperatureDataFile_;
    std::string temperatureDataFile_;
    std::vector<double> temperatureData_[2];

    Scalar roughness_;
};

} //end namespace

#endif // DUMUX_WINDTUNNEL_STOKES_SUBPROBLEM_HH
