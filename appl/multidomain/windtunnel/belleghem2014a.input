###############################################################
# Configuration file for experiments published in:
# Belleghem, M. V.; Steeman, M.; Janssen, H.; Janssens, A. & Paepe, M. D.
# Validation of a coupled heat, vapour and liquid moisture transport model for porous materials implemented in CFD
# Building and Environment, 2014, 81, 340-353 , doi: 10.1016/j.buildenv.2014.06.024
###############################################################
[TimeManager]
DtInitial = 5e-3 # [s]
MaxTimeStepSize = 300 # [s]
EpisodeLength = 600 # [s]
TEnd = 43200 # [s] # 0.5d

[FreeFlow]
TopIsSymmetry = true
[Grid]
Positions0 = -0.01 0.0 0.09 0.1
Positions1 = -0.03 0.0 0.005
Positions2 = -0.045 -0.005 0.0 0.005 0.045
Cells0 = 7 9 2
Cells1 = 20 15
Cells2 = 1 1 1 1
Grading0 = 1.0 1.2 1.0
Grading1 = -1.23 1.15
Grading2 = 1.0 1.5 -1.5 1.0

NoDarcyX1 = 0.0 # [m] # Beginning of PM below
NoDarcyX2 = 0.09 # [m] # End of PM below
NoDarcyZ1 = -0.005 # [m] # Beginning of PM below
NoDarcyZ2 = 0.005 # [m] # End of PM below
InterfacePosY = 0.0 # [m] # Vertical position of coupling interface
Verbosity = true

[Output]
PlotMaterialLaw = false
PlotEvaporationRate = false
FreqOutput = 100000
Name = belleghem2014a

###############################################################
# Define conditions in the two subdomains
###############################################################
[FreeFlow]
RefVelocity = 2.3 # [m/s]
RefPressure = 1e5 # [Pa]
RefRelativeHumidity = 0.44 # [-]
RefTemperature = 296.95 # [K]

[ZeroEq]
EddyViscosityModel = 3 # 3 = Baldwin Lomax
EddyDiffusivityModel = 1 # 1 = Reynolds analogy
EddyConductivityModel = 1 # 1 = Reynolds analogy

[PorousMedium]
InitialPhasePresence = "bothPhases" # bothPhases, nPhaseOnly
SolDependentEnergyWalls = true
SolDependentEnergySource = false
SolDependentEnergyTemperature = 296.95 # [K]
PlexiglassThermalConductivity = 0.033 # [W/(m*K)] wikipedia, styrofoam
PlexiglassThickness = 0.015 # [m] # bottom 20mm, front back 15mm, side 30mm
RefPressure = 1e5 # [Pa]
RefTemperature = 296.95 # [K]
RefWaterSaturation = 0.97 # [-]

[SpatialParams]
NumberOfSoilLayers = 1
SoilLayerIdx1 = 1
SoilLayerIdx2 = 1

[Soil1]
# brick
AlphaBJ = 1.0 # [-]
Permeability = 1.12e-15 # [m^2] # k_l [s] * nu_l [m^2/s] = K [m^2]
Porosity = 0.13 # [-]
Swr = 0.00 # [-]
Snr = 0.00 # [-]
# VgAlpha = 1.845e-5 # [1/Pa] # base
# VgN = 2.179 # [-] # base
VgAlpha = 1.728e-5 # [1/Pa] # adjusted by belleghem2014a
VgN = 1.755 # [-] # adjusted by belleghem2014a
ThermalConductivitySolid = 1.6 # [W/(m*K)]
SolidDensity = 2087.0 # [kg/m^3]
SolidHeatCapacity = 840 # [J/(kg*K)]


###############################################################
# Newton and Linear solver parameters
###############################################################
[Newton]
MaxRelativeShift = 1e-6
ResidualReduction = 1e-5
TargetSteps = 5
MaxSteps = 12
WriteConvergence = false
