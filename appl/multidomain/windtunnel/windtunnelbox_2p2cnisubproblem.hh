// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Non-isothermal two-phase two-component porous-medium subproblem
 *        with coupling at the top boundary.
 */
#ifndef DUMUX_WINDTUNNEL2P2CNISUBPROBLEM_HH
#define DUMUX_WINDTUNNEL2P2CNISUBPROBLEM_HH

#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivityjohansen.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivitysomerton.hh>
#include <dumux/multidomain/subdomainpropertydefaults.hh>
#include <dumux/multidomain/localoperator.hh>
#include <dumux/multidomain/2cnistokes2p2cni/2p2cnicouplinglocalresidual.hh>
#include <dumux/porousmediumflow/2p2c/implicit/indices.hh>
#include <dumux/porousmediumflow/implicit/problem.hh>

#include <dumux/io/readwritedatafile.hh>

#include "windtunnelbox_spatialparameters.hh"

namespace Dumux
{
template <class TypeTag>
class PorousMediumSubProblem;

namespace Properties
{
NEW_TYPE_TAG(PorousMediumSubProblem,
             INHERITS_FROM(BoxTwoPTwoCNI, SubDomain, WindTunnelSpatialParams));

// Set the problem property
SET_TYPE_PROP(PorousMediumSubProblem, Problem, PorousMediumSubProblem<TTAG(PorousMediumSubProblem)>);

// Use the 2p2cni local jacobian operator for the 2p2cniCoupling model
SET_TYPE_PROP(PorousMediumSubProblem, LocalResidual, TwoPTwoCNICouplingLocalResidual<TypeTag>);

// Choose pn and Sw as primary variables
SET_INT_PROP(PorousMediumSubProblem, Formulation, TwoPTwoCFormulation::pnsw);

// The gas component balance (air) is replaced by the total mass balance
SET_INT_PROP(PorousMediumSubProblem, ReplaceCompEqIdx, GET_PROP_TYPE(TypeTag, Indices)::contiNEqIdx);

// Used the fluid system from the coupled problem
SET_TYPE_PROP(PorousMediumSubProblem, FluidSystem,
              typename GET_PROP_TYPE(typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag), FluidSystem));

// Use formulation based on mass fractions
SET_BOOL_PROP(PorousMediumSubProblem, UseMoles, false);

// Use Kelvin equation to adapt the saturation vapor pressure
SET_BOOL_PROP(PorousMediumSubProblem, UseKelvinEquation, true);

// Enable/disable velocity output
SET_BOOL_PROP(PorousMediumSubProblem, VtkAddVelocity, true);

// Enable gravity
SET_BOOL_PROP(PorousMediumSubProblem, ProblemEnableGravity, true);

// Depth in third dimension
NEW_PROP_TAG(GridExtrusionFactor);
SET_SCALAR_PROP(PorousMediumSubProblem, GridExtrusionFactor, 1.0);

// Live plot of the evaporation rates
NEW_PROP_TAG(OutputPlotEvaporationRate);
SET_BOOL_PROP(PorousMediumSubProblem, OutputPlotEvaporationRate, false);

// Forward declarations
NEW_PROP_TAG(OutputFreqFluxOutput);
NEW_PROP_TAG(TimeManagerInitTime);

// Do not abort if the evaporation is constant
NEW_PROP_TAG(TimeManagerConstantEvapRateThreshold);
SET_SCALAR_PROP(PorousMediumSubProblem, TimeManagerConstantEvapRateThreshold, -1);
}

/*!
 * \ingroup ImplicitTestProblems
 * \ingroup MultidomainProblems
 * \brief Non-isothermal two-phase two-component porous-medium subproblem
 *        with coupling at the top boundary.
 *
 * \todo please doc me
 *
 * This sub problem uses the \ref TwoPTwoCModel. It is part of the 2p2cni model and
 * is combined with a model for the free flow domain.
 */
template <class TypeTag = TTAG(PorousMediumSubProblem) >
class PorousMediumSubProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView::Grid Grid;

    // copy some indices for convenience
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum { numEq = GET_PROP_VALUE(TypeTag, NumEq) };
    enum { numPhases = GET_PROP_VALUE(TypeTag, NumPhases) };
    enum { numComponents = GET_PROP_VALUE(TypeTag, NumComponents) };
    // the equation indices
    enum { contiTotalMassIdx = Indices::contiNEqIdx,
           contiWEqIdx = Indices::contiWEqIdx,
           energyEqIdx = Indices::energyEqIdx };
    // the indices of the primary variables
    enum { pressureIdx = Indices::pressureIdx,
           switchIdx = Indices::switchIdx,
           temperatureIdx = Indices::temperatureIdx };
    // the indices for the phase presence
    enum { wPhaseOnly = Indices::wPhaseOnly,
           nPhaseOnly = Indices::nPhaseOnly,
           bothPhases = Indices::bothPhases };
    // the indices for the phase presence
    enum { wCompIdx = Indices::wCompIdx,
           nCompIdx = Indices::nCompIdx };
    enum { wPhaseIdx = Indices::wPhaseIdx,
           nPhaseIdx = Indices::nPhaseIdx };
    // grid and world dimension
    enum { dim = GridView::dimension,
           dimWorld = GridView::dimensionworld };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    using ThermalConductivityModel = typename GET_PROP_TYPE(TypeTag, ThermalConductivityModel);
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    /*!
     * \brief The sub-problem for the porous-medium subdomain
     *
     * \param timeManager The TimeManager which is used by the simulation
     * \param gridView The simulation's idea about physical space
     */
    PorousMediumSubProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
        , gravity_(0.0)
    {
        if (GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, EnableGravity))
            gravity_[1] = -9.81;

        std::vector<Scalar> positions0 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions0);
        std::vector<Scalar> positions1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions1);

        bBoxMin_[0] = std::max(positions0.front(),GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1));
        bBoxMax_[0] = std::min(positions0.back(),GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2));
#if DUMUX_MULTIDOMAIN_DIM > 2
        bBoxMin_[2] = std::max(positions0.front(),GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyZ1));
        bBoxMax_[2] = std::min(positions0.back(),GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyZ2));
#endif
        bBoxMin_[1] = positions1.front();
        bBoxMax_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePosY);
        extrusionFactor_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, ExtrusionFactor);

        // parts of the interface without coupling
        try { runUpDistanceX1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, RunUpDistanceX1); }
        catch (...) { runUpDistanceX1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1); }
        try { runUpDistanceX2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, RunUpDistanceX2); }
        catch (...) { runUpDistanceX2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2); }

        refVelocity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefVelocity);

        refTemperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefTemperature);
        refPressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefPressure);
        refWaterSaturation_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefWaterSaturation);
        initialPhasePresence_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, PorousMedium, InitialPhasePresence);
        solDependentEnergyWalls_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, PorousMedium, SolDependentEnergyWalls);
        solDependentEnergySource_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, PorousMedium, SolDependentEnergySource);

        try {
            temperatureDataFilePM_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, PorousMedium, TemperatureDataFile);
            useTemperatureDataFilePM_ = true;
            readData(temperatureDataFilePM_, temperatureDataPM_);
        }
        catch (...) {
            useTemperatureDataFilePM_ = false;
        }

        freqFluxOutput_ = GET_PARAM_FROM_GROUP(TypeTag, int, Output, FreqFluxOutput);

        initializationTime_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, InitTime);

        constantEvapRateThreshold_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, ConstantEvapRateThreshold);
        storageLastTimestep_ = Scalar(0);
        storageChangeLastTimestep_ = Scalar(0);
        lastMassOutputTime_ = Scalar(0);

        std::string storageFile = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Output, Name) + "_box-storage.csv";
        outfile.open(storageFile);
        outfile << "#Time[s]" << ";"
                << "TotalMassChange[kg/(s*extFac)]" << ";"
                << "WaterMassChange[kg/(s*extFac))]" << ";"
                << "IntEnergyChange[J/(m^3*s*extFac)]" << ";"
                << "WaterMass[kg/extFac]" << ";"
                << "WaterMassLoss[kg/extFac]" << ";"
                << "EvaporationRate[mm/d]" << ";"
                << "moistureContent[-]"
                << std::endl;
    }

    //! \brief The destructor
    ~PorousMediumSubProblem()
    {
        outfile.close();
    }

    /*!
     * \name Problem parameters
     */
    // \{

    //! \copydoc BoxProblem::&bBoxMin()
    const GlobalPosition &bBoxMin() const
    { return bBoxMin_; }

    //! \copydoc BoxProblem::&bBoxMax()
    const GlobalPosition &bBoxMax() const
    { return bBoxMax_; }

    //! \copydoc Dumux::ImplicitProblem::name()
    const std::string name() const
    {
        return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Output, Name)
               + std::string("_box-pm");
    }

    //! \copydoc Dumux::ImplicitProblem::init()
    void init()
    {
        ParentType::init();
        this->model().globalStorage(storageLastTimestep_);
        initialWaterContent_ = storageLastTimestep_[contiWEqIdx];
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::boundaryTypesAtPos()
    void boundaryTypesAtPos(BoundaryTypes &values, const GlobalPosition &globalPos) const
    {
        Scalar time = this->timeManager().time();

        values.setAllNeumann();

        if (onUpperBoundary_(globalPos))
        {
            values.setAllNeumann();
//             values.setDirichlet(temperatureIdx, energyEqIdx);

            if (globalPos[0] > runUpDistanceX1_ + eps_
                && globalPos[0] < runUpDistanceX2_ - eps_
                && time >= initializationTime_)
            {
                values.setAllCouplingNeumann();
#if TRANSPORT
                values.setCouplingDirichlet(contiTotalMassIdx);
#endif
            }
        }
    }

    //! \copydoc Dumux::ImplicitProblem::dirichletAtPos()
    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        initial_(values, globalPos);
    }

    //! \copydoc Dumux::ImplicitProblem::solDependentNeumann()
    void solDependentNeumann(PrimaryVariables &values,
                             const Element &element,
                             const FVElementGeometry &fvGeometry,
                             const Intersection &intersection,
                             const int scvIdx,
                             const int boundaryFaceIdx,
                             const ElementVolumeVariables &elemVolVars) const
    {
        values = 0.;

        if(solDependentEnergyWalls_)
        {
            // assume thermal conduction through the plexiglass box
            Scalar plexiglassThermalConductivity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThermalConductivity);
            Scalar plexiglassThickness = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThickness);
            Scalar temperatureInside = elemVolVars[scvIdx].temperature();
            Scalar temperatureRef = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, SolDependentEnergyTemperature);
            if (useTemperatureDataFilePM_)
                temperatureRef = evaluateData(temperatureDataPM_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize());
            // NOTE: area of boundary face is multiplied automatically
            values[energyEqIdx] += plexiglassThermalConductivity
                                  * (temperatureInside - temperatureRef)
                                  / plexiglassThickness;
        }
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::sourceAtPos()

    void solDependentSource(PrimaryVariables &values,
                            const Element &element,
                            const FVElementGeometry &fvGeometry,
                            const int scvIdx,
                            const ElementVolumeVariables &elemVolVars) const
    {
        values = 0.;
        if(solDependentEnergySource_ && dim < 3)
        {
            // assume thermal conduction through the plexiglass box
            Scalar plexiglassThermalConductivity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThermalConductivity);
            Scalar plexiglassThickness = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThickness);
            Scalar thermalConductivityInside = ThermalConductivityModel::effectiveThermalConductivity(
                                                  elemVolVars[scvIdx].saturation(wPhaseIdx),
                                                  elemVolVars[scvIdx].fluidThermalConductivity(wPhaseIdx),
                                                  elemVolVars[scvIdx].fluidThermalConductivity(nPhaseIdx),
                                                  this->spatialParams().solidThermalConductivity(element, fvGeometry, scvIdx),
                                                  this->spatialParams().porosity(element, fvGeometry, scvIdx),
                                                  this->spatialParams().solidDensity(element, fvGeometry, scvIdx));
            Scalar harmonicThermalConductivity = 2.0 * plexiglassThermalConductivity * thermalConductivityInside
                                                 / (plexiglassThermalConductivity + thermalConductivityInside);
            Scalar temperatureInside = elemVolVars[scvIdx].temperature();
            Scalar temperatureRef = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, SolDependentEnergyTemperature);
            if (useTemperatureDataFilePM_)
                temperatureRef = evaluateData(temperatureDataPM_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize());

            // NOTE: - sign switches compared to Neumann boundary conditions
            //       - account for additional thickness with extrusionFactorForSourceTerm_ / 2.0, dofs are not located at the boundary
            // NOTE: - discussed and confirmed with Martin S. 2017-09-20
            Scalar extrusionFactor = this->extrusionFactorAtPos(element.geometry().corner(scvIdx));
            values[energyEqIdx] -= 2.0 * harmonicThermalConductivity
                                   * (temperatureInside - temperatureRef)
                                   / (plexiglassThickness + extrusionFactor / 2.0)
                                   / extrusionFactor;
        }
    }


    //! \copydoc Dumux::ImplicitProblem::initialAtPos()
     void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        values = 0.;

        initial_(values, globalPos);
    }

    // \}

    /*!
     * \brief Return the initial phase state inside a control volume.
     *
     * \param globalPos The global position
     */
    int initialPhasePresenceAtPos(const GlobalPosition &globalPos) const
    {
        if (std::strcmp(initialPhasePresence_.c_str(), "bothPhases") == 0)
        {
            return bothPhases;
        }
        else if (std::strcmp(initialPhasePresence_.c_str(), "nPhaseOnly") == 0)
        {
            return nPhaseOnly;
        }
        else
        {
            DUNE_THROW(Dune::NotImplemented, "Initial phases presence is unkown " << initialPhasePresence_.c_str());
        }
    }

    /*!
     * \brief Called by the time manager after the time integration to
     *        do some post processing on the solution.
     */
    void postTimeStep()
    {
        // Calculate masses
        PrimaryVariables storage;

        this->model().globalStorage(storage);
        const Scalar time = this->timeManager().time() +  this->timeManager().timeStepSize();

        // Write mass balance information for rank 0
        if (this->gridView().comm().rank() == 0)
        {
            if (this->timeManager().timeStepIndex() % freqFluxOutput_ == 0
                || this->timeManager().episodeWillBeFinished()
                || this->timeManager().willBeFinished())
            {
                GlobalPosition globalPos(0.0);
                PrimaryVariables storageChange(0.);
                storageChange = storageLastTimestep_ - storage;

                assert(time - lastMassOutputTime_ != 0);
                storageChange /= (time - lastMassOutputTime_);

                Scalar evaprate = storageChange[contiWEqIdx] / (bBoxMax_[0]-bBoxMin_[0]) * 86400.0;
#if DUMUX_MULTIDOMAIN_DIM > 2
                evaprate /= (bBoxMax_[2]-bBoxMin_[2]);
#else
                evaprate /= this->extrusionFactorAtPos(globalPos);
#endif

                std::cout << "Time[s]: " << time
                          << " TotalMass[kg]: " << storage[contiTotalMassIdx]
                          << " WaterMass[kg]: " << storage[contiWEqIdx]
                          << " IntEnergy[J/m^3]: " << storage[energyEqIdx]
                          << " WaterMassChange[kg/s]: " << storageChange[contiWEqIdx]
                          << " EvaporationRate[mm/d]: " << evaprate
                          << std::endl;
                if (this->timeManager().time() != 0.)
                    outfile << time << ";"
                            << storageChange[contiTotalMassIdx] << ";"
                            << storageChange[contiWEqIdx] << ";"
                            << storageChange[energyEqIdx] << ";"
                            << storage[contiWEqIdx] << ";"
                            << initialWaterContent_ - storage[contiWEqIdx] << ";"
                            << evaprate << ";"
                            << moistureContent()
                            << std::endl;
                static std::vector<double> x;
                static std::vector<double> y;

                x.push_back(time / 86400.0); // d
                y.push_back(evaprate);

                gnuplot_.resetPlot();
                gnuplot_.setDatafileSeparator(';');
                gnuplot_.setXRange(0, x[x.size()-1]);
                gnuplot_.setYRange(0, 15);
                if (constantEvapRateThreshold_ > 0)
                {
                    gnuplot_.setYRange(0, 1.5*evaprate);
                }
                gnuplot_.setXlabel("time [d]");
                gnuplot_.setYlabel("evaporation rate [mm/d]");
                gnuplot_.addDataSetToPlot(x, y, "#t[d];e[mm/d]", name() + "_evaprate.csv");
                if (GET_PARAM_FROM_GROUP(TypeTag, bool, Output, PlotEvaporationRate))
                {
                    gnuplot_.plot(name() + "_evaprate");
                }

                if (this->timeManager().timeStepIndex() % freqFluxOutput_ == 0
                    || this->timeManager().episodeWillBeFinished()
                    || this->timeManager().willBeFinished())
                {
                    std::cout << "eNew/eOld[-]: " << storageChange[contiWEqIdx] / storageChangeLastTimestep_[contiWEqIdx]
                              << " de[mm/d]: " << evaprate - evaprateLastTimestep_
                              << " e[mm/d]: " << evaprate
                              << std::endl;

                    if (storageChangeLastTimestep_[contiWEqIdx] / storageChange[contiWEqIdx] < 1.0 + constantEvapRateThreshold_
                        && storageChangeLastTimestep_[contiWEqIdx] / storageChange[contiWEqIdx] > 1.0 - constantEvapRateThreshold_
                        && (time - lastMassOutputTime_) > GET_PARAM_FROM_GROUP(TypeTag, int, TimeManager, MaxTimeStepSize))
                    {
                        gnuplot_.setOpenPlotWindow(false);
                        gnuplot_.plot(name() + "_evaprate"); // always create a png image
                        std::cout << "Evaporation rate is constant, simulation will be aborted." << std::endl;
                        std::cout << "Final evaporation rate [mm/d]: " << evaprate
                                  << " steady-state[d]: " << time / 86400.0
                                  << " lastRelativeChange[-]: " << storageChange[contiWEqIdx] / storageChangeLastTimestep_[contiWEqIdx]
                                  << " velocity[m/s]: " << refVelocity_
                                  << " patchSize[m]: " << (bBoxMax_[0]-bBoxMin_[0])
                                  << " patchLocation[m]: " << (bBoxMax_[0]+bBoxMin_[0]) / 2.0
                                  << std::endl;
                        exit(0);
                    }
                    else if (this->timeManager().willBeFinished())
                    {
                        gnuplot_.setOpenPlotWindow(false);
                        gnuplot_.plot(name() + "_evaprate"); // always create a png image
                        std::cout << "Final evaporation rate [mm/d]: " << evaprate
                                  << " tEnd[d]: " << time / 86400.0
                                  << " lastRelativeChange[-]: " << storageChange[contiWEqIdx] / storageChangeLastTimestep_[contiWEqIdx]
                                  << " velocity[m/s]: " << refVelocity_
                                  << " patchSize[m]: " << (bBoxMax_[0]-bBoxMin_[0])
                                  << " patchLocation[m]: " << (bBoxMax_[0]+bBoxMin_[0]) / 2.0
                                  << std::endl;
                    }
                }

                storageLastTimestep_ = storage;
                storageChangeLastTimestep_ = storageChange;
                evaprateLastTimestep_ = evaprate;
                lastMassOutputTime_ = time;
            }
        }
    }

    /*!
     * \brief Compute the integral moisture content
     */
    Scalar moistureContent()
    {
        Scalar vaporContent = 0.;
        Scalar liquidWaterContent = 0.;
        Scalar solidContent = 0.;

        for (const auto& element : elements(this->gridView()))
        {
            if(element.partitionType() == Dune::InteriorEntity)
            {
                FVElementGeometry fvGeometry;
                fvGeometry.update(this->gridView(), element);

                ElementVolumeVariables elemVolVars;
                elemVolVars.update(*this,
                                   element,
                                   fvGeometry,
                                   false /* oldSol? */);

                for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
                {
                    Scalar porosity = this->spatialParams().porosity(element, fvGeometry, scvIdx);
                    liquidWaterContent += porosity
                                          * elemVolVars[scvIdx].saturation(wPhaseIdx)
                                          * elemVolVars[scvIdx].density(wPhaseIdx)
                                          * fvGeometry.subContVol[scvIdx].volume;

                    vaporContent += porosity
                                    * elemVolVars[scvIdx].saturation(nPhaseIdx)
                                    * elemVolVars[scvIdx].density(nPhaseIdx)
                                    * elemVolVars[scvIdx].moleFraction(nPhaseIdx, wCompIdx)
                                    * fvGeometry.subContVol[scvIdx].volume;

                    solidContent += (1.0 - porosity)
                                    * this->spatialParams().solidDensity(element, fvGeometry, scvIdx)
                                    * fvGeometry.subContVol[scvIdx].volume;
                }
            }
        }
        return (liquidWaterContent + vaporContent)
               / (liquidWaterContent + vaporContent + solidContent);
    }

    /*!
     * \brief The depth of the problem in third dimension
     */
    Scalar extrusionFactorAtPos(const GlobalPosition &globalPos) const
    { return extrusionFactor_; }

    //! \brief Returns the acceleration due to gravity
    const GlobalPosition &gravity() const
    { return gravity_; }

private:
    /*!
     * \brief Internal method for the initial condition
     *        (reused for the dirichlet conditions!)
     */
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        if (GET_PROP_VALUE(TypeTag, Formulation) == TwoPTwoCFormulation::pnsw)
        {
            values[pressureIdx] = refPressure_;
            values[switchIdx] = refWaterSaturation_;
        }
        else
        {
            values[pressureIdx] = refPressure_
                                  + 1000. * this->gravity()[1] * (globalPos[1] - bBoxMax_[1]);
            values[switchIdx] = 1.0 - refWaterSaturation_;
        }
        values[temperatureIdx] = refTemperature_;
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < bBoxMin_[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > bBoxMax_[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < bBoxMin_[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > bBoxMax_[1] - eps_; }

    static constexpr Scalar eps_ = 1e-8;
    GlobalPosition bBoxMin_;
    GlobalPosition bBoxMax_;
    Scalar extrusionFactor_;

    int freqFluxOutput_;
    Scalar constantEvapRateThreshold_;
    PrimaryVariables storageLastTimestep_;
    PrimaryVariables storageChangeLastTimestep_;
    Scalar evaprateLastTimestep_;
    Scalar lastMassOutputTime_;
    Scalar initialWaterContent_;

    GlobalPosition gravity_;
    Scalar refVelocity_;
    Scalar refTemperature_;
    Scalar refPressure_;
    Scalar refWaterSaturation_;
    std::string initialPhasePresence_;
    bool solDependentEnergyWalls_;
    bool solDependentEnergySource_;
    bool useTemperatureDataFilePM_;
    std::string temperatureDataFilePM_;
    std::vector<double> temperatureDataPM_[2];

    Scalar runUpDistanceX1_;
    Scalar runUpDistanceX2_;
    Scalar initializationTime_;
    std::ofstream outfile;
    Dumux::GnuplotInterface<double> gnuplot_;
};
} //end namespace Dumux

#endif // DUMUX_TWOPTWOCNI_SUBPROBLEM_HH
