// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \brief Non-isothermal two-component Transport subproblem with air flowing
 *        from the left to the right and coupling at the bottom.
 */
#ifndef DUMUX_TRANSPORTTWOCNI_SUBPROBLEM_HH
#define DUMUX_TRANSPORTTWOCNI_SUBPROBLEM_HH

#include <dumux/multidomain/subdomainpropertydefaults.hh>
#include <dumux/material/components/air.hh>

#include <dumux/freeflow/transportncni/model.hh>
#include <dumux/io/readwritedatafile.hh>
#include <dumux/multidomain/2cnitransport2p2cni/transportncnicouplinglocalresidual.hh>

namespace Dumux
{

template <class TypeTag>
class FreeFlowSubProblem;

namespace Properties
{
NEW_TYPE_TAG(FreeFlowSubProblem,
             INHERITS_FROM(BoxTransportncni, SubDomain));

// Set the problem property
SET_TYPE_PROP(FreeFlowSubProblem, Problem, Dumux::FreeFlowSubProblem<TypeTag>);

// Use the TransportncniCouplingLocalResidual for the computation of the local residual in the Transport domain
SET_TYPE_PROP(FreeFlowSubProblem, LocalResidual, TransportncniCouplingLocalResidual<TypeTag>);

// Use the fluid system from the coupled problem
SET_TYPE_PROP(FreeFlowSubProblem, FluidSystem,
              typename GET_PROP_TYPE(typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag), FluidSystem));

// Disable use of mole formulation
SET_BOOL_PROP(FreeFlowSubProblem, UseMoles, false);

// // Ensures that we are on the correct dumux branch
NEW_PROP_TAG(FreeFlowTurbulentSchmidtNumber);
NEW_PROP_TAG(FreeFlowTurbulentPrandtlNumber);
SET_SCALAR_PROP(FreeFlowSubProblem, FreeFlowTurbulentSchmidtNumber, 1.0);
SET_SCALAR_PROP(FreeFlowSubProblem, FreeFlowTurbulentPrandtlNumber, 1.0);

// Depth in third dimension
NEW_PROP_TAG(GridExtrusionFactor);
SET_SCALAR_PROP(FreeFlowSubProblem, GridExtrusionFactor, 1.0);
}

/*!
 * \ingroup ImplicitTestProblems
 * \ingroup MultidomainProblems
 * \brief Transport2cni problem with air flowing from the left to the right.
 *
 * This sub problem uses the \ref Transport2cniModel. It is part of the 2cnitransport2p2cni model and
 * is combined with the 2p2cnisubproblem for the Darcy domain.
 */
template <class TypeTag>
class FreeFlowSubProblem : public TransportncniProblem<TypeTag>
{
    typedef FreeFlowSubProblem<TypeTag> ThisType;
    typedef TransportncniProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum { dim = GridView::dimension };
    // equation indices
    enum { transportEqIdx = Indices::transportEqIdx, // Index of the transport equation (massfraction)
           energyEqIdx =    Indices::energyEqIdx };  // Index of the energy equation (temperature)
    // primary variable indices
    enum { massOrMoleFracIdx = Indices::massOrMoleFracIdx,
           temperatureIdx = Indices::temperatureIdx };
    enum { transportCompIdx = Indices::transportCompIdx, // water component index
           phaseCompIdx = Indices::phaseCompIdx };       // air component index

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;

    typedef typename GridView::ctype CoordScalar;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<CoordScalar, dim> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dim> DimVector;

public:
    /*!
     * \brief The sub-problem for the Transport subdomain
     *
     * \param timeManager The TimeManager which is used by the simulation
     * \param gridView The simulation's idea about physical space
     */
    FreeFlowSubProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        std::vector<Scalar> positions0 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions0);
        std::vector<Scalar> positions1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions1);

        bBoxMin_[0] = positions0.front();
        bBoxMax_[0] = positions0.back();
        bBoxMin_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfacePosY);
        bBoxMax_[1] = positions1.back();
        extrusionFactor_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, ExtrusionFactor);

        // parts of the interface without coupling
        try { runUpDistanceX1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, RunUpDistanceX1); }
        catch (...) { runUpDistanceX1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1); }
        try { runUpDistanceX2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, RunUpDistanceX2); }
        catch (...) { runUpDistanceX2_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2); }

        colCellX0_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Input, ColumnCellCoordinate0);
        colCellX1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Input, ColumnCellCoordinate1);
        colFaceX0_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Input, ColumnFaceCoordinate0);
        colFaceX1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Input, ColumnFaceCoordinate1);
        colV0_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Input, ColumnVelocity0);
        colV1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Input, ColumnVelocity1);
        colP_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Input, ColumnPressure);
        colRho_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Input, ColumnDensity);
        colNut_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, Input, ColumnKinematicEddyViscosity);

        std::string inputDataCell = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Input, DataCell);
        readDataNew<Scalar>(inputDataCell, inputDataCell_, ',');
        std::string inputDataFace = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Input, DataFace);
        readDataNew<Scalar>(inputDataFace, inputDataFace_, ',');

        cellCoordinates_[0] = colCellX0_;
        cellCoordinates_[1] = colCellX1_;
        faceCoordinates_[0] = colFaceX0_;
        faceCoordinates_[1] = colFaceX1_;

        turbulentSchmidtNumber_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, TurbulentSchmidtNumber);
        turbulentPrandtlNumber_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, TurbulentPrandtlNumber);
    }

    /*!
     * \name Problem parameters
     */
    // \{

    //! \copydoc BoxProblem::&bBoxMin()
    const GlobalPosition &bBoxMin() const
    { return bBoxMin_; }

    //! \copydoc BoxProblem::&bBoxMax()
    const GlobalPosition &bBoxMax() const
    { return bBoxMax_; }

    //! \copydoc Dumux::ImplicitProblem::name()
    const std::string name() const
    {
        return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Output, Name)
               + std::string("_box-ff");
    }

    /*!
     * \brief Returns the velocity \f$[m/s]\f$
     *
     * \param globalPos The global position
     */
    DimVector velocityAtPos(const GlobalPosition &globalPos) const
    {
        DimVector velocity(0.0);
        velocity[0] = getNearestDataPoint<Scalar, dim>(inputDataCell_, globalPos, cellCoordinates_, colV0_, 1e-5, "v[0]");
        velocity[1] = getNearestDataPoint<Scalar, dim>(inputDataCell_, globalPos, cellCoordinates_, colV1_, 1e-5, "v[1]");
        return velocity;
    }

    /*!
     * \brief Returns the pressure \f$[Pa]\f$
     *
     * \param globalPos The global position
     */
    Scalar pressureAtPos(const GlobalPosition &globalPos) const
    {
//         return 1e5;
        return getNearestDataPoint<Scalar, dim>(inputDataCell_, globalPos, cellCoordinates_, colP_, 1e-5, "p");
    }

    /*!
     * \brief Returns the density \f$[kg/m^3]\f$
     *
     * \param globalPos The global position
     */
    Scalar densityAtPos(const GlobalPosition &globalPos, Scalar warningThreshold=1e-5) const
    {
        return getNearestDataPoint<Scalar, dim>(inputDataCell_, globalPos, cellCoordinates_, colRho_, warningThreshold, "rho");
    }

    /*!
     * \brief Returns the eddy diffusivity \f$[m^2/s]\f$
     *
     * \param globalPos The global position
     */
    Scalar eddyDiffusivityAtPos(const GlobalPosition &globalPos) const
    {
        if (colNut_ > 0)
            return getNearestDataPoint<Scalar, dim>(inputDataFace_, globalPos, faceCoordinates_, colNut_, 1e-5, "nu_t")
                   / turbulentSchmidtNumber_;
        else
            return 0.0;
    }

    /*!
     * \brief Returns the eddy conductivity \f$[W/(m*K)]\f$
     *
     * \param globalPos The global position
     */
    Scalar eddyThermalConductivityAtPos(const GlobalPosition &globalPos) const
    {
        if (colNut_ > 0)
            return getNearestDataPoint<Scalar, dim>(inputDataFace_, globalPos, faceCoordinates_, colNut_, 1e-5, "nu_t")
                   * Air<Scalar>::gasHeatCapacity(293,1e5)
                   * densityAtPos(globalPos, 9e9)
                   / turbulentPrandtlNumber_;
        else
            return 0.0;
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::boundaryTypesAtPos()
    void boundaryTypesAtPos(BoundaryTypes &values,
                            const GlobalPosition &globalPos) const
    {
        values.setAllDirichlet();

        if (onUpperBoundary_(globalPos))
        {
            values.setAllNeumann();
        }

        if (onLowerBoundary_(globalPos))
        {
            values.setAllNeumann();

            if (globalPos[0] > runUpDistanceX1_ + eps_
                && globalPos[0] < runUpDistanceX2_ - eps_)
            {
                values.setAllCouplingDirichlet();
            }
        }

        if (onLeftBoundary_(globalPos))
        {
            // setting the cornerpoints Dirichlet makes results look nicer
            values.setAllDirichlet();
        }

        if (onRightBoundary_(globalPos))
        {
            values.setAllOutflow();
            if (onUpperBoundary_(globalPos) || onLowerBoundary_(globalPos)) // corner point
            {
                values.setAllDirichlet();
            }
        }
    }

    //! \copydoc Dumux::ImplicitProblem::dirichletAtPos()
    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        initial_(values, globalPos);
    }

    //! \copydoc Dumux::ImplicitProblem::neumannAtPos()
    void neumannAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        values = 0.0;
    }

    //! \copydoc Dumux::ImplicitProblem::sourceAtPos()
    void sourceAtPos(PrimaryVariables &values,
                     const GlobalPosition &globalPos) const
    {
        values = 0.0;
    }

    //! \copydoc Dumux::ImplicitProblem::initialAtPos()
    void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        initial_(values, globalPos);
    }

    // \}

    /*!
     * \brief The depth of the problem in third dimension
     */
    Scalar extrusionFactorAtPos(const GlobalPosition &globalPos) const
    { return extrusionFactor_; }

private:
    // Internal method for the initial and Dirichlet conditions
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        values[massOrMoleFracIdx] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefMassFraction);
        values[temperatureIdx] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefTemperature);
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < bBoxMin_[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > bBoxMax_[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < bBoxMin_[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > bBoxMax_[1] - eps_; }

    static constexpr Scalar eps_ = 1e-8;
    GlobalPosition bBoxMin_;
    GlobalPosition bBoxMax_;
    Scalar extrusionFactor_;

    Scalar runUpDistanceX1_;
    Scalar runUpDistanceX2_;

    int colCellX0_;
    int colCellX1_;
    int colFaceX0_;
    int colFaceX1_;
    int colV0_;
    int colV1_;
    int colP_;
    int colRho_;
    int colNut_;
    DimVector cellCoordinates_;
    DimVector faceCoordinates_;
    mutable std::vector<std::vector<Scalar>> inputDataCell_;
    mutable std::vector<std::vector<Scalar>> inputDataFace_;

    Scalar turbulentSchmidtNumber_;
    Scalar turbulentPrandtlNumber_;
};
} //end namespace

#endif // DUMUX_TRANSPORTTWOCNI_SUBPROBLEM_HH
