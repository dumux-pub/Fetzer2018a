makeWarn() { make $@ 2> >(grep -B 3 -A 2 -E '/temp/fetzer/dumux.*/dumux.*warning|/temp/fetzer/dumux.*/dumux.*error'); }
makeWarn windtunnelstaggered_zeroeq &
makeWarn windtunnelstaggered_kepsilon
rm zeroeq*.csv zeroeq*.log
rm kepsilon*.csv kepsilon*.log

if [ "$1" == "lowVel" ]; then
  VELOCITY="-FreeFlow.RefVelocity 1.2"
  CELLS=(2 3 4 6 8 12 24)
  ZEROEQ_CELLS=(2 4 8 12 24)
  GNUPLOT_SCRIPT=scripts/wallfunctions_lowVel.gp
elif [ "$1" == "highVel" ]; then
  VELOCITY="-FreeFlow.RefVelocity 5.2"
  CELLS=(2 3 4 5 6 8 10 12 24)
  ZEROEQ_CELLS=(4 6 8 10 12 24)
  GNUPLOT_SCRIPT=scripts/wallfunctions_highVel.gp
else
  echo "No velocity specified. Abort."
  exit 3
fi
echo "Using" $VELOCITY "m/s"

KEPSILON_OPTS=(
  "$VELOCITY"
  "$VELOCITY -KEpsilon.WallFunctionTemperatureMinimum 30"
  "$VELOCITY -KEpsilon.WallFunctionComponentMinimum 30"
  "$VELOCITY -KEpsilon.WallFunctionComponentMinimum 30 -KEpsilon.WallFunctionTemperatureMinimum 30"
        )
NAMES=(
  "wf"
  "wf_onlyT"
  "wf_onlyC"
  "wf_CT"
)

CONFIG_TYPES=(
  "-KEpsilon.WallFunctionReplaceFluxesType 0"
  "-KEpsilon.WallFunctionReplaceFluxesType 1"
  "-KEpsilon.WallFunctionReplaceFluxesType 2"
  "-KEpsilon.WallFunctionReplaceFluxesType 3"
)
CONFIG_NAMES=(
  "added"
  "replaceTurb"
  "replaceLamTurb"
  "replaceAll"
)


for ((k=0;k<${#ZEROEQ_CELLS[@]};++k)); do
  CELL="${ZEROEQ_CELLS[k]}"
  echo "zeroeq" $CELL
  ZEROEQ_OPTS="$VELOCITY -ZeroEq.TurbulentSchmidtNumber 1. -ZeroEq.TurbulentPrandtlNumber 1."
 ./windtunnelstaggered_zeroeq wallfunctions_testing.input $ZEROEQ_OPTS -Grid.Cells1 "10 $CELL"  -Output.Name "zeroeq-$CELL" &> zeroeq-$CELL.log
  ZEROEQ_OPTS="$VELOCITY"
 ./windtunnelstaggered_zeroeq wallfunctions_testing.input $ZEROEQ_OPTS -Grid.Cells1 "10 $CELL"  -Output.Name "zeroeq_adapt-$CELL" &> zeroeq_adapt-$CELL.log
done

OUTFOLDER=~/Dropbox/Arbeit-IWS/wallfunctions/$1
mkdir -p $OUTFOLDER
for ((j=0;j<${#CONFIG_TYPES[@]};++j)); do
  CONFIG_TYPE=${CONFIG_TYPES[j]}
  CONFIG_NAME=${CONFIG_NAMES[j]}
  for ((i=0;i<${#KEPSILON_OPTS[@]};++i)); do
    rm kepsilon-*.csv
    OPT="$CONFIG_TYPE ${KEPSILON_OPTS[i]}"
    OUT="$CONFIG_NAME"_"${NAMES[i]}"
    rm useWallFunctions.txt
    for ((k=0;k<${#CELLS[@]};++k)); do
      CELL="${CELLS[k]}"
      OUTNAME=$OUT-$CELL
      echo $OUT $CELL
      ./windtunnelstaggered_kepsilon wallfunctions_testing.input $OPT -Grid.Cells1 "10 $CELL"  -Output.Name "kepsilon_$OUTNAME" &> kepsilon_$OUTNAME.log
      cp kepsilon_"$OUTNAME"_staggered_evaprate.csv kepsilon-"$CELL"_staggered_evaprate.csv
      grep -ni "use wall f" kepsilon_$OUTNAME.log > temp.txt
      USE_WF=`echo $?`
      if [ "$USE_WF" == "0" ]; then
        printf "%s " $CELL >> useWallFunctions.txt
      fi
    done
    gnuplot -e "useWF=\"`cat useWallFunctions.txt`\"" $GNUPLOT_SCRIPT
    mv wallfunctions.png $OUTFOLDER/wallfunctions_$OUT.png
#    mv kepsilon*.log $OUTFOLDER
  done
done

rm temp.txt

