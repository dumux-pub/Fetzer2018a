reset
set datafile separator ';'

set xrange [0:1.5]
set yrange [0:50]

set title 'with wall function '.useWF

set key out

plot \
'zeroeq-4_staggered_evaprate.csv'              w l  lc 7 lw 1 dt 2 t 'ny=4 zeroeq' , \
'zeroeq-8_staggered_evaprate.csv'              w l  lc 7 lw 2 dt 3 t 'ny=8 zeroeq' , \
'zeroeq-12_staggered_evaprate.csv'             w l  lc 7 lw 2 dt 4 t 'ny=12 zeroeq' , \
'zeroeq-24_staggered_evaprate.csv'             w l  lc 7 lw 2 dt 1 t 'ny=24 zeroeq' , \
'zeroeq_adapt-4_staggered_evaprate.csv'        w l  lc 1 lw 1 dt 2 t 'Sct=0.61, Prt=0.8, ny=4 zeroeq' , \
'zeroeq_adapt-8_staggered_evaprate.csv'        w l  lc 1 lw 2 dt 3 t 'Sct=0.61, Prt=0.8, ny=8 zeroeq' , \
'zeroeq_adapt-12_staggered_evaprate.csv'       w l  lc 1 lw 2 dt 4 t 'Sct=0.61, Prt=0.8, ny=12 zeroeq' , \
'zeroeq_adapt-24_staggered_evaprate.csv'       w l  lc 1 lw 2 dt 1 t 'Sct=0.61, Prt=0.8, ny=24 zeroeq' , \
'kepsilon-2_staggered_evaprate.csv'            w l  lc 2 lw 1 dt 3 t 'ny=2 kepsilon' , \
'kepsilon-4_staggered_evaprate.csv'            w l  lc 2 lw 1 dt 2 t 'ny=4 kepsilon' , \
'kepsilon-6_staggered_evaprate.csv'            w l  lc 2 lw 1 dt 4 t 'ny=6 kepsilon' , \
'kepsilon-8_staggered_evaprate.csv'            w l  lc 2 lw 2 dt 3 t 'ny=8 kepsilon' , \
'kepsilon-10_staggered_evaprate.csv'           w l  lc 2 lw 2 dt 2 t 'ny=10 kepsilon' , \
'kepsilon-12_staggered_evaprate.csv'           w l  lc 2 lw 2 dt 4 t 'ny=12 kepsilon' , \
'kepsilon-24_staggered_evaprate.csv'           w l  lc 2 lw 2 dt 1 t 'ny=24 kepsilon' , \


set term pngcairo dashed size 1024,768
set out "wallfunctions.png"
replot
