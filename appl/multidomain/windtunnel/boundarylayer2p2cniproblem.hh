// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief The problem description of a simple evaporation from a porous medium
 *        using a boundary layer model.
 */
#ifndef DUMUX_BOUNDARYLAYER2P2CNIPROBLEM_HH
#define DUMUX_BOUNDARYLAYER2P2CNIPROBLEM_HH

#include <dune/grid/io/file/dgfparser.hh>

#include <dumux/freeflow/boundarylayermodel.hh>
#include <dumux/freeflow/masstransfermodel.hh>
#include <dumux/io/gnuplotinterface.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivityjohansen.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivitysomerton.hh>
#include <dumux/material/fluidstates/compositional.hh>
#include <dumux/material/fluidsystems/h2oair.hh>
#include <dumux/porousmediumflow/2p2c/implicit/model.hh>
#include <dumux/porousmediumflow/implicit/problem.hh>

#include <dumux/io/readwritedatafile.hh>

#include "windtunnelbox_spatialparameters.hh"

namespace Dumux
{
template <class TypeTag>
class BoundaryLayerTwoPTwoCNIProblem;

namespace Properties
{
#ifdef IS_CC
NEW_TYPE_TAG(BoundaryLayerTwoPTwoCNIProblem, INHERITS_FROM(CCTwoPTwoCNI, WindTunnelSpatialParams));
#else
NEW_TYPE_TAG(BoundaryLayerTwoPTwoCNIProblem, INHERITS_FROM(BoxTwoPTwoCNI, WindTunnelSpatialParams));
#endif

// Set the grid type
#if DIMENSION == 3
#error This problem is not implemented for 3D.
#elif DIMENSION == 2
SET_TYPE_PROP(BoundaryLayerTwoPTwoCNIProblem, Grid, Dune::YaspGrid<2, Dune::TensorProductCoordinates<typename GET_PROP_TYPE(TypeTag, Scalar), 2> >);
#else
SET_TYPE_PROP(BoundaryLayerTwoPTwoCNIProblem, Grid, Dune::YaspGrid<1, Dune::TensorProductCoordinates<typename GET_PROP_TYPE(TypeTag, Scalar), 1> >);
#endif

// Set the problem property
SET_TYPE_PROP(BoundaryLayerTwoPTwoCNIProblem, Problem, Dumux::BoundaryLayerTwoPTwoCNIProblem<TypeTag>);

// Choose pn and Sw as primary variables
SET_INT_PROP(BoundaryLayerTwoPTwoCNIProblem, Formulation, TwoPTwoCFormulation::pnsw);

// The gas component balance (air) is replaced by the total mass balance
SET_INT_PROP(BoundaryLayerTwoPTwoCNIProblem, ReplaceCompEqIdx, GET_PROP_TYPE(TypeTag, Indices)::contiNEqIdx);

// Set fluid configuration
SET_TYPE_PROP(BoundaryLayerTwoPTwoCNIProblem, FluidSystem,
              FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar)>);

// Use a smaller FluidSystem table
NEW_PROP_TAG(ProblemUseSmallFluidSystemTable);
SET_BOOL_PROP(BoundaryLayerTwoPTwoCNIProblem, ProblemUseSmallFluidSystemTable, false);

// Use formulation based on mass fractions
SET_BOOL_PROP(BoundaryLayerTwoPTwoCNIProblem, UseMoles, false);

// Use Kelvin equation to adapt the saturation vapor pressure
SET_BOOL_PROP(BoundaryLayerTwoPTwoCNIProblem, UseKelvinEquation, true);

// Enable/disable velocity output
SET_BOOL_PROP(BoundaryLayerTwoPTwoCNIProblem, VtkAddVelocity, true);

// Enable gravity
SET_BOOL_PROP(BoundaryLayerTwoPTwoCNIProblem, ProblemEnableGravity, true);

// If UMFPack is not available, SuperLU solver is used:
#ifdef HAVE_UMFPACK
SET_TYPE_PROP(BoundaryLayerTwoPTwoCNIProblem, LinearSolver, UMFPackBackend<TypeTag>);
#else
SET_TYPE_PROP(BoundaryLayerTwoPTwoCNIProblem, LinearSolver, SuperLUBackend<TypeTag>);
#endif

// Set the default episode length
NEW_PROP_TAG(TimeManagerEpisodeLength);
SET_INT_PROP(BoundaryLayerTwoPTwoCNIProblem, TimeManagerEpisodeLength, 43200);

// No MassTransferModel on default
NEW_PROP_TAG(MassTransferModel);
SET_INT_PROP(BoundaryLayerTwoPTwoCNIProblem, MassTransferModel, 0);

// Depth in third dimension
NEW_PROP_TAG(GridExtrusionFactor);
SET_SCALAR_PROP(BoundaryLayerTwoPTwoCNIProblem, GridExtrusionFactor, 1.0);

// Live plot of the evaporation rates
NEW_PROP_TAG(OutputPlotEvaporationRate);
NEW_PROP_TAG(OutputPlotRefEvaporationRate);
SET_BOOL_PROP(BoundaryLayerTwoPTwoCNIProblem, OutputPlotEvaporationRate, false);
SET_BOOL_PROP(BoundaryLayerTwoPTwoCNIProblem, OutputPlotRefEvaporationRate, false);

// Set the output frequency
NEW_PROP_TAG(OutputFreqRestart);
NEW_PROP_TAG(OutputFreqFluxOutput);
NEW_PROP_TAG(OutputFreqOutput);
SET_INT_PROP(BoundaryLayerTwoPTwoCNIProblem, OutputFreqRestart, 10);
SET_INT_PROP(BoundaryLayerTwoPTwoCNIProblem, OutputFreqFluxOutput, 100000);
SET_INT_PROP(BoundaryLayerTwoPTwoCNIProblem, OutputFreqOutput, 5);

// Do not abort if the evaporation is constant
NEW_PROP_TAG(TimeManagerConstantEvapRateThreshold);
SET_SCALAR_PROP(BoundaryLayerTwoPTwoCNIProblem, TimeManagerConstantEvapRateThreshold, -1);
}

/*!
 * \ingroup ImplicitTestProblems
 * \ingroup MultidomainProblems
 * \brief Non-isothermal two-phase two-component porous-medium subproblem
 *        with coupling at the top boundary.
 *
 * \todo update description
 *
 * This sub problem uses the \ref TwoPTwoCModel. It is part of the 2p2cni model and
 * is combined with the zeroeq2cnisubproblem for the free flow domain.
 */
template <class TypeTag>
class BoundaryLayerTwoPTwoCNIProblem : public ImplicitPorousMediaProblem<TypeTag>
{
    typedef ImplicitPorousMediaProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GridView::Grid Grid;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum { // the equation indices
        contiTotalMassIdx = Indices::contiNEqIdx, // this contiNEqIdx index is replaced by the property above
        contiWEqIdx = Indices::contiWEqIdx,
        energyEqIdx = Indices::energyEqIdx
    };
    enum { // the indices of the primary variables
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,
        temperatureIdx = Indices::temperatureIdx
    };
    enum { // the indices for the components
        wCompIdx = Indices::wCompIdx,
        nCompIdx = Indices::nCompIdx
    };
    enum { // the indices for the phase presence
        wPhaseOnly = Indices::wPhaseOnly,
        nPhaseOnly = Indices::nPhaseOnly,
        bothPhases = Indices::bothPhases
    };
    enum {
        wPhaseIdx = Indices::wPhaseIdx,
        nPhaseIdx = Indices::nPhaseIdx
    };
    enum { // grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld
    };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::Intersection Intersection;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename Dumux::CompositionalFluidState<Scalar, FluidSystem> FluidState;
    using ThermalConductivityModel = typename GET_PROP_TYPE(TypeTag, ThermalConductivityModel);

    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    /*!
     * \brief The sub-problem for the porous-medium subdomain
     *
     * \param timeManager The TimeManager which is used by the simulation
     * \param gridView The simulation's idea about physical space
     */
    BoundaryLayerTwoPTwoCNIProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    {
        std::vector<Scalar> positions0 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions0);
        if (dim > 1)
        {
            std::vector<Scalar> positions1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<Scalar>, Grid, Positions1);
            Scalar noDarcyX1 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1);
            Scalar noDarcyX2 = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX2);

            bBoxMin_[0] = std::max(positions0.front(),noDarcyX1);
            bBoxMax_[0] = std::min(positions0.back(),noDarcyX2);
            bBoxMin_[1] = positions1.front();
            bBoxMax_[1] = positions1.back();
        }
        else
        {
            bBoxMin_[0] = positions0.front();
            bBoxMax_[0] = positions0.back();
        }
        // parts of the interface without coupling
        try { runUpDistanceX1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, RunUpDistanceX1); }
        catch (...) { runUpDistanceX1_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, NoDarcyX1); }

        extrusionFactor_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, ExtrusionFactor);

        refTemperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefTemperature);
        refPressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefPressure);
        refWaterSaturation_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, RefWaterSaturation);
        initialPhasePresence_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, PorousMedium, InitialPhasePresence);
        solDependentEnergyWalls_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, PorousMedium, SolDependentEnergyWalls);
        solDependentEnergySource_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, bool, PorousMedium, SolDependentEnergySource);

        try {
            velDataFile_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, FreeFlow, VelocityDataFile);
            useVelDataFile_ = true;
            readData(velDataFile_, velocityData_);
        }
        catch (...) {
            useVelDataFile_ = false;
        }

        try {
            massFractionDataFile_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, FreeFlow, MassFractionDataFile);
            useMassFractionDataFile_ = true;
            readData(massFractionDataFile_, massFractionData_);
        }
        catch (...) {
            useMassFractionDataFile_ = false;
        }

        try {
            temperatureDataFile_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, FreeFlow, TemperatureDataFile);
            useTemperatureDataFile_ = true;
            readData(temperatureDataFile_, temperatureData_);
        }
        catch (...) {
            useTemperatureDataFile_ = false;
        }

        try {
            temperatureDataFilePM_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, PorousMedium, TemperatureDataFile);
            useTemperatureDataFilePM_ = true;
            readData(temperatureDataFilePM_, temperatureDataPM_);
        }
        catch (...) {
            useTemperatureDataFilePM_ = false;
        }

        if (useTemperatureDataFile_ && !useMassFractionDataFile_)
        {
            try {
                relativeHumidityDataFile_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, FreeFlow, RelativeHumidityDataFile);
                useMassFractionDataFile_ = true;
                readData(relativeHumidityDataFile_, relativeHumidityData_);

                FluidState fluidState;
                fluidState.setPressure(nPhaseIdx, refPressure_);
                for (unsigned int i = 0; i < relativeHumidityData_[0].size()
                                         && i < temperatureData_[0].size(); ++i)
                {
                    fluidState.setTemperature(temperatureData_[1][i]);
                    Scalar moleFraction = relativeHumidityData_[1][i]
                                          * FluidSystem::vaporPressure(fluidState, wCompIdx)
                                          / fluidState.pressure(nPhaseIdx);
                    fluidState.setMoleFraction(nPhaseIdx, wCompIdx, moleFraction);
                    fluidState.setMoleFraction(nPhaseIdx, nCompIdx, 1-moleFraction);

                    massFractionData_[0].push_back(relativeHumidityData_[0][i]/*the time*/);
                    massFractionData_[1].push_back(fluidState.massFraction(nPhaseIdx, wCompIdx));
                }
                checkData(massFractionData_, "massFraction");
                std::string dataName[2] = {"Time[s]", "massFraction[-]"};
                writeDataFile(massFractionData_, dataName, "massFraction.dat");
            }
            catch (...) {
                useMassFractionDataFile_ = false;
            }
        }

        numberOfSoilLayers_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, SpatialParams, NumberOfSoilLayers);

        // define the free flow state
        refFFVelocity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefVelocity);
        refFFTemperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefTemperature);
        try {
            refFFMassFrac_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefMassFraction);
        }
        catch (...) {
            FluidState fluidState;
            fluidState.setPressure(nPhaseIdx, GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefPressure));
            fluidState.setTemperature(refFFTemperature_);
            Scalar relativeHumidity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, RefRelativeHumidity);
            fluidState.template setRelativeHumidity<FluidState>(fluidState, nPhaseIdx, wCompIdx, relativeHumidity);
            refFFMassFrac_ = fluidState.massFraction(nPhaseIdx, wCompIdx);
        }

        // define output options
        freqRestart_ = GET_PARAM_FROM_GROUP(TypeTag, int, Output, FreqRestart);
        freqOutput_ = GET_PARAM_FROM_GROUP(TypeTag, int, Output, FreqOutput);
        freqFluxOutput_ = GET_PARAM_FROM_GROUP(TypeTag, int, Output, FreqFluxOutput);
        vtkInternalIdx_ = 0;

        episodeLength_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, EpisodeLength);
        initializationTime_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, InitTime);
        if (initializationTime_ > 0.0)
            this->timeManager().startNextEpisode(initializationTime_);
        else
            this->timeManager().startNextEpisode(episodeLength_);

        constantEvapRateThreshold_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, TimeManager, ConstantEvapRateThreshold);
        storageLastTimestep_ = Scalar(0);
        storageChangeLastTimestep_ = Scalar(0);
        lastMassOutputTime_ = Scalar(0);

        std::string storageFileName = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Output, Name) + "_boundarylayer-storage.csv";
        storageFile_.open(storageFileName);
        storageFile_ << "#Time[s]" << ";"
                     << "TotalMassChange[kg/(s*mDepth)]" << ";"
                     << "WaterMassChange[kg/(s*mDepth))]" << ";"
                     << "IntEnergyChange[J/(m^3*s*mDepth)]" << ";"
                     << "WaterMass[kg/mDepth]" << ";"
                     << "WaterMassLoss[kg/mDepth]" << ";"
                     << "EvaporationRate[mm/s]" << ";"
                     << "moistureContent[-]"
                     << std::endl;

        for (unsigned int i = 0; i < numberOfSoilLayers_+1; ++i)
        {
            char outputname[20];
            if (i < numberOfSoilLayers_)
            {
                sprintf(outputname, "%s%02d%s","fluxes", i, ".csv");
            }
            else
            {
                sprintf(outputname, "%s%s","fluxes", ".csv");
            }
            fluxFileNames_.push_back(outputname);
            std::ofstream outfile(outputname, std::ios_base::out);
            outfile << "Time;"
                    << "TotalWaterMassFluxFF;"
                    << "EnergyFluxFF;"
                    << std::endl;
            outfile.close();
        }

        // spatial parameter stuff, if requested
        this->spatialParams().initRandomField(gridView);
        this->spatialParams().plotMaterialLaw();

        // initialize the tables of the fluid system
        if (GET_PARAM_FROM_GROUP(TypeTag, bool, Problem, UseSmallFluidSystemTable))
        {
            FluidSystem::init(/*tempMin=*/273.15, /*tempMax=*/323.15, /*numTemp=*/50,
                              /*pMin=*/5e4, /*pMax=*/1.5e5, /*numP=*/100);
        }
        else
        {
            FluidSystem::init(/*tempMin=*/273.15, /*tempMax=*/343.15, /*numTemp=*/140,
                              /*pMin=*/5e4, /*pMax=*/1e7, /*numP=*/995);
        }
    }

    //! \brief The destructor
    ~BoundaryLayerTwoPTwoCNIProblem()
    {
        storageFile_.close();
    }

    /*!
     * \name Problem parameters
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::name()
    const std::string name() const
    {
        return GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Output, Name)
               + std::string("_boundarylayer-pm");
    }

    //! \copydoc Dumux::ImplicitProblem::init()
    void init()
    {
        ParentType::init();
        this->model().globalStorage(storageLastTimestep_);
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::boundaryTypesAtPos()
    void boundaryTypesAtPos(BoundaryTypes &values,
                            const GlobalPosition &globalPos) const
    {
        values.setAllNeumann();

        if (onCouplingInterface_(globalPos))
        {
            values.setAllNeumann();
            values.setDirichlet(pressureIdx, contiTotalMassIdx);
        }
    }

    //! \copydoc Dumux::ImplicitProblem::dirichletAtPos()
    void dirichletAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        initial_(values, globalPos);
    }

    //! \copydoc Dumux::ImplicitProblem::solDependentNeumann()
    void solDependentNeumann(PrimaryVariables &values,
                             const Element &element,
                             const FVElementGeometry &fvGeometry,
                             const Intersection &intersection,
                             const int scvIdx,
                             const int boundaryFaceIdx,
                             const ElementVolumeVariables &elemVolVars) const
    {
        values = 0.;
        GlobalPosition globalPos = intersection.geometry().center();
        if(isBox)
        {
            globalPos = element.geometry().corner(scvIdx);
        }
        else
        {
            DUNE_THROW(Dune::NotImplemented, "boundary layer models are not implemented for CC, because harmonic averages are need, because dofs are not located on the boundary.");
        }

        // assume thermal conduction through the plexiglass box
        Scalar plexiglassThermalConductivity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThermalConductivity);
        Scalar plexiglassThickness = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThickness);
        Scalar temperatureInside = elemVolVars[scvIdx].temperature();
        Scalar temperatureRef = refFFTemperature_;
        if(solDependentEnergyWalls_)
        {
            temperatureRef = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, SolDependentEnergyTemperature);
            if (useTemperatureDataFilePM_)
                temperatureRef = evaluateData(temperatureDataPM_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize());
            values[energyEqIdx] += plexiglassThermalConductivity
                                  * (temperatureInside - temperatureRef)
                                  / plexiglassThickness;
        }

        if (onCouplingInterface_(globalPos))
        {
            Scalar velocity = refFFVelocity_;
            if (useVelDataFile_)
                velocity = evaluateData(velocityData_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize());

            temperatureInside = elemVolVars[scvIdx].temperature();
            temperatureRef = refFFTemperature_;
            if (useTemperatureDataFile_)
                temperatureRef = evaluateData(temperatureData_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize());

            Scalar moleFracInside = elemVolVars[scvIdx].moleFraction(nPhaseIdx, wCompIdx);
            Scalar massFracRef = refFFMassFrac_;
            if (useMassFractionDataFile_)
            {
                massFracRef = evaluateData(massFractionData_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize());
            }
            Dumux::CompositionalFluidState<Scalar, FluidSystem> fluidState;
            fluidState.setTemperature(temperatureRef);
            fluidState.setPressure(nPhaseIdx, refPressure_);
            fluidState.setMassFraction(nPhaseIdx, wCompIdx, massFracRef);
            Scalar moleFracRef = fluidState.moleFraction(nPhaseIdx, wCompIdx);

            Scalar xPosition = 0.0;
            if (dim > 1)
                xPosition = globalPos[0];

            Scalar distance = xPosition
                              + runUpDistanceX1_
                              + GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BoundaryLayer, Offset);
            Scalar kinematicViscosity = elemVolVars[scvIdx].viscosity(nPhaseIdx) / elemVolVars[scvIdx].density(nPhaseIdx);

            // initialize and run the boundary layer model
            unsigned blModel = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, int, BoundaryLayer, Model);
            BoundaryLayerModel<TypeTag> blmodel(velocity, distance, kinematicViscosity, blModel);
            if (blModel == 1)
                blmodel.setConstThickness(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BoundaryLayer, ConstThickness));
            if (blModel >= 4)
                blmodel.setRoughnessLength(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BoundaryLayer, RoughnessLength));
            blmodel.setYPlus(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BoundaryLayer, YPlus));
            Scalar massBLThickness = blmodel.massBoundaryLayerThickness();
            Scalar thermalBLThickness = blmodel.thermalBoundaryLayerThickness();

            // store the minimum and maximum boundary layer for output
            static Scalar xMin = 1e99;
            static Scalar xMax = -1e99;
            if (xPosition < xMin + eps_)
            {
                xMin = std::min(xPosition, xMin);
                viscousBLThicknessLeft_ = blmodel.viscousBoundaryLayerThickness();
            }
            if (xPosition > xMax - eps_)
            {
                xMax = std::max(xPosition, xMax);
                viscousBLThicknessRight_ = blmodel.viscousBoundaryLayerThickness();
            }

            // initialize and run the mass transfer model
            unsigned mtModel = GET_PARAM_FROM_GROUP(TypeTag, int, MassTransfer, Model);
            Scalar massTransferCoefficient = 1.0;
            if (mtModel != 0)
            {
                MassTransferModel<TypeTag> massTransferModel(elemVolVars[scvIdx].saturation(wPhaseIdx),
                                                             elemVolVars[scvIdx].porosity(),
                                                             massBLThickness, mtModel);
                if (mtModel == 1)
                    massTransferModel.setMassTransferCoeff(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, MassTransfer, Coefficient));
                if (mtModel == 2 || mtModel == 4)
                    massTransferModel.setCharPoreRadius(GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, MassTransfer, CharPoreRadius));
                if (mtModel == 3)
                    massTransferModel.setCapillaryPressure(elemVolVars[scvIdx].capillaryPressure());

                massTransferCoefficient = massTransferModel.massTransferCoefficient();
            }

            // calculate fluxes
            Scalar diffusiveMoleFluxWater = massTransferCoefficient
                                            * elemVolVars[scvIdx].diffCoeff(nPhaseIdx) // tortuosity is not included, pm requires phaseIdx
                                            * (moleFracInside - moleFracRef)
                                            / massBLThickness
                                            * elemVolVars[scvIdx].molarDensity(nPhaseIdx);
//             diffusiveMoleFluxWater = 1.0 /*mm/d*/ / 86400 /*s/d*/ / 1000 /*mm/m*/ * 1000 /*kg/m^3*/ / 18E-3;
            Scalar diffusiveMassFluxWater = diffusiveMoleFluxWater * FluidSystem::molarMass(wCompIdx);
            values[contiWEqIdx] = diffusiveMassFluxWater;

            // energy balance
            values[energyEqIdx] = 0.0;
            values[energyEqIdx] += FluidSystem::componentEnthalpy(elemVolVars[scvIdx].fluidState(), nPhaseIdx, wCompIdx)
                                   * diffusiveMassFluxWater;
            values[energyEqIdx] -= FluidSystem::componentEnthalpy(elemVolVars[scvIdx].fluidState(), nPhaseIdx, nCompIdx)
                                   * diffusiveMoleFluxWater * FluidSystem::molarMass(nCompIdx);
            values[energyEqIdx] += FluidSystem::thermalConductivity(elemVolVars[scvIdx].fluidState(), nPhaseIdx)
                                   * (temperatureInside - temperatureRef)
                                   / thermalBLThickness;


            FluxVariables boundaryVars;
            boundaryVars.update(*this,
                                element,
                                fvGeometry,
                                boundaryFaceIdx,
                                elemVolVars,
                                /*onBoundary=*/true);
            bool found = false;
            for (unsigned int i = 0; i < position_.size(); ++i)
            {
                if (position_[i][0] > globalPos[0] - eps_
                    && position_[i][0] < globalPos[0] + eps_
#if DIMENSION > 1
                    && position_[i][1] > globalPos[1] - eps_
                    && position_[i][1] < globalPos[1] + eps_
#endif
                   )
                {
                    waterFlux_[i] = values[contiWEqIdx];
                    energyFlux_[i] = values[energyEqIdx];
                    area_[i] = boundaryVars.face().area;
                    found = true;
                }
            }
            if (!found)
            {
                position_.push_back(globalPos);
                waterFlux_.push_back(0);
                energyFlux_.push_back(0);
                area_.push_back(0);
            }
        }
    }

    // \}

    /*!
     * \name Volume terms
     */
    // \{

    //! \copydoc Dumux::ImplicitProblem::sourceAtPos()

    void solDependentSource(PrimaryVariables &values,
                            const Element &element,
                            const FVElementGeometry &fvGeometry,
                            const int scvIdx,
                            const ElementVolumeVariables &elemVolVars) const
    {
        values = 0.;
        if(solDependentEnergySource_ && dim < 3)
        {
            // assume thermal conduction through the plexiglass box
            Scalar plexiglassThermalConductivity = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThermalConductivity);
            Scalar plexiglassThickness = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, PlexiglassThickness);
            Scalar thermalConductivityInside = ThermalConductivityModel::effectiveThermalConductivity(
                                                  elemVolVars[scvIdx].saturation(wPhaseIdx),
                                                  elemVolVars[scvIdx].fluidThermalConductivity(wPhaseIdx),
                                                  elemVolVars[scvIdx].fluidThermalConductivity(nPhaseIdx),
                                                  this->spatialParams().solidThermalConductivity(element, fvGeometry, scvIdx),
                                                  this->spatialParams().porosity(element, fvGeometry, scvIdx),
                                                  this->spatialParams().solidDensity(element, fvGeometry, scvIdx));

            Scalar extrusionFactor = this->extrusionFactorAtPos(element.geometry().corner(scvIdx));
            Scalar harmonicThermalConductivityFactor = plexiglassThermalConductivity * thermalConductivityInside
                                                       / (plexiglassThermalConductivity * extrusionFactor / 2.0
                                                          + thermalConductivityInside * plexiglassThickness);

            Scalar temperatureInside = elemVolVars[scvIdx].temperature();
            Scalar temperatureRef = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, PorousMedium, SolDependentEnergyTemperature);
            if (useTemperatureDataFilePM_)
                temperatureRef = evaluateData(temperatureDataPM_, this->timeManager().time(), this->timeManager().time()+this->timeManager().timeStepSize());

            // NOTE: - sign switches compared to Neumann boundary conditions
            //       - account for additional thickness with extrusionFactorForSourceTerm_ / 2.0, dofs are not located at the boundary
            // NOTE: - discussed and confirmed with Martin S. 2017-09-20
            values[energyEqIdx] -= 2.0 * harmonicThermalConductivityFactor
                                   * (temperatureInside - temperatureRef)
                                   / extrusionFactor;
        }
    }

    //! \copydoc Dumux::ImplicitProblem::initialAtPos()
    void initialAtPos(PrimaryVariables &values, const GlobalPosition &globalPos) const
    {
        values = 0.;
        initial_(values, globalPos);
    }

    /*!
     * \brief Return the initial phase state inside a control volume.
     *
     * \param globalPos The global position
     */
    int initialPhasePresenceAtPos(const GlobalPosition &globalPos) const
    {
        if (std::strcmp(initialPhasePresence_.c_str(), "bothPhases") == 0)
        {
            return bothPhases;
        }
        else if (std::strcmp(initialPhasePresence_.c_str(), "nPhaseOnly") == 0)
        {
            return nPhaseOnly;
        }
        else
        {
            DUNE_THROW(Dune::NotImplemented, "Initial phases presence is unkown " << initialPhasePresence_.c_str());
        }
    }

    // \}

    /*!
     * \brief Called by the time manager after the time integration to
     *        do some post processing on the solution.
     */
    void postTimeStep()
    {
        // Calculate masses
        PrimaryVariables storage;

        this->model().globalStorage(storage);
        const Scalar time = this->timeManager().time() +  this->timeManager().timeStepSize();

        static Scalar initialWaterContent;
        if (this->timeManager().time() <  this->timeManager().timeStepSize() + 1e-10)
            initialWaterContent = storage[contiWEqIdx];

        // Write mass balance information for rank 0
        if (this->gridView().comm().rank() == 0)
        {
            if (this->timeManager().timeStepIndex() % freqFluxOutput_ == 0
                || this->timeManager().episodeWillBeFinished()
                || this->timeManager().willBeFinished())
            {
                PrimaryVariables storageChange(0.);
                storageChange = storageLastTimestep_ - storage;

                assert(time - lastMassOutputTime_ != 0);
                storageChange /= (time - lastMassOutputTime_);

                // terminal output
                std::cout << "Time[s]: " << time
                          << " TotalMass[kg]: " << storage[contiTotalMassIdx]
                          << " WaterMass[kg]: " << storage[contiWEqIdx]
                          << " IntEnergy[J/m^3]: " << storage[energyEqIdx]
                          << " WaterMassChange[kg/s]: " << storageChange[contiWEqIdx]
                          << std::endl;

                // file output
                GlobalPosition globalPos(0.0);
                if (this->timeManager().time() != 0.)
                    storageFile_ << time << ";"
                                 << storageChange[contiTotalMassIdx] << ";"
                                 << storageChange[contiWEqIdx] << ";"
                                 << storageChange[energyEqIdx] << ";"
                                 << storage[contiWEqIdx] << ";"
                                 << initialWaterContent - storage[contiWEqIdx] << ";"
                                 << storageChange[contiWEqIdx]
#if DIMENSION > 1
                                    / (bBoxMax_[0]-bBoxMin_[0])
#endif
                                    / this->extrusionFactorAtPos(globalPos) << ";"
                                 << moistureContent()
                                 << std::endl;

                Scalar evaprate = storageChange[contiWEqIdx]
#if DIMENSION > 1
                                  / (bBoxMax_[0]-bBoxMin_[0])
#endif
                                  / this->extrusionFactorAtPos(globalPos)
                                  * 86400.0; // mm/d

                static double yMax = -1e100;
                static std::vector<double> x;
                static std::vector<double> y;

                x.push_back(time / 86400.0); // d
                y.push_back(evaprate);
                yMax = std::min(15.0,std::max(yMax, evaprate));

                gnuplot_.resetPlot();
                gnuplot_.setDatafileSeparator(';');
                gnuplot_.setXRange(0, x[x.size()-1]);
                gnuplot_.setYRange(0, yMax);
                gnuplot_.setXlabel("time [d]");
                gnuplot_.setYlabel("evaporation rate [mm/d]");
                try {
                    std::vector<std::string> refEvapRates = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::vector<std::string>, Output, PlotRefEvaporationRate);
                    for (unsigned int i = 0; i < refEvapRates.size(); ++i)
                        gnuplot_.addFileToPlot(refEvapRates[i], "w lp lw 2");
                }
                catch (...) { }

                gnuplot_.addDataSetToPlot(x, y, "#t[d];e[mm/d]", "evaprate.csv");
                if (GET_PARAM_FROM_GROUP(TypeTag, bool, Output, PlotEvaporationRate))
                {
                    gnuplot_.plot("0_evaprate");
                }

                std::cout << "---" << std::endl;
                std::cout << "- BoundaryLayerThicknessLeft [m]:" << viscousBLThicknessLeft_ << std::endl;
                std::cout << "- BoundaryLayerThicknessRight [m]:" << viscousBLThicknessRight_ << std::endl;
                std::cout << "- EvaporationRate [mm/d]: " << evaprate << std::endl;
                std::cout << "---" << std::endl;

                if (this->timeManager().timeStepIndex() % freqFluxOutput_ == 0
                    || this->timeManager().episodeWillBeFinished()
                    || this->timeManager().willBeFinished())
                {
                    std::cout << "eNew/eOld[-]: " << storageChange[contiWEqIdx] / storageChangeLastTimestep_[contiWEqIdx]
                              << " de[mm/d]: " << evaprate - evaprateLastTimestep_
                              << " e[mm/d]: " << evaprate
                              << std::endl;

                    Scalar patchSize = (bBoxMax_[0]-bBoxMin_[0]);
                    Scalar patchLocation = (bBoxMax_[0]+bBoxMin_[0]) / 2.0;
                    if (dim == 1)
                    {
                        patchSize = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BoundaryLayer, Offset) * 2.0;
                        patchLocation = runUpDistanceX1_ + GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, BoundaryLayer, Offset);
                    }

                    if (storageChangeLastTimestep_[contiWEqIdx] / storageChange[contiWEqIdx] < 1.0 + constantEvapRateThreshold_
                        && storageChangeLastTimestep_[contiWEqIdx] / storageChange[contiWEqIdx] > 1.0 - constantEvapRateThreshold_
                        && (time - lastMassOutputTime_) > GET_PARAM_FROM_GROUP(TypeTag, int, TimeManager, MaxTimeStepSize))
                    {
                        gnuplot_.setOpenPlotWindow(false);
                        gnuplot_.plot(name() + "_evaprate"); // always create a png image
                        std::cout << "Evaporation rate is constant, simulation will be aborted." << std::endl;
                        std::cout << "Final evaporation rate [mm/d]: " << evaprate
                                  << " steady-state[d]: " << time / 86400.0
                                  << " lastRelativeChange[-]: " << storageChange[contiWEqIdx] / storageChangeLastTimestep_[contiWEqIdx]
                                  << " velocity[m/s]: " << refFFVelocity_
                                  << " patchSize[m]: " << patchSize
                                  << " patchLocation[m]: " << patchLocation
                                  << std::endl;
                        exit(0);
                    }
                    else if (this->timeManager().willBeFinished())
                    {
                        gnuplot_.setOpenPlotWindow(false);
                        gnuplot_.plot(name() + "_evaprate"); // always create a png image
                        std::cout << "Final evaporation rate [mm/d]: " << evaprate
                                  << " tEnd[d]: " << time / 86400.0
                                  << " lastRelativeChange[-]: " << storageChange[contiWEqIdx] / storageChangeLastTimestep_[contiWEqIdx]
                                  << " velocity[m/s]: " << refFFVelocity_
                                  << " patchSize[m]: " << patchSize
                                  << " patchLocation[m]: " << patchLocation
                                  << std::endl;
                    }
                }

                storageLastTimestep_ = storage;
                storageChangeLastTimestep_ = storageChange;
                evaprateLastTimestep_ = evaprate;
                lastMassOutputTime_ = time;
            }
        }

        if (this->timeManager().timeStepIndex() % freqFluxOutput_ == 0
            || this->timeManager().episodeWillBeFinished()
            || this->timeManager().willBeFinished())
        {
            Scalar sumTotalWaterFlux = 0.;
            Scalar sumEnergyFlux = 0.;
            Scalar sumArea = 0.;
            int sum = 0;
            std::vector<Scalar> totalWaterFlux(numberOfSoilLayers_, 0.0);
            std::vector<Scalar> energyFlux(numberOfSoilLayers_, 0.0);
            std::vector<Scalar> area(numberOfSoilLayers_, 0.0);
            std::vector<int> number(numberOfSoilLayers_, 0);

            for (unsigned int i = 0; i < position_.size() && dim > 1; ++i)
            {
                int curLayerIdx = this->spatialParams().layerIdx(position_[i])[0];
                sumTotalWaterFlux += waterFlux_[i];
                sumEnergyFlux += energyFlux_[i];
                sumArea += area_[i];
                sum += 1;
                totalWaterFlux[curLayerIdx] += waterFlux_[i];
                energyFlux[curLayerIdx] += energyFlux_[i];
                area[curLayerIdx] += area_[i];
                number[curLayerIdx] += 1;
            }

            // TODO split contributions for different layers
            std::ofstream outfile(fluxFileNames_.back(), std::ios_base::app);
            outfile << time << ";"
                    << sumTotalWaterFlux / sum * sumArea << ";"
                    << sumEnergyFlux / sum * sumArea << ";"
                    << std::endl;
            outfile.close();
            for (unsigned int curLayerIdx = 0; curLayerIdx < numberOfSoilLayers_; ++curLayerIdx)
            {
                outfile.open(fluxFileNames_[curLayerIdx], std::ios_base::app);
                outfile << time << ";"
                        << totalWaterFlux[curLayerIdx] / number[curLayerIdx] * area[curLayerIdx] << ";"
                        << energyFlux[curLayerIdx] / number[curLayerIdx] * area[curLayerIdx] << ";"
                        << std::endl;
                outfile.close();
            }
        }
    }

    /*!
     * \brief Compute the integral moisture content
     */
    Scalar moistureContent()
    {
        Scalar vaporContent = 0.;
        Scalar liquidWaterContent = 0.;
        Scalar solidContent = 0.;

        for (const auto& element : elements(this->gridView()))
        {
            if(element.partitionType() == Dune::InteriorEntity)
            {
                FVElementGeometry fvGeometry;
                fvGeometry.update(this->gridView(), element);

                ElementVolumeVariables elemVolVars;
                elemVolVars.update(*this,
                                   element,
                                   fvGeometry,
                                   false /* oldSol? */);

                for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
                {
                    Scalar porosity = this->spatialParams().porosity(element, fvGeometry, scvIdx);
                    liquidWaterContent += porosity
                                          * elemVolVars[scvIdx].saturation(wPhaseIdx)
                                          * elemVolVars[scvIdx].density(wPhaseIdx)
                                          * fvGeometry.subContVol[scvIdx].volume;

                    vaporContent += porosity
                                    * elemVolVars[scvIdx].saturation(nPhaseIdx)
                                    * elemVolVars[scvIdx].density(nPhaseIdx)
                                    * elemVolVars[scvIdx].moleFraction(nPhaseIdx, wCompIdx)
                                    * fvGeometry.subContVol[scvIdx].volume;

                    solidContent += (1.0 - porosity)
                                    * this->spatialParams().solidDensity(element, fvGeometry, scvIdx)
                                    * fvGeometry.subContVol[scvIdx].volume;
                }
            }
        }
        return (liquidWaterContent + vaporContent)
               / (liquidWaterContent + vaporContent + solidContent);
    }

    //! \copydoc Dumux::CoupledProblem::episodeEnd()
    void episodeEnd()
    { this->timeManager().startNextEpisode(episodeLength_); }

    //! \copydoc Dumux::shouldWriteRestartFile()
    bool shouldWriteRestartFile() const
    {
        return (vtkInternalIdx_ % freqRestart_ == 0
                && this->timeManager().episodeWillBeFinished())
               || this->timeManager().willBeFinished();
    }

    //! \copydoc Dumux::shouldWriteOutput()
    bool shouldWriteOutput()
    {
        if (this->timeManager().timeStepIndex() % freqOutput_ == 0
            || this->timeManager().episodeWillBeFinished()
            || this->timeManager().willBeFinished())
        {
            vtkInternalIdx_++;
            return true;
        }
        return false;
    }

    /*!
     * \brief The depth of the problem in third dimension
     */
    Scalar extrusionFactorAtPos(const GlobalPosition &globalPos) const
    { return extrusionFactor_; }

private:
    /*!
     * \brief Internal method for the initial condition
     *        (reused for the dirichlet conditions!)
     */
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        if (GET_PROP_VALUE(TypeTag, Formulation) == TwoPTwoCFormulation::pnsw)
        {
            values[pressureIdx] = refPressure_;
            values[switchIdx] = refWaterSaturation_;
        }
        else
        {
            values[pressureIdx] = refPressure_
                                  + 1000. * this->gravity()[1] * (globalPos[1] - bBoxMax_[1]);
            values[switchIdx] = 1.0 - refWaterSaturation_;
        }
        values[temperatureIdx] = refTemperature_;
    }

    bool onCouplingInterface_(const GlobalPosition &globalPos) const
    {
        // excluding corner points leads too lower evaporation rates
        // ATTENTION: including them leads to trouble with the BCType for contiTotalMassIdx
        //            further they are normally excluded in coupled models, the evaprates are adapted the
        //            effects in lowering the evaporation rates
      if (dim > 1)
        return (onUpperBoundary_(globalPos) && !onCornerPoint_(globalPos));
      else
        return (onRightBoundary_(globalPos));
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < bBoxMin_[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > bBoxMax_[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < bBoxMin_[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > bBoxMax_[1] - eps_; }

    bool onCornerPoint_(const GlobalPosition &globalPos) const
    {
        return ((onLeftBoundary_(globalPos) && onLowerBoundary_(globalPos))
                || (onLeftBoundary_(globalPos) && onUpperBoundary_(globalPos))
                || (onRightBoundary_(globalPos) && onLowerBoundary_(globalPos))
                || (onRightBoundary_(globalPos) && onUpperBoundary_(globalPos)));
    }

    static constexpr Scalar eps_ = 1e-8;
    GlobalPosition bBoxMin_;
    GlobalPosition bBoxMax_;
    Scalar runUpDistanceX1_;
    Scalar extrusionFactor_;

    int freqRestart_;
    int freqOutput_;
    int freqFluxOutput_;
    unsigned int vtkInternalIdx_;

    Scalar constantEvapRateThreshold_;
    PrimaryVariables storageLastTimestep_;
    PrimaryVariables storageChangeLastTimestep_;
    Scalar evaprateLastTimestep_;
    Scalar episodeLength_;
    Scalar lastMassOutputTime_;

    Scalar refTemperature_;
    Scalar refPressure_;
    Scalar refWaterSaturation_;

    Scalar refFFVelocity_;
    Scalar refFFTemperature_;
    Scalar refFFMassFrac_;

    std::string initialPhasePresence_;
    bool solDependentEnergyWalls_;
    bool solDependentEnergySource_;

    bool useVelDataFile_;
    std::string velDataFile_;
    std::vector<double> velocityData_[2];
    bool useMassFractionDataFile_;
    std::string relativeHumidityDataFile_;
    std::string massFractionDataFile_;
    std::vector<double> relativeHumidityData_[2];
    std::vector<double> massFractionData_[2];
    bool useTemperatureDataFile_;
    bool useTemperatureDataFilePM_;
    std::string temperatureDataFile_;
    std::string temperatureDataFilePM_;
    std::vector<double> temperatureData_[2];
    std::vector<double> temperatureDataPM_[2];

    int numberOfSoilLayers_;

    mutable Scalar viscousBLThicknessLeft_;
    mutable Scalar viscousBLThicknessRight_;

    Scalar initializationTime_;
    std::ofstream storageFile_;
    std::vector<std::string> fluxFileNames_;
    Dumux::GnuplotInterface<double> gnuplot_;

    mutable std::vector<GlobalPosition> position_;
    mutable std::vector<Scalar> waterFlux_;
    mutable std::vector<Scalar> energyFlux_;
    mutable std::vector<Scalar> area_;
};
} //end namespace Dumux

#endif // DUMUX_TWOPTWOCNI_SUBPROBLEM_HH
