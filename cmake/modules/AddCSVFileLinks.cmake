# Creates a symbolic link to all *.csv files in the source directory
macro(add_csv_file_links)
  FILE(GLOB csv_files *.csv)
  foreach(VAR ${csv_files})
    get_filename_component(file_name ${VAR} NAME)
    dune_symlink_to_source_files(FILES ${file_name})
  endforeach()
endmacro()
