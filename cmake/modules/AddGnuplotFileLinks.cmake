# Creates a symbolic link to all gnuplot files in the source directory
macro(add_gnuplot_file_links)
  FILE(GLOB gnuplot_files *.gp)
  foreach(VAR ${gnuplot_files})
    get_filename_component(file_name ${VAR} NAME)
    dune_symlink_to_source_files(FILES ${file_name})
  endforeach()

  FILE(GLOB gnuplot_files *.gnuplot)
  foreach(VAR ${gnuplot_files})
    get_filename_component(file_name ${VAR} NAME)
    dune_symlink_to_source_files(FILES ${file_name})
  endforeach()
endmacro()
