#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/windtunnel/wavysurface/plateau/

# given names
sourcedir=$moduledir/appl/multidomain/windtunnel/
builddir=$builddir/appl/multidomain/windtunnel/
executable=windtunnelstaggered_lowrekepsilon
input=wavysurface.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

# the investigated shape of the interface
coeff=(0.84 0.98) # 0.84 \approx 0.84483 = 0.25 / 0.29 * 0.98

echo "" > simulation.sh
for ((i=0;i<${#coeff[@]};++i)); do
  echo "echo \"simulation starts on $HOST\" | tee logfile_sw${coeff[i]}.out" >> simulation.sh
  COMMAND="./$executable $input \
    -Grid.InterfaceProfile rectangle \
    -Grid.ObstacleScaling 1.6 \
    -PorousMedium.RefWaterSaturation ${coeff[i]} \
    -Output.Name \"plateau_sw${coeff[i]}\"
    | tee -a logfile_sw${coeff[i]}.out"
  echo $COMMAND >> simulation.sh
  echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile_sw${coeff[i]}.out" >> simulation.sh
  echo "cp -v \"plateau_sw${coeff[i]}\"_staggered-storage.csv $moduledir/results/wavysurface/" >> simulation.sh
done
chmod u+x simulation.sh
./simulation.sh
exit 0
