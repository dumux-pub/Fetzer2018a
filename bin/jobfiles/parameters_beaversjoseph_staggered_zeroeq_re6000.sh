#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/parameters/beaversjoseph/zeroeq_re6000

# given names
sourcedir=$moduledir/appl/multidomain/coupling/
builddir=$builddir/appl/multidomain/coupling/
helper1=filterconstantevaprate.sh
helper2=filtervtufiles.sh
helper3=extractlinedata.py
executable=parallelstaggered_zeroeq
input=parallel.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cp $moduledir/bin/scripts/$helper1 $simdir
cp $moduledir/bin/scripts/$helper2 $simdir
cp $moduledir/../dumux/bin/postprocessing/$helper3 $simdir
cd $simdir

# the investigated coefficients
coeff=(0.005 0.01 0.05 0.1 0.5 1.0 5.0)

echo "" > simulation.sh
for ((i=0;i<${#coeff[@]};++i)); do
  echo "echo \"simulation starts on $HOST\" | tee logfile_${coeff[i]}.out" >> simulation.sh
  COMMAND="./$executable $input \
    -FreeFlow.RefVelocity 0.35 \
    -TimeManager.DtInitial 1e-2 \
    -TimeManager.TEnd 345600 \
    -TimeManager.MaxTimeStepSize 7200 \
    -TimeManager.EpisodeLength 21600 \
    -Soil1.AlphaBJ ${coeff[i]} \
    -Newton.MaxRelativeShift 1e-5 \
    -Newton.ResidualReduction 1e-4 \
    -Newton.SatisfyResidualAndShiftCriterion true \
    -Output.Name \"beaversjoseph_${coeff[i]}\"
    | tee -a logfile_${coeff[i]}.out"
  echo $COMMAND >> simulation.sh
  echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile_${coeff[i]}.out" >> simulation.sh
  echo "pvpython $helper3 \
        -f \"beaversjoseph_${coeff[i]}\"_staggered-ffSecondary-00017.vtu -of zeroeq_re6000_bj${coeff[i]}_interface \
        -p1 0.0 0.25 0.0 -p2 1.5 0.25 0.0 -v 1" >> simulation.sh
  echo "cp zeroeq_re6000_bj${coeff[i]}_interface.csv $moduledir/results/parameters/beaversjoseph/" >> simulation.sh
  echo "pvpython $helper3 \
        -f \"beaversjoseph_${coeff[i]}\"_staggered-ffSecondary-00017.vtu -of zeroeq_re6000_bj${coeff[i]}_vertical \
        -p1 1.25 0.25 0.0 -p2 1.25 0.5 0.0 -v 1" >> simulation.sh
#   echo "cp zeroeq_re6000_bj${coeff[i]}_vertical.csv $moduledir/results/parameters/beaversjoseph/" >> simulation.sh
  echo "./$helper2 935 interface zeroeq_re6000_vtu.txt" >> simulation.sh
  echo "cp zeroeq_re6000_vtu.txt $moduledir/results/parameters/beaversjoseph/" >> simulation.sh
  echo "./$helper1 logfile zeroeq_re6000.txt" >> simulation.sh
  echo "cp zeroeq_re6000.txt $moduledir/results/parameters/beaversjoseph/" >> simulation.sh
done
chmod u+x simulation.sh
./simulation.sh
exit 0
