#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/windtunnel/experiments/davarzani2014a/reference2d

# given names
sourcedir=$moduledir/appl/multidomain/windtunnel/
builddir=$builddir/appl/multidomain/windtunnel/
helper=extractpointdataovertime.py
executable=windtunnelstaggered_zeroeq
input=davarzani2014a_1.22mps.input
data=($moduledir/data/davarzani2014a/1.22mps_massFraction.dat $moduledir/data/davarzani2014a/1.22mps_temperature.dat $moduledir/data/davarzani2014a/1.22mps_windSpeed.dat)

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cp ${data[*]} $simdir
cp $moduledir/../dumux/bin/postprocessing/$helper $simdir
cd $simdir

echo "echo \"simulation starts on $HOST\" | tee logfile.out" > simulation.sh
COMMAND="./$executable $input \
  -FreeFlow.VelocityDataFile \"1.22mps_windSpeed.dat\" \
  -FreeFlow.MassFractionDataFile \"1.22mps_massFraction.dat\" \
  -FreeFlow.TemperatureDataFile \"1.22mps_temperature.dat\" \
  | tee -a logfile.out"
echo $COMMAND >> simulation.sh
echo "pvpython $helper \
      -f davarzani2014a_staggered-pm.pvd -of davarzani2014a_sensor3 \
      -p 0.925 0.225 0.0 -v 1" >> simulation.sh
echo "pvpython $helper \
      -f davarzani2014a_staggered-pm.pvd -of davarzani2014a_sensor8 \
      -p 0.925 0.175 0.0 -v 1" >> simulation.sh
echo "cp -v davarzani2014a_sensor3.csv $moduledir/results/experiments/davarzani2014a_reference2d_sensor3.csv" >> simulation.sh
echo "cp -v davarzani2014a_sensor8.csv $moduledir/results/experiments/davarzani2014a_reference2d_sensor8.csv" >> simulation.sh
echo "cp -v davarzani2014a_staggered-storage.csv $moduledir/results/experiments/davarzani2014a_reference2d_storage.csv" >> simulation.sh
echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile.out" >> simulation.sh
chmod u+x simulation.sh
./simulation.sh
exit 0
