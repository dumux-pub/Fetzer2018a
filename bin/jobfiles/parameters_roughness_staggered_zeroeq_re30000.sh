#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/parameters/roughness/zeroeq_re30000

# given names
sourcedir=$moduledir/appl/multidomain/coupling/
builddir=$builddir/appl/multidomain/coupling/
helper1=filterconstantevaprate.sh
executable=parallelstaggered_zeroeq
input=parallel.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cp $moduledir/bin/scripts/$helper1 $simdir
cd $simdir

# the investigated roughness length in mm
coeff=(0.0 0.25 0.5 1.0 2.0 4.0 8.0)

echo "" > simulation.sh
for ((i=0;i<${#coeff[@]};++i)); do
  echo "echo \"simulation starts on $HOST\" | tee logfile_${coeff[i]}mm.out" >> simulation.sh
  COMMAND="./$executable $input \
    -FreeFlow.RefVelocity 1.75 \
    -ZeroEq.BBoxMinSandGrainRoughness ${coeff[i]}e-3 \
    -TimeManager.ConstantEvapRateThreshold 0.001 \
    -TimeManager.DtInitial 1e-2 \
    -TimeManager.TEnd 345600 \
    -TimeManager.MaxTimeStepSize 7200 \
    -TimeManager.EpisodeLength 21600 \
    -Output.Name \"roughness_${coeff[i]}\"
    | tee -a logfile_${coeff[i]}mm.out"
  echo $COMMAND >> simulation.sh
  echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile_${coeff[i]}mm.out" >> simulation.sh
  echo "./$helper1 logfile zeroeq_re30000.txt" >> simulation.sh
  echo "cp zeroeq_re30000.txt $moduledir/results/parameters/roughness/" >> simulation.sh
done
chmod u+x simulation.sh
./simulation.sh
exit 0
