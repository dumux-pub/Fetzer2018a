#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/windtunnel/wavysurface/height/

# given names
sourcedir=$moduledir/appl/multidomain/windtunnel/
builddir=$builddir/appl/multidomain/windtunnel/
executable=windtunnelstaggered_lowrekepsilon
input=wavysurface.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

# the investigated height of the elements in m
coeff=(0.0 0.0025 0.005 0.01 0.02) # 0.04 corresponds to shape_sinus

echo "" > simulation.sh
for ((i=0;i<${#coeff[@]};++i)); do
  echo "echo \"simulation starts on $HOST\" | tee logfile_${coeff[i]}m.out" >> simulation.sh
  COMMAND="./$executable $input \
    -Grid.ObstacleAmplitude ${coeff[i]} \
    -Output.Name \"height_${coeff[i]}\"
    | tee -a logfile_${coeff[i]}m.out"
  echo $COMMAND >> simulation.sh
  echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile_${coeff[i]}m.out" >> simulation.sh
  echo "cp -v \"height_${coeff[i]}\"_staggered-storage.csv $moduledir/results/wavysurface/" >> simulation.sh
done
chmod u+x simulation.sh
./simulation.sh
exit 0
