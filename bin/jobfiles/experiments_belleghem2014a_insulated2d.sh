#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/windtunnel/experiments/belleghem2014a/insulated2d

# given names
sourcedir=$moduledir/appl/multidomain/windtunnel/
builddir=$builddir/appl/multidomain/windtunnel/
helper=extractpointdataovertime.py
executable=windtunnelstaggered_zeroeq
input=belleghem2014a.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cp $moduledir/../dumux/bin/postprocessing/$helper $simdir
cd $simdir

echo "echo \"simulation starts on $HOST\" | tee logfile.out" > simulation.sh
COMMAND="./$executable $input \
  | tee -a logfile.out"
echo $COMMAND >> simulation.sh
echo "pvpython $helper \
      -f belleghem2014a_staggered-pm.pvd -of belleghem2014a_pm_10mm \
      -p 0.045 -0.01 0.0 -v 1" >> simulation.sh
echo "cp -v belleghem2014a_pm_10mm.csv $moduledir/results/experiments/belleghem2014a_insulated2d_pm_10mm.csv" >> simulation.sh
echo "cp -v belleghem2014a_staggered-storage.csv $moduledir/results/experiments/belleghem2014a_insulated2d_storage.csv" >> simulation.sh
echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile.out" >> simulation.sh
chmod u+x simulation.sh
./simulation.sh
exit 0
