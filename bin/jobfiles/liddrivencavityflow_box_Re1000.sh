#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/test/numerical/liddrivencavity_box_Re1000

# given names
sourcedir=$moduledir/test/numerical/
builddir=$builddir/test/numerical/
helper=extractlinedata.py
executable=test_liddrivencavityflownavierstokes_box
input=test_liddrivencavityflownavierstokes.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cp $moduledir/../dumux/bin/postprocessing/$helper $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
echo $PWD
COMMAND="./$executable $input\
  | tee -a logfile.out"
echo $COMMAND > simulation.sh
echo "pvpython $helper \
      -f liddrivencavitynavierstokes-00040.vtu -of box_Re1000U \
      -p1 0.5 0.0 0.0 -p2 0.5 1.0 0.0 -v 1" >> simulation.sh
echo "cp box_Re1000U.csv $moduledir/results/tests/numerical/" >> simulation.sh
echo "pvpython $helper \
      -f liddrivencavitynavierstokes-00040.vtu -of box_Re1000V \
      -p1 0.0 0.5 0.0 -p2 1.0 0.5 0.0 -v 1" >> simulation.sh
echo "cp box_Re1000V.csv $moduledir/results/tests/numerical/" >> simulation.sh
chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out
exit 0
