if [ "$1" == "output" -o $# -lt 1 ]; then
  sed -i "s#simdir=\$builddir/#simdir=/temp/fetzer/resultsPromo/#g" *.sh
fi
if [ "$1" == "mpirun" -o $# -lt 1 ]; then
  sed -i "s#COMMAND=\".\/\$executable#COMMAND=\"mpirun -np 1 \$executable#g" *.sh
fi
if [ "$1" == "reset" ]; then
  sed -i "s#^simdir=/temp/fetzer/resultsPromo/#simdir=\$builddir/#g" *.sh
  sed -i "s#COMMAND=\"mpirun -np 1 \$executable#COMMAND=\".\/\$executable#g" *.sh
fi
