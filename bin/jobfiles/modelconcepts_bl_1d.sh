#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/windtunnel/modelconcepts/boundarylayer_1d

# given names
sourcedir=$moduledir/appl/multidomain/windtunnel/
builddir=$builddir/appl/multidomain/windtunnel/
executable=boundarylayer_1d
input=modelconcepts.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "echo \"simulation starts on $HOST\" | tee logfile.out" > simulation.sh
COMMAND="./$executable $input \
  -Grid.Cells0 '16' \
  -Grid.Positions0 '0.0 0.25' \
  -Grid.Grading0 '-1.57539' \
  -BoundaryLayer.Offset 1.25 \
  -BoundaryLayer.Model 4 \
  -BoundaryLayer.YPlus 15 \
  -BoundaryLayer.RoughnessLength 0. \
  | tee -a logfile.out"
echo $COMMAND >> simulation.sh
echo "cp -v evaprate.csv $moduledir/results/modelconcepts/evaprate_boundarylayer_1d.csv" >> simulation.sh
echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile.out" >> simulation.sh
chmod u+x simulation.sh
./simulation.sh
exit 0
