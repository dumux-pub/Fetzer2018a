#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/coupling/parallel/staggered_cm2_navierstokes

# given names
sourcedir=$moduledir/appl/multidomain/coupling/
builddir=$builddir/appl/multidomain/coupling/
helper=filterconstantevaprate.sh
executable=parallelstaggered_zeroeq
input=parallel.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cp $moduledir/bin/scripts/$helper $simdir
cd $simdir

# for 16 cells
gradings=(1.02599 1.10962 1.18759 1.26345 1.33916 1.41590)
names=(12.8 6.4 3.2 1.6 0.8 0.4)

# for 32 cells
# gradings=(1.24959 1.21763 1.18574 1.15371 1.12118 1.08759 1.05198 1.01252)
# names=(0.05 0.1 0.2 0.4 0.8 1.6 3.2 6.4)

echo "" > simulation.sh
for ((i=0;i<${#gradings[@]};++i)); do
  echo "echo \"simulation starts on $HOST\" | tee logfile_${names[i]}mm.out" >> simulation.sh
  COMMAND="./$executable $input \
    -FreeFlow.RefVelocity 0.035 \
    -Grid.Grading1 '-${gradings[i]} ${gradings[i]}' \
    -ZeroEq.EddyViscosityModel 0 \
    -Coupling.Method 2 \
    -Output.Name \"parallel_${names[i]}\"
    | tee -a logfile_${names[i]}mm.out"
  echo $COMMAND >> simulation.sh
  echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile_${names[i]}mm.out" >> simulation.sh
  echo "./$helper logfile staggered_cm2_navierstokes.txt" >> simulation.sh
  echo "cp staggered_cm2_navierstokes.txt $moduledir/results/coupling/parallel/" >> simulation.sh
done
chmod u+x simulation.sh
./simulation.sh
exit 0
