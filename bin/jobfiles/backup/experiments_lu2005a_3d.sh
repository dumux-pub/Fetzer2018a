#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/windtunnel/experiments/lu2005a/3d

# given names
sourcedir=$moduledir/appl/multidomain/windtunnel/
builddir=$builddir/appl/multidomain/windtunnel/
helper=extractpointdataovertime.py
executable=windtunnelstaggered_zeroeq_3d
input=lu2005a.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cp $moduledir/../dumux/bin/postprocessing/$helper $simdir
cd $simdir

echo "echo \"simulation starts on $HOST\" | tee logfile.out" > simulation.sh
COMMAND="./$executable $input \
  -Newton.SatisfyResidualAndShiftCriterion true \
  -Newton.MaxRelativeShift 1e-4 \
  -Newton.ResidualReduction 1e-6 \
  -Newton.MaxAbsoluteResidual 1e-5 \
  -Newton.UseLineSearch true \
  | tee -a logfile.out"
echo $COMMAND >> simulation.sh
echo "pvpython $helper \
      -f lu2005a_staggered-pm.pvd -of lu2005a_surfacecenter \
      -p 0.2225 0.015 0.0725 -v 1" >> simulation.sh
echo "cp -v lu2005a_surfacecenter.csv $moduledir/results/experiments/lu2005a_3d_surfacecenter.csv" >> simulation.sh
echo "cp -v lu2005a_staggered-storage.csv $moduledir/results/experiments/lu2005a_3d_storage.csv" >> simulation.sh
echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile.out" >> simulation.sh
chmod u+x simulation.sh
./simulation.sh
exit 0
