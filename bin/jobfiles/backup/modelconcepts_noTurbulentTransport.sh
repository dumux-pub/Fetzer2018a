#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/windtunnel/modelconcepts/noTurbulentTransport

# given names
sourcedir=$moduledir/appl/multidomain/windtunnel/
builddir=$builddir/appl/multidomain/windtunnel/
executable=windtunnelbox_zeroeq
input=modelconcepts.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "echo \"simulation starts on $HOST\" | tee logfile.out" > simulation.sh
COMMAND="./$executable $input \
  -ZeroEq.EddyViscosityModel 2 \
  -ZeroEq.EddyDiffusivityModel 0 \
  -ZeroEq.EddyConductivityModel 0 \
  | tee -a logfile.out"
echo $COMMAND >> simulation.sh
echo "cp -v modelconcepts_box-pm_evaprate.csv $moduledir/results/modelconcepts/evaprate_noTurbulentTransport.csv" >> simulation.sh
echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile.out" >> simulation.sh
chmod u+x simulation.sh
./simulation.sh
exit 0
