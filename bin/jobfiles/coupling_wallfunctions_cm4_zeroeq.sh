#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/coupling/wallfunctions/staggered_cm4_zeroeq

# given names
sourcedir=$moduledir/appl/multidomain/coupling/
builddir=$builddir/appl/multidomain/coupling/
helper=filterconstantevaprate.sh
helper3=extractlinedata.py
executable=parallelstaggered_zeroeq
input=parallel.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cp $moduledir/bin/scripts/$helper $simdir
cp $moduledir/../dumux/bin/postprocessing/$helper3 $simdir
cd $simdir

cells=(2 4 8 16 20 24 28 32)
episode=(7200 7200 7200 7200 10800 10800 21600 21600)

echo "" > simulation.sh
for ((i=0;i<${#cells[@]};++i)); do
  echo "echo \"simulation starts on $HOST\" | tee logfile_${cells[i]}.out" >> simulation.sh
  COMMAND="./$executable $input \
    -Grid.Cells1 '16 ${cells[i]}' \
    -Grid.Grading1 '-1.57539 1.0' \
    -Coupling.Method 4 \
    -TimeManager.ConstantEvapRateThreshold 0.01 \
    -TimeManager.EpisodeLength ${episode[i]} \
    -Output.Name \"wallfunctions_${cells[i]}\"
    | tee -a logfile_${cells[i]}.out"
  echo $COMMAND >> simulation.sh
  echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile_${cells[i]}.out" >> simulation.sh
  echo "pvpython $helper3 \
        -f \"wallfunctions_${cells[i]}\"_staggered-ffSecondary-00009.vtu -of staggered_cm4_zeroeq_${cells[i]}_vertical \
        -p1 1.25 0.25 0.0 -p2 1.25 0.5 0.0 -v 1" >> simulation.sh
  echo "cp staggered_cm4_zeroeq_${cells[i]}_vertical.csv $moduledir/results/coupling/wallfunctions/" >> simulation.sh
  echo "./$helper logfile staggered_cm4_zeroeq.txt" >> simulation.sh
  echo "cp staggered_cm4_zeroeq.txt $moduledir/results/coupling/wallfunctions/" >> simulation.sh
done
chmod u+x simulation.sh
./simulation.sh
exit 0
