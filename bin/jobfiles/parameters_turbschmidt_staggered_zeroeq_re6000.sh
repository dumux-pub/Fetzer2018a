#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/parameters/turbschmidt/zeroeq_re6000

# given names
sourcedir=$moduledir/appl/multidomain/coupling/
builddir=$builddir/appl/multidomain/coupling/
helper1=filterconstantevaprate.sh
executable=parallelstaggered_zeroeq
input=parallel.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cp $moduledir/bin/scripts/$helper1 $simdir
cd $simdir

# the investigated coefficients
coeff=(0.4 0.6 0.8 1.0 1.3 1.6)

echo "" > simulation.sh
for ((i=0;i<${#coeff[@]};++i)); do
  echo "echo \"simulation starts on $HOST\" | tee logfile_${coeff[i]}.out" >> simulation.sh
  COMMAND="./$executable $input \
    -FreeFlow.RefVelocity 0.35 \
    -FreeFlow.TurbulentSchmidtNumber ${coeff[i]} \
    -TimeManager.ConstantEvapRateThreshold 0.001 \
    -TimeManager.DtInitial 1e-2 \
    -TimeManager.TEnd 345600 \
    -TimeManager.MaxTimeStepSize 7200 \
    -TimeManager.EpisodeLength 21600 \
    -Output.Name \"turbschmidt_${coeff[i]}\"
    | tee -a logfile_${coeff[i]}.out"
  echo $COMMAND >> simulation.sh
  echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile_${coeff[i]}.out" >> simulation.sh
  echo "./$helper1 logfile zeroeq_re6000.txt" >> simulation.sh
  echo "cp zeroeq_re6000.txt $moduledir/results/parameters/turbschmidt/" >> simulation.sh
done
chmod u+x simulation.sh
./simulation.sh
exit 0
