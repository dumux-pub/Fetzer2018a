#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/windtunnel/modelconcepts/transport_convergence

# given names
sourcedir=$moduledir/appl/multidomain/windtunnel/
builddir=$builddir/appl/multidomain/windtunnel/
helper=filterconstantevaprate.sh
executable=windtunnelbox_transport
helper_executable=windtunnelbox_zeroeq
input=modelconcepts.input

# make executable
cd $builddir
make $executable
make $helper_executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $builddir/$helper_executable $simdir
cp $sourcedir/$input $simdir
cp $moduledir/bin/scripts/$helper $simdir
cd $simdir

# the porous medium lengths
lengths=( 128.0 64.0 32.0 16.0 8.0 4.0 2.0 1.0 0.5 0.25 )

echo "" > simulation.sh
for ((i=0;i<${#lengths[@]};++i)); do
  position=`python -c "print ${lengths[i]}+1"`
  echo "echo \"simulation starts on $HOST\" | tee logfile_${lengths[i]}m.out" >> simulation.sh
  echo "./$helper_executable $input \
            -Grid.Positions0 '0.0 1.0 $position' \
            -Grid.NoDarcyX2 '$position' \
            -TimeManager.DtInitial 1e-1 \
            -TimeManager.TEnd 10800 \
            | tee logfileHelper_${lengths[i]}m.out" >> simulation.sh
  echo "mv volVarsData.csv zeroeqBoxVolVars_${lengths[i]}.csv" >> simulation.sh
  echo "mv fluxVarsData.csv zeroeqBoxFluxVars_${lengths[i]}.csv" >> simulation.sh
  COMMAND="./$executable $input \
    -Grid.Positions0 '0.0 1.0 $position' \
    -Grid.NoDarcyX2 '$position' \
    -Input.DataCell \"zeroeqBoxVolVars_${lengths[i]}.csv\" \
    -Input.DataFace \"zeroeqBoxFluxVars_${lengths[i]}.csv\" \
    -TimeManager.DtInitial 1e-1 \
    -TimeManager.TEnd 432000 \
    -Output.Name \"length_${lengths[i]}\" \
    | tee -a logfile_${lengths[i]}m.out"
  echo $COMMAND >> simulation.sh
  echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile_${lengths[i]}m.out" >> simulation.sh
  echo "./$helper logfile_ transport_convergence.txt" >> simulation.sh
  echo "cp transport_convergence.txt $moduledir/results/modelconcepts/" >> simulation.sh
done
chmod u+x simulation.sh
./simulation.sh
exit 0
