#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/test/analytical/doneahuerta/

# given names
sourcedir=$builddir/test/analytical/
builddir=$builddir/test/analytical/
helper=errorConvergence.sh
executable=test_doneahuerta
input=test_doneahuerta.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cp $sourcedir/$helper $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
echo $PWD
COMMAND="./$helper $executable \
  doneahuerta 2 4 6 \
  | tee -a logfile.out"
echo $COMMAND > simulation.sh
echo "cp error_doneahuerta.txt $moduledir/results/tests/analytical/error_doneahuerta.txt"  >> simulation.sh
chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out
exit 0
