#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/windtunnel/modelconcepts/transport

# given names
sourcedir=$moduledir/appl/multidomain/windtunnel/
builddir=$builddir/appl/multidomain/windtunnel/
executable=windtunnelbox_transport
helper_executable=windtunnelbox_zeroeq
input=modelconcepts.input

# make executable
cd $builddir
make $executable
make $helper_executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $builddir/$helper_executable $simdir
cp $sourcedir/$input $simdir
# cp $sourcedir/data/zeroeqBox*Vars.csv $simdir
cd $simdir

echo "echo \"simulation starts on $HOST\" | tee logfile.out" > simulation.sh
echo "./$helper_executable $input -TimeManager.TEnd 10800 | tee logfile_helper.out" >> simulation.sh
echo "mv volVarsData.csv zeroeqBoxVolVars.csv" >> simulation.sh
echo "mv fluxVarsData.csv zeroeqBoxFluxVars.csv" >> simulation.sh
COMMAND="./$executable $input \
  -Input.DataCell \"zeroeqBoxVolVars.csv\" \
  -Input.DataFace \"zeroeqBoxFluxVars.csv\" \
  | tee -a logfile.out"
echo $COMMAND >> simulation.sh
echo "cp -v modelconcepts_box-pm_evaprate.csv $moduledir/results/modelconcepts/evaprate_transport.csv" >> simulation.sh
echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile.out" >> simulation.sh
chmod u+x simulation.sh
./simulation.sh
exit 0
