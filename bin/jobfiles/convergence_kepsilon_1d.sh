#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/test/analytical/kepsilon_1d/

# given names
sourcedir=$builddir/test/analytical/
builddir=$builddir/test/analytical/
helper=errorConvergence.sh
executable=test_analytic_kepsilon_1d
input=test_analytic_kepsilon_1d.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cp $sourcedir/$helper $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
echo $PWD
COMMAND="./$helper $executable \
  analytic_kepsilon_1d 1 4 8 \
  | tee -a logfile.out"
echo $COMMAND > simulation.sh
echo "cp error_analytic_kepsilon_1d.txt $moduledir/results/tests/analytical/error_analytic_kepsilon_1d.txt"  >> simulation.sh
chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out
exit 0
