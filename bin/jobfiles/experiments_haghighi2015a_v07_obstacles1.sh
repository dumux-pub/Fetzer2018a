#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/obstacles/experiments/haghighi2015a/v07_obstacles1

# given names
sourcedir=$moduledir/appl/multidomain/obstacles/
builddir=$builddir/appl/multidomain/obstacles/
helper=extractpointdataovertime.py
executable=obstacles_lowrekepsilon_3d
input=haghighi2015a.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cp $moduledir/../dumux/bin/postprocessing/$helper $simdir
cd $simdir

echo "echo \"simulation starts on $HOST\" | tee logfile.out" > simulation.sh
COMMAND="./$executable $input \
  -Grid.NumCylinders 1 \
  | tee -a logfile.out"
echo $COMMAND >> simulation.sh
echo "cp -v haghighi2015a_staggered-pm-00011.vtu $moduledir/results/experiments/haghighi2015a_v07_obstacles1-pm-00011.vtu" >> simulation.sh
echo "cp -v haghighi2015a_staggered-storage.csv $moduledir/results/experiments/haghighi2015a_v07_obstacles1_storage.csv" >> simulation.sh
echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile.out" >> simulation.sh
chmod u+x simulation.sh
./simulation.sh
exit 0
