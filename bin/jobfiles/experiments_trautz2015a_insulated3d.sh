#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/windtunnel/experiments/trautz2015a/insulated3d

# given names
sourcedir=$moduledir/appl/multidomain/windtunnel/
builddir=$builddir/appl/multidomain/windtunnel/
helper=extractpointdataovertime.py
executable=windtunnelstaggered_zeroeq_3d
input=trautz2015a.input
data=($moduledir/data/trautz2015a/inflowMassFraction.dat $moduledir/data/trautz2015a/inflowTemperature.dat)

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cp ${data[*]} $simdir
cp $moduledir/../dumux/bin/postprocessing/$helper $simdir
cd $simdir

echo "echo \"simulation starts on $HOST\" | tee logfile.out" > simulation.sh
COMMAND="./$executable $input \
  -FreeFlow.MassFractionDataFile \"inflowMassFraction.dat\" \
  -FreeFlow.TemperatureDataFile \"inflowTemperature.dat\" \
  -PorousMedium.SolDependentEnergyWalls true \
  -Newton.MaxRelativeShift 1e-5 \
  -Newton.TargetSteps 7 \
  | tee -a logfile.out"
echo $COMMAND >> simulation.sh
echo "pvpython $helper \
      -f trautz2015a_staggered-pm.pvd -of trautz2015a_at18cm_firstRow \
      -p 0.18 -0.025 0.0 -v 1" >> simulation.sh
echo "pvpython $helper \
      -f trautz2015a_staggered-pm.pvd -of trautz2015a_at313cm_firstRow \
      -p 3.13 -0.025 0.0 -v 1" >> simulation.sh
echo "pvpython $helper \
      -f trautz2015a_staggered-pm.pvd -of trautz2015a_at711cm_firstRow \
      -p 7.11 -0.025 0.0 -v 1" >> simulation.sh
echo "pvpython $helper \
      -f trautz2015a_staggered-pm.pvd -of trautz2015a_at482cm_surface \
      -p 4.82 0.0 0.0 -v 1" >> simulation.sh
echo "cp -v trautz2015a_at18cm_firstRow.csv $moduledir/results/experiments/trautz2015a_insulated3d_at18cm_firstRow.csv" >> simulation.sh
echo "cp -v trautz2015a_at313cm_firstRow.csv $moduledir/results/experiments/trautz2015a_insulated3d_at313cm_firstRow.csv" >> simulation.sh
echo "cp -v trautz2015a_at711cm_firstRow.csv $moduledir/results/experiments/trautz2015a_insulated3d_at711cm_firstRow.csv" >> simulation.sh
echo "cp -v trautz2015a_at482cm_surface.csv $moduledir/results/experiments/trautz2015a_insulated3d_at482cm_surface.csv" >> simulation.sh
echo "cp -v trautz2015a_staggered-storage.csv $moduledir/results/experiments/trautz2015a_insulated3d_storage.csv" >> simulation.sh
echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile.out" >> simulation.sh
chmod u+x simulation.sh
./simulation.sh
exit 0
