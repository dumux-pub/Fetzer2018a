#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/coupling/parallel/staggered_cm4_kepsilon

# given names
sourcedir=$moduledir/appl/multidomain/coupling/
builddir=$builddir/appl/multidomain/coupling/
helper=filterconstantevaprate.sh
helper3=extractlinedata.py
executable=parallelstaggered_kepsilon
input=parallel.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cp $moduledir/bin/scripts/$helper $simdir
cp $moduledir/../dumux/bin/postprocessing/$helper3 $simdir
cd $simdir

# for 16 cells
gradings=(1.02599 1.10962 1.18759 1.26345 1.33916 1.41590 1.49446 1.57539 1.65913)
names=(12.8 6.4 3.2 1.6 0.8 0.4 0.2 0.1 0.05)

# for 32 cells
# gradings=(1.24959 1.21763 1.18574 1.15371 1.12118 1.08759 1.05198 1.01252)
# names=(0.05 0.1 0.2 0.4 0.8 1.6 3.2 6.4)

echo "" > simulation.sh
for ((i=0;i<${#gradings[@]};++i)); do
  echo "echo \"simulation starts on $HOST\" | tee logfile_${names[i]}mm.out" >> simulation.sh
  COMMAND="./$executable $input \
    -Grid.Grading1 '-${gradings[i]} ${gradings[i]}' \
    -Coupling.Method 4 \
    -TimeManager.DtInitial 1e-6 \
    -KEpsilon.UpdateStoredVariablesUntil 50.0 \
    -KEpsilon.UpdateStoredVariablesMaxTimeStep 1.0 \
    -KEpsilon.UseStoredEddyViscosity false \
    -Output.Name \"parallel_${names[i]}\"
    | tee -a logfile_${names[i]}mm.out"
  echo $COMMAND >> simulation.sh
  echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile_${names[i]}mm.out" >> simulation.sh
  echo "./$helper logfile staggered_cm4_kepsilon.txt" >> simulation.sh
  echo "cp staggered_cm4_kepsilon.txt $moduledir/results/coupling/parallel/" >> simulation.sh
done
echo "pvpython $helper3 \
        -f parallel_0.05_staggered-ffSecondary-00009.vtu -of staggered_cm4_kepsilon_0.05_vertical \
        -p1 1.25 0.25 0.0 -p2 1.25 0.5 0.0 -v 1" >> simulation.sh
echo "cp staggered_cm4_kepsilon_0.05_vertical.csv $moduledir/results/coupling/parallel/" >> simulation.sh
chmod u+x simulation.sh
./simulation.sh
exit 0
