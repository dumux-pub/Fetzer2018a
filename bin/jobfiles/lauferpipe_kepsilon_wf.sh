#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/test/physical/lauferpipe_kepsilon_wf

# given names
sourcedir=$moduledir/test/physical/
builddir=$builddir/test/physical/
helper=extractlinedata.py
executable=test_lauferpipe_kepsilon
input=test_lauferpipe.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cp $moduledir/../dumux/bin/postprocessing/$helper $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
COMMAND="./$executable $input \
  -Grid.Cells1 \"10 10\" \
  -Grid.Grading1 \"1.0 1.0\" \
  -Newton.TimeStepIncrementFactor 0.5 \
  | tee -a logfile.out"
echo $COMMAND > simulation.sh
echo "pvpython $helper \
      -f lauferpipe-ffSecondary-00020.vtu -of kepsilon_wf \
      -p1 8.0 0.0 0.0 -p2 8.0 0.2469 0.0 -v 1 -r 1000" >> simulation.sh
echo "cp kepsilon_wf.csv $moduledir/results/tests/physical/" >> simulation.sh
chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out
exit 0
