rm joblist.txt
if [ "$HOST" == "hal" ]; then
  find /temp/fetzer/dumux-promo/dumux-Fetzer2018a/bin/jobfiles -name \*.sh -exec sh -c 'echo sbatch "{}" -f >> temp.txt' \;
else
  find /temp/fetzer/dumux-promo/dumux-Fetzer2018a/bin/jobfiles -name \*.sh -exec sh -c 'echo sh "{}" -f >> temp.txt' \;
fi
sort temp.txt | tee joblist.txt
rm temp.txt
wc -l joblist.txt
