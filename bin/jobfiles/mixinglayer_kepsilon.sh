#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/test/numerical/mixinglayer_kepsilon

# given names
sourcedir=$moduledir/test/numerical/
builddir=$builddir/test/numerical/
helper=extractlinedata.py
executable=test_mixinglayer_kepsilon
input=test_mixinglayer_kepsilon.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cp $moduledir/../dumux/bin/postprocessing/$helper $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
echo $PWD
COMMAND="./$executable $input\
  | tee -a logfile.out"
echo $COMMAND > simulation.sh
echo "cp *0018.vtu $moduledir/results/tests/numerical/" >> simulation.sh
echo "pvpython $helper \
      -f mixinglayer_kepsilon-ffSecondary-00018.vtu -of y0.0 \
      -p1 0.0 0.0 0.0 -p2 3.0 0.0 0.0 -v 1" >> simulation.sh
echo "cp y0.0.csv $moduledir/results/tests/numerical/" >> simulation.sh
echo "pvpython $helper \
      -f mixinglayer_kepsilon-ffSecondary-00018.vtu -of x0.2 \
      -p1 0.2 -0.3 0.0 -p2 0.2 0.3 0.0 -v 1" >> simulation.sh
echo "cp x0.2.csv $moduledir/results/tests/numerical/" >> simulation.sh
echo "pvpython $helper \
      -f mixinglayer_kepsilon-ffSecondary-00018.vtu -of x1.0 \
      -p1 1.0 -0.3 0.0 -p2 1.0 0.3 0.0 -v 1" >> simulation.sh
echo "cp x1.0.csv $moduledir/results/tests/numerical/" >> simulation.sh
echo "pvpython $helper \
      -f mixinglayer_kepsilon-ffSecondary-00018.vtu -of x2.0 \
      -p1 2.0 -0.3 0.0 -p2 2.0 0.3 0.0 -v 1" >> simulation.sh
echo "cp x2.0.csv $moduledir/results/tests/numerical/" >> simulation.sh
chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out
exit 0
