#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/test/analytical/kovasznay/

# given names
sourcedir=$builddir/test/analytical/
builddir=$builddir/test/analytical/
helper=errorConvergence.sh
executable=test_kovasznay
input=test_kovasznay.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$helper $simdir
cp $sourcedir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "simulation starts on $HOST" | tee logfile.out
echo $PWD
COMMAND="./$helper $executable \
  kovasznay 2 4 6 \
  | tee -a logfile.out"
echo $COMMAND > simulation.sh
echo "cp error_kovasznay.txt $moduledir/results/tests/analytical/error_kovasznay.txt"  >> simulation.sh
chmod u+x simulation.sh
./simulation.sh
echo -e "\nsimulation ended on $HOST" | tee -a logfile.out
exit 0
