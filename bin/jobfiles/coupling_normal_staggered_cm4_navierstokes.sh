#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/coupling/normal/staggered_cm4_navierstokes

# given names
sourcedir=$moduledir/appl/multidomain/coupling/
builddir=$builddir/appl/multidomain/coupling/
helper1=evaluatesimulations.sh
helper2=gridconvergenceerror.sh
executable=normalstaggered
input=normal.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cp $moduledir/bin/scripts/$helper1 $simdir
cp $moduledir/bin/scripts/$helper2 $simdir
cd $simdir

refinements=( 0 1 2 3 4 5 )

echo "" > simulation.sh
for ((i=0;i<${#refinements[@]};++i)); do
  echo "echo \"simulation starts on $HOST\" | tee logfile_grid${refinements[i]}.out" >> simulation.sh
  COMMAND="./$executable $input \
    -Grid.Refinement ${refinements[i]} \
    -Coupling.Method 4 \
    -Output.Name \"normal_cm4_grid${refinements[i]}\"
    | tee -a logfile_grid${refinements[i]}.out"
  echo $COMMAND >> simulation.sh
  echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile_grid${refinements[i]}.out" >> simulation.sh
done
echo "cp normal_cm4_grid?-ff-00004_before.csv $moduledir/results/coupling/normal/" >> simulation.sh
echo "cd .." >> simulation.sh
echo "$simdir/$helper1 ." >> simulation.sh
echo "$simdir/$helper2 ." >> simulation.sh
echo "cp simTimeDetails*.txt cm?_vs_cm?.dat $moduledir/results/coupling/normal/" >> simulation.sh
chmod u+x simulation.sh
./simulation.sh
exit 0
