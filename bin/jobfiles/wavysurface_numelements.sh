#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/windtunnel/wavysurface/numelements/

# given names
sourcedir=$moduledir/appl/multidomain/windtunnel/
builddir=$builddir/appl/multidomain/windtunnel/
executable=windtunnelstaggered_lowrekepsilon
input=wavysurface.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

# the investigated length of one period elements in m
coeff=(-1.6 1.6 0.8 0.4 0.1) # 0.2 corresponds to shape_sinus
inittime=(10. 10. 10. 1000. 1000.)

echo "" > simulation.sh
for ((i=0;i<${#coeff[@]};++i)); do
  echo "echo \"simulation starts on $HOST\" | tee logfile_${coeff[i]}m.out" >> simulation.sh
  COMMAND="./$executable $input \
    -Grid.ObstacleScaling ${coeff[i]} \
    -FreeFlow.IncreaseVelocityUntil ${inittime[i]} \
    -Output.Name \"numelements_${coeff[i]}\"
    | tee -a logfile_${coeff[i]}m.out"
  echo $COMMAND >> simulation.sh
  echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile_${coeff[i]}m.out" >> simulation.sh
  echo "cp -v \"numelements_${coeff[i]}\"_staggered-storage.csv $moduledir/results/wavysurface/" >> simulation.sh
done
chmod u+x simulation.sh
./simulation.sh
exit 0
