#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/windtunnel/wavysurface/shape/

# given names
sourcedir=$moduledir/appl/multidomain/windtunnel/
builddir=$builddir/appl/multidomain/windtunnel/
executable=windtunnelstaggered_lowrekepsilon
input=wavysurface.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

# the investigated shape of the interface
coeff=(rectangle sawtooth sawtoothreversed sinus triangle)
inittime=(10. 10. 10. 1000. 10.)

echo "" > simulation.sh
for ((i=0;i<${#coeff[@]};++i)); do
  echo "echo \"simulation starts on $HOST\" | tee logfile_${coeff[i]}.out" >> simulation.sh
  COMMAND="./$executable $input \
    -Grid.InterfaceProfile \"${coeff[i]}\" \
    -FreeFlow.IncreaseVelocityUntil ${inittime[i]} \
    -Output.Name \"shape_${coeff[i]}\"
    | tee -a logfile_${coeff[i]}.out"
  echo $COMMAND >> simulation.sh
  echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile_${coeff[i]}.out" >> simulation.sh
  echo "cp -v \"shape_${coeff[i]}\"_staggered-pm-00009.vtu $moduledir/results/wavysurface/\"shape_${coeff[i]}\"-pm-2d.vtu" >> simulation.sh
  echo "cp -v \"shape_${coeff[i]}\"_staggered-ff-00009.vtu $moduledir/results/wavysurface/\"shape_${coeff[i]}\"-ff-2d.vtu" >> simulation.sh
  echo "cp -v \"shape_${coeff[i]}\"_staggered-pm-00033.vtu $moduledir/results/wavysurface/\"shape_${coeff[i]}\"-pm-8d.vtu" >> simulation.sh
  echo "cp -v \"shape_${coeff[i]}\"_staggered-ff-00033.vtu $moduledir/results/wavysurface/\"shape_${coeff[i]}\"-ff-8d.vtu" >> simulation.sh
  echo "cp -v \"shape_${coeff[i]}\"_staggered-pm-00057.vtu $moduledir/results/wavysurface/\"shape_${coeff[i]}\"-pm-14d.vtu" >> simulation.sh
  echo "cp -v \"shape_${coeff[i]}\"_staggered-ff-00057.vtu $moduledir/results/wavysurface/\"shape_${coeff[i]}\"-ff-14d.vtu" >> simulation.sh
  echo "cp -v \"shape_${coeff[i]}\"_staggered-storage.csv $moduledir/results/wavysurface/" >> simulation.sh
done
chmod u+x simulation.sh
./simulation.sh
exit 0
