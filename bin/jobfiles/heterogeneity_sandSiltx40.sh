#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/evapcon/heterogeneity_sandSilt

# given names
sourcedir=$moduledir/appl/multidomain/evapcon/
builddir=$builddir/appl/multidomain/evapcon/
executable=test_evapcon
input=heterogeneity.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "echo \"simulation starts on $HOST\" | tee logfile.out" > simulation.sh
COMMAND="./$executable $input \
  -Grid.Cells0 '10 21 10' \
  -SpatialParams.LeftSoil 2 \
  -SpatialParams.RightSoil 1 \
  | tee -a logfile.out"
echo $COMMAND >> simulation.sh
echo "cp -v fluxes.csv $moduledir/results/heterogeneity/sandSilt.csv" >> simulation.sh
echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile.out" >> simulation.sh
chmod u+x simulation.sh
./simulation.sh
exit 0
