#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/windtunnel/wavysurface/coupling/

# given names
sourcedir=$moduledir/appl/multidomain/windtunnel/
builddir=$builddir/appl/multidomain/windtunnel/
executable=windtunnelstaggered_zeroeq
input=wavysurface.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

# the investigated shape of the interface
coeff=(4)

echo "" > simulation.sh
for ((i=0;i<${#coeff[@]};++i)); do
  echo "echo \"simulation starts on $HOST\" | tee logfile_cm${coeff[i]}.out" >> simulation.sh
  COMMAND="./$executable $input \
    -TimeManager.EpisodeLength 720 \
    -TimeManager.TEnd 3600 \
    -Grid.Cells0 \"5 80 4 3\" \
    -Grid.Cells1 \"5 5 10 5\" \
    -Grid.InterfaceProfile rectangle \
    -Grid.ObstacleScaling 0.8 \
    -Grid.ObstacleOffset 1.2 \
    -Grid.ObstacleBaseline 0.27 \
    -Grid.ObstacleAmplitude 0.02 \
    -Coupling.Method ${coeff[i]} \
    -Coupling.InitMethod 3 \
    -Coupling.InitTime 360.0 \
    -Output.Name \"coupling_cm${coeff[i]}\"
    | tee -a logfile_sw${coeff[i]}.out"
  echo $COMMAND >> simulation.sh
  echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile_cm${coeff[i]}.out" >> simulation.sh
done
chmod u+x simulation.sh
./simulation.sh
exit 0
