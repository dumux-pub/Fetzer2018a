#!/bin/bash
umask 022

# custom input and output folder (adapt to your needs)
moduledir=/temp/fetzer/dumux-promo/dumux-Fetzer2018a/
builddir=$moduledir/build-clang/
simdir=$builddir/appl/multidomain/windtunnel/wavysurface/forchheimer/

# given names
sourcedir=$moduledir/appl/multidomain/windtunnel/
builddir=$builddir/appl/multidomain/windtunnel/
executable=windtunnelstaggered_lowrekepsilon_forchheimer
input=wavysurface.input

# make executable
cd $builddir
make $executable

# create output folder and copy all necessary files
if [ -e $simdir ] && [ "$1" != "-f" ]; then
  exit 1
fi
mkdir -p $simdir

cp $builddir/$executable $simdir
cp $sourcedir/$input $simdir
cd $simdir

echo "" > simulation.sh
# for ((i=0;i<${#coeff[@]};++i)); do
  echo "echo \"simulation starts on $HOST\" | tee logfile_forchheimer.out" >> simulation.sh
  COMMAND="./$executable $input \
    -Grid.InterfaceProfile rectangle \
    -Output.Name \"forchheimer\" \
    | tee -a logfile_forchheimer.out"
  echo $COMMAND >> simulation.sh
  echo "echo -e \"\nsimulation ended on $HOST\" | tee -a logfile_forchheimer.out" >> simulation.sh
  echo "cp -v \"forchheimer\"_staggered_evaprate.csv $moduledir/results/wavysurface/" >> simulation.sh
# done
chmod u+x simulation.sh
./simulation.sh
exit 0
