L2ERRORSCRIPT="/temp/fetzer/dumux-promo/dumux/bin/postprocessing/l2error.py"

function calculateError {
if [ -f $2 ]; then
  TEMP1=`python $L2ERRORSCRIPT -f1 $REFERENCE_FOLDERFILE -f2 $2 -x1 8 -y1 2 -x2 8 -y2 2`
  TEMP2=`python $L2ERRORSCRIPT -f1 $REFERENCE_FOLDERFILE -f2 $2 -x1 8 -y1 2 -x2 8 -y2 2 -xMin 0.0 -xMax 0.2001`
  TEMP3=`python $L2ERRORSCRIPT -f1 $REFERENCE_FOLDERFILE -f2 $2 -x1 8 -y1 2 -x2 8 -y2 2 -xMin 0.2001 -xMax 1.0`
  ABS_ALL=`echo $TEMP1 | awk '{ printf($2) }'`
  REL_ALL=`echo $TEMP1 | awk '{ printf($4) }'`
  ABS_FF=`echo $TEMP2 | awk '{ printf($2) }'`
  REL_FF=`echo $TEMP2 | awk '{ printf($4) }'`
  ABS_PM=`echo $TEMP3 | awk '{ printf($2) }'`
  REL_PM=`echo $TEMP3 | awk '{ printf($4) }'`
  printf "%-30s %9.4f %9.4f %9.4f  %9.4f %9.4f %9.4f\n" $1 $ABS_ALL $ABS_FF $ABS_PM $REL_ALL $REL_FF $REL_PM | tee -a $OUTFILE
else
  printf "%-30s %9s %9s %9s  %9s %9s %9s\n" $1 dnc dnc dnc dnc dnc dnc | tee -a $OUTFILE
fi
}

DATA_FOLDER=(
cm1
cm2
cm3
cm4
cm1
cm2
cm3
cm4
)
REFERENCE_FOLDER=(
cm1
cm2
cm3
cm4
cm4
cm4
cm4
cm4
)

RESULTPATH=$1

for (( i=0; i<${#DATA_FOLDER[@]}; i++ )); do
  DATA=${DATA_FOLDER[$((i))]}
  REFERENCE=${REFERENCE_FOLDER[$((i))]}
  OUTFILE=$RESULTPATH"/"$DATA"_vs_"$REFERENCE.dat
  echo -e "\n"$OUTFILE
  printf "%-30s %9s %9s %9s  %9s %9s %9s\n" " " " " Absolute " " " " Relative " " | tee  $OUTFILE
  printf "%-30s %9s %9s %9s  %9s %9s %9s\n" Simulation All FF PM All FF PM | tee -a $OUTFILE

  REFERENCE_FOLDERFILE="$RESULTPATH/staggered_"$REFERENCE"_navierstokes/normal_"$REFERENCE"_grid5-ff-00004_before.csv"
  calculateError $DATA"/grid0" $RESULTPATH/staggered_"$DATA"_navierstokes/normal_"$DATA"_grid0-ff-00004_before.csv
  calculateError $DATA"/grid1" $RESULTPATH/staggered_"$DATA"_navierstokes/normal_"$DATA"_grid1-ff-00004_before.csv
  calculateError $DATA"/grid2" $RESULTPATH/staggered_"$DATA"_navierstokes/normal_"$DATA"_grid2-ff-00004_before.csv
  calculateError $DATA"/grid3" $RESULTPATH/staggered_"$DATA"_navierstokes/normal_"$DATA"_grid3-ff-00004_before.csv
  calculateError $DATA"/grid4" $RESULTPATH/staggered_"$DATA"_navierstokes/normal_"$DATA"_grid4-ff-00004_before.csv
  calculateError $DATA"/grid5" $RESULTPATH/staggered_"$DATA"_navierstokes/normal_"$DATA"_grid5-ff-00004_before.csv
done
