#!/bin/bash

echo "# line $1 of files matching *$2*" > temp.txt
for FILE in *$2*; do
  VTUDATA=`sed "$1q;d" $FILE`
  echo $FILE $VTUDATA >> temp.txt
done
sed "s#_# #g" -i temp.txt
sed "s# re# re #g" -i temp.txt
sed "s# bj# bj #g" -i temp.txt
sed "s#,# #g" -i temp.txt
mv temp.txt $3
