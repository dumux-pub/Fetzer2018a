#!/bin/bash

# find . -name logfile.out -c 'grep "blabla" "{}" | tail -n 1 >> test.txt' \;
grep "Final evaporation rate" $1* > temp.txt
sed "s#\_# #g" -i temp.txt
sed "s#mm.out:# #g" -i temp.txt
sed "s#.out:# #g" -i temp.txt
sed "s#\.\.\/##g" -i temp.txt
sort -nrk 2,2 temp.txt > $2
cat $2
rm temp.txt
