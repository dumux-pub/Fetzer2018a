if [ $# -lt 1 -o "${args[0]}" = "-h" ]; then
  echo;
  echo "USAGE: $0 DIRECTORY OPTIONS"
  echo "       DIRECTORY    to be scanned for simulations (processes all subdirectories)"
  echo "       OPTIONS"
  echo "                -r  list only running simualtions"
  echo "                -v  output verbosity"
  echo; exit
fi

echo $1
LOGFILES=($(find $1 -name logfile\*.out | sort))
GLOBALOUTFILE="simTimeDetailsAll.txt"

printf "%-50s & %5s & %10s & %10s & %10s & %10s & %10s & %10s & %10s\n\\midrule\n" Directory Status TEnd[s] SimT[s] WallT[s] noTS[-] noNewS[-] AssmbT[s] SolveT[s] > $GLOBALOUTFILE

for i in "${LOGFILES[@]}"
do
    INFILE=$i
    INDIRECTORY=`dirname $INFILE`
    WALL_TIME=`grep "Time step" $INFILE | tail -n 1 | awk '{ printf("%s\n", $6) }'`
    WALL_TIME=${WALL_TIME#time:}
    WALL_TIME=${WALL_TIME%,}
    SIM_TIME=`grep "Time step" $INFILE | tail -n 1 | awk '{ printf("%s\n", $7) }'`
    SIM_TIME=${SIM_TIME#time:}
    SIM_TIME=${SIM_TIME%,}
    grep "Newton iteration" $INFILE > temp.txt
    NUM_NEWTONSTEPS=`wc -l temp.txt | awk '{ printf("%s\n", $1) }'`
    grep "Time step" $INFILE > temp.txt
    NUM_TIMESTEPS=`wc -l temp.txt | awk '{ printf("%s\n", $1) }'`
    TEND=`grep ^"TEnd = " $INDIRECTORY/*input | awk '{print $3}'`
    PERCENTAGE=`echo $SIM_TIME $TEND | awk '{ printf("%f\n", $1/$2*100) }' 2> /dev/null`
    if [ "$PERCENTAGE" == "" ]; then
      PERCENTAGE=0.0
    fi
    grep -i "Assemble/" $INFILE > temp.txt
    sed -i "s#(# #g" temp.txt
    sed -i "s#)/# #g" temp.txt
    ASSEMBLE_TIME=`awk '{ sum += $3 } END { print sum }' temp.txt`
    SOLVE_TIME=`awk '{ sum += $5 } END { print sum }' temp.txt`
    UPDATE_TIME=`awk '{ sum += $7 } END { print sum }' temp.txt`

    SIMULATION_FINISHED=`tail -n 1 $INFILE | awk '{ printf("%s", $1) }'`
    grep -nri -E "Simulation took" $INFILE
    FINISHED_CORRECTLY=`echo $?`

    # special case: abort if evaporation rate was constant
    if [ $FINISHED_CORRECTLY -ne 0 ]; then
      grep -nri -E "Final steady state evaporation rate" $INFILE > /dev/null
      FINISHED_CORRECTLY=`echo $?`
      if [ $FINISHED_CORRECTLY -eq 0 ]; then
        TEND=`grep "Time[s]: " $INDIRECTORY/*input | awk '{print $2}'`
        PERCENTAGE=100
      fi
    fi

    NAME=$INDIRECTORY
    if [ "$2" == "-v" ]; then
      NAME=$i
    fi

    if [ "$SIMULATION_FINISHED" == "simulation" -a "$2" != "-r" ]; then # finished
      if [ $FINISHED_CORRECTLY -ne 0 ]; then # dnf
        printf "%-50s &    dnf & %10.0f & %10.0f & %10.1f & %10.0f & %10.0f & %10.0f & %10.0f\n" $NAME $TEND $SIM_TIME $WALL_TIME $NUM_TIMESTEPS $NUM_NEWTONSTEPS $ASSEMBLE_TIME $SOLVE_TIME >> $GLOBALOUTFILE
      else # finished
        printf "%-50s &  %5.1f & %10.0f & %10.0f & %10.1f & %10.0f & %10.0f & %10.0f & %10.0f\n" $NAME $PERCENTAGE $TEND $SIM_TIME $WALL_TIME $NUM_TIMESTEPS $NUM_NEWTONSTEPS $ASSEMBLE_TIME $SOLVE_TIME >> $GLOBALOUTFILE
      fi
    elif [ "$SIMULATION_FINISHED" != "simulation" ]; then # still running
        printf "%-50s &  %5.1f & %10.0f & %10.0f & %10.0f & %10.0f & %10.0f & %10.0f & %10.0f\n" $NAME $PERCENTAGE $TEND $SIM_TIME $WALL_TIME $NUM_TIMESTEPS $NUM_NEWTONSTEPS $ASSEMBLE_TIME $SOLVE_TIME >> $GLOBALOUTFILE
    fi

#     # DEBUG
#     echo SIMULATION_FINISHED $SIMULATION_FINISHED
done

cat $GLOBALOUTFILE
rm temp.txt
