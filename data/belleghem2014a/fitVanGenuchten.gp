reset
set datafile separator ';'

# initial guess for fit
Swr=0.0
n1=2.17926100789614
a1=1.84514773686053e-05
n2=1.75454022996585
a2=1.7283229034735e-05

# pc-Sw vanGenuchten
vanGenuchten(x,a,n)=1/a *(((x-Swr)/(1-Swr))**(-1/(1-1/n))-1)**(1/n)
# brooksCorey(x,pcEntry,lambda)=pcEntry*((x-Swr)/(1-Swr))**(-1/lambda)

# perform the fit
fit [0.01:1] log10(vanGenuchten(x,a1,n1)) 'capillaryPressureMoistureContent_original.csv' u 4:(log10($3)) via a1,n1
fit [0.01:1] log10(vanGenuchten(x,a2,n2)) 'capillaryPressureMoistureContent_adjusted.csv' u 4:(log10($3)) via a2,n2
# fit [0.01:1] log10(brooksCorey(x,pcEntry2,lambda2)) 'capillaryPressureMoistureContent_adjusted.csv' u 4:(log10($3)) via pcEntry2,lambda2

# # print results
set print "-"
print "\nRESULTS\n-------\n"
print "vgN_ori      = ", n1
print "vgAlpha_ori  = ", a1
print "vgN_adj      = ", n2
print "vgAlpha_adj  = ", a2

set xrange[0:1]
set yrange[1e3:1e8]
set log y
plot 'capillaryPressureMoistureContent_original.csv' u 4:3 w p lc 1, \
     vanGenuchten(x,a1,n1) w l lc 1, \
     'capillaryPressureMoistureContent_adjusted.csv' u 4:3 w p lc 2, \
     vanGenuchten(x,a2,n2) w l lc 2


set term pngcairo size 800,600
set output "fitVanGenuchten.png"
replot
