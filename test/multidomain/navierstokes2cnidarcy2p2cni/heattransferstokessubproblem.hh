// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

#ifndef DUMUX_HEATTRANSFER_STOKES_SUBPROBLEM_HH
#define DUMUX_HEATTRANSFER_STOKES_SUBPROBLEM_HH

#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cniproblem.hh>
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes2cni/navierstokes2cnipropertydefaults.hh>

#include <appl/staggeredgrid/multidomain/navierstokes2ctdarcy2p2ct/properties.hh>

namespace Dumux
{
template <class TypeTag>
class HeatTransferStokesSubProblem;

namespace Properties
{
// Use pressure constraints
SET_BOOL_PROP(StokesSubProblem, FixPressureConstraints, true);

// Use complete Navier-Stokes equation
SET_BOOL_PROP(StokesSubProblem, ProblemEnableNavierStokes, false);

// Disable gravity field
SET_BOOL_PROP(StokesSubProblem, ProblemEnableGravity, false);
}

template <class TypeTag>
class HeatTransferStokesSubProblem : public NavierStokesTwoCNIProblem<TypeTag>
{
    using ParentType = NavierStokesTwoCNIProblem<TypeTag>;
    using MultiDomainTypeTag = typename GET_PROP_TYPE(TypeTag, MultiDomainTypeTag);

    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using DimVector = typename GET_PROP_TYPE(TypeTag, DimVector);
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);

public:
    HeatTransferStokesSubProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView), gridView_(gridView)
    {
        velocity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Velocity);
        pressure_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Pressure);
        temperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, Temperature);
        massMoleFrac_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, FreeFlow, MassMoleFrac);

        // exclude Darcy domain from bounding box
        bBoxMinFiltered_ = ParentType::bBoxMin();
        bBoxMinFiltered_[1] = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, InterfaceVerticalPos);
        darcyXLeft_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyXLeft);
        darcyXRight_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, DarcyXRight);
    }

    //! \copydoc Dumux::ImplicitProblem::init()
    void init()
    {
        // overwrite the parent function
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        std::string string = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
        return string.c_str();
    }

    //! \brief Velocity boundary condition types
    bool bcVelocityIsWall(const DimVector& global) const
    { return !bcVelocityIsCoupling(global); }
    bool bcVelocityIsCoupling(const DimVector& global) const
    { return isOnCouplingFace(global); }

    //! \brief Pressure boundary condition types
    bool bcPressureIsDirichlet(const DimVector& global) const
    { return (global[1] > this->bBoxMax()[1] - eps_); }
    bool bcPressureIsOutflow(const DimVector& global) const
    {
        return (!bcPressureIsDirichlet(global)
                && !bcPressureIsCoupling(global));
    }
    bool bcPressureIsCoupling(const DimVector& global) const
    { return isOnCouplingFace(global); }

    //! \brief MassMoleFrac boundary condition types
    bool bcMassMoleFracIsWall(const DimVector& global) const
    {
        return (!bcMassMoleFracIsInflow(global)
                && !bcMassMoleFracIsCoupling(global));
    }
    bool bcMassMoleFracIsInflow(const DimVector& global) const
    { return (global[1] > this->bBoxMax()[1] - eps_); }
    bool bcMassMoleFracIsCoupling(const DimVector& global) const
    { return isOnCouplingFace(global); }

    //! \brief Temperature boundary condition types
    bool bcTemperatureIsWall(const DimVector& global) const
    {
        return (!bcTemperatureIsInflow(global)
                && !bcTemperatureIsCoupling(global));
    }
    bool bcTemperatureIsInflow(const DimVector& global) const
    { return (global[1] > this->bBoxMax()[1] - eps_); }
    bool bcTemperatureIsCoupling(const DimVector& global) const
    { return isOnCouplingFace(global); }

    //! \brief Dirichlet values
    DimVector dirichletVelocityAtPos(const Element& e, const DimVector& global) const
    {
        return DimVector(0);
    }

    Scalar dirichletPressureAtPos(const DimVector& global) const
    {
        return pressure_;
    }
    Scalar dirichletMassMoleFracAtPos(const DimVector& global) const
    {
        return massMoleFrac_;
    }
    Scalar dirichletTemperatureAtPos(const DimVector& global) const
    {
        return temperature_;
    }

    /*!
     * \brief The coordinate of the corner of the GridView's bounding
     *        box with the smallest values.
     *
     * Filter out Darcy part
     */
    const DimVector &bBoxMin() const
    {
        return bBoxMinFiltered_;
    }

    //! \brief Returns whether we are on a coupling face
    const bool isOnCouplingFace(const DimVector& global) const
    {
        return global[1] < bBoxMinFiltered_[1] + eps_
               && global[0] > darcyXLeft_
               && global[0] < darcyXRight_;
    }

private:
    std::string name_;
    const GridView gridView_;
    static constexpr Scalar eps_ = 1e-6;
    DimVector bBoxMinFiltered_;
    Scalar darcyXLeft_;
    Scalar darcyXRight_;

    Scalar velocity_;
    Scalar pressure_;
    Scalar temperature_;
    Scalar massMoleFrac_;
};

} //end namespace

#endif // DUMUX_HEATTRANSFER_STOKES_SUBPROBLEM_HH
