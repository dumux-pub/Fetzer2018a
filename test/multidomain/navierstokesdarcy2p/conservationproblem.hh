#ifndef DUMUX_CONSERVATION_PROBLEM_HH
#define DUMUX_CONSERVATION_PROBLEM_HH

#include <dumux/material/fluidsystems/h2oair.hh>

#include <appl/staggeredgrid/multidomain/navierstokesdarcy2p/problem.hh>

#include "conservationdarcysubproblem.hh"
#include "conservationstokessubproblem.hh"

namespace Dumux
{

template <class TypeTag>
class ConservationProblem;

template <class TypeTag> class MultiDomainProblem;

namespace Properties
{
// problems
SET_TYPE_PROP(MDSNavierStokesDarcyTwoP, Problem,
              Dumux::ConservationProblem<TTAG(MDSNavierStokesDarcyTwoP)>);
SET_TYPE_PROP(StokesSubProblem, Problem,
              Dumux::ConservationStokesSubProblem<TTAG(StokesSubProblem)>);
SET_TYPE_PROP(DarcySubProblem, Problem,
              Dumux::ConservationDarcySubProblem<TTAG(DarcySubProblem)>);

// Set the fluid system to use complex relations (last argument)
SET_TYPE_PROP(MDSNavierStokesDarcyTwoP, FluidSystem,
              FluidSystems::H2OAir<typename GET_PROP_TYPE(TypeTag, Scalar)>);
SET_TYPE_PROP(DarcySubProblem, FluidSystem, typename GET_PROP_TYPE(TTAG(MDSNavierStokesDarcyTwoP), FluidSystem));
SET_TYPE_PROP(StokesSubProblem, FluidSystem, typename GET_PROP_TYPE(TTAG(MDSNavierStokesDarcyTwoP), FluidSystem));

// Define whether mole(true) or mass (false) fractions are used
SET_BOOL_PROP(MDSNavierStokesDarcyTwoP, UseMoles, false);
SET_BOOL_PROP(DarcySubProblem, UseMoles, GET_PROP_VALUE(TTAG(MDSNavierStokesDarcyTwoP), UseMoles));
SET_BOOL_PROP(StokesSubProblem, UseMoles, GET_PROP_VALUE(TTAG(MDSNavierStokesDarcyTwoP), UseMoles));
}

template <class TypeTag = TTAG(MDSNavierStokesDarcyTwoP)>
class ConservationProblem
: public MultiDomainProblem<TypeTag>
{
    using ParentType = MultiDomainProblem<TypeTag>;

    using FluidSystem = typename GET_PROP_TYPE(TypeTag, FluidSystem);
    using GridView = typename GET_PROP_TYPE(TypeTag, GridView);
    using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);

public:
    /*!
     * \brief Base class for the multi domain problem
     *
     * \param timeManager The TimeManager which is used by the simulation
     * \param gridView The GridView
     */
    ConservationProblem(TimeManager &timeManager,
                     GridView gridView)
    : ParentType(timeManager, gridView)
    {
        FluidSystem::init(/*tempMin=*/273.15, /*tempMax=*/343.15, /*numTemp=*/140,
                          /*pMin=*/5e4, /*pMax=*/1.5e5, /*numP=*/100);
    }

private:
};

} // end namespace Dumux

#endif // DUMUX_CONSERVATION_PROBLEM_HH
