// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem for a backward-facing step flow using a two-eq turbulence model
 *
 * \todo compare this test against solutions from literature
 */

#ifndef DUMUX_PROBLEM_BACKWARD_FACING_STEP_HH
#define DUMUX_PROBLEM_BACKWARD_FACING_STEP_HH

#include <dumux/material/components/constant.hh>

#include <dumux/freeflow/turbulenceproperties.hh>
#include <dumux/material/fluidsystems/gasphase.hh>

#if KEPSILON
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon/kepsilonproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon/kepsilonpropertydefaults.hh>
#else
#include <appl/staggeredgrid/freeflow/twoeq/komega/komegaproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/komega/komegapropertydefaults.hh>
#endif

namespace Dumux
{
template <class TypeTag>
class BackwardFacingStepProblem;

namespace Properties
{
// Set the used local and transient operators
#if KEPSILON
NEW_TYPE_TAG(BackwardFacingStepProblem, INHERITS_FROM(StaggeredGridKEpsilon));
SET_TYPE_PROP(BackwardFacingStepProblem, LocalOperator, Dune::PDELab::KEpsilonStaggeredGrid<TypeTag>);
SET_TYPE_PROP(BackwardFacingStepProblem, TransientLocalOperator, Dune::PDELab::KEpsilonTransientStaggeredGrid<TypeTag>);
#else
NEW_TYPE_TAG(BackwardFacingStepProblem, INHERITS_FROM(StaggeredGridKOmega));
SET_TYPE_PROP(BackwardFacingStepProblem, LocalOperator, Dune::PDELab::KOmegaStaggeredGrid<TypeTag>);
SET_TYPE_PROP(BackwardFacingStepProblem, TransientLocalOperator, Dune::PDELab::KOmegaTransientStaggeredGrid<TypeTag>);
#endif

// Set the problem property
SET_TYPE_PROP(BackwardFacingStepProblem, Problem,
              Dumux::BackwardFacingStepProblem<TypeTag>);


// Set the grid type
SET_TYPE_PROP(BackwardFacingStepProblem, Grid,
              Dune::UGGrid<2>);

// Disable gravity field
SET_BOOL_PROP(BackwardFacingStepProblem, ProblemEnableGravity, false);

// Use constant fluid properties
SET_TYPE_PROP(BackwardFacingStepProblem, Fluid,
              FluidSystems::GasPhase<typename GET_PROP_TYPE(TypeTag, Scalar),
                                     Constant<TypeTag, typename GET_PROP_TYPE(TypeTag, Scalar)> >);
}

template <class TypeTag>
#if KEPSILON
class BackwardFacingStepProblem : public KEpsilonProblem<TypeTag>
{
    typedef KEpsilonProblem<TypeTag> ParentType;
#else
class BackwardFacingStepProblem : public KOmegaProblem<TypeTag>
{
    typedef KOmegaProblem<TypeTag> ParentType;
#endif
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum { dim = GridView::dimension };

    typedef typename GridView::template Codim<0>::Entity Element;

    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    enum { phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx) };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;

public:
    BackwardFacingStepProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView), gridView_(gridView)
    {
        velocity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Velocity);
        temperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Temperature);
        massMoleFrac_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, MassMoleFrac);
        turbulentKineticEnergy_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, TurbulentKineticEnergy);
        dissipation_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Dissipation);
        dissipationRate_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, DissipationRate);
        smallestGridCellY_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, SmallestGridCellY);
        stepHeight_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, StepHeight);
        FluidSystem::init();

        Dumux::TurbulenceProperties<Scalar, dim, true> turbulenceProperties;
        FluidState fluidState;
        fluidState.setPressure(phaseIdx, 1e5);
        fluidState.setTemperature(temperature_);
        Scalar density = FluidSystem::density(fluidState, phaseIdx);
        kinematicViscosity_ = FluidSystem::viscosity(fluidState, phaseIdx) / density;
        Dune::FieldVector<double, dim> halfWay;
        halfWay[0] = 0.5 * (this->bBoxMax()[0] - this->bBoxMin()[0]) + this->bBoxMin()[0];
        halfWay[1] = 0.5 * smallestGridCellY_;
        Dune::FieldVector<double, dim> end;
        end[0] = this->bBoxMax()[0];
        end[1] = 0.5 * smallestGridCellY_;
        Scalar entrenceHeight = (this->bBoxMax()[1] - this->bBoxMin()[1]) - stepHeight_;
        std::cout << "entrenceHeight: " << entrenceHeight << std::endl;
        turbulenceProperties.yPlusEstimation(velocity_, halfWay, kinematicViscosity_, density);
        turbulenceProperties.yPlusEstimation(velocity_, end, kinematicViscosity_, density);
        turbulenceProperties.entranceLength(velocity_, entrenceHeight, kinematicViscosity_);
        turbulenceProperties.turbulentKineticEnergy(velocity_, entrenceHeight, kinematicViscosity_);
        turbulenceProperties.dissipation(velocity_, entrenceHeight, kinematicViscosity_);
        turbulenceProperties.dissipationRate(velocity_, entrenceHeight, kinematicViscosity_);
        std::cout << std::endl;
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        std::string string = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
        return string.c_str();
    }

    //! \brief Velocity boundary condition types
    bool bcVelocityIsWall(const DimVector& global) const
    { return (!bcVelocityIsInflow(global) && !bcVelocityIsOutflow(global) && !bcVelocityIsSymmetry(global)); }
    bool bcVelocityIsInflow(const DimVector& global) const
    { return (this->onLeftBoundary_(global) && global[1] > stepHeight_); }
    bool bcVelocityIsOutflow(const DimVector& global) const
    { return this->onRightBoundary_(global); }
    bool bcVelocityIsSymmetry(const DimVector& global) const
    { return this->onUpperBoundary_(global); }

    //! \brief Pressure boundary condition types
    bool bcPressureIsDirichlet(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }
    bool bcPressureIsOutflow(const DimVector& global) const
    { return !bcPressureIsDirichlet(global); }

    //! \brief TurbulentKineticEnergy boundary condition types
    bool bcTurbulentKineticEnergyIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global); }
    bool bcTurbulentKineticEnergyIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }
    bool bcTurbulentKineticEnergyIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }
    bool bcTurbulentKineticEnergyIsSymmetry(const DimVector& global) const
    { return bcVelocityIsSymmetry(global); }

    //! \brief Dissipation boundary condition types
    bool bcDissipationIsWall(const DimVector& global) const
    { return bcVelocityIsWall(global); }
    bool bcDissipationIsInflow(const DimVector& global) const
    { return bcVelocityIsInflow(global); }
    bool bcDissipationIsOutflow(const DimVector& global) const
    { return bcVelocityIsOutflow(global); }
    bool bcDissipationIsSymmetry(const DimVector& global) const
    { return bcVelocityIsSymmetry(global); }

    //! \brief Dirichlet values
    DimVector dirichletVelocityAtPos(const Element& e, const DimVector& global) const
    {
        DimVector y(0.0);
        if (bcVelocityIsInflow(global))
        {
            y[0] = velocity_;
        }
        return y;
    }
    Scalar dirichletPressureAtPos(const DimVector& global) const
    { return 1e5; }
    Scalar dirichletTurbulentKineticEnergyAtPos(const Element& e, const DimVector& global) const
    {
#if KEPSILON
        if (bcTurbulentKineticEnergyIsInflow(global) || this->timeManager().time() < eps_)
        {
            return turbulentKineticEnergy_;
        }
        return this->kEpsilonWallFunctions().wallFunctionTurbulentKineticEnergy(e);
#elif LOWREKEPSILON || KOMEGA
        if (bcTurbulentKineticEnergyIsWall(global)
            && this->timeManager().time() > 0.0)
        {
            return 0.0;
        }
        return turbulentKineticEnergy_;
#endif
    }
    Scalar dirichletDissipationAtPos(const Element& e, const DimVector& global) const
    {
#if KEPSILON
        if (bcDissipationIsInflow(global) || this->timeManager().time() < eps_)
        {
            return dissipation_;
        }
        return this->kEpsilonWallFunctions().wallFunctionDissipation(e);
#else //  KOMEGA
        if (bcDissipationIsWall(global)
            && this->timeManager().time() > 0.0)
        {
            Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGElementLayout> dofMapper(gridView_);
            return 6.0 * kinematicViscosity_
                   / (this->betaOmega() * std::pow(this->lop_.storedDistanceToWall[dofMapper.index(e)], 2));
        }
        return dissipationRate_;
#endif
    }

    //! \brief Return the temperature at a given position
    Scalar massMoleFracAtPos(const DimVector& global) const
    { return massMoleFrac_; }

    //! \brief Return the temperature at a given position
    Scalar temperatureAtPos(const DimVector& global) const
    { return temperature_; }

private:
    std::string name_;
    const GridView gridView_;
    static constexpr Scalar eps_ = 1e-6;

    Scalar velocity_;
    Scalar temperature_;
    Scalar massMoleFrac_;
    Scalar turbulentKineticEnergy_;
    Scalar dissipation_;
    Scalar dissipationRate_;
    Scalar smallestGridCellY_;
    Scalar stepHeight_;
    Scalar kinematicViscosity_;
};

} //end namespace


#endif // DUMUX_PROBLEM_BACKWARD_FACING_STEP_HH
