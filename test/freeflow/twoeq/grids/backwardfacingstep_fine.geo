length=150;
stepLength=110;
height=9.0;
stepHeight=1.0;
cellsX=75;
cellsY=45;

// points
Point(0) = {0, height, 0};
Point(1) = {stepLength, height, 0};
Point(2) = {length, height, 0};
Point(3) = {0, stepHeight, 0};
Point(4) = {stepLength, stepHeight, 0};
Point(5) = {length, stepHeight, 0};
Point(6) = {stepLength, 0, 0};
Point(7) = {length, 0, 0};

// lines
Line(1) = {3, 4};
Line(2) = {4, 1};
Line(3) = {1, 0};
Line(4) = {0, 3};

Line(5) = {4, 5};
Line(6) = {5, 2};
Line(7) = {2, 1};
Line(8) = {1, 4};

Line(9) = {6, 7};
Line(10) = {7, 5};
Line(11) = {5, 4};
Line(12) = {4, 6};

Transfinite Line{1, 3} = cellsX/length*stepLength+1;
Transfinite Line{2, 4} = cellsY/height*(height-stepHeight)+1;
Transfinite Line{5, 7} = cellsX/length*(length-stepLength)+1;
Transfinite Line{6, 8} = cellsY/height*(height-stepHeight)+1;
Transfinite Line{9, 11} = cellsX/length*(length-stepLength)+1;
Transfinite Line{10, 12} = cellsY/height*(stepHeight)+1;

// line loops of the three surfaces
Line Loop(21) = {1, 2, 3, 4};
Line Loop(22) = {5, 6, 7, -2};
Line Loop(23) = {9, 10, -5, 12};

// surfaces
Plane Surface(31) = {21};
Transfinite Surface{31};
Recombine Surface{31};
Plane Surface(32) = {22};
Transfinite Surface{32};
Recombine Surface{32};
Plane Surface(33) = {23};
Transfinite Surface{33};
Recombine Surface{33};
