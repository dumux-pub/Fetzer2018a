plotOverLine () {
if [ -e $1.vtu ]; then
  pvpython ../../../../../dumux/bin/postprocessing/extractlinedata.py -f $1.vtu -of $2 -p1 $P1 -p2 $P2 -v 1
  cp $2.csv $3
else
  echo "warning: File \"$1.vtu\" does not exist, cannot produce data for plot over line."
fi
}

P1="0.5 0.0 0.0"; P2="0.5 1.0 0.0"
plotOverLine $1 $2U
P1="0.0 0.5 0.0"; P2="1.0 0.5 0.0"
plotOverLine $1 $2V
