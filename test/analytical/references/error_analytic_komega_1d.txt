# error for test_analytic_komega_1d
L2 error for      4 elements. pressure: 1.2734e-01 velocity: 3.0811e-01 turbulentKineticEnergy: 7.1378e-02 dissipation: 4.9160e-02
L2 error for      8 elements. pressure: 9.4145e-02 velocity: 1.4928e-01 turbulentKineticEnergy: 3.0998e-02 dissipation: 2.4923e-02
L2 error for     16 elements. pressure: 7.0634e-02 velocity: 7.2577e-02 turbulentKineticEnergy: 1.5409e-02 dissipation: 1.2581e-02
L2 error for     32 elements. pressure: 4.4061e-02 velocity: 3.5533e-02 turbulentKineticEnergy: 7.7800e-03 dissipation: 6.3622e-03
L2 error for     64 elements. pressure: 2.4641e-02 velocity: 1.7540e-02 turbulentKineticEnergy: 3.9299e-03 dissipation: 3.1972e-03
L2 error for    128 elements. pressure: 1.3039e-02 velocity: 8.7080e-03 turbulentKineticEnergy: 1.9707e-03 dissipation: 1.6067e-03
L2 error for    256 elements. pressure: 6.7045e-03 velocity: 4.3378e-03 turbulentKineticEnergy: 9.8893e-04 dissipation: 8.0423e-04
