reset
set terminal pngcairo size 1000,750 solid
set datafile separator ','
DATA='./'

set output 'convergence_velprofile.png'
set xlabel '$\nicefrac{u}{u_\text{max}}$'
set ylabel '$\nicefrac{y}{h}$'
set xrange [0:1.05]
set yrange [0:0.5]
set key left Left reverse center samplen 1
plot \
  '~/results/lauferpipe/Laufer_PipeRe50000.csv' using 7:6 w p ps 1 pt 2 t 'Laufer 1954, Re=50000', \
  DATA.'lauferpipe_100.csv'    u ($5):($40/0.2469) w l lw 2 t '35', \
  DATA.'lauferpipe_105.csv'    u ($5):($40/0.2469) w l lw 2 t '40', \
  DATA.'lauferpipe_110.csv'    u ($5):($40/0.2469) w l lw 2 t '45', \
  DATA.'lauferpipe_115.csv'    u ($5):($40/0.2469) w l lw 2 t '50', \
  DATA.'lauferpipe_120.csv'    u ($5):($40/0.2469) w l lw 2 t '55', \
  DATA.'lauferpipe_125.csv'    u ($5):($40/0.2469) w l lw 2 t '60', \
  DATA.'lauferpipe_130.csv'    u ($5):($40/0.2469) w l lw 2 t '70', \
