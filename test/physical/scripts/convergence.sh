DATA="."
SCRIPT="/temp/fetzer/dumux-promo/dumux/bin/postprocessing/extractlinedata.py"
rm logfile*.out lauferpipe_*.csv

runSim () {
./test_lauferpipe_kepsilon $INPUT \
    -TimeManager.TEnd 1000 \
    -KEpsilon.WallFunctionYPlusThreshold $1 \
    -Newton.MaxRelativeShift 1e-6 \
    -Newton.ResidualReduction 1e-8 \
    -Newton.TargetSteps 3 \
    | tee -a logfile_$2.out
input=`ls -ltr laufer*Secondary*vtu | tail -n 1 | awk '{print $9}'`
echo $input" -> "$2 | tee -a logfile_$2.out
pvpython $SCRIPT -f $input -o $DATA -of $2 -p1 $P1 -p2 $P2 -v 2 -r 10000 | tee -a logfile_$2.out
gnuplot scripts/velprofile.gp
gnuplot scripts/frictionvelocity.gp
}

### lauferpipe
P1="8.0 0.0 0.0"; P2="8.0 0.2469 0.0"
CELLS="25 25"
INPUT=test_lauferpipe.input
runSim '35' lauferpipe_100
runSim '40' lauferpipe_105
runSim '45' lauferpipe_110
runSim '50' lauferpipe_115
runSim '55' lauferpipe_120
runSim '60' lauferpipe_125
runSim '70' lauferpipe_130

gnuplot scripts/velprofile.gp
gnuplot scripts/frictionvelocity.gp
