reset
set terminal pngcairo size 1000,750
set datafile separator ','
DATA='./'

set output 'convergence_frictionvelocity.png'
set log x
set xlabel '$y^+$'
set ylabel '$u^+$'
set xrange [1:3000]
set yrange [0:25]
set key right bottom samplen 1
plot \
  '~/results/lauferpipe/Laufer_u+y+_50000.csv' using 6:7 w p ps 1 pt 2 t 'Laufer 1954, Re=50000', \
  DATA.'lauferpipe_100.csv'    u 34:33 w lp lw 2 t '35', \
  DATA.'lauferpipe_105.csv'    u 34:33 w lp lw 2 t '40', \
  DATA.'lauferpipe_110.csv'    u 34:33 w lp lw 2 t '45', \
  DATA.'lauferpipe_115.csv'    u 34:33 w lp lw 2 t '50', \
  DATA.'lauferpipe_120.csv'    u 34:33 w lp lw 2 t '55', \
  DATA.'lauferpipe_125.csv'    u 34:33 w lp lw 2 t '60', \
  DATA.'lauferpipe_130.csv'    u 34:33 w lp lw 2 t '70', \
  1/0.41*log(x)+5 w l lc rgb 'gray40', \
  x w l lc rgb 'gray40'


set output 'convergence_frictionvelocity_new.png'
plot \
  '~/results/lauferpipe/Laufer_u+y+_50000.csv' using 6:7 w p ps 1 pt 2 t 'Laufer 1954, Re=50000', \
  DATA.'lauferpipe_100.csv'    u 36:35 w lp lw 2 t '35', \
  DATA.'lauferpipe_105.csv'    u 36:35 w lp lw 2 t '40', \
  DATA.'lauferpipe_110.csv'    u 36:35 w lp lw 2 t '45', \
  DATA.'lauferpipe_115.csv'    u 36:35 w lp lw 2 t '50', \
  DATA.'lauferpipe_120.csv'    u 36:35 w lp lw 2 t '55', \
  DATA.'lauferpipe_125.csv'    u 36:35 w lp lw 2 t '60', \
  DATA.'lauferpipe_130.csv'    u 36:35 w lp lw 2 t '70', \
  1/0.41*log(x)+5 w l lc rgb 'gray40', \
  x w l lc rgb 'gray40'
