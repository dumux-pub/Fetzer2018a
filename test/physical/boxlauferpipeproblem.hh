/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/**
 * \file
 * \brief A test problem based on pipe flow experiments by
 *        John Laufers experiments in 1954 \cite Laufer1954a.
 */
#ifndef DUMUX_ZEROEQTESTPROBLEM_HH
#define DUMUX_ZEROEQTESTPROBLEM_HH

#if HAVE_UG
#include <dune/grid/io/file/dgfparser/dgfug.hh>
#endif
#include <dune/grid/io/file/dgfparser/dgfs.hh>
#include <dune/grid/io/file/dgfparser/dgfyasp.hh>

#include <dumux/material/fluidsystems/h2oair.hh>
#include <dumux/material/fluidsystems/gasphase.hh>

#include <dumux/freeflow/zeroeq/model.hh>
#include <dumux/freeflow/zeroeq/problem.hh>

namespace Dumux
{

template <class TypeTag>
class BoxLauferPipeProblem;

//////////
// Specify the properties for the zeroeq problem
//////////
namespace Properties
{
NEW_TYPE_TAG(BoxLauferPipeProblem, INHERITS_FROM(BoxZeroEq));

// Set the grid type
SET_TYPE_PROP(BoxLauferPipeProblem, Grid,
              Dune::YaspGrid<2, Dune::TensorProductCoordinates<typename GET_PROP_TYPE(TypeTag, Scalar), 2> >);

// Set the problem property
SET_TYPE_PROP(BoxLauferPipeProblem, Problem, Dumux::BoxLauferPipeProblem<TypeTag>);


// Use air as fluid
SET_TYPE_PROP(BoxLauferPipeProblem, Fluid,
              FluidSystems::GasPhase<typename GET_PROP_TYPE(TypeTag, Scalar),
                                     Air<typename GET_PROP_TYPE(TypeTag, Scalar)> >);

SET_TYPE_PROP(BoxLauferPipeProblem, Scalar, double);

// Enable gravity
SET_BOOL_PROP(BoxLauferPipeProblem, ProblemEnableGravity, false);

// Enable Navier-Stokes
SET_BOOL_PROP(BoxLauferPipeProblem, EnableNavierStokes, true);

// Set no of intervals
SET_INT_PROP(BoxLauferPipeProblem, NumberOfIntervals, 1000);

// Set no of intervals
NEW_PROP_TAG(OutputFreqOutput);
SET_INT_PROP(BoxLauferPipeProblem, OutputFreqOutput, 1);
}

/*!
 * \ingroup ZeroEqModel
 * \ingroup ImplicitTestProblems
 * \brief ZeroEq problem with air flowing from the left to the right.
 *
 * The domain is sized 10m times 0.2469m. The problem is taken from
 * experimental setup by John Laufer (The structure of turbulence in fully developed pipe flow,
 * NACA Report, 1954). The boundary conditions for the momentum balances
 * are set to Dirichlet with outflow on the right boundary. The mass balance has
 * outflow BCs, which are replaced in the localresidual by the sum
 * of the two momentum balances. In the middle of the right boundary,
 * one vertex receives Dirichlet BCs to set the pressurelevel.
 *
 * This problem uses the \ref ZeroEqModel.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_zeroeq -ParameterFile ./test_zeroeq.input</tt>
 */
template <class TypeTag>
class BoxLauferPipeProblem : public ZeroEqProblem<TypeTag>
{
    typedef ZeroEqProblem<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;
    enum {
        // Number of equations and grid dimension
        dim = GridView::dimension,

        // copy some indices for convenience
        massBalanceIdx = Indices::massBalanceIdx,
        momentumXIdx = Indices::momentumXIdx, //!< Index of the x-component of the momentum balance
        momentumYIdx = Indices::momentumYIdx //!< Index of the y-component of the momentum balance
    };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GridView::template Codim<dim>::Entity Vertex;
    typedef typename GridView::ctype CoordScalar;
    typedef typename GridView::Intersection Intersection;

    typedef typename GET_PROP_TYPE(TypeTag, GridCreator) GridCreator;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, Fluid) Fluid;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<CoordScalar, dim> GlobalPosition;

public:
    BoxLauferPipeProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
        , flowNormal_(0)
        , wallNormal_(1)
    {
        eps_ = 1e-6;
        injectionVelocity_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.Velocity);
        temperature_ = GET_RUNTIME_PARAM(TypeTag, Scalar, Problem.Temperature);
        freqOutput_ = GET_PARAM_FROM_GROUP(TypeTag, int, Output, FreqOutput);
        this->timeManager().startNextEpisode(1.0);
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string &name() const
    {
        return GET_RUNTIME_PARAM(TypeTag, std::string, Problem.Name);
    }

    /*!
     * \brief Returns the temperature within the domain.
     *
     * This problem assumes a constant temperature of 10 degrees Celsius.
     */
    Scalar temperature() const
    {
        return temperature_;
    }

    // \}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment.
     *
     * \param values The boundary types for the conservation equations
     * \param vertex The vertex on the boundary for which the
     *               conditions needs to be specified
     */
    void boundaryTypes(BoundaryTypes &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();

        values.setAllDirichlet();

        // the mass balance has to be of type outflow
        values.setOutflow(massBalanceIdx);

        if(onRightBoundary_(globalPos) &&
                globalPos[1] < this->bBoxMax()[1]-eps_ && globalPos[1] > this->bBoxMin()[1]+eps_)
            values.setAllOutflow();

        // set pressure at one point
        if (onRightBoundary_(globalPos))
            values.setDirichlet(massBalanceIdx);
    }

    /*!
     * \brief Evaluate the boundary conditions for a dirichlet
     *        control volume.
     *
     * \param values The dirichlet values for the primary variables
     * \param vertex The vertex representing the "half volume on the boundary"
     *
     * For this method, the \a values parameter stores primary variables.
     */
    void dirichlet(PrimaryVariables &values, const Vertex &vertex) const
    {
        const GlobalPosition globalPos = vertex.geometry().center();

        initial_(values, globalPos);
    }

    /*!
     * \brief Evaluate the boundary conditions for a neumann
     *        boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     *
     * A neumann condition for the RANS momentum equation equation corresponds to:
     * \f[ -\mu \nabla {\bf v} \cdot {\bf n} + p \cdot {\bf n} = q_N \f]
     */
    void neumann(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvElemGeom,
                 const Intersection &is,
                 int scvIdx,
                 int boundaryFaceIdx) const
    {
        values = 0.0;
    }
    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluate the source term for all phases within a given
     *        sub-control-volume.
     *
     * For this method, the \a values parameter stores the rate mass
     * generated or annihilate per volume unit. Positive values mean
     * that mass is created, negative ones mean that it vanishes.
     */
    void source(PrimaryVariables &values,
                const Element &element,
                const FVElementGeometry &,
                int subControlVolumeIdx) const
    {
        // ATTENTION: The source term of the mass balance has to be chosen as
        // div (q_momentum) in the problem file
        values = Scalar(0.0);
    }

    /*!
     * \brief Evaluate the initial value for a control volume.
     *
     * For this method, the \a values parameter stores primary
     * variables.
     */
    void initial(PrimaryVariables &values,
                 const Element &element,
                 const FVElementGeometry &fvElemGeom,
                 int scvIdx) const
    {
        const GlobalPosition &globalPos = element.geometry().corner(scvIdx);

        initial_(values, globalPos);
    }
    // \}

    //! \copydoc Dumux::CoupledProblem::episodeEnd()
    void episodeEnd()
    {
      if (this->timeManager().time() < 9.9)
        this->timeManager().startNextEpisode(1.0);
      else
        this->timeManager().startNextEpisode(10.0);
    }

    //! \copydoc Dumux::shouldWriteOutput()
    bool shouldWriteOutput()
    {
        if (this->timeManager().timeStepIndex() % freqOutput_ == 0
            || this->timeManager().episodeWillBeFinished()
            || this->timeManager().willBeFinished())
        {
            return true;
        }
        return false;
    }

private:
    // internal method for the initial condition (reused for the
    // dirichlet conditions!)
    void initial_(PrimaryVariables &values,
                  const GlobalPosition &globalPos) const
    {
        values = 0.0;
        values[flowNormal_] = injectionVelocity_;

        if(onLowerBoundary_(globalPos) || onUpperBoundary_(globalPos))
            values[flowNormal_] = 0.0;

        values[massBalanceIdx] = 1e5;
    }

    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] < this->bBoxMin()[0] + eps_; }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[0] > this->bBoxMax()[0] - eps_; }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] < this->bBoxMin()[1] + eps_; }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    { return globalPos[1] > this->bBoxMax()[1] - eps_; }

    Scalar eps_;
    const Scalar flowNormal_;
    const Scalar wallNormal_;
    Scalar injectionVelocity_;
    Scalar temperature_;
    unsigned int freqOutput_;

};

} //end namespace

#endif // DUMUX_ZEROEQTESTPROBLEM_HH
