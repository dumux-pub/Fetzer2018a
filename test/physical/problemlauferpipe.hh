// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief A test problem based on pipe flow experiments by
 *        John Laufers experiments in 1954 \cite Laufer1954a.
 */

#ifndef DUMUX_PROBLEM_LAUFERPIPE_DUMUX_HH
#define DUMUX_PROBLEM_LAUFERPIPE_DUMUX_HH

#include <dumux/freeflow/turbulenceproperties.hh>
#include <dumux/material/fluidsystems/h2oair.hh>
#include <dumux/material/fluidsystems/gasphase.hh>

#if ZEROEQ
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq/zeroeqproblem.hh>
#include <appl/staggeredgrid/freeflow/zeroeq/zeroeq/zeroeqpropertydefaults.hh>
#elif ONEEQ
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras/spalartallmarasproblem.hh>
#include <appl/staggeredgrid/freeflow/oneeq/spalartallmaras/spalartallmaraspropertydefaults.hh>
#elif KEPSILON
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon/kepsilonproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/kepsilon/kepsilonpropertydefaults.hh>
#elif LOWREKEPSILON
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon/lowrekepsilonproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/lowrekepsilon/lowrekepsilonpropertydefaults.hh>
#elif KOMEGA
#include <appl/staggeredgrid/freeflow/twoeq/komega/komegaproblem.hh>
#include <appl/staggeredgrid/freeflow/twoeq/komega/komegapropertydefaults.hh>
#else
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes/navierstokesproblem.hh>
#include <appl/staggeredgrid/freeflow/navierstokes/navierstokes/navierstokespropertydefaults.hh>
#endif

namespace Dumux
{
template <class TypeTag>
class LauferPipeProblem;

namespace Properties
{
#if ZEROEQ
NEW_TYPE_TAG(LauferPipeProblem, INHERITS_FROM(StaggeredGridZeroEq));
SET_TYPE_PROP(LauferPipeProblem, LocalOperator, Dune::PDELab::ZeroEqStaggeredGrid<TypeTag>);
SET_TYPE_PROP(LauferPipeProblem, TransientLocalOperator, Dune::PDELab::NavierStokesTransientStaggeredGrid<TypeTag>);
#elif ONEEQ
NEW_TYPE_TAG(LauferPipeProblem, INHERITS_FROM(StaggeredGridSpalartAllmaras));
SET_TYPE_PROP(LauferPipeProblem, LocalOperator, Dune::PDELab::SpalartAllmarasStaggeredGrid<TypeTag>);
SET_TYPE_PROP(LauferPipeProblem, TransientLocalOperator, Dune::PDELab::SpalartAllmarasTransientStaggeredGrid<TypeTag>);
#elif KEPSILON
NEW_TYPE_TAG(LauferPipeProblem, INHERITS_FROM(StaggeredGridKEpsilon));
SET_TYPE_PROP(LauferPipeProblem, LocalOperator, Dune::PDELab::KEpsilonStaggeredGrid<TypeTag>);
SET_TYPE_PROP(LauferPipeProblem, TransientLocalOperator, Dune::PDELab::KEpsilonTransientStaggeredGrid<TypeTag>);
#elif LOWREKEPSILON
NEW_TYPE_TAG(LauferPipeProblem, INHERITS_FROM(StaggeredGridLowReKEpsilon));
SET_TYPE_PROP(LauferPipeProblem, LocalOperator, Dune::PDELab::LowReKEpsilonStaggeredGrid<TypeTag>);
SET_TYPE_PROP(LauferPipeProblem, TransientLocalOperator, Dune::PDELab::LowReKEpsilonTransientStaggeredGrid<TypeTag>);
#elif KOMEGA
NEW_TYPE_TAG(LauferPipeProblem, INHERITS_FROM(StaggeredGridKOmega));
SET_TYPE_PROP(LauferPipeProblem, LocalOperator, Dune::PDELab::KOmegaStaggeredGrid<TypeTag>);
SET_TYPE_PROP(LauferPipeProblem, TransientLocalOperator, Dune::PDELab::KOmegaTransientStaggeredGrid<TypeTag>);
#else
NEW_TYPE_TAG(LauferPipeProblem, INHERITS_FROM(StaggeredGridNavierStokes));
SET_TYPE_PROP(LauferPipeProblem, LocalOperator, Dune::PDELab::NavierStokesStaggeredGrid<TypeTag>);
SET_TYPE_PROP(LauferPipeProblem, TransientLocalOperator, Dune::PDELab::NavierStokesTransientStaggeredGrid<TypeTag>);
#endif

// Set the problem property
SET_TYPE_PROP(LauferPipeProblem, Problem, Dumux::LauferPipeProblem<TypeTag>);

// Set the grid type
SET_TYPE_PROP(LauferPipeProblem, Grid,
              Dune::YaspGrid<2, Dune::TensorProductCoordinates<typename GET_PROP_TYPE(TypeTag, Scalar), 2> >);

// Disable gravity field
SET_BOOL_PROP(LauferPipeProblem, ProblemEnableGravity, false);

// Use air as fluid
SET_TYPE_PROP(LauferPipeProblem, Fluid,
              FluidSystems::GasPhase<typename GET_PROP_TYPE(TypeTag, Scalar),
                                     Air<typename GET_PROP_TYPE(TypeTag, Scalar)> >);

// Set no of intervals
NEW_PROP_TAG(OutputFreqOutput);
SET_INT_PROP(LauferPipeProblem, OutputFreqOutput, 1);
}

template <class TypeTag>
#if ZEROEQ
class LauferPipeProblem : public ZeroEqProblem<TypeTag>
{
    typedef ZeroEqProblem<TypeTag> ParentType;
#elif ONEEQ
    class LauferPipeProblem : public SpalartAllmarasProblem<TypeTag>
{
    typedef SpalartAllmarasProblem<TypeTag> ParentType;
#elif KEPSILON
class LauferPipeProblem : public KEpsilonProblem<TypeTag>
{
    typedef KEpsilonProblem<TypeTag> ParentType;
#elif LOWREKEPSILON
class LauferPipeProblem : public LowReKEpsilonProblem<TypeTag>
{
    typedef LowReKEpsilonProblem<TypeTag> ParentType;
#elif KOMEGA
class LauferPipeProblem : public KOmegaProblem<TypeTag>
{
    typedef KOmegaProblem<TypeTag> ParentType;
#else
class LauferPipeProblem : public NavierStokesProblem<TypeTag>
{
    typedef NavierStokesProblem<TypeTag> ParentType;
#endif
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;

    typedef typename GET_PROP_TYPE(TypeTag, Grid) Grid;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum { dim = GridView::dimension };

    typedef typename GridView::template Codim<0>::Entity Element;

    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    enum { phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx) };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, DimVector) DimVector;

public:
    LauferPipeProblem(TimeManager &timeManager, const GridView &gridView)
    : ParentType(timeManager, gridView), gridView_(gridView)
    {
        velocity_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Velocity);
        temperature_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, Temperature);
        massMoleFrac_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Problem, MassMoleFrac);
        smallestGridCellY_ = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, Scalar, Grid, SmallestGridCellY);
        FluidSystem::init();

        Dumux::TurbulenceProperties<Scalar, dim, true> turbulenceProperties;
        FluidState fluidState;
        fluidState.setPressure(phaseIdx, 1e5);
        fluidState.setTemperature(temperature_);
        Scalar density = FluidSystem::density(fluidState, phaseIdx);
        kinematicViscosity_ = FluidSystem::viscosity(fluidState, phaseIdx) / density;
        Dune::FieldVector<double, dim> halfWay;
        halfWay[0] = 0.5 * (this->bBoxMax()[0] - this->bBoxMin()[0]) + this->bBoxMin()[0];
        halfWay[1] = 0.5 * smallestGridCellY_;
        Dune::FieldVector<double, dim> end;
        end[0] = this->bBoxMax()[0];
        end[1] = 0.5 * smallestGridCellY_;
        turbulenceProperties.yPlusEstimation(velocity_, halfWay, kinematicViscosity_, density);
        turbulenceProperties.yPlusEstimation(velocity_, end, kinematicViscosity_, density);
        turbulenceProperties.entranceLength(velocity_, (this->bBoxMax()[1] - this->bBoxMin()[1]), kinematicViscosity_);
        viscosityTilde_ = turbulenceProperties.viscosityTilde(velocity_, (this->bBoxMax()[1] - this->bBoxMin()[1]), kinematicViscosity_);
        turbulentKineticEnergy_ = turbulenceProperties.turbulentKineticEnergy(velocity_, (this->bBoxMax()[1] - this->bBoxMin()[1]), kinematicViscosity_);
        dissipation_ = turbulenceProperties.dissipation(velocity_, (this->bBoxMax()[1] - this->bBoxMin()[1]), kinematicViscosity_);
        dissipationRate_ = turbulenceProperties.dissipationRate(velocity_, (this->bBoxMax()[1] - this->bBoxMin()[1]), kinematicViscosity_,false);
        std::cout << std::endl;

        freqOutput_ = GET_PARAM_FROM_GROUP(TypeTag, int, Output, FreqOutput);
        this->timeManager().startNextEpisode(1.0);
    }

    /*!
     * \brief The problem name.
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const char *name() const
    {
        std::string string = GET_RUNTIME_PARAM_FROM_GROUP(TypeTag, std::string, Problem, Name);
        return string.c_str();
    }

    //! \brief Velocity boundary condition types
    bool bcVelocityIsWall(const DimVector& global) const
    { return (!bcVelocityIsInflow(global) && !bcVelocityIsOutflow(global)); }
    bool bcVelocityIsInflow(const DimVector& global) const
    { return this->onLeftBoundary_(global); }
    bool bcVelocityIsOutflow(const DimVector& global) const
    { return this->onRightBoundary_(global); }
//     bool bcVelocityIsSymmetry(const DimVector& global) const
//     { return false; }

    //! \brief Pressure boundary condition types
    bool bcPressureIsDirichlet(const DimVector& global) const
    { return this->onRightBoundary_(global); }
    bool bcPressureIsOutflow(const DimVector& global) const
    { return !bcPressureIsDirichlet(global); }

    //! \brief Dirichlet values
    DimVector dirichletVelocityAtPos(const Element& e, const DimVector& global) const
    {
        DimVector y(0.0);
        if (global[1] > this->bBoxMin()[1] && global[1] < this->bBoxMax()[1]
            && this->timeManager().time() > eps_)
        {
            y[0] = velocity_;
        }
        return y;
    }
    Scalar dirichletPressureAtPos(const DimVector& global) const
    { return 1e5; }

    //! \brief Return the temperature at a given position
    Scalar temperatureAtPos(const DimVector& global) const
    { return 298.0; }

    //! \brief Return the temperature at a given position
    Scalar massMoleFracAtPos(const DimVector& global) const
    { return massMoleFrac_; }

#if ONEEQ
    //! \brief ViscosityTilde boundary condition types
    bool bcViscosityTildeIsWall(const DimVector& global) const
    { return (!bcViscosityTildeIsInflow(global) && !bcViscosityTildeIsOutflow(global)); }
    bool bcViscosityTildeIsInflow(const DimVector& global) const
    { return this->onLeftBoundary_(global); }
    bool bcViscosityTildeIsOutflow(const DimVector& global) const
    { return this->onRightBoundary_(global); }
//     bool bcViscosityTildeIsSymmetry(const DimVector& global) const
//     { return false; }

    Scalar dirichletViscosityTildeAtPos(const Element& e, const DimVector& global) const
    {
        if (bcViscosityTildeIsWall(global) &&  this->onBoundary(global))
            return 0.0;
        return viscosityTilde_;
    }
#endif

#if KEPSILON || LOWREKEPSILON || KOMEGA
    //! \brief TurbulentKineticEnergy boundary condition types
    bool bcTurbulentKineticEnergyIsWall(const DimVector& global) const
    { return (!bcTurbulentKineticEnergyIsInflow(global) && !bcTurbulentKineticEnergyIsOutflow(global)); }
    bool bcTurbulentKineticEnergyIsInflow(const DimVector& global) const
    { return this->onLeftBoundary_(global); }
    bool bcTurbulentKineticEnergyIsOutflow(const DimVector& global) const
    { return this->onRightBoundary_(global); }
//     bool bcTurbulentKineticEnergyIsSymmetry(const DimVector& global) const
//     { return false; }

    Scalar dirichletTurbulentKineticEnergyAtPos(const Element& e, const DimVector& global) const
    {
#if KEPSILON
        if (this->kEpsilonWallFunctions().useWallFunctionTurbulentKineticEnergy(e)
            && this->timeManager().time() > eps_)
        {
            return this->kEpsilonWallFunctions().wallFunctionTurbulentKineticEnergy(e);
        }
#elif LOWREKEPSILON || KOMEGA
        if (bcTurbulentKineticEnergyIsWall(global)
            && this->timeManager().time() > 0.0)
        {
            return 0.0;
        }
#endif
        return turbulentKineticEnergy_;
    }


    //! \brief Dissipation boundary condition types
    bool bcDissipationIsWall(const DimVector& global) const
    { return (!bcDissipationIsInflow(global) && !bcDissipationIsOutflow(global)); }
    bool bcDissipationIsInflow(const DimVector& global) const
    { return this->onLeftBoundary_(global); }
    bool bcDissipationIsOutflow(const DimVector& global) const
    { return this->onRightBoundary_(global); }
//     bool bcDissipationIsSymmetry(const DimVector& global) const
//     { return false; }

    Scalar dirichletDissipationAtPos(const Element& e, const DimVector& global) const
    {
#if KEPSILON
        if (this->kEpsilonWallFunctions().useWallFunctionDissipation(e)
            && this->timeManager().time() > eps_)
        {
            return this->kEpsilonWallFunctions().wallFunctionDissipation(e);
        }
        return dissipation_;
#elif LOWREKEPSILON
        if (bcDissipationIsWall(global)
            && this->timeManager().time() > 0.0)
        {
            return 0.0;
        }
        return dissipation_;
#else //  KOMEGA
        if (bcDissipationIsWall(global)
            && this->timeManager().time() > 0.0)
        {
            Dune::MultipleCodimMultipleGeomTypeMapper<GridView, Dune::MCMGElementLayout> dofMapper(gridView_);
            return 6.0 * kinematicViscosity_
                   / (this->betaOmega() * std::pow(this->lop_.storedDistanceToWall[dofMapper.index(e)], 2));
        }
        return dissipationRate_;
#endif
    }
#endif

    //! \copydoc Dumux::CoupledProblem::episodeEnd()
    void episodeEnd()
    {
      if (this->timeManager().time() < 9.9)
        this->timeManager().startNextEpisode(1.0);
      else
        this->timeManager().startNextEpisode(10.0);
    }

    //! \copydoc Dumux::shouldWriteOutput()
    bool shouldWriteOutput()
    {
        if (this->timeManager().timeStepIndex() % freqOutput_ == 0
            || this->timeManager().episodeWillBeFinished()
            || this->timeManager().willBeFinished())
        {
            return true;
        }
        return false;
    }


private:
    std::string name_;
    const GridView gridView_;
    static constexpr Scalar eps_ = 1e-6;

    Scalar velocity_;
    Scalar temperature_;
    Scalar massMoleFrac_;
    Scalar kinematicViscosity_;
    Scalar viscosityTilde_;
    Scalar turbulentKineticEnergy_;
    Scalar dissipation_;
    Scalar dissipationRate_;
    Scalar smallestGridCellY_;
    unsigned int freqOutput_;
};

} //end namespace


#endif // DUMUX_PROBLEM_LAUFERPIPE_DUMUX_HH
