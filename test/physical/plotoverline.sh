plotOverLine () {
if [ -e $1.vtu ]; then
  pvpython ../../../../../dumux/bin/postprocessing/extractlinedata.py -f $1.vtu -of $2 -p1 $P1 -p2 $P2 -v 1
  cp $2.csv $3
else
  echo "warning: File \"$1.vtu\" does not exist, cannot produce data for plot over line."
fi
}

P1="8.0 0.0 0.0"; P2="8.0 0.2469 0.0"
plotOverLine $1 $2U $3

# # plotOverLine $DUMUX/appl/staggeredgrid/boxcomparison/zeroeq/lauferpipe_box-00027 lauferpipe_vanDriest_box.csv
# plotOverLine $RESULTS/tests/physical/lauferpipe_baldwinlomax_box/lauferpipe_box-00024 lauferpipe_baldwinLomax_box.csv
# # plotOverLine $DUMUX/appl/staggeredgrid/freeflow/zeroeq/zeroeq/lauferpipe-ffSecondary-00030 lauferpipe_vanDriest_staggered.csv
# plotOverLine $RESULTS/tests/physical/lauferpipe_baldwinlomax_staggered/lauferpipe-ffSecondary-00010 lauferpipe_baldwinLomax_staggered.csv
# plotOverLine $RESULTS/tests/physical/lauferpipe_spalartallmaras_staggered/lauferpipe-ffSecondary-00017 lauferpipe_spalartallmaras_staggered.csv
# plotOverLine $RESULTS/tests/physical/lauferpipe_lowrekepsilon_staggered/lauferpipe-ffSecondary-00016 lauferpipe_lowrekepsilon_staggered.csv
# plotOverLine $RESULTS/tests/physical/lauferpipe_kepsilon_staggered/lauferpipe-ffSecondary-00013 lauferpipe_kepsilon_staggered.csv
# # plotOverLine /temp/fetzer/dumux-210/dumux-devel/build-clang/appl/staggeredgrid/boxcomparison/zeroeq/lauferpipe_box-00024 lauferpipe_baldwinLomax_box.csv
# # plotOverLine /temp/fetzer/dumux-210/dumux-devel/build-clang/appl/staggeredgrid/freeflow/zeroeq/zeroeq/lauferpipe-ffSecondary-00010 lauferpipe_baldwinLomax_staggered.csv
# # plotOverLine /temp/fetzer/dumux-210/dumux-devel/build-clang/appl/staggeredgrid/freeflow/oneeq/spalartallmaras/lauferpipe-ffSecondary-00017 lauferpipe_spalartallmaras_staggered.csv
# # plotOverLine /temp/fetzer/dumux-210/dumux-devel/build-clang/appl/staggeredgrid/freeflow/twoeq/lowrekepsilon/lauferpipe-ffSecondary-00016 lauferpipe_lowrekepsilon_staggered.csv
# # plotOverLine /temp/fetzer/dumux-210/dumux-devel/build-clang/appl/staggeredgrid/freeflow/twoeq/kepsilon/lauferpipe-ffSecondary-00013 lauferpipe_kepsilon_staggered.csv
# # }