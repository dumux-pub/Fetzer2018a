Summary
=======
This is the DuMuX module containing the code for producing the results
of the PhD Thesis of Thomas Fetzer:

T. Fetzer<br>
[On the conditions for coupling free and porous-medium flow in a finite volume framework](http://dx.doi.org/10.18419/opus-10016)<br>
University of Stuttgart, February 2018.<br>
DOI: http://dx.doi.org/10.18419/opus-10016

You can use the .bib file provided [here](Fetzer2018a.bib).


Installation
============
The easiest way to install this module is to create a new folder and to execute the file
[installFetzer2018a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Fetzer2018a/raw/master/installFetzer2018a.sh)
in this folder.

```bash
mkdir -p Fetzer2018a && cd Fetzer2018a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/Fetzer2018a/raw/master/installFetzer2018a.sh
sh ./installFetzer2018a.sh
```

For a detailed information on installation have a look at the DuMuX installation
guide or use the DuMuX handbook, chapter 2.


Folder Structure
================
This dumux module has the following structure.
The *appl/multidomain* folder contains the problem specific source code for the presented results,
whereas *appl/staggeredgrid* contains all basic MAC models.
*bin/jobfiles* contains script files to re-run all used simulations and
scripts for all post-processing in *bin/scripts*.
In *dumux*, the basic transport model and input/output helper files are located.
The plot of material laws and fluid properties are performed by executables built from the *material* folder.
For re-simulating the OpenFOAM results, the files in *openfoam* can be used.
The data sets of which all plots and pictures in the results chapter are made of can be found in *results*.
Note that these data sets can be automatically reproduced by running the scripts from the *bin* folder.
Finally, *test* contains setups which ensure the basic functionality of the code.
Some of these setups are presented in the appendix.


Run Simulations
===============
<!--
| Fig./Tab.          | executable                             | script        | result            |
| ------------------ | -------------------------------------- | ------------- | ----------------- |
| Fig. 2.2           | material/fluidmatrixinteractions/...   | script        | result            |
-->
To test whether your installation produces the same results as used in this work, go
to your build directory and execute `ctest`.
Ctest tests simplified versions of the main results (located in *appl* )  and
some code-validation tests (located in *test* ).
In order to re-run the simulations, you can use the jobfiles in *bin/jobfiles* (make sure
to use the right build and output directory).
To run simulations for a changed setup (here *windtunnelstaggered_zeroeq* ), type e.g.:
```bash
cd dumux-Fetzer2018a/build-cmake/appl/multidomain/windtunnel/
make windtunnelstaggered_zeroeq
./windtunnelstaggered_zeroeq windtunneltopopen_reference.input
```


Remarks
=======
The following remarks are made as a result of the discussions with my colleagues after the finalization of the thesis.
- Many parts of the source code are documented and a doxygen documentation can be built.
  However, the quality and intensity of the documentation is strongly varying.
  In case of doubt, the reader is referred to this PhD thesis or to the PhD thesis
  by Christoph Grüninger:
  [Numerical Coupling of Navier-Stokes and Darcy Flow for Soil-Water Evaporation](https://dx.doi.org/10.18419/opus-9657).
- For the 1-eq. turbulence models, the bounday values could be changed from the precalculated values to $`\tilde{\nu} = 0`$.
  This does not affect the results for the reference pipe-flow case by J. Laufer, but would affect the coupled evaporation rates.
- At the coupling interface, the phase diffusion coefficient instead of the millington diffusivity is used.
  This enables the drying of a fully saturated soil and both versions converge to almost the same evaporation rates
  for more unsaturated and dry surfaces.
- Inside the calculation of the energy and the component wall-functions one pair of bracket is wrongly placed:
  the $´\mathcal{P}´$- and $´\mathcal{S}´$-functions should not be divieded by the Karman-constant $`\kappa`$.


Used Versions and Software
==========================
For an overview on the used versions of the DUNE and DuMuX modules, please have a look at
[installFetzer2018a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/Fetzer2018a/raw/master/installFetzer2018a.sh).
This publication bases on dumux-2.12 but requires some patches.<br>
In addition, external software packages are necessary for compiling the executables and has been tested
for the following versions:

| software           | version | type              |
| ------------------ | ------- | ----------------- |
| cmake              | 3.5.2   | build tool        |
| clang/clang++      | 3.5.0   | compiler          |
| gcc/g++            | 6.2.1   | compiler          |
| UMFPack            | 5.7.1   | linear solver     |
| SuperLU            | 4.3     | linear solver     |
| suitesparse        | 4.5.5   | matrix algorithms |
