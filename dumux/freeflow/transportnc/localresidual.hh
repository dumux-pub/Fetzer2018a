// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using the n-component Transport model.
 *
 */
#ifndef DUMUX_TRANSPORTNC_LOCAL_RESIDUAL_HH
#define DUMUX_TRANSPORTNC_LOCAL_RESIDUAL_HH

#include <dumux/implicit/model.hh>

#include "fluxvariables.hh"
#include "properties.hh"
#include "volumevariables.hh"

namespace Dumux
{
/*!
 * \ingroup ImplicitTransportncModel
 * \ingroup ImplicitLocalResidual
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using the n-component Transport model.
 */
template<class TypeTag>
class TransportncLocalResidual : public GET_PROP_TYPE(TypeTag, BaseLocalResidual)
{
    typedef typename GET_PROP_TYPE(TypeTag, BaseLocalResidual) ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, LocalResidual) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    //dimensions
    enum {  dim = GridView::dimension };
    //number of equations
    enum {  numEq = GET_PROP_VALUE(TypeTag, NumEq) };
    //equation indices
    enum {  transportEqIdx = Indices::transportEqIdx };
    //phase employed
    enum {  phaseIdx = Indices::phaseIdx };
    //number of components
    enum {  numComponents = Indices::numComponents };
    //component indices
    enum {  phaseCompIdx = Indices::phaseCompIdx,
            transportCompIdx = Indices::transportCompIdx };

    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

    typedef Dune::FieldVector<Scalar, dim> DimVector;

    typedef typename GridView::Intersection Intersection;
    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;

    //! property that defines whether mole or mass fractions are used
    static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);

public:
    //! \brief Constructor. Sets the upwind weight.
    TransportncLocalResidual()
    {
        massUpwindWeight_ = GET_PARAM_FROM_GROUP(TypeTag, Scalar, Implicit, MassUpwindWeight);
    }

    /*!
     * \brief Evaluate the stored amount of quantities additional to the Transport model
     *        (transport equations). For using mole fraction also momentum balances and mass balance
     *        have to be calculated using molar quantities
     *
     * The result should be averaged over the volume (e.g. phase mass
     * inside a sub control volume divided by the volume)
     *
     *  \param storage The mass of the component within the sub-control volume
     *  \param scvIdx The SCV (sub-control-volume) index
     *  \param usePrevSol Evaluate function with solution of current or previous time step
     */
    void computeStorage(PrimaryVariables &storage, const int scvIdx, const bool usePrevSol) const
    {
        storage = 0.0;
        // if flag usePrevSol is set, the solution from the previous
        // time step is used, otherwise the current solution is
        // used. The secondary variables are used accordingly. This
        // is required to compute the derivative of the storage term
        // using the implicit Euler method.
        const ElementVolumeVariables &elemVolVars = usePrevSol ? this->prevVolVars_() : this->curVolVars_();
        const VolumeVariables &volVars = elemVolVars[scvIdx];

        if (numComponents != 2)
            DUNE_THROW(Dune::NotImplemented, "Storage term is only implemented for two components.");

        //storage of transported component
        if (!useMoles)
        {

            storage[transportEqIdx] = volVars.density()
                                      * volVars.massFraction(transportCompIdx);

            Valgrind::CheckDefined(volVars.density());
            Valgrind::CheckDefined(volVars.massFraction(transportCompIdx));
        }
        else
        {
            storage[transportEqIdx] = volVars.molarDensity()
                                      * volVars.moleFraction(transportCompIdx);

            Valgrind::CheckDefined(volVars.molarDensity());
            Valgrind::CheckDefined(volVars.moleFraction(transportCompIdx));
        }
        Valgrind::CheckDefined(storage[transportEqIdx]);
    }

    /*!
     * \brief Evaluates the total flux of all conservation quantities
     *        over a face of a sub-control volume. The face may be within
     *        an element (SCV face) or on the boundary. The advective and
     *        the diffusive fluxes are computed.
     *
     * \param flux The flux over the SCV (sub-control-volume) face
     * \param fIdx The index of the SCV face (may also be a boundary face)
     * \param onBoundary Indicates, if the flux is evaluated on a boundary face. If it is true,
     *        the created fluxVars object contains boundary variables evaluated at the IP of the
     *        boundary face
     */
    void computeFlux(PrimaryVariables &flux, const int fIdx, const bool onBoundary=false) const
    {
        FluxVariables fluxVars;
        fluxVars.update(this->problem_(),
                        this->element_(),
                        this->fvGeometry_(),
                        fIdx,
                        this->curVolVars_(),
                        onBoundary);
        flux = 0.0;

        asImp_()->computeAdvectiveFlux(flux, fluxVars);
        Valgrind::CheckDefined(flux);
        asImp_()->computeDiffusiveFlux(flux, fluxVars);
        Valgrind::CheckDefined(flux);
    }

    /*!
     * \brief Evaluates the advective component (mass) flux
     * over a face of a sub-control volume and writes the result in
     * the flux vector.
     *
     * This method is called by compute flux (base class).
     *
     * \param flux The advective flux over the sub-control-volume face for each component
     * \param fluxVars The flux variables at the current SCV/boundary face
     */
    void computeAdvectiveFlux(PrimaryVariables &flux,
                              const FluxVariables &fluxVars) const
    {
        // data attached to upstream and the downstream vertices
        const VolumeVariables &up = this->curVolVars_(fluxVars.upstreamIdx());
        const VolumeVariables &dn = this->curVolVars_(fluxVars.downstreamIdx());

        Scalar tmp = fluxVars.normalVelocity();

        if (numComponents != 2)
            DUNE_THROW(Dune::NotImplemented, "Advection term is only implemented for two components.");

        // advective flux for the component
        if(!useMoles)
        {
            tmp *= (this->massUpwindWeight_ * up.density() * up.massFraction(transportCompIdx)
                    + (1.-this->massUpwindWeight_) * dn.density() * dn.massFraction(transportCompIdx));
        }
        else
        {
            tmp *= (this->massUpwindWeight_ * up.molarDensity() * up.moleFraction(transportCompIdx)
                    + (1.-this->massUpwindWeight_) * dn.molarDensity() * dn.moleFraction(transportCompIdx));
        }

        flux[transportEqIdx] += tmp;
        Valgrind::CheckDefined(flux[transportEqIdx]);
    }

    /*!
     * \brief Adds the diffusive component flux to the flux vector over
     *        the face of a sub-control volume.
     *
     * \param flux The diffusive flux over the SCV face or boundary face
     * \param fluxVars The flux variables at the current SCV/boundary face
     */
    void computeDiffusiveFlux(PrimaryVariables &flux,
                              const FluxVariables &fluxVars) const
    {
        if (numComponents != 2)
            DUNE_THROW(Dune::NotImplemented, "Diffusion term is only implemented for two components.");

        // diffusive component flux
        Scalar diffusiveFlux = fluxVars.moleFractionGrad(transportCompIdx)
                                * fluxVars.face().normal
                                *(fluxVars.diffusionCoeff(transportCompIdx) + fluxVars.eddyDiffusivity())
                                * fluxVars.molarDensity();

        // for mass fraction formulation, the molar based fluxes have to be converted
        if(!useMoles)
            diffusiveFlux *= FluidSystem::molarMass(transportCompIdx);// Multiplied by molarMass [kg/mol] to convert form [mol/m^3 s] to [kg/m^3 s]

        flux[transportEqIdx] -= diffusiveFlux;
        Valgrind::CheckDefined(flux[transportEqIdx]);
    }

    /*!
     * \brief Calculate the source term of all equations.
     *
     * \param source The source/sink in the sub control volume for each component
     * \param scvIdx The local index of the sub-control volume
     */
    void computeSource(PrimaryVariables &source, const int scvIdx)
    {
        // retrieve the source term intrinsic to the problem
        this->problem_().solDependentSource(source,
                                            this->element_(),
                                            this->fvGeometry_(),
                                            scvIdx,
                                            this->curVolVars_());
    }

protected:
    Scalar massUpwindWeight_;

    Implementation *asImp_()
    { return static_cast<Implementation *>(this); }
    const Implementation *asImp_() const
    { return static_cast<const Implementation *>(this); }
};

}

#endif
