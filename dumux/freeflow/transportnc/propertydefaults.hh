// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup TransportncModel
 *
 * \file
 *
 * \brief Defines the properties required for the n component transport box model.
 */
#ifndef DUMUX_TRANSPORTNC_PROPERTY_DEFAULTS_HH
#define DUMUX_TRANSPORTNC_PROPERTY_DEFAULTS_HH

#include <dumux/material/components/nullcomponent.hh>
#include <dumux/material/fluidstates/compositional.hh>
#include <dumux/material/fluidsystems/gasphase.hh>
#include <dumux/material/fluidsystems/liquidphase.hh>
#include <dumux/material/fluidsystems/1p.hh>

#include "fluxvariables.hh"
#include "indices.hh"
#include "localresidual.hh"
#include "model.hh"
#include "properties.hh"
#include "volumevariables.hh"

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Properties
//////////////////////////////////////////////////////////////////

//! Define that mass fractions are used in the balance equations
SET_BOOL_PROP(Transportnc, UseMoles, false);

//! set the number of equations
SET_PROP(Transportnc, NumEq)
{
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
public:
    static constexpr int value = FluidSystem::numComponents - 1;
};

//! Use the Tranportnc local residual function
SET_TYPE_PROP(Transportnc, LocalResidual, TransportncLocalResidual<TypeTag>);

//! The Model property
SET_TYPE_PROP(Transportnc, Model, TransportncModel<TypeTag>);

//! The VolumeVariables property
SET_TYPE_PROP(Transportnc, VolumeVariables, TransportncVolumeVariables<TypeTag>);

//! The FluxVariables property
SET_TYPE_PROP(Transportnc, FluxVariables, TransportncFluxVariables<TypeTag>);

//! The upwind factor
SET_SCALAR_PROP(Transportnc, ImplicitMassUpwindWeight, 1.0);

//! Set the Indices for the Transportnc model.
SET_TYPE_PROP(Transportnc, Indices, TransportncCommonIndices<TypeTag>);

//! Choose the type of the employed fluid state
SET_PROP(Transportnc, FluidState)
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
public:
    typedef Dumux::CompositionalFluidState<Scalar, FluidSystem> type;
};

//! Choose the considered phase (single-phase system); the gas phase is used
SET_PROP(Transportnc, PhaseIdx)
{
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
public:
    static constexpr int value = FluidSystem::nPhaseIdx;
};
} // namespace Properties
} // namespace Dumux
#endif // DUMUX_TRANSPORTNC_PROPERTY_DEFAULTS_HH
