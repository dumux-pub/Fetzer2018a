// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup ImplicitTransportncModel
 *
 * \file
 *
 * \brief Defines the supplementary properties required for the n component
 *        transport box model.
 */
#ifndef DUMUX_TRANSPORTNC_PROPERTIES_HH
#define DUMUX_TRANSPORTNC_PROPERTIES_HH

#include <dumux/implicit/box/properties.hh>
#include <dumux/implicit/cellcentered/properties.hh>

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////
//! The type tag for the Transportnc problems
NEW_TYPE_TAG(Transportnc);
NEW_TYPE_TAG(BoxTransportnc, INHERITS_FROM(BoxModel, Transportnc));
NEW_TYPE_TAG(CCTransportnc, INHERITS_FROM(CCModel, Transportnc));


//////////////////////////////////////////////////////////////////
// Property tags
//////////////////////////////////////////////////////////////////

NEW_PROP_TAG(ImplicitMassUpwindWeight); //!< The value of the upwind parameter for the mobility
NEW_PROP_TAG(Indices); //!< Enumerations for the model
NEW_PROP_TAG(FluidSystem); //!< The employed fluid system
NEW_PROP_TAG(FluidState); //!< The employed fluid state
NEW_PROP_TAG(UseMoles); //!< Defines whether molar (true) or mass (false) density is used

NEW_PROP_TAG(PhaseIdx); //!< A phase index in case that a two-phase fluidsystem is used
NEW_PROP_TAG(SpatialParams); //!< The type of the spatial parameters
}

}

#endif
