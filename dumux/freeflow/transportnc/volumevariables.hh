// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Contains the quantities which are constant within a
 *        finite volume in the n-component transport model.
 */
#ifndef DUMUX_TRANSPORTNC_VOLUME_VARIABLES_HH
#define DUMUX_TRANSPORTNC_VOLUME_VARIABLES_HH

#include <dumux/implicit/volumevariables.hh>
#include <dumux/material/fluidstates/immiscible.hh>

#include "properties.hh"

namespace Dumux
{

/*!
 * \ingroup ImplicitTransportncModel
 * \ingroup ImplicitVolumeVariables
 * \brief Contains the quantities which are are constant within a
 *        finite volume in the n-component transport model.
 */
template <class TypeTag>
class TransportncVolumeVariables : public ImplicitVolumeVariables<TypeTag>
{
    typedef ImplicitVolumeVariables<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) Implementation;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    enum { dim = GridView::dimension,
           dimWorld = GridView::dimensionworld };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, FluidState) FluidState;
    typedef typename GET_PROP_TYPE(TypeTag, PrimaryVariables) PrimaryVariables;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    //employed phase index
    enum { phaseIdx = Indices::phaseIdx };
    //number of components
    enum { numComponents = Indices::numComponents };
    //component indices
    enum { transportCompIdx = Indices::transportCompIdx,
           phaseCompIdx = Indices::phaseCompIdx };
    //equation indices
    enum { transportEqIdx = Indices::transportEqIdx };
    //primary variable indices
    enum { massOrMoleFracIdx = Indices::massOrMoleFracIdx };

    // some constant properties
    static const bool useMoles = GET_PROP_VALUE(TypeTag, UseMoles);
    static const bool isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox);
    enum { dofCodim = isBox ? dim : 0 };

public:
    /*!
     * \copydoc ImplicitVolumeVariables::update()
     */
    void update(const PrimaryVariables &priVars,
                const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                const int scvIdx,
                const bool isOldSol)
    {
        // update vertex data for the mass and momentum balance
        ParentType::update(priVars,
                           problem,
                           element,
                           fvGeometry,
                           scvIdx,
                           isOldSol);

        // set the mole fractions first
        completeFluidState(priVars, problem, element, fvGeometry, scvIdx, fluidState(), isOldSol);

        int dofIdxGlobal = problem.model().dofMapper().subIndex(element, scvIdx, dofCodim);

        globalPos_ = fvGeometry.subContVol[scvIdx].global;
        pressure_ = problem.model().pressure(dofIdxGlobal);
        velocity_ = DimVector(0.0);
        eddyDiffusivity_ = Scalar(0.0);
        if (isBox)
            velocity_ = problem.model().velocity(dofIdxGlobal);
        else
            eddyDiffusivity_ = problem.model().eddyDiffusivity(dofIdxGlobal);

        // Model is restricted to 2 components when using mass fractions
        if (!useMoles && numComponents > 2)
        {
            DUNE_THROW(Dune::NotImplemented, "This model is restricted to 2 components when using mass fractions!\
                                              To use mole fractions set property UseMoles true ...");
        }

        // Second instance of a parameter cache.
        // Could be avoided if diffusion coefficients also became part of the fluid state.
        typename FluidSystem::ParameterCache paramCache;
        paramCache.updateAll(fluidState());

        for (int compIdx=0; compIdx<numComponents; compIdx++)
        {
            if (phaseCompIdx!=compIdx)
            {
                diffCoeff_[compIdx] = FluidSystem::binaryDiffusionCoefficient(fluidState(),
                                                                              paramCache,
                                                                              phaseIdx,
                                                                              compIdx,
                                                                              phaseCompIdx);
            }
            else
                diffCoeff_[compIdx] = 0.0;

            Valgrind::CheckDefined(diffCoeff_[compIdx]);
        }
    }

    /*!
     * \copydoc ImplicitModel::completeFluidState()
     * \param isOldSol Specifies whether this is the previous solution or the current one
     */
    static void completeFluidState(const PrimaryVariables& priVars,
                                   const Problem& problem,
                                   const Element& element,
                                   const FVElementGeometry& fvGeometry,
                                   const int scvIdx,
                                   FluidState& fluidState,
                                   const bool isOldSol = false)
    {
        Scalar temperature = Implementation::temperature_(priVars, problem, element, fvGeometry, scvIdx);
        fluidState.setTemperature(temperature);
        fluidState.setPressure(phaseIdx, problem.pressureAtPos(fvGeometry.subContVol[scvIdx].global));

        if(!useMoles) //mass-fraction formulation
        {
            fluidState.setMassFraction(phaseIdx, transportCompIdx, priVars[massOrMoleFracIdx]);
        }
        else
        {
            if (numComponents != 2)
                DUNE_THROW(Dune::NotImplemented, "Calculation of phase mole fraction has to be changed for more than 2 components.");

            fluidState.setMoleFraction(phaseIdx, transportCompIdx, priVars[massOrMoleFracIdx]);
            fluidState.setMoleFraction(phaseIdx, phaseCompIdx, 1-priVars[massOrMoleFracIdx]);
        }

        // create NullParameterCache and do dummy update
        typename FluidSystem::ParameterCache paramCache;
        paramCache.updateAll(fluidState);

        // get density from fluidsystem if the function is not overwritten by the problem
        int dofIdxGlobal = problem.model().dofMapper().subIndex(element, scvIdx, dofCodim);
        Scalar density = problem.model().density(dofIdxGlobal);
        if (density < 0)
            density = FluidSystem::density(fluidState, paramCache, phaseIdx);

        fluidState.setDensity(phaseIdx, density);

        // compute and set the enthalpy
        Scalar h = Implementation::enthalpy_(fluidState, paramCache, phaseIdx);
        fluidState.setEnthalpy(phaseIdx, h);
    }

// transport
    /*!
     * \brief Returns the global position for the control-volume.
     *
     * \param fvGeometry The finite volume geometry for the element
     * \param scvIdx Local index of the sub control volume which is inside the element
     */
    const GlobalPosition globalPos() const
    { return globalPos_; }

// stokes
    /*!
     * \brief Returns the phase state for the control-volume.
     */
    const FluidState &fluidState() const
    { return fluidState_; }
    FluidState &fluidState()
    { return fluidState_; }

    /*!
     * \brief Returns the mass density \f$\mathrm{[kg/m^3]}\f$ of the fluid within the
     *        sub-control volume.
     */
    Scalar density() const
    { return fluidState_.density(phaseIdx); }

    /*!
     * \brief Returns the molar density \f$\mathrm{[mol/m^3]}\f$ of the fluid within the
     *        sub-control volume.
     */
    Scalar molarDensity() const
    { return fluidState_.density(phaseIdx) / fluidState_.averageMolarMass(phaseIdx); }

    /*!
     * \brief Returns temperature\f$\mathrm{[K]}\f$ inside the sub-control volume.
     */
    Scalar temperature() const
    { return fluidState_.temperature(phaseIdx); }

    /*!
     * \brief Returns the fluid pressure \f$\mathrm{[Pa]}\f$ within
     *        the sub-control volume.
     */
    Scalar pressure() const
    { return pressure_; }

    /*!
     * \brief Returns the velocity vector in the sub-control volume.
     */
    const DimVector &velocity() const
    { return velocity_; }

// stokesnc
    /*!
     * \brief Returns the mass fraction of a given component in the
     *        given fluid phase within the control volume.
     *
     * \param compIdx The component index
     */
    Scalar massFraction(const int compIdx) const
    { return fluidState_.massFraction(phaseIdx, compIdx); }

    /*!
     * \brief Returns the mole fraction of a given component in the
     *        given fluid phase within the control volume.
     *
     * \param compIdx The component index
     */
    Scalar moleFraction(const int compIdx) const
    { return fluidState_.moleFraction(phaseIdx, compIdx); }

    /*!
     * \brief Returns the binary (mass) diffusion coefficient
     *
     * \param compIdx The component index
     */
    Scalar diffusionCoeff(int compIdx) const
    { return diffCoeff_[compIdx]; }

    /*!
     * \brief Returns the eddy diffusivity \f$ \frac{m^2}{s} \f$ in
     *        the sub-control volume.
     */
    Scalar eddyDiffusivity() const
    { return eddyDiffusivity_; }

protected:
// transport
    GlobalPosition globalPos_;
// stokes
    FluidState fluidState_;
    Scalar pressure_;
    DimVector velocity_;
// stokesnc
    Scalar diffCoeff_[numComponents];
    Scalar eddyDiffusivity_;

// stokes
    static Scalar temperature_(const PrimaryVariables &priVars,
                               const Problem& problem,
                               const Element &element,
                               const FVElementGeometry &fvGeometry,
                               const int scvIdx)
    { return problem.temperatureAtPos(fvGeometry.subContVol[scvIdx].global); }


    template<class ParameterCache>
    static Scalar enthalpy_(const FluidState& fluidState,
                            const ParameterCache& paramCache,
                            int phaseIdx)
    { return 0; }
};

} // end namespace

#endif
