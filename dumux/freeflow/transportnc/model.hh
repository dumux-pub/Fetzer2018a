// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Adaptation of the implicit scheme to the n-component Transport model.
 */
#ifndef DUMUX_TRANSPORTNC_MODEL_HH
#define DUMUX_TRANSPORTNC_MODEL_HH

#include <dune/grid/common/grid.hh>

#include <dumux/implicit/model.hh>
#include <dumux/implicit/problem.hh>

#include "localresidual.hh"
#include "problem.hh"
#include "properties.hh"

namespace Dumux {
/*!
 * \ingroup ImplicitTransportncModel
 * \brief Adaptation of the implicit scheme to the n-component Transport model.
 *
 * This model implements an isothermal n-component Transport on a given
 * velocity field. When using mole fractions naturally the densities represent molar
 * densities
 *
 * Component mass balance equations:
 * \f[
 \frac{\partial \left(\varrho_g X_g^\kappa\right)}{\partial t}
 + \boldsymbol{\nabla} \boldsymbol{\cdot} \left( \varrho_g {\boldsymbol{v}}_g X_g^\kappa
 - D^\kappa_g \varrho_g \frac{M^\kappa}{M_g} \boldsymbol{\nabla} X_g^\kappa \right)
 - q_g^\kappa = 0
 * \f]
 *
 * This is discretized using a fully-coupled vertex centered finite volume
 * (box) scheme as spatial and the implicit Euler method in time.
 */
template<class TypeTag>
class TransportncModel : public GET_PROP_TYPE(TypeTag, BaseModel)
{
    typedef typename GET_PROP_TYPE(TypeTag, Model) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum { dim = GridView::dimension,
           dimWorld = GridView::dimensionworld,
           transportCompIdx = Indices::transportCompIdx,
           phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx),
           numComponents = Indices::numComponents };

    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, ElementBoundaryTypes) ElementBoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

    typedef Dune::FieldVector<Scalar, dimWorld> DimVector;

    // some constant properties
    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

public:
    //! \copydoc ImplicitModel::addOutputVtkFields
    template <class MultiWriter>
    void addOutputVtkFields(const SolutionVector &sol,
                            MultiWriter &writer)
    {
        typedef Dune::BlockVector<Dune::FieldVector<Scalar, 1> > ScalarField;
        typedef Dune::BlockVector<DimVector > VectorField;

        // create the required scalar fields
        unsigned numDofs = this->numDofs();
        ScalarField *density = writer.allocateManagedBuffer(numDofs);
        ScalarField *diffCoeff = writer.allocateManagedBuffer(numDofs);
        ScalarField *eddyDiffusivity = writer.allocateManagedBuffer(numDofs);
        ScalarField *massFraction[numComponents];
        for (int i = 0; i < numComponents; ++i)
            massFraction[i] = writer.template allocateManagedBuffer<Scalar, 1>(numDofs);
        ScalarField *moleFraction[numComponents];
        for (int i = 0; i < numComponents; ++i)
            moleFraction[i] = writer.template allocateManagedBuffer<Scalar, 1>(numDofs);
        ScalarField *pressure = writer.allocateManagedBuffer(numDofs);
        ScalarField *pressureDelta = writer.allocateManagedBuffer(numDofs);
        ScalarField *temperature = writer.allocateManagedBuffer(numDofs);

        // create the required vector fields
        VectorField *globalPos = writer.template allocateManagedBuffer<Scalar, dimWorld> (numDofs);
        VectorField *velocity = writer.template allocateManagedBuffer<Scalar, dimWorld> (numDofs);

        // create the required data per element fields
        unsigned numElements = this->gridView_().size(0);
        ScalarField *rank = writer.allocateManagedBuffer(numElements);

        FVElementGeometry fvGeometry;
        ElementVolumeVariables elemVolVars;
        VolumeVariables volVars;
        ElementBoundaryTypes elemBcTypes;

        ElementIterator eIt = this->gridView_().template begin<0>();
        ElementIterator eEndIt = this->gridView_().template end<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            if(eIt->partitionType() == Dune::InteriorEntity)
            {
                int eIdx = this->elementMapper().index(*eIt);
                (*rank)[eIdx] = this->gridView_().comm().rank();

                fvGeometry.update(this->gridView_(), *eIt);
                elemVolVars.update(this->problem_(), *eIt, fvGeometry, false /* oldSol? */);
                elemBcTypes.update(this->problem_(), *eIt, fvGeometry);

                for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
                {
                        int dofIdxGlobal = this->dofMapper().subIndex(*eIt, scvIdx, dofCodim);
                        (*density)[dofIdxGlobal] = elemVolVars[scvIdx].density();
                        (*diffCoeff)[dofIdxGlobal] = elemVolVars[scvIdx].diffusionCoeff(transportCompIdx);
                        if (!isBox)
                            (*eddyDiffusivity)[dofIdxGlobal] = elemVolVars[scvIdx].eddyDiffusivity();
                        (*globalPos)[dofIdxGlobal] = elemVolVars[scvIdx].globalPos();
                        (*pressure)[dofIdxGlobal] = elemVolVars[scvIdx].pressure();
                        (*pressureDelta)[dofIdxGlobal] = elemVolVars[scvIdx].pressure() - 1e5;
                        (*temperature)[dofIdxGlobal] = elemVolVars[scvIdx].temperature();
                        if (isBox)
                            (*velocity)[dofIdxGlobal] = elemVolVars[scvIdx].velocity();

                        for (int compIdx = 0; compIdx < numComponents; ++compIdx)
                        {
                            (*massFraction[compIdx])[dofIdxGlobal] = elemVolVars[scvIdx].massFraction(compIdx);
                            (*moleFraction[compIdx])[dofIdxGlobal] = elemVolVars[scvIdx].moleFraction(compIdx);
                        }
                }
            }
        }
        writer.attachDofData(*density, "rho", isBox);
        writer.attachDofData(*diffCoeff, "D", isBox);
        if (!isBox)
            writer.attachDofData(*eddyDiffusivity, "D_t", isBox);
        writer.attachDofData(*globalPos, "globalPos", isBox, dimWorld);
        writer.attachDofData(*pressure, "p", isBox);
        writer.attachDofData(*pressureDelta, "delP", isBox);
        writer.attachDofData(*temperature, "temperature", isBox);
        if (isBox)
            writer.attachDofData(*velocity, "v", isBox, dimWorld);

        for (int j = 0; j < numComponents; ++j)
        {
            std::ostringstream moleFrac, massFrac;
            moleFrac << "x_" << FluidSystem::phaseName(phaseIdx) << "^" << FluidSystem::componentName(j);
            writer.attachDofData(*moleFraction[j], moleFrac.str().c_str(), isBox);

            massFrac << "X_" << FluidSystem::phaseName(phaseIdx) << "^" << FluidSystem::componentName(j);
            writer.attachDofData(*massFraction[j], massFrac.str().c_str(), isBox);
        }

        // write flux values to .csv
        std::ofstream fluxFile("fluxVarsData.csv", std::ios_base::out);
        asImp_().writeFluxFileHeader(fluxFile);
        fluxFile << std::endl;
        unsigned int numFaces = isBox ? numScvFacesBox_ : numFacesCC;
        for (unsigned int i = 0; i < numElements; ++i)
        {
            for (unsigned int j = 0; j < numFaces; ++j)
            {
                asImp_().writeFluxFileData(fluxFile, i, j);
                fluxFile << std::endl;
            }
        }
        fluxFile.close();
    }


    /*!
     * \brief Writes the header for the fluxData.csv file
     *
     * \param fluxFile The output file stream
     */
    void writeFluxFileHeader(std::ofstream &fluxFile)
    {
        std::cout << "Writing fluxVars file for \"" << this->problem_().name() << "\"" << std::endl;
        fluxFile << "#globalPos[0]";
        for (unsigned int curDim = 1; curDim < dim; ++curDim)
            fluxFile << "," << "globalPos[" << curDim << "]";
        if (!isBox)
            for (unsigned int curDim = 0; curDim < dim; ++curDim)
                fluxFile << "," << "velocity[" << curDim << "]";
        else
            fluxFile  << "," << "eddyDiffusivity";
    }

    /*!
     * \brief Writes the data into the fluxData.csv file
     *
     * \param fluxFile The output file stream
     * \param idx Index. For box: degree of freedom. For cell-centered: element
     * \param fIdx Index of the element face (for box unused)
     */
    void writeFluxFileData(std::ofstream &fluxFile, unsigned int idx, unsigned int fIdx)
    {
        fluxFile << globalPos(idx, fIdx)[0];
        for (unsigned int curDim = 1; curDim < dim; ++curDim)
            fluxFile << "," << globalPos(idx, fIdx)[curDim];
        if (!isBox)
            for (unsigned int curDim = 0; curDim < dim; ++curDim)
                fluxFile << "," << velocity(idx, fIdx)[curDim];
        else
            fluxFile  << "," << eddyDiffusivity(idx, fIdx);
    }

    /*!
     * \brief Resizes the static data vectors the correct size, which depends
     *        on the used discretization method
     */
    void initializeData()
    {
        std::cout << "Initializing static data" << std::endl;
        unsigned numDofs = this->numDofs();
        unsigned numElements = this->gridView_().size(0);
        pressure_.resize(numDofs, 0.0);
        density_.resize(numDofs, 0.0);

        if (isBox)
        {
            velocity_.resize(numDofs, std::vector<DimVector>(0.0));
            for (unsigned int i = 0; i < velocity_.size(); ++i)
                velocity_[i].resize(1, DimVector(0.0)); // only 1 entry, is located at dof

            globalPos_.resize(numElements, std::vector<DimVector>(0.0));
            for (unsigned int i = 0; i < globalPos_.size(); ++i)
                globalPos_[i].resize(numScvFacesBox_, DimVector(0.0)); // several entries, is located at face

            eddyDiffusivity_.resize(numElements, std::vector<Scalar>(0.0));
            for (unsigned int i = 0; i < eddyDiffusivity_.size(); ++i)
                eddyDiffusivity_[i].resize(numScvFacesBox_, 0.0); // several entries, is located at face
        }
        else
        {
            globalPos_.resize(numElements, std::vector<DimVector>(0.0));
            for (unsigned int i = 0; i < globalPos_.size(); ++i)
                globalPos_[i].resize(numFacesCC, DimVector(0.0)); // several entries, is located at face

            velocity_.resize(numElements, std::vector<DimVector>(0.0));
            for (unsigned int i = 0; i < velocity_.size(); ++i)
                velocity_[i].resize(numFacesCC, DimVector(0.0)); // several entries, is located at face

            eddyDiffusivity_.resize(numDofs, std::vector<Scalar>(0.0));
            for (unsigned int i = 0; i < eddyDiffusivity_.size(); ++i)
                eddyDiffusivity_[i].resize(1, 0.0); // only 1 entry, is located at dof
        }
    }

    /*!
     * \brief Fills the static data vectors with data specified on the level
     *        of the specific problem implementation
     */
    void updateData()
    {
        std::cout << "Updating static data" << std::endl;
        FVElementGeometry fvGeometry;

        ElementIterator eIt = this->gridView_().template begin<0>();
        ElementIterator eEndIt = this->gridView_().template end<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            fvGeometry.update(this->gridView_(), *eIt);

            for (int i = 0; i < fvGeometry.numScv; ++i)
            {
                int dofIdxGlobal = this->dofMapper().subIndex(*eIt, i, dofCodim);

                // the position has to be given directly, because initializing a volVars
                // variable is not possible (because there is no SolutionVector)
                DimVector globalPos = fvGeometry.subContVol[i].global;

                pressure_[dofIdxGlobal] = this->problem_().pressureAtPos(globalPos);
                density_[dofIdxGlobal] = this->problem_().densityAtPos(globalPos);
                if (isBox)
                {
                    velocity_[dofIdxGlobal][0] = this->problem_().velocityAtPos(globalPos);
                }
                else
                {
                    eddyDiffusivity_[dofIdxGlobal][0] = this->problem_().eddyDiffusivityAtPos(globalPos);
                }
            }

            int eIdxGlobal = this->elementMapper().index(*eIt);
            if (isBox)
            {
                for (unsigned int fIdx = 0; fIdx < numScvFacesBox_; ++fIdx)
                {
                    DimVector elementLocal = mapFaceIdxToLocalCoordinate(fIdx);
                    DimVector globalPos = eIt->geometry().global(elementLocal);

                    eddyDiffusivity_[eIdxGlobal][fIdx] = this->problem_().eddyDiffusivityAtPos(globalPos);
                    globalPos_[eIdxGlobal][fIdx] = globalPos;
                }
            }
            else
            {
                for (unsigned int iIdx = 0; iIdx < numFacesCC; ++iIdx)
                {
                    DimVector localPos(0.5);
                    localPos[(unsigned int)std::floor(iIdx/dim)] = iIdx%2;
                    DimVector globalPos = eIt->geometry().global(localPos);
                    velocity_[eIdxGlobal][iIdx] = this->problem_().velocityAtPos(globalPos);
                    globalPos_[eIdxGlobal][iIdx] = globalPos;
                }
            }
        }
    }

    /*!
     * \brief Returns the fluid pressure \f$\mathrm{[Pa]}\f$ for a degree of freedom.
     *
     * \param dofIdx Index of the degree of freedom
     */
    Scalar pressure(unsigned int dofIdx) const
    { return pressure_[dofIdx]; }

    /*!
     * \brief Returns the mass density \f$\mathrm{[kg/m^3]}\f$ for a degree of freedom.
     *
     * \param dofIdx Index of the degree of freedom
     */
    Scalar density(unsigned int dofIdx) const
    { return density_[dofIdx]; }

    /*!
     * \brief Returns the eddy diffusivity \f$\mathrm{[m^2/s]}\f$ for the intersections of an element.
     *
     * \param idx Index. For box: degree of freedom. For cell-centered: element
     * \param fIdx Index of the element face (for box unused)
     */
    Scalar eddyDiffusivity(unsigned int idx, unsigned int fIdx = 0) const
    { return eddyDiffusivity_[idx][fIdx]; }

    /*!
     * \brief Returns the globalPos \f$\mathrm{[m/s]}\f$ for the intersections of an element.
     *
     * \param idx Index. For box: degree of freedom. For cell-centered: element
     * \param fIdx Index of the element face (for box unused)
     */
    DimVector globalPos(unsigned int idx, unsigned int fIdx = 0) const
    { return globalPos_[idx][fIdx]; }

    /*!
     * \brief Returns the velocity \f$\mathrm{[m/s]}\f$ for the intersections of an element.
     *
     * \param idx Index. For box: degree of freedom. For cell-centered: element
     * \param fIdx Index of the element face (for box unused)
     */
    DimVector velocity(unsigned int idx, unsigned int fIdx = 0) const
    { return velocity_[idx][fIdx]; }


    /*!
     * \brief \todo please doc me
     */
    void initializeFaceMapper() const
    {
        std::cout << "Initializing face mapper" << std::endl;

        std::vector<DimVector> coordinatesTemp;
        coordinatesTemp.resize(numScvFacesBox_);
        if (dim == 1)
        {
            coordinatesTemp[0] = 0.5;
        }
        else if (dim == 2)
        {
            coordinatesTemp[0][0] = 0.5;
            coordinatesTemp[0][1] = 0.25;
            coordinatesTemp[1][0] = 0.5;
            coordinatesTemp[1][1] = 0.75;
            coordinatesTemp[2][0] = 0.25;
            coordinatesTemp[2][1] = 0.5;
            coordinatesTemp[3][0] = 0.75;
            coordinatesTemp[3][1] = 0.5;
        }
        else // dim == 3
        {
            // x plane
            coordinatesTemp[0][0] = 0.5;
            coordinatesTemp[0][1] = 0.25;
            coordinatesTemp[0][2] = 0.25;
            coordinatesTemp[1][0] = 0.5;
            coordinatesTemp[1][1] = 0.25;
            coordinatesTemp[1][2] = 0.75;
            coordinatesTemp[2][0] = 0.5;
            coordinatesTemp[2][1] = 0.75;
            coordinatesTemp[2][2] = 0.25;
            coordinatesTemp[3][0] = 0.5;
            coordinatesTemp[3][1] = 0.75;
            coordinatesTemp[3][2] = 0.75;
            // y plane
            coordinatesTemp[4][0] = 0.25;
            coordinatesTemp[4][1] = 0.5;
            coordinatesTemp[4][2] = 0.25;
            coordinatesTemp[5][0] = 0.25;
            coordinatesTemp[5][1] = 0.5;
            coordinatesTemp[5][2] = 0.75;
            coordinatesTemp[6][0] = 0.75;
            coordinatesTemp[6][1] = 0.5;
            coordinatesTemp[6][2] = 0.25;
            coordinatesTemp[7][0] = 0.75;
            coordinatesTemp[7][1] = 0.5;
            coordinatesTemp[7][2] = 0.75;
            // z plane
            coordinatesTemp[8][0] = 0.25;
            coordinatesTemp[8][1] = 0.25;
            coordinatesTemp[8][2] = 0.5;
            coordinatesTemp[9][0] = 0.25;
            coordinatesTemp[9][1] = 0.75;
            coordinatesTemp[9][2] = 0.5;
            coordinatesTemp[10][0] = 0.75;
            coordinatesTemp[10][1] = 0.25;
            coordinatesTemp[10][2] = 0.5;
            coordinatesTemp[11][0] = 0.75;
            coordinatesTemp[11][1] = 0.75;
            coordinatesTemp[11][2] = 0.5;
        }

        for (unsigned int i = 0; i < numScvFacesBox_; ++i)
        {
            int idx = coordinateIdx(coordinatesTemp[i]);
            mapLocalIdxToFaceIdx_[idx] = i;
            mapFaceIdxToLocal_[i] = coordinatesTemp[i];
            std::cout << "l2x[" << idx << "] " << mapLocalIdxToFaceIdx_[idx] << '\n';
            std::cout << "x2l[" << mapLocalIdxToFaceIdx_[idx] << "] " << mapFaceIdxToLocal_[mapLocalIdxToFaceIdx_[idx]] << '\n';
        }
    }

    /*!
     * \brief \todo please doc me
     *
     * map local coordinates to an unique index
     */
    int coordinateIdx(DimVector local) const
    {
        Scalar value = 0;
        for (unsigned int curDim = 0; curDim < dim; ++curDim)
            value += value*10 + local[curDim]*10;
//         std::cout << "local " << local << " value " << int(value) << std::endl;
        return int(value);
    }

    /*!
     * \brief \todo please doc me
     */
    int mapLocalCoordinateToFaceIdx(DimVector local) const
    {
        return mapLocaIdxToFaceIdx(coordinateIdx(local));
    }

    /*!
     * \brief \todo please doc me
     *
     * \return Face idx
     */
    int mapLocaIdxToFaceIdx(int localIdx) const
    {
        return mapLocalIdxToFaceIdx_[localIdx];
    }

    /*!
     * \brief \todo please doc me
     *
     * \return Local coordinates of the scv face
     */
    DimVector mapFaceIdxToLocalCoordinate(int faceIdx) const
    {
        return mapFaceIdxToLocal_[faceIdx];
    }

private:
    std::vector<Scalar> density_;
    std::vector<Scalar> pressure_;
    std::vector<std::vector<Scalar>> eddyDiffusivity_;
    std::vector<std::vector<DimVector>> globalPos_;
    std::vector<std::vector<DimVector>> velocity_;
    const int numFacesCC = 2*dim;
    const int numScvFacesBox_ = dim*pow(2, dim-1);

    mutable std::map<int, int> mapLocalIdxToFaceIdx_;
    mutable std::map<int, DimVector> mapFaceIdxToLocal_;

protected:
    //! Current implementation.
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }
    //! Current implementation.
    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }
};

}

#include "propertydefaults.hh"

#endif
