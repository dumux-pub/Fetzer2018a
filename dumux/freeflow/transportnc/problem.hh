// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Base class for the n-component transport problems which use the implicit scheme.
 */
#ifndef DUMUX_TRANSPORTNC_PROBLEM_HH
#define DUMUX_TRANSPORTNC_PROBLEM_HH

#include <dumux/implicit/problem.hh>

#include "properties.hh"

namespace Dumux
{
/*!
 * \ingroup ImplicitTransportncProblems
 * \brief Base class for all problems which use the n-component Transport implicit model.
 *
 * This implements a function returning the eddy diffusivity.
 */
template<class TypeTag>
class TransportncProblem : public ImplicitProblem<TypeTag>
{
    typedef ImplicitProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GridView::Grid Grid;

    enum { dim = GridView::dimension,
           dimWorld = Grid::dimensionworld };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;
    typedef Dune::FieldVector<Scalar, dim> DimVector;

public:
    TransportncProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    { }

    /*!
     * \brief Called by the Dumux::TimeManager in order to initialize the problem.
     *
     * This initializes the ParentType class.
     * Further it initialize the static data fields (like velocity, pressure, etc.)
     * and stores them. So that they are only evaluate once.
     */
    void init()
    {
        ParentType::init();
        this->model().initializeFaceMapper();
        this->model().initializeData();
        this->model().updateData();
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Returns the pressure \f$Pa\f$ at a given global position.
     *        This function has to be overwritten by the problem.
     *
     * \param globalPos The position in global coordinates
     */
    Scalar pressureAtPos(const GlobalPosition &globalPos) const
    {
        DUNE_THROW(Dune::NotImplemented,
                   "The problem does not provide a pressureAtPos() method.");
    }

    /*!
     * \brief Returns the velocity vector \f$\frac{m}{s}\f$ at a given global position.
     *        This function has to be overwritten by the problem.
     *
     * \param globalPos The position in global coordinates
     */
    DimVector velocityAtPos(const GlobalPosition &globalPos) const
    {
        DUNE_THROW(Dune::NotImplemented,
                   "The problem does not provide a velocityAtPos() method.");
    }

    /*!
     * \brief Returns the temperature \f$K\f$ at a given global position.
     *        This function has to be overwritten by the problem.
     *
     * \param globalPos The position in global coordinates
     */
    Scalar temperatureAtPos(const GlobalPosition &globalPos) const
    {
        DUNE_THROW(Dune::NotImplemented,
                   "The problem does not provide a temperatureAtPos() method.");
    }

    /*!
     * \brief Returns the eddy diffusivity \f$\frac{m^2}{s}\f$ at a given global position.
     *
     * This is not specific to the discretization. By default it is returns zero.
     *
     * \param globalPos The position in global coordinates
     */
    Scalar eddyDiffusivityAtPos(const GlobalPosition &globalPos) const
    { return 0.0; }

    /*!
     * \brief Returns the density of the fluid \f$\frac{kg}{m^3}\f$ at a given global position.
     *
     * This is not specific to the discretization, by default it is returns a negative
     * value which is used to decide whether to use the density from the fluid system
     * or from the problem.
     *
     * \param globalPos The position in global coordinates
     */
    Scalar densityAtPos(const GlobalPosition &globalPos) const
    { return -1.0; }

    // \}
};

}

#endif
