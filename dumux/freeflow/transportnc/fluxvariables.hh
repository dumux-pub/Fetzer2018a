// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief This file contains the data which is required to calculate the
 *        component fluxes over a face of a finite volume.
 *
 * This means concentration gradients, diffusion coefficients, mass fractions, etc.
 * at the integration point.
 */
#ifndef DUMUX_TRANSPORTNC_FLUX_VARIABLES_HH
#define DUMUX_TRANSPORTNC_FLUX_VARIABLES_HH

#include <dumux/common/math.hh>

#include "properties.hh"

namespace Dumux
{

/*!
 * \ingroup ImplicitTransportncModel
 * \ingroup ImplicitFluxVariables
 * \brief This template class contains data which is required to
 *        calculate the component fluxes over a face of a finite
 *        volume for the n-component Transport model.
 *
 * This means concentration gradients, diffusion coefficient, mass fractions, etc.
 * at the integration point of a SCV or boundary face.
 */
template <class TypeTag>
class TransportncFluxVariables
{
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename FVElementGeometry::SubControlVolumeFace SCVFace;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    //dimensions
    enum { dim = GridView::dimension,
           dimWorld = GridView::dimensionworld };
    //phase indices
    enum { phaseIdx = Indices::phaseIdx };
    //component indices
    enum { phaseCompIdx = Indices::phaseCompIdx,
           transportCompIdx = Indices::transportCompIdx };
    //number of components
    enum { numComponents = Indices::numComponents };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef Dune::FieldVector<Scalar, dim> DimVector;
    typedef Dune::FieldMatrix<Scalar, dim, dim> DimMatrix;

    typedef typename GridView::ctype CoordScalar;
    typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;

    // some constant properties
    static const bool isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox);
    enum { dofCodim = isBox ? dim : 0 };

public:
    TransportncFluxVariables() = default;

    /*!
     * \brief Compute / update the flux variables
     *
     * \param problem The problem
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param fIdx The local index of the SCV (sub-control-volume) face
     * \param elemVolVars The volume variables of the current element
     * \param onBoundary A boolean variable to specify whether the flux variables
     * are calculated for interior SCV faces or boundary faces, default=false
     */
    void update(const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                const int fIdx,
                const ElementVolumeVariables &elemVolVars,
                const bool onBoundary = false)
    {
        fvGeometryPtr_ = &fvGeometry;
        onBoundary_ = onBoundary;
        fIdx_ = fIdx;
        calculateValues_(problem, element, elemVolVars);
        determineUpwindDirection_(elemVolVars);
    }

protected:
    // return const reference to fvGeometry
    const FVElementGeometry& fvGeometry_() const
    { return *fvGeometryPtr_; }

    bool onBoundary_;

    void calculateValues_(const Problem &problem,
                          const Element &element,
                          const ElementVolumeVariables &elemVolVars)
    {
        globalPos_ = this->face().ipGlobal;

        // calculate gradients and secondary variables at IPs
        pressure_ = Scalar(0);
        density_ = Scalar(0);
        velocity_ = Scalar(0);
        normalvelocity_ = Scalar(0);
        eddyDiffusivity_ = Scalar(0);

//         std::cout << " face().numFap " << face().numFap << std::endl;
        for (int idx = 0; idx < face().numFap; idx++) // loop over adjacent vertices
        {
            // index for the element volume variables
            int volVarsIdx = face().fapIndices[idx];
//             std::cout << " volVarsIdx " << volVarsIdx
//                          << " idx " << idx
//                          << std::endl;
//             if (volVarsIdx == 6)
//                 volVarsIdx = 0;
//             std::cout << " volVarsIdx " << volVarsIdx
//                       << " idx " << idx
//                       << std::endl;

            // phase density and viscosity at IP
            density_ += elemVolVars[volVarsIdx].density()
                        * face().shapeValue[idx];
            pressure_ += elemVolVars[volVarsIdx].pressure()
                         * face().shapeValue[idx];

            // use data from correct location
            if (isBox)
            {
                DimVector velocityTimesShapeValue = elemVolVars[volVarsIdx].velocity();
                velocityTimesShapeValue *= face().shapeValue[idx];
                velocity_ += velocityTimesShapeValue;
            }
            else
            {
                eddyDiffusivity_ += elemVolVars[volVarsIdx].eddyDiffusivity()
                                    * face().shapeValue[idx];
            }
        }

        // use data from correct location
        int eIdxGlobal = problem.elementMapper().index(element);
        if (isBox)
        {
            int fIdx = problem.model().mapLocalCoordinateToFaceIdx(element.geometry().local(globalPos_));
            eddyDiffusivity_ = problem.model().eddyDiffusivity(eIdxGlobal, fIdx);
        }
        else
        {
            DimVector faceCenterLocalInElement = element.geometry().local(globalPos_);
            int iIdx = 0;
            for(unsigned int dimIdx = 0; dimIdx < dim; ++dimIdx)
                if(faceCenterLocalInElement[dimIdx] < 0.25 || faceCenterLocalInElement[dimIdx] > 0.75)
                    iIdx = 2*dimIdx + int(faceCenterLocalInElement[dimIdx]);
            velocity_ = problem.model().velocity(eIdxGlobal, iIdx);
        }

        normalvelocity_ = velocity_ * face().normal;

        // loop over all components
        for (int compIdx=0; compIdx<numComponents; compIdx++)
        {
            massFraction_[compIdx] = Scalar(0.0);
            moleFraction_[compIdx] = Scalar(0.0);
            diffusionCoeff_[compIdx] = Scalar(0.0);
            moleFractionGrad_[compIdx] = Scalar(0.0);

            if (phaseCompIdx!=compIdx) //no transport equation parameters needed for the mass balance
            {
                molarDensity_ = Scalar(0.0);

                for (int idx = 0; idx < face().numFap; idx++)
                {
                    // index for the element volume variables
                    int volVarsIdx = face().fapIndices[idx];

                    molarDensity_ += elemVolVars[volVarsIdx].molarDensity()
                                     * face().shapeValue[idx];
                    massFraction_[compIdx] += elemVolVars[volVarsIdx].massFraction(compIdx)
                                              * face().shapeValue[idx];
                    moleFraction_[compIdx] += elemVolVars[volVarsIdx].moleFraction(compIdx)
                                              * face().shapeValue[idx];
                    diffusionCoeff_[compIdx] += elemVolVars[volVarsIdx].diffusionCoeff(compIdx)
                                                * face().shapeValue[idx];

                    // the gradient of the mole fraction at the IP
                    GlobalPosition tmp = this->face().grad[idx];
                    tmp *= elemVolVars[volVarsIdx].moleFraction(compIdx);
                    moleFractionGrad_[compIdx] += tmp;
                }

                Valgrind::CheckDefined(molarDensity_);
                Valgrind::CheckDefined(massFraction_[compIdx]);
                Valgrind::CheckDefined(moleFraction_[compIdx]);
                Valgrind::CheckDefined(diffusionCoeff_[compIdx]);
                Valgrind::CheckDefined(moleFractionGrad_[compIdx]);
            }
        }

        Valgrind::CheckDefined(pressure_);
        Valgrind::CheckDefined(density_);
        Valgrind::CheckDefined(velocity_);
        Valgrind::CheckDefined(normalvelocity_);
        Valgrind::CheckDefined(eddyDiffusivity_);
    }

    void determineUpwindDirection_(const ElementVolumeVariables &elemVolVars)
    {
        upstreamIdx_ = face().i;
        downstreamIdx_ = face().j;

        if (normalVelocity() < 0)
            std::swap(upstreamIdx_, downstreamIdx_);
    }

    int fIdx_; // the index of the considered face
    int upstreamIdx_; // local index of the upwind vertex
    int downstreamIdx_; // local index of the downwind vertex

    // values at the integration point
    DimVector globalPos_;
    Scalar pressure_;
    Scalar density_;
    DimVector velocity_;
    Scalar normalvelocity_;
    Scalar molarDensity_;
    Scalar massFraction_[numComponents];
    Scalar moleFraction_[numComponents];
    Scalar diffusionCoeff_[numComponents];
    DimVector moleFractionGrad_[numComponents];
    Scalar eddyDiffusivity_;

private:
    const FVElementGeometry* fvGeometryPtr_; //!< Information about the geometry of discretization

public:
// transport
    /*!
     * \brief Returns the global coordinates of the integration point.
     */
    DimVector globalPos() const
    { return globalPos_; }

// stokes
    /*!
     * \brief The face of the current sub-control volume. This may be either
     *        an inner sub-control-volume face or a face on the boundary.
     */
    const SCVFace &face() const
    {
        if (onBoundary_)
            return fvGeometry_().boundaryFace[fIdx_];
        else
            return fvGeometry_().subContVolFace[fIdx_];
    }

    /*!
     * \brief Return the average volume of the upstream and the downstream sub-control volume;
     *        this is required for the stabilization.
     */
    const Scalar averageSCVVolume() const
    {
        return 0.5 * (fvGeometry_().subContVol[upstreamIdx_].volume
                      + fvGeometry_().subContVol[downstreamIdx_].volume);
    }

    /*!
     * \brief Return the local index of the upstream sub-control volume.
     */
    int upstreamIdx() const
    { return upstreamIdx_; }

    /*!
     * \brief Return the local index of the downstream sub-control volume.
     */
    int downstreamIdx() const
    { return downstreamIdx_; }

    /*!
     * \brief Indicates if a face is on a boundary. Used for in the
     *        face() method (e.g. for outflow boundary conditions).
     */
    bool onBoundary() const
    { return onBoundary_; }

    /*!
     * \brief Return the pressure \f$\mathrm{[Pa]}\f$ at the integration
     *        point.
     */
    Scalar pressure() const
    { return pressure_; }

    /*!
     * \brief Return the mass density \f$ \mathrm{[kg/m^3]} \f$ at the integration
     *        point.
     */
    Scalar density() const
    { return density_; }

    /*!
     * \brief Return the velocity vector at the integration point.
     */
    const DimVector &velocity() const
    { return velocity_; }

    /*!
     * \brief Return the velocity \f$ \mathrm{[m/s]} \f$ at the integration
     *        point multiplied by the normal and the area.
     */
    Scalar normalVelocity() const
    { return normalvelocity_; }


// stokesnc
    /*!
     * \brief Return the molar density \f$ \mathrm{[mol/m^3]} \f$ at the integration point.
     */
    const Scalar molarDensity() const
    { return molarDensity_; }

    /*!
     * \brief Return the mass fraction of a transported component at the integration point.
     */
    const Scalar massFraction(int compIdx) const
    { return massFraction_[compIdx]; }

    /*!
     * \brief Return the mole fraction of a transported component at the integration point.
     */
    const Scalar moleFraction(int compIdx) const
    { return moleFraction_[compIdx]; }

    /*!
     * \brief Return the molar diffusion coefficient of a transported component at the integration point.
     */
    const Scalar diffusionCoeff(int compIdx) const
    { return diffusionCoeff_[compIdx]; }

    /*!
     * \brief Return the gradient of the mole fraction at the integration point.
     */
    const DimVector &moleFractionGrad(int compIdx) const
    { return moleFractionGrad_[compIdx]; }

    /*!
     * \brief Return the eddy diffusivity (if implemented).
     */
    const Scalar eddyDiffusivity() const
    { return eddyDiffusivity_; }
};

} // end namespace

#endif
