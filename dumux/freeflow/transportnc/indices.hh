// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/

/*!
 * \file
 *
 * \brief Defines the indices required for n-component transport box model.
 */
#ifndef DUMUX_TRANSPORTNC_INDICES_HH
#define DUMUX_TRANSPORTNC_INDICES_HH

#include "properties.hh"

namespace Dumux
{
// \{

/*!
 * \ingroup ImplicitTransportncModel
 * \ingroup ImplicitIndices
 * \brief Defines the indices required for n component transport box model.
 *
 * \tparam PVOffset The first index in a primary variable vector.
 */
template <class TypeTag, int PVOffset = 0>
struct TransportncCommonIndices
{
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;

public:
    //! Index of the employed phase
    static const int phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx);

    //! Number of components in employed fluidsystem
    static const int numComponents = FluidSystem::numComponents;

    //! The index of the main component of the considered phase
    static const int phaseCompIdx = phaseIdx;
    //! The index of the first transported component; ASSUMES phase indices of 0 and 1
    //! For n>2 please don't use this index, because it looses its actual meaning.
    static const int transportCompIdx = (unsigned int)(1-phaseIdx);

    // Transport equation indices
    //! The index of the transport equation for a two component model.
    //! For n>2 please don't use this index, because it looses its actual meaning.
    static const int transportEqIdx = PVOffset;

    // Primary variables
    //! The index of the first mass or mole fraction of the transported component in primary variable vectors
    static const int massOrMoleFracIdx = transportEqIdx;
};
} // end namespace

#endif
