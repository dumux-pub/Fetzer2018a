// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup ImplicitTransportncniModel
 *
 * \file
 *
 * \brief Defines the additional properties required for the non-isothermal n-compontent
 *        Transport box model.
 */
#ifndef DUMUX_TRANSPORTNCNI_PROPERTIES_HH
#define DUMUX_TRANSPORTNCNI_PROPERTIES_HH

#include <dumux/implicit/box/properties.hh>
#include <dumux/implicit/cellcentered/properties.hh>

#include <dumux/freeflow/transportnc/properties.hh>

namespace Dumux
{
namespace Properties
{
//////////////////////////////////////////////////////////////////
// Type tags
//////////////////////////////////////////////////////////////////
//! The type tag for the Transportncni problems
NEW_TYPE_TAG(Transportncni, INHERITS_FROM(Transportnc));
NEW_TYPE_TAG(BoxTransportncni, INHERITS_FROM(BoxModel, Transportncni));
NEW_TYPE_TAG(CCTransportncni, INHERITS_FROM(CCModel, Transportncni));
}

}
#endif
