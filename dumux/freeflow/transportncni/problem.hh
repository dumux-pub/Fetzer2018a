// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Base class for the non-isothermal n-component transport problems
 *        which use the implicit scheme.
 */
#ifndef DUMUX_TRANSPORTNCNI_PROBLEM_HH
#define DUMUX_TRANSPORTNCNI_PROBLEM_HH

#include <dumux/freeflow/transportnc/problem.hh>

#include "properties.hh"

namespace Dumux
{
/*!
 * \ingroup ImplicitTransportncniProblems
 * \brief Base class for the non-isothermal n-component transport problems
 *        which use the implicit scheme.
 *
 * This implements a function returning the eddy conductivity.
 */
template<class TypeTag>
class TransportncniProblem : public TransportncProblem<TypeTag>
{
    typedef TransportncProblem<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, TimeManager) TimeManager;
    typedef typename GridView::Grid Grid;

    enum { dimWorld = Grid::dimensionworld };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef Dune::FieldVector<Scalar, dimWorld> GlobalPosition;

public:
    TransportncniProblem(TimeManager &timeManager, const GridView &gridView)
        : ParentType(timeManager, gridView)
    { }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Returns the eddy thermal conductivity \f$\mathrm{[W/(m*K)]}\f$ at a given global position.
     *
     * This is not specific to the discretization. By default it is returns zero.
     *
     * \param globalPos The position in global coordinates
     */
    Scalar eddyThermalConductivityAtPos(const GlobalPosition &globalPos) const
    { return 0.0; }

    // \}
};

}

#endif
