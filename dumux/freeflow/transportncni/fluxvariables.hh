// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This file contains the data which is required to calculate the
 *        energy fluxes over a face of a finite volume.
 *
 * This means concentration temperature gradients and heat conductivity at
 * the integration point.
 */
#ifndef DUMUX_TRANSPORTNCNI_FLUX_VARIABLES_HH
#define DUMUX_TRANSPORTNCNI_FLUX_VARIABLES_HH

#include <dumux/common/math.hh>

#include <dumux/freeflow/transportnc/fluxvariables.hh>

namespace Dumux
{

/*!
 * \ingroup ImplicitTransportncniModel
 * \ingroup ImplicitFluxVariables
 * \brief This template class contains data which is required to
 *        calculate the energy fluxes over a face of a finite
 *        volume for the non-isothermal n-component Transport box model.
 *
 * This means temperature gradients and heat conductivity
 * at the integration point of a SCV face or boundary face.
 */
template <class TypeTag>
class TransportncniFluxVariables : public TransportncFluxVariables<TypeTag>
{
    typedef TransportncFluxVariables<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    //dimensions
    enum { dim = GridView::dimension,
           dimWorld = GridView::dimensionworld };

    enum {  numComponents = Indices::numComponents };

    typedef typename GridView::template Codim<0>::Entity Element;
    typedef Dune::FieldVector<Scalar, dim> DimVector;

    typedef typename GridView::ctype CoordScalar;
    typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;

    // some constant properties
    static const bool isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox);
    enum { dofCodim = isBox ? dim : 0 };

public:
    TransportncniFluxVariables() = default;

    /*!
     * \brief Compute / update the flux variables
     *
     * \param problem The problem
     * \param element The finite element
     * \param fvGeometry The finite-volume geometry
     * \param fIdx The local index of the SCV (sub-control-volume) face
     * \param elemVolVars The volume variables of the current element
     * \param onBoundary A boolean variable to specify whether the flux variables
     * are calculated for interior SCV faces or boundary faces, default=false
     */
    void update(const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry,
                const int fIdx,
                const ElementVolumeVariables &elemVolVars,
                const bool onBoundary = false)
    {
        ParentType::update(problem, element, fvGeometry, fIdx, elemVolVars, onBoundary);
        calculateValues_(problem, element, elemVolVars);
    }

    /*!
     * \brief Returns the temperature \f$\mathrm{[K]}\f$ at the integration point.
     */
    const Scalar temperature() const
    { return temperature_; }

    /*!
     * \brief Returns the thermal conductivity \f$\mathrm{[W/(m*K)]}\f$ at the integration point.
     */
    const Scalar thermalConductivity() const
    { return thermalConductivity_; }

    /*!
     * \brief Returns the specific isobaric heat capacity \f$\mathrm{[J/(kg*K)]}\f$
     *        at the integration point.
     */
    Scalar heatCapacity() const
    { return heatCapacity_; }

    /*!
     * \brief Return the enthalpy of a component \f$\mathrm{[J/kg]}\f$ at the integration point.
     */
    const Scalar componentEnthalpy(int componentIdx) const
    { return componentEnthalpy_[componentIdx]; }

    /*!
     * \brief Returns the temperature gradient \f$\mathrm{[K/m]}\f$ at the integration point.
     */
    const DimVector &temperatureGrad() const
    { return temperatureGrad_; }

    /*!
     * \brief Return the thermal eddy conductivity \f$\mathrm{[W/(m*K)]}\f$ (if implemented).
     */
    const Scalar eddyThermalConductivity() const
    { return eddyThermalConductivity_; }


protected:
    void calculateValues_(const Problem &problem,
                          const Element &element,
                          const ElementVolumeVariables &elemVolVars)
    {
        temperature_ = Scalar(0);
        thermalConductivity_ = Scalar(0);
        eddyThermalConductivity_ = Scalar(0);
        heatCapacity_ = Scalar(0);
        temperatureGrad_ = Scalar(0);

        for (int idx = 0; idx < this->face().numFap; idx++) // loop over adjacent vertices
        {
            // index for the element volume variables
            int volVarsIdx = this->face().fapIndices[idx];

            temperature_ += elemVolVars[volVarsIdx].temperature()
                            * this->face().shapeValue[idx];

            thermalConductivity_ += elemVolVars[volVarsIdx].thermalConductivity()
                                    * this->face().shapeValue[idx];

            // use data from correct location
            if (!isBox)
            {
                eddyThermalConductivity_ += elemVolVars[volVarsIdx].eddyThermalConductivity()
                                            * this->face().shapeValue[idx];
            }

            heatCapacity_ += elemVolVars[volVarsIdx].heatCapacity()
                             * this->face().shapeValue[idx];

            // the gradient of the temperature at the IP
            GlobalPosition tmp = this->face().grad[idx];
            tmp *= elemVolVars[volVarsIdx].temperature();
            temperatureGrad_ += tmp;
        }

        // use data from correct location
        int eIdxGlobal = problem.elementMapper().index(element);
        if (isBox)
        {
            int fIdx = problem.model().mapLocalCoordinateToFaceIdx(element.geometry().local(this->globalPos()));
            eddyThermalConductivity_ = problem.model().eddyThermalConductivity(eIdxGlobal, fIdx);
        }

        Valgrind::CheckDefined(temperature_);
        Valgrind::CheckDefined(thermalConductivity_);
        Valgrind::CheckDefined(eddyThermalConductivity_);
        Valgrind::CheckDefined(heatCapacity_);
        Valgrind::CheckDefined(temperatureGrad_);

        for (unsigned int i = 0; i < numComponents; ++i)
        {
            componentEnthalpy_[i] = Scalar(0.0);
            for (int idx = 0; idx < this->face().numFap; idx++) // loop over vertices of the element
            {
                componentEnthalpy_[i] += elemVolVars[idx].componentEnthalpy(i)
                                         * this->face().shapeValue[idx];
            }
            Valgrind::CheckDefined(componentEnthalpy_[i]);
        }
    }

    Scalar temperature_;
    Scalar thermalConductivity_;
    Scalar eddyThermalConductivity_;
    Scalar heatCapacity_;
    Scalar componentEnthalpy_[numComponents];
    DimVector temperatureGrad_;
};

} // end namespace

#endif
