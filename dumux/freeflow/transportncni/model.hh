// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Adaption of the implicit scheme to the non-isothermal n-component Transport model.
 */
#ifndef DUMUX_TRANSPORTNCNI_MODEL_HH
#define DUMUX_TRANSPORTNCNI_MODEL_HH

#include <dumux/freeflow/transportnc/model.hh>

#include "localresidual.hh"
#include "problem.hh"
#include "properties.hh"

namespace Dumux {
/*!
 * \ingroup ImplicitTransportncniModel
 * \brief Adaption of the implicit scheme to the non-isothermal n-component Transport model.
 *
 * This model implements a non-isothermal n-component Transport flow of a fluid
 * on a given velocity field.
 *
 * Component mass balance equation:
 * \f[
 \frac{\partial \left(\varrho_g X_g^\kappa\right)}{\partial t}
 + \boldsymbol{\nabla} \boldsymbol{\cdot} \left( \varrho_g {\boldsymbol{v}}_g X_g^\kappa
 - D^\kappa_g \varrho_g \frac{M^\kappa}{M_g} \boldsymbol{\nabla} x_g^\kappa \right)
 - q_g^\kappa = 0
 * \f]
 *
 * Energy balance equation:
 * \f[
\frac{\partial (\varrho_g  u_g)}{\partial t}
+ \boldsymbol{\nabla} \boldsymbol{\cdot} \left( \varrho_g h_g {\boldsymbol{v}}_g
- \sum_\kappa \left[ h^\kappa_g D^\kappa_g \varrho_g \frac{M^\kappa}{M_g} \nabla x^\kappa_g \right]
- \lambda_g \boldsymbol{\nabla} T \right) - q_T = 0
 * \f]
 *
 * This is discretized using a fully-coupled vertex centered finite volume
 * (box) the implicit Euler method as temporal discretization.
 *
 */
template<class TypeTag>
class TransportncniModel : public TransportncModel<TypeTag>
{
    typedef TransportncModel<TypeTag> ParentType;
    typedef typename GET_PROP_TYPE(TypeTag, Model) Implementation;
    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum { dim = GridView::dimension,
           dimWorld = GridView::dimensionworld,
           transportCompIdx = Indices::transportCompIdx,
           phaseIdx = GET_PROP_VALUE(TypeTag, PhaseIdx),
           useMoles = GET_PROP_VALUE(TypeTag, UseMoles),
           numComponents = Indices::numComponents };

    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::IntersectionIterator IntersectionIterator;

    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;
    typedef typename GET_PROP_TYPE(TypeTag, ElementBoundaryTypes) ElementBoundaryTypes;
    typedef typename GET_PROP_TYPE(TypeTag, SolutionVector) SolutionVector;

    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
    typedef typename GET_PROP_TYPE(TypeTag, FluxVariables) FluxVariables;
    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;
    typedef typename GET_PROP_TYPE(TypeTag, ElementVolumeVariables) ElementVolumeVariables;

    typedef Dune::FieldVector<Scalar, dimWorld> DimVector;

    // some constant properties
    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    enum { dofCodim = isBox ? dim : 0 };

public:
    //! \copydoc ImplicitModel::addOutputVtkFields
    template <class MultiWriter>
    void addOutputVtkFields(const SolutionVector &sol,
                            MultiWriter &writer)
    {
        typedef Dune::BlockVector<Dune::FieldVector<Scalar, 1> > ScalarField;
        typedef Dune::BlockVector<Dune::FieldVector<Scalar, dimWorld> > VectorField;

        // create the required scalar fields
        unsigned numDofs = this->numDofs();
        ScalarField *density = writer.allocateManagedBuffer(numDofs);
        ScalarField *diffCoeff = writer.allocateManagedBuffer(numDofs);
        ScalarField *eddyDiffusivity = writer.allocateManagedBuffer(numDofs);
        ScalarField *eddyThermalConductivity = writer.allocateManagedBuffer(numDofs);
        ScalarField *massFraction[numComponents];
        for (int i = 0; i < numComponents; ++i)
            massFraction[i] = writer.template allocateManagedBuffer<Scalar, 1>(numDofs);
        ScalarField *moleFraction[numComponents];
        for (int i = 0; i < numComponents; ++i)
            moleFraction[i] = writer.template allocateManagedBuffer<Scalar, 1>(numDofs);
        ScalarField *pressure = writer.allocateManagedBuffer(numDofs);
        ScalarField *pressureDelta = writer.allocateManagedBuffer(numDofs);
        ScalarField *temperature = writer.allocateManagedBuffer(numDofs);
        ScalarField *thermalConductivity = writer.allocateManagedBuffer(numDofs);

        // create the required vector fields
        VectorField *globalPos = writer.template allocateManagedBuffer<Scalar, dimWorld> (numDofs);
        VectorField *velocity = writer.template allocateManagedBuffer<Scalar, dimWorld> (numDofs);

        // create the required data per element fields
        unsigned numElements = this->gridView_().size(0);
        ScalarField *rank = writer.allocateManagedBuffer(numElements);

        FVElementGeometry fvGeometry;
        ElementVolumeVariables elemVolVars;
        VolumeVariables volVars;
        ElementBoundaryTypes elemBcTypes;

        ElementIterator eIt = this->gridView_().template begin<0>();
        ElementIterator eEndIt = this->gridView_().template end<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            if(eIt->partitionType() == Dune::InteriorEntity)
            {
                int eIdx = this->elementMapper().index(*eIt);
                (*rank)[eIdx] = this->gridView_().comm().rank();

                fvGeometry.update(this->gridView_(), *eIt);
                elemVolVars.update(this->problem_(), *eIt, fvGeometry, false /* oldSol? */);
                elemBcTypes.update(this->problem_(), *eIt, fvGeometry);

                for (int scvIdx = 0; scvIdx < fvGeometry.numScv; ++scvIdx)
                {
                        int dofIdxGlobal = this->dofMapper().subIndex(*eIt, scvIdx, dofCodim);
                        (*density)[dofIdxGlobal] = elemVolVars[scvIdx].density();
                        (*diffCoeff)[dofIdxGlobal] = elemVolVars[scvIdx].diffusionCoeff(transportCompIdx);
                        if (!isBox)
                        {
                            (*eddyDiffusivity)[dofIdxGlobal] = elemVolVars[scvIdx].eddyDiffusivity();
                            (*eddyThermalConductivity)[dofIdxGlobal] = elemVolVars[scvIdx].eddyThermalConductivity();
                        }
                        (*globalPos)[dofIdxGlobal] = elemVolVars[scvIdx].globalPos();
                        (*pressure)[dofIdxGlobal] = elemVolVars[scvIdx].pressure();
                        (*pressureDelta)[dofIdxGlobal] = elemVolVars[scvIdx].pressure() - 1e5;
                        (*temperature)[dofIdxGlobal] = elemVolVars[scvIdx].temperature();
                        (*thermalConductivity)[dofIdxGlobal] = elemVolVars[scvIdx].thermalConductivity();
                        if (isBox)
                            (*velocity)[dofIdxGlobal] = elemVolVars[scvIdx].velocity();

                        for (int compIdx = 0; compIdx < numComponents; ++compIdx)
                        {
                            (*massFraction[compIdx])[dofIdxGlobal] = elemVolVars[scvIdx].massFraction(compIdx);
                            (*moleFraction[compIdx])[dofIdxGlobal] = elemVolVars[scvIdx].moleFraction(compIdx);
                        }
                }
            }
        }
        writer.attachDofData(*density, "rho", isBox);
        writer.attachDofData(*diffCoeff, "D", isBox);
        if (!isBox)
        {
            writer.attachDofData(*eddyDiffusivity, "D_t", isBox);
            writer.attachDofData(*eddyThermalConductivity, "lambda_t", isBox);
        }
        writer.attachDofData(*globalPos, "globalPos", isBox, dimWorld);
        writer.attachDofData(*pressure, "p", isBox);
        writer.attachDofData(*pressureDelta, "delP", isBox);
        writer.attachDofData(*temperature, "temperature", isBox);
        writer.attachDofData(*thermalConductivity, "lambda", isBox);
        if (isBox)
            writer.attachDofData(*velocity, "v", isBox, dimWorld);

        for (int j = 0; j < numComponents; ++j)
        {
            std::ostringstream moleFrac, massFrac;
            moleFrac << "x_" << FluidSystem::phaseName(phaseIdx) << "^" << FluidSystem::componentName(j);
            writer.attachDofData(*moleFraction[j], moleFrac.str().c_str(), isBox);

            massFrac << "X_" << FluidSystem::phaseName(phaseIdx) << "^" << FluidSystem::componentName(j);
            writer.attachDofData(*massFraction[j], massFrac.str().c_str(), isBox);
        }

        // write flux values to .csv
        std::ofstream fluxFile("fluxVarsData.csv", std::ios_base::out);
        asImp_().writeFluxFileHeader(fluxFile);
        fluxFile << std::endl;
        unsigned int numFaces = isBox ? numScvFacesBox_ : numFacesCC_;
        for (unsigned int i = 0; i < numElements; ++i)
        {
            for (unsigned int j = 0; j < numFaces; ++j)
            {
                asImp_().writeFluxFileData(fluxFile, i, j);
                fluxFile << std::endl;
            }
        }
        fluxFile.close();
    }

    //! \copydoc TransportncModel::writeFluxFileHeader
    void writeFluxFileHeader(std::ofstream &fluxFile)
    {
        ParentType::writeFluxFileHeader(fluxFile);
        if (isBox)
            fluxFile << "," << "eddyThermalConductivity";
    }

    //! \copydoc TransportncModel::writeFluxFileData
    void writeFluxFileData(std::ofstream &fluxFile, unsigned int idx, unsigned int fIdx)
    {
        ParentType::writeFluxFileData(fluxFile, idx, fIdx);
        if (isBox)
            fluxFile << "," << eddyThermalConductivity(idx, fIdx);
    }

    //! \copydoc TransportncModel::initializeData
    void initializeData()
    {
        ParentType::initializeData();

        unsigned numDofs = this->numDofs();
        unsigned numElements = this->gridView_().size(0);
        if (isBox)
        {
            eddyThermalConductivity_.resize(numElements, std::vector<Scalar>(0.0));
            for (unsigned int i = 0; i < eddyThermalConductivity_.size(); ++i)
                eddyThermalConductivity_[i].resize(numScvFacesBox_, 0.0); // several entries, is located at face
        }
        else
        {
            eddyThermalConductivity_.resize(numDofs, std::vector<Scalar>(0.0));
            for (unsigned int i = 0; i < eddyThermalConductivity_.size(); ++i)
                eddyThermalConductivity_[i].resize(1, 0.0); // only 1 entry, is located at dof
        }
    }

    //! \copydoc TransportncModel::updateData
    void updateData()
    {
        ParentType::updateData();

        FVElementGeometry fvGeometry;

        ElementIterator eIt = this->gridView_().template begin<0>();
        ElementIterator eEndIt = this->gridView_().template end<0>();
        for (; eIt != eEndIt; ++eIt)
        {
            fvGeometry.update(this->gridView_(), *eIt);

            int eIdxGlobal = this->elementMapper().index(*eIt);
            if (isBox)
            {
                for (unsigned int fIdx = 0; fIdx < numScvFacesBox_; ++fIdx)
                {
                    DimVector elementLocal = this->mapFaceIdxToLocalCoordinate(fIdx);
                    DimVector globalPos = eIt->geometry().global(elementLocal);

                    eddyThermalConductivity_[eIdxGlobal][fIdx]
                        = this->problem_().eddyThermalConductivityAtPos(globalPos);
                }
            }
            else
            {
                for (int i = 0; i < fvGeometry.numScv; ++i)
                {
                    int dofIdxGlobal = this->dofMapper().subIndex(*eIt, i, dofCodim);
                    DimVector globalPos = fvGeometry.subContVol[i].global;
                    eddyThermalConductivity_[dofIdxGlobal][0]
                        = this->problem_().eddyThermalConductivityAtPos(globalPos);
                }
            }
        }
    }

    /*!
     * \brief Returns the thermal eddy diffusivity \f$\mathrm{[W/(m*K)]}\f$
     *        for a degree of freedom and its faces.
     *
     * \param idx Index. For box: degree of freedom. For cell-centered: element
     * \param fIdx Index of the element face (for box unused)
     */
    Scalar eddyThermalConductivity(unsigned int idx, unsigned int fIdx = 0) const
    { return eddyThermalConductivity_[idx][fIdx]; }

protected:
    std::vector<std::vector<Scalar>> eddyThermalConductivity_;
    const int numFacesCC_ = 2*dim;
    const int numScvFacesBox_ = dim*pow(2, dim-1);

    //! Current implementation.
    Implementation &asImp_()
    { return *static_cast<Implementation*>(this); }
    //! Current implementation.
    const Implementation &asImp_() const
    { return *static_cast<const Implementation*>(this); }
};
}

#include "propertydefaults.hh"

#endif
