// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup Properties
 * \ingroup ImplicitProperties
 * \ingroup TransportncniModel
 *
 * \file
 *
 * \brief Sets default properties for the non-isothermal
 *        n-component Transport box model.
 */
#ifndef DUMUX_TRANSPORTNCNI_PROPERTY_DEFAULTS_HH
#define DUMUX_TRANSPORTNCNI_PROPERTY_DEFAULTS_HH

#include "fluxvariables.hh"
#include "indices.hh"
#include "localresidual.hh"
#include "model.hh"
#include "volumevariables.hh"

namespace Dumux
{

namespace Properties
{
//////////////////////////////////////////////////////////////////
// Properties
//////////////////////////////////////////////////////////////////

//! set the number of equations
SET_PROP(Transportncni, NumEq)
{
    typedef typename GET_PROP_TYPE(TypeTag, FluidSystem) FluidSystem;
 public:
    static constexpr int value = FluidSystem::numComponents - 1 + /*energyequation*/1;
};

//! Use the transportncni local residual function
SET_TYPE_PROP(Transportncni, LocalResidual, TransportncniLocalResidual<TypeTag>);

//! The Model property
SET_TYPE_PROP(Transportncni, Model, TransportncniModel<TypeTag>);

//! The VolumeVariables property
SET_TYPE_PROP(Transportncni, VolumeVariables, TransportncniVolumeVariables<TypeTag>);

//! The FluxVariables property
SET_TYPE_PROP(Transportncni, FluxVariables, TransportncniFluxVariables<TypeTag>);

//! Set the indices for the Transportncni model
SET_TYPE_PROP(Transportncni, Indices,  TransportncniCommonIndices<TypeTag>);
}
}
#endif
