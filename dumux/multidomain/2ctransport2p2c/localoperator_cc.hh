// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The local operator for the coupling of a two-component Transport model
 *        and a two-phase two-component porous-medium model under isothermal conditions.
 */
#ifndef DUMUX_2CTRANSPORT_2P2C_LOCALOPERATOR_HH
#define DUMUX_2CTRANSPORT_2P2C_LOCALOPERATOR_HH

#include <iostream>

#include <dune/common/version.hh>

#include <dune/pdelab/multidomain/couplingutilities.hh>
#include <dune/pdelab/localoperator/pattern.hh>
#include <dune/pdelab/localoperator/idefault.hh>

#include <dumux/multidomain/properties.hh>
#include <dumux/porousmediumflow/2p2c/implicit/model.hh>

#include <dumux/freeflow/transportnc/model.hh>
#include <dumux/multidomain/2ctransport2p2c/propertydefaults.hh>

namespace Dumux {

/*!
 * \ingroup TwoPTwoCTransportTwoCModel
 * \brief The local operator for the coupling of a two-component Transport model
 *        and a two-phase two-component porous-medium model under isothermal conditions.
 *
 * This model implements the coupling between a Transport model
 * and a porous-medium flow model under isothermal conditions.
 * Here the coupling conditions for the individual balance are presented:
 *
 * \todo add coupling conditions
 */
template<class TypeTag>
class TwoCTransportTwoPTwoCLocalOperator :
        public Dune::PDELab::MultiDomain::CouplingOperatorDefaultFlags,
        public Dune::PDELab::MultiDomain::NumericalJacobianCoupling<TwoCTransportTwoPTwoCLocalOperator<TypeTag>>,
        public Dune::PDELab::MultiDomain::FullCouplingPattern,
        public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
{
 public:
    typedef typename GET_PROP_TYPE(TypeTag, Problem) GlobalProblem;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainCouplingLocalOperator) Implementation;

    typedef typename GET_PROP_TYPE(TypeTag, SubDomain1TypeTag) Transport2cTypeTag;
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain2TypeTag) TwoPTwoCTypeTag;

    typedef typename GET_PROP_TYPE(TwoPTwoCTypeTag, FluidSystem) FluidSystem;

    typedef typename GET_PROP_TYPE(Transport2cTypeTag, ElementVolumeVariables) ElementVolumeVariables1;
    typedef typename GET_PROP_TYPE(TwoPTwoCTypeTag, ElementVolumeVariables) ElementVolumeVariables2;

    typedef typename GET_PROP_TYPE(Transport2cTypeTag, FluxVariables) BoundaryVariables1;
    typedef typename GET_PROP_TYPE(TwoPTwoCTypeTag, FluxVariables) BoundaryVariables2;

    typedef typename GET_PROP_TYPE(Transport2cTypeTag, ElementBoundaryTypes) ElementBoundaryTypes1;
    typedef typename GET_PROP_TYPE(TwoPTwoCTypeTag, ElementBoundaryTypes) ElementBoundaryTypes2;

    typedef typename GET_PROP_TYPE(Transport2cTypeTag, BoundaryTypes) BoundaryTypes1;
    typedef typename GET_PROP_TYPE(TwoPTwoCTypeTag, BoundaryTypes) BoundaryTypes2;

    typedef typename GET_PROP_TYPE(Transport2cTypeTag, FVElementGeometry) FVElementGeometry1;
    typedef typename GET_PROP_TYPE(TwoPTwoCTypeTag, FVElementGeometry) FVElementGeometry2;

    // Multidomain Grid types
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MDGrid;
    typedef typename MDGrid::Traits::template Codim<0>::Entity MDElement;

    typedef typename GET_PROP_TYPE(Transport2cTypeTag, GridView) Transport2cGridView;
    typedef typename GET_PROP_TYPE(TwoPTwoCTypeTag, GridView) TwoPTwoCGridView;
    typedef typename Transport2cGridView::template Codim<0>::Entity SDElement1;
    typedef typename TwoPTwoCGridView::template Codim<0>::Entity SDElement2;

    typedef typename GET_PROP_TYPE(Transport2cTypeTag, Indices) Transport2cIndices;
    typedef typename GET_PROP_TYPE(TwoPTwoCTypeTag, Indices) TwoPTwoCIndices;

    enum {
        dim = MDGrid::dimension,
        dimWorld = MDGrid::dimensionworld
    };
    enum { isBox = GET_PROP_VALUE(Transport2cTypeTag, ImplicitIsBox) };

    // Transport
    enum { numEq1 = GET_PROP_VALUE(Transport2cTypeTag, NumEq) };
    enum { nPhaseIdx1 = Transport2cIndices::phaseIdx,
           numComponents1 = Transport2cIndices::numComponents };
    // equation indices in the Transport domain
    enum { transportEqIdx1 = Transport2cIndices::transportEqIdx };
    // components indices
    enum { transportCompIdx1 = Transport2cIndices::transportCompIdx,
           phaseCompIdx1 = Transport2cIndices::phaseCompIdx };

    // POROUS MEDIUM
    enum { numEq2 = GET_PROP_VALUE(TwoPTwoCTypeTag, NumEq) };
    enum { numPhases2 = GET_PROP_VALUE(TwoPTwoCTypeTag, NumPhases) };
    // equation indices in the Darcy domain
    enum { contiWEqIdx2 = TwoPTwoCIndices::contiWEqIdx,
           contiTotalMassIdx2 = TwoPTwoCIndices::contiNEqIdx };
    // component indices
    enum { wCompIdx2 = TwoPTwoCIndices::wCompIdx,
           nCompIdx2 = TwoPTwoCIndices::nCompIdx };
    // phase indices
    enum { wPhaseIdx2 = TwoPTwoCIndices::wPhaseIdx,
           nPhaseIdx2 = TwoPTwoCIndices::nPhaseIdx };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename MDGrid::ctype CoordScalar;
    typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;

    typedef typename Transport2cGridView::template Codim<dim>::EntityPointer VertexPointer1;
    typedef typename TwoPTwoCGridView::template Codim<dim>::EntityPointer VertexPointer2;

    static const bool useMoles = GET_PROP_VALUE(Transport2cTypeTag, UseMoles);

    // multidomain flags
    static const bool doAlphaCoupling = true;
    static const bool doPatternCoupling = true;

public:
    //! \brief The constructor
    TwoCTransportTwoPTwoCLocalOperator(GlobalProblem& globalProblem)
        : globalProblem_(globalProblem)
    {
        if (GET_PROP_VALUE(Transport2cTypeTag, UseMoles) != GET_PROP_VALUE(TwoPTwoCTypeTag, UseMoles))
            DUNE_THROW(Dune::NotImplemented, "Please use the same formulation (mass/mole) in both domains.");
    }


    /*!
     * \brief Do the coupling. The unknowns are transferred from dune-multidomain.
     *        Based on them, a coupling residual is calculated and added at the
     *        respective positions in the matrix.
     *
     * \param intersectionGeometry the geometry of the intersection
     * \param lfsu1 local basis for the trial space of the Transport domain
     * \param unknowns1 the unknowns vector of the Transport element (formatted according to PDELab)
     * \param lfsv1 local basis for the test space of the Transport domain
     * \param lfsu2 local basis for the trail space of the Darcy domain
     * \param unknowns2 the unknowns vector of the Darcy element (formatted according to PDELab)
     * \param lfsv2 local basis for the test space of the Darcy domain
     * \param couplingRes1 the coupling residual from the Transport domain
     * \param couplingRes2 the coupling residual from the Darcy domain
     *
     */
#warning move alpha_coupling to general multidomainboxlocaloperator and multidomaincclocaloperator
    template<typename IntersectionGeom, typename LFSU1, typename LFSU2,
             typename X, typename LFSV1, typename LFSV2,typename RES>
    void alpha_coupling(const IntersectionGeom& intersectionGeometry,
                        const LFSU1& lfsu1, const X& unknowns1, const LFSV1& lfsv1,
                        const LFSU2& lfsu2, const X& unknowns2, const LFSV2& lfsv2,
                        RES& couplingRes1, RES& couplingRes2) const
    {
        const std::shared_ptr<MDElement> mdElement1
            = std::make_shared<MDElement>(intersectionGeometry.inside());
        const std::shared_ptr<MDElement> mdElement2
            = std::make_shared<MDElement>(intersectionGeometry.inside());

        // the subdomain elements
        const std::shared_ptr<SDElement1> sdElement1
            = std::make_shared<SDElement1>(globalProblem_.sdElementPointer1(*mdElement1));
        const std::shared_ptr<SDElement2> sdElement2
            = std::make_shared<SDElement2>(globalProblem_.sdElementPointer2(*mdElement2));

        // a container for the parameters on each side of the coupling interface (see below)
        CParams cParams;

        if (!isBox)
            Dune::dgrave << "--- ab hier wirds wichtig ---" << std::endl;

        // update fvElementGeometry and the element volume variables
        updateElemVolVars(lfsu1, lfsu2,
                          unknowns1, unknowns2,
                          *sdElement1, *sdElement2,
                          cParams);

        // first element
        const int faceIdx1 = intersectionGeometry.indexInInside();
        const Dune::ReferenceElement<typename MDGrid::ctype,dim>& referenceElement1 =
            Dune::ReferenceElements<typename MDGrid::ctype,dim>::general((*mdElement1).type());

        // second element
        const int faceIdx2 = intersectionGeometry.indexInOutside();
        const Dune::ReferenceElement<typename MDGrid::ctype,dim>& referenceElement2 =
            Dune::ReferenceElements<typename MDGrid::ctype,dim>::general((*mdElement2).type());

#if !IsCC
        const int numVerticesOfFace = referenceElement1.size(faceIdx1, 1, dim);
        for (int vertexInFace = 0; vertexInFace < numVerticesOfFace; ++vertexInFace)
        {
            const int vertInElem1 = referenceElement1.subEntity(faceIdx1, 1, vertexInFace, dim);
            const int vertInElem2 = referenceElement2.subEntity(faceIdx2, 1, vertexInFace, dim);

            const int boundaryFaceIdx1 = cParams.fvGeometry1.boundaryFaceIndex(faceIdx1, vertexInFace);
            const int boundaryFaceIdx2 = cParams.fvGeometry2.boundaryFaceIndex(faceIdx2, vertexInFace);

            // obtain the boundary types
            const VertexPointer1 vPtr1 = (*sdElement1).template subEntity<dim>(vertInElem1);
            const VertexPointer2 vPtr2 = (*sdElement2).template subEntity<dim>(vertInElem2);

            globalProblem_.sdProblem1().boundaryTypes(cParams.boundaryTypes1, vPtr1);
            globalProblem_.sdProblem2().boundaryTypes(cParams.boundaryTypes2, vPtr2);

            BoundaryVariables1 boundaryVars1;
            boundaryVars1.update(globalProblem_.sdProblem1(),
                                 *sdElement1,
                                 cParams.fvGeometry1,
                                 boundaryFaceIdx1,
                                 cParams.elemVolVarsCur1,
                                 /*onBoundary=*/true);
            BoundaryVariables2 boundaryVars2;
            boundaryVars2.update(globalProblem_.sdProblem2(),
                                 *sdElement2,
                                 cParams.fvGeometry2,
                                 boundaryFaceIdx2,
                                 cParams.elemVolVarsCur2,
                                 /*onBoundary=*/true);

            asImp_()->evalCoupling(lfsu1, lfsu2, // local function spaces
                                   vertInElem1, vertInElem2,
                                   *sdElement1, *sdElement2,
                                   boundaryVars1, boundaryVars2,
                                   cParams,
                                   couplingRes1, couplingRes2);
        }
#else // IsCC
        const int vertInElem1 = referenceElement1.subEntity(faceIdx1, 1, /*vertexInFace*/0, dim);
        const int vertInElem2 = referenceElement2.subEntity(faceIdx2, 1, /*vertexInFace*/0, dim);

        for (const auto& intersection : Dune::intersections(globalProblem_.sdGridView1(), *sdElement1))
        {
            // handle only faces on the boundary
            if (!intersection.boundary())
                continue;

            globalProblem_.sdProblem1().boundaryTypes(cParams.boundaryTypes1, intersection);

//             const Dune::FieldVector<Scalar, dim-1>& faceCenterLocal =
//               Dune::ReferenceElements<Scalar, dim-1>::general(intersectionGeometry.geometry().type()).position(0, 0);
//             Dune::FieldVector<Scalar, dim> faceCenterGlobal =
//               intersectionGeometry.geometry().global(faceCenterLocal);
//             Dune::dwarn << " faceCenterGlobal: " << faceCenterGlobal << std::endl;
//             Dune::dwarn << " globalPos: " << cParams.fvGeometry1.subContVol[vertInElem1].global << std::endl;
        }
        for (const auto& intersection : Dune::intersections(globalProblem_.sdGridView2(), *sdElement2))
        {
            // handle only faces on the boundary
            if (!intersection.boundary())
                continue;

            globalProblem_.sdProblem2().boundaryTypes(cParams.boundaryTypes2, intersection);
        }

        if (!isBox)
        {
            Dune::dgrave << "boundaryVars2" << std::endl;
            Dune::dgrave << "faceIdx2 " << faceIdx2 << std::endl;
        }
        BoundaryVariables2 boundaryVars2;
        boundaryVars2.update(globalProblem_.sdProblem2(),
                             *sdElement2,
                             cParams.fvGeometry2,
                             faceIdx2,
                             cParams.elemVolVarsCur2,
                             /*onBoundary=*/true);

        if (!isBox)
        {
            Dune::dgrave << "boundaryVars1" << std::endl;
            Dune::dgrave << "faceIdx1" << std::endl;
        }
        BoundaryVariables1 boundaryVars1;
        boundaryVars1.update(globalProblem_.sdProblem1(),
                             *sdElement1,
                             cParams.fvGeometry1,
                             faceIdx1,
                             cParams.elemVolVarsCur1,
                             /*onBoundary=*/true);

        asImp_()->evalCoupling(lfsu1, lfsu2, // local function spaces
                               vertInElem1, vertInElem2,
                               *sdElement1, *sdElement2,
                               boundaryVars1, boundaryVars2,
                               cParams,
                               couplingRes1, couplingRes2);
#endif
    }

    /*!
     * \brief Update the volume variables of the element and extract the unknowns from dune-pdelab vectors
     *        and bring them into a form which fits to dumux.
     *
     * \param lfsu1 local basis for the trial space of the Transport domain
     * \param lfsu2 local basis for the trial space of the Darcy domain
     * \param unknowns1 the unknowns vector of the Transport element (formatted according to PDELab)
     * \param unknowns2 the unknowns vector of the Darcy element (formatted according to PDELab)
     * \param sdElement1 the element in the Transport domain
     * \param sdElement2 the element in the Darcy domain
     * \param cParams a parameter container
     *
     */
    template<typename LFSU1, typename LFSU2, typename X, typename CParams>
    void updateElemVolVars(const LFSU1& lfsu1, const LFSU2& lfsu2,
                           const X& unknowns1, const X& unknowns2,
                           const SDElement1& sdElement1, const SDElement2& sdElement2,
                           CParams &cParams) const
    {
        // TODO: move updateElemVolVars to general multidomainboxlocaloperator and multidomaincclocaloperator
#if IsCC
//         Dune::dwarn << "cParams.fvGeometry1.update" << std::endl;
//         cParams.fvGeometry1.updatePDELab(globalProblem_.mdGridView(), sdElement1);
//         Dune::dwarn << "cParams.fvGeometry2.update" << std::endl;
//         cParams.fvGeometry1.updatePDELab(globalProblem_.mdGridView(), sdElement2);
        cParams.fvGeometry1.update(globalProblem_.sdGridView1(), sdElement1);
        cParams.fvGeometry2.update(globalProblem_.sdGridView2(), sdElement2);
#else
        cParams.fvGeometry1.update(globalProblem_.sdGridView1(), sdElement1);
        cParams.fvGeometry2.update(globalProblem_.sdGridView2(), sdElement2);
#endif

        //bring the local unknowns x1 into a form that can be passed to elemVolVarsCur.updatePDELab()
        Dune::BlockVector<Dune::FieldVector<Scalar,1>> elementSol1(0.);
        Dune::BlockVector<Dune::FieldVector<Scalar,1>> elementSol2(0.);
        elementSol1.resize(unknowns1.size());
        elementSol2.resize(unknowns2.size());

//         const int numVertsOfElem1 = cParams.fvGeometry1.numNeighbors;
//         const int numVertsOfElem2 = cParams.fvGeometry2.numNeighbors;
//         const int numVertsOfElem1 = 1;//cParams.fvGeometry1.boundaryFace[0].numFap;
//         const int numVertsOfElem2 = 1;//cParams.fvGeometry2.boundaryFace[0].numFap;
#if !IsCC
        const int numVertsOfElem1 = sdElement1.subEntities(dim);
        const int numVertsOfElem2 = sdElement2.subEntities(dim);
#endif

#if IsCC
        for (int eqIdx1=0; eqIdx1<numEq1; ++eqIdx1)
            elementSol1[eqIdx1] = unknowns1(lfsu1.child(eqIdx1),0);
        for (int eqIdx2=0; eqIdx2<numEq2; ++eqIdx2)
            elementSol2[eqIdx2] = unknowns2(lfsu2.child(eqIdx2),0);
#else
        for (int scvIdx = 0; scvIdx < numVertsOfElem1; ++scvIdx)
        {
            for (int eqIdx1=0; eqIdx1<numEq1; ++eqIdx1)
                elementSol1[eqIdx1*numVertsOfElem1+scvIdx] = unknowns1(lfsu1.child(eqIdx1),scvIdx);
            for (int eqIdx2=0; eqIdx2<numEq2; ++eqIdx2)
                elementSol2[eqIdx2*numVertsOfElem2+scvIdx] = unknowns2(lfsu2.child(eqIdx2),scvIdx);
        }
#endif


#if HAVE_VALGRIND
        for (unsigned int i = 0; i < elementSol1.size(); i++)
        {
            Valgrind::CheckDefined(elementSol1[i]);
            if (!isBox)
                Dune::dgrave << "elementSol1[" << i << "]: " << elementSol1[i] << std::endl;
        }
        for (unsigned int i = 0; i < elementSol2.size(); i++)
        {
            Valgrind::CheckDefined(elementSol2[i]);
            if (!isBox)
                Dune::dgrave << "elementSol2[" << i << "]: " << elementSol2[i] << std::endl;
        }
#endif // HAVE_VALGRIND

#if IsCC
        cParams.elemVolVarsPrev1.update(globalProblem_.sdProblem1(), sdElement1,
                                        cParams.fvGeometry1, true);
        cParams.elemVolVarsPrev2.update(globalProblem_.sdProblem2(), sdElement2,
                                        cParams.fvGeometry2, true);
        cParams.elemVolVarsCur1.updatePDELab(globalProblem_.sdProblem1(), sdElement1,
                                             cParams.fvGeometry1, elementSol1);
        cParams.elemVolVarsCur2.updatePDELab(globalProblem_.sdProblem2(), sdElement2,
                                             cParams.fvGeometry2, elementSol2);
#else
        cParams.elemVolVarsPrev1.update(globalProblem_.sdProblem1(), sdElement1,
                                        cParams.fvGeometry1, true);
        cParams.elemVolVarsPrev2.update(globalProblem_.sdProblem2(), sdElement2,
                                        cParams.fvGeometry2, true);
        cParams.elemVolVarsCur1.updatePDELab(globalProblem_.sdProblem1(), sdElement1,
                                             cParams.fvGeometry1, elementSol1);
        cParams.elemVolVarsCur2.updatePDELab(globalProblem_.sdProblem2(), sdElement2,
                                             cParams.fvGeometry2, elementSol2);
#endif

        // create and update element boundary conditions
        ElementBoundaryTypes1 bcTypes1;
        ElementBoundaryTypes2 bcTypes2;
        if (!isBox)
            Dune::dgrave << "bcTypes1.update" << std::endl;
        bcTypes1.update(globalProblem_.sdProblem1(), sdElement1, cParams.fvGeometry1);
        if (!isBox)
            Dune::dgrave << "bcTypes2.update" << std::endl;
        bcTypes2.update(globalProblem_.sdProblem2(), sdElement2, cParams.fvGeometry2);

        // evaluate the local residual with the PDELab solution
        if (!isBox)
            Dune::dgrave << "globalProblem_.localResidual1().evalPDELab" << std::endl;
        globalProblem_.localResidual1().evalPDELab(sdElement1, cParams.fvGeometry1,
                                                   cParams.elemVolVarsPrev1, cParams.elemVolVarsCur1,
                                                   bcTypes1);
        if (!isBox)
            Dune::dgrave << "globalProblem_.localResidual2().evalPDELab" << std::endl;
        globalProblem_.localResidual2().evalPDELab(sdElement2, cParams.fvGeometry2,
                                                   cParams.elemVolVarsPrev2, cParams.elemVolVarsCur2,
                                                   bcTypes2);
    }

    /*!
     * \brief Evaluation of the coupling between the Transport (1) and Darcy (2).
     *
     * Dirichlet-like and Neumann-like conditions for the respective domain are evaluated.
     *
     * \param lfsu1 local basis for the trial space of the Transport domain
     * \param lfsu2 local basis for the trial space of the Darcy domain
     * \param vertInElem1 local vertex index in element1
     * \param vertInElem2 local vertex index in element2
     * \param sdElement1 the element in the Stokes domain
     * \param sdElement2 the element in the Darcy domain
     * \param boundaryVars1 the boundary variables at the interface of the Stokes domain
     * \param boundaryVars2 the boundary variables at the interface of the Darcy domain
     * \param cParams a parameter container
     * \param couplingRes1 the coupling residual from the Stokes domain
     * \param couplingRes2 the coupling residual from the Darcy domain
     */
    template<typename LFSU1, typename LFSU2, typename RES1, typename RES2, typename CParams>
    void evalCoupling(const LFSU1& lfsu1, const LFSU2& lfsu2,
                      const int vertInElem1, const int vertInElem2,
                      const SDElement1& sdElement1, const SDElement2& sdElement2,
                      const BoundaryVariables1& boundaryVars1, const BoundaryVariables2& boundaryVars2,
                      const CParams &cParams,
                      RES1& couplingRes1, RES2& couplingRes2) const
    {
        const GlobalPosition& globalPos1 = cParams.fvGeometry1.subContVol[vertInElem1].global;
//         const GlobalPosition& globalPos2 = cParams.fvGeometry2.subContVol[vertInElem2].global;

        const GlobalPosition& bfNormal1 = boundaryVars1.face().normal;
        // TODO check if this makes sense, because velocity from transport normally is 0
        Scalar normalMassOrMoleFlux1 = boundaryVars1.normalVelocity();

        if (useMoles)
            normalMassOrMoleFlux1 *= cParams.elemVolVarsCur1[vertInElem1].density();
        else
            normalMassOrMoleFlux1 *= cParams.elemVolVarsCur1[vertInElem1].molarDensity();

        // MASS Balance
        // Neumann-like conditions
        if (cParams.boundaryTypes2.isCouplingNeumann(contiTotalMassIdx2))
        {
#if !IsCC
            if (globalProblem_.sdProblem1().isCornerPoint(globalPos1))
            {
                couplingRes2.accumulate(lfsu2.child(contiTotalMassIdx2), vertInElem2,
                                        -normalMassOrMoleFlux1);
            }
            else
            {
                couplingRes2.accumulate(lfsu2.child(contiTotalMassIdx2), vertInElem2,
                                        // TODO: das residual hier ist falsch, da es im ff keine Phasen-Massenbilanz gibt
                                        //       also sollte immer rho*v genommen werden
                                        globalProblem_.localResidual1().residual(vertInElem1)[transportEqIdx1]);
            }
#endif
        }

        // MASS Balance
        // Dirichlet-like
        if (cParams.boundaryTypes2.isCouplingDirichlet(contiTotalMassIdx2))
        {
            couplingRes2.accumulate(lfsu2.child(contiTotalMassIdx2), vertInElem2,
                                    -cParams.elemVolVarsCur1[vertInElem1].pressure());
        }

//                 Dune::dwarn << "at cornerpoint" << std::endl;
#if IsCC
//         Dune::dwarn << " cParams.elemVolVarsCur1[vertInElem1].moleFraction() " << cParams.elemVolVarsCur1[vertInElem1].moleFraction(transportCompIdx1)
//                   << " cParams.elemVolVarsCur2[vertInElem2].moleFraction() " << cParams.elemVolVarsCur2[vertInElem2].moleFraction(nPhaseIdx2, wCompIdx2)
//                   << " cParams.fvGeometry1.subContVol[vertInElem1].global " << cParams.fvGeometry1.subContVol[vertInElem1].global
//                   << " cParams.fvGeometry2.subContVol[vertInElem2].global " << cParams.fvGeometry2.subContVol[vertInElem2].global
//                   << std::endl;
        // TODO genereralize direction
        Scalar moleFracGrad = (cParams.elemVolVarsCur1[vertInElem1].moleFraction(transportCompIdx1)
                              - cParams.elemVolVarsCur2[vertInElem2].moleFraction(nPhaseIdx2, wCompIdx2))
                                / (cParams.fvGeometry1.subContVol[vertInElem1].global[1]
                                  - cParams.fvGeometry2.subContVol[vertInElem2].global[1]);
        Scalar diffusiveFlux1 = bfNormal1[1]
                                * moleFracGrad
                                * (cParams.elemVolVarsCur1[vertInElem1].diffusionCoeff(transportCompIdx1)
                                    + cParams.elemVolVarsCur1[vertInElem1].eddyDiffusivity())
                                * cParams.elemVolVarsCur1[vertInElem1].molarDensity();

        Dune::dgrave << "diffusiveFlux1 " << diffusiveFlux1 << std::endl;
//         checkParameter(globalPos1, cParams.elemVolVarsCur1[vertInElem1].moleFraction(transportCompIdx1), "elemVolVarsCur1.moleFraction(transportCompIdx1)", 1);
//         checkParameter(globalPos2, cParams.elemVolVarsCur2[vertInElem2].moleFraction(nPhaseIdx2, wCompIdx2), "elemVolVarsCur2.moleFraction(nPhaseIdx2, wCompIdx2)", 1);
//         checkParameter(globalPos1, bfNormal1[0], "bfNormal1[0]");
//         checkParameter(globalPos1, bfNormal1[1], "bfNormal1[1]");
//         checkParameter(globalPos1, moleFracGrad, "moleFracGrad");
//         checkParameter(globalPos1, moleFracGrad[0], "moleFracGrad[0]", 2);
//         checkParameter(globalPos1, moleFracGrad[1], "moleFracGrad[1]", 2);
//         checkParameter(globalPos1, boundaryVars1.diffusionCoeff(transportCompIdx1), "boundaryVars1.diffusionCoeff(transportCompIdx1)");
//         checkParameter(globalPos1, boundaryVars1.eddyDiffusivity(), "boundaryVars1.eddyDiffusivity()");
//         checkParameter(globalPos1, boundaryVars1.molarDensity(), "boundaryVars1.molarDensity");
//         checkParameter(globalPos2, boundaryVars2.molarDensity(nPhaseIdx2), "boundaryVars2.molarDensity(nPhaseIdx2)");
//         checkParameter(globalPos1, cParams.elemVolVarsCur1[vertInElem1].eddyDiffusivity(), "elemVolVarsCur1.eddyDiffusivity()");
//         checkParameter(globalPos1, cParams.elemVolVarsCur1[vertInElem1].molarDensity(), "elemVolVarsCur1.molarDensity()");
//         checkParameter(globalPos1, cParams.elemVolVarsCur2[vertInElem2].molarDensity(nPhaseIdx2), "elemVolVarsCur2.molarDensity(nPhaseIdx2)");
//         checkParameter(globalPos1, cParams.elemVolVarsCur1[vertInElem1].fluidState().density(nPhaseIdx1), "elemVolVarsCur1.density");
//         checkParameter(globalPos1, cParams.elemVolVarsCur1[vertInElem1].fluidState().averageMolarMass(nPhaseIdx1), "elemVolVarsCur1.averageMolarMass");
#else
        Scalar diffusiveFlux1 = bfNormal1
                                * boundaryVars1.moleFractionGrad(transportCompIdx1)
                                * (boundaryVars1.diffusionCoeff(transportCompIdx1)
                                    + boundaryVars1.eddyDiffusivity())
                                * boundaryVars1.molarDensity();
#endif
        Scalar advectiveFlux1 = normalMassOrMoleFlux1;

        if (useMoles)
        {
            advectiveFlux1 *= cParams.elemVolVarsCur1[vertInElem1].moleFraction(transportCompIdx1);
            // diffusive fluxes don't have to be converted for mole fraction formulation
        }
        else
        {
            advectiveFlux1 *= cParams.elemVolVarsCur1[vertInElem1].massFraction(transportCompIdx1);
            diffusiveFlux1 *= FluidSystem::molarMass(transportCompIdx1);
        }

        // COMPONENT Balance
        // Neumann-like conditions
        if (cParams.boundaryTypes1.isCouplingNeumann(transportEqIdx1))
        {
#if IsCC
            couplingRes1.accumulate(lfsu1.child(transportEqIdx1), vertInElem1, (advectiveFlux1 - diffusiveFlux1));
#else
            DUNE_THROW(Dune::NotImplemented, "The coupling condition isCouplingNeumann(transportEqIdx1) is not implemented.");
#endif
        }
        if (cParams.boundaryTypes2.isCouplingNeumann(contiWEqIdx2))
        {
//             Dune::dwarn << "Neumann condition for contiWEqIdx2" << std::endl;
#if IsCC
            couplingRes2.accumulate(lfsu2.child(contiWEqIdx2), vertInElem2, -(advectiveFlux1 - diffusiveFlux1));
#else
            // compute fluxes explicitly at corner points - only quarter control volume
            if (globalProblem_.sdProblem1().isCornerPoint(globalPos1))
            {
                couplingRes2.accumulate(lfsu2.child(contiWEqIdx2), vertInElem2,
                                        -(advectiveFlux1 - diffusiveFlux1));
            }
            else // coupling via the defect
            {
                static_assert(GET_PROP_VALUE(Transport2cTypeTag, UseMoles) == GET_PROP_VALUE(TwoPTwoCTypeTag, UseMoles),
                              "This coupling condition is not implemented for different formulations (mass/mole) in the subdomains.");
                // the component mass flux from the transport domain
                couplingRes2.accumulate(lfsu2.child(contiWEqIdx2), vertInElem2,
                                        globalProblem_.localResidual1().residual(vertInElem1)[transportEqIdx1]);
            }
#endif
        }

        // COMPONENT Balance
        // Dirichlet-like conditions (coupling residual is added to "real" residual)
        // TODO implement a DUNE_THROW for use of Dirichlet coupling conditions in a CC framework
        if (cParams.boundaryTypes1.isCouplingDirichlet(transportEqIdx1))
        {
            static_assert(numComponents1 == 2,
                          "This part of the coupling is only implemented for 2 components in the Transport domain.");

            // set residualTransport[transportEqIdx1] = x in transport2clocalresidual.hh
            if (useMoles)
                couplingRes1.accumulate(lfsu1.child(transportEqIdx1), vertInElem1,
                                        -cParams.elemVolVarsCur2[vertInElem2].moleFraction(nPhaseIdx2, wCompIdx2));
            else
                couplingRes1.accumulate(lfsu1.child(transportEqIdx1), vertInElem1,
                                        -cParams.elemVolVarsCur2[vertInElem2].massFraction(nPhaseIdx2, wCompIdx2));
        }
        if (cParams.boundaryTypes2.isCouplingDirichlet(contiWEqIdx2))
        {
            DUNE_THROW(Dune::NotImplemented, "This part of the coupling is only implemented for a 2-phase system in the Darcy domain.");
        }
    }

 protected:
    GlobalProblem& globalProblem() const
    { return globalProblem_; }

    Implementation *asImp_()
    { return static_cast<Implementation *> (this); }
    const Implementation *asImp_() const
    { return static_cast<const Implementation *> (this); }

 private:
    /*!
     * \brief A struct that contains data of the FF and PM including boundary types,
     *        volume variables in both subdomains and geometric information
     */
    struct CParams
    {
        BoundaryTypes1 boundaryTypes1;
        BoundaryTypes2 boundaryTypes2;
        ElementVolumeVariables1 elemVolVarsPrev1;
        ElementVolumeVariables1 elemVolVarsCur1;
        ElementVolumeVariables2 elemVolVarsPrev2;
        ElementVolumeVariables2 elemVolVarsCur2;
        FVElementGeometry1 fvGeometry1;
        FVElementGeometry2 fvGeometry2;
    };

    GlobalProblem& globalProblem_;

    void checkParameter(GlobalPosition globalPos, Scalar value, std::string name, int checkRange = 0) const
    {
        if (std::isnan(value))
        {
            std::cout << name << " is nan at " << globalPos << std::endl;
        }
        if (std::isinf(value))
        {
            std::cout << name << " is inf at " << globalPos << std::endl;
        }
        if (checkRange == 1)
        {
            if (value < 0 || value > 1)
            {
                std::cout << name << " is out of range: " << value << " at " << globalPos << std::endl;
            }
        }
        else if (checkRange == 2)
        {
            Dune::dgrave << " globalPos " << globalPos
                        << " name " << name
                        << " value " << value
                        << std::endl;
        }
    }
};

} // end namespace Dumux

#endif // DUMUX_2CTRANSPORT_2P2C_LOCALOPERATOR_HH
