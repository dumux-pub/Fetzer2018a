// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief The local operator for the coupling of a two-component Transport model
 *        and a two-phase two-component porous-medium model under isothermal conditions.
 */
#ifndef DUMUX_2CTRANSPORT_2P2C_LOCALOPERATOR_HH
#define DUMUX_2CTRANSPORT_2P2C_LOCALOPERATOR_HH

#include <iostream>

#include <dune/common/version.hh>

#include <dune/pdelab/multidomain/couplingutilities.hh>
#include <dune/pdelab/localoperator/pattern.hh>
#include <dune/pdelab/localoperator/idefault.hh>

#include <dumux/multidomain/properties.hh>
#include <dumux/porousmediumflow/2p2c/implicit/model.hh>

#include <dumux/freeflow/transportnc/model.hh>
#include <dumux/multidomain/2ctransport2p2c/propertydefaults.hh>

namespace Dumux {

/*!
 * \ingroup TwoPTwoCTransportTwoCModel
 * \brief The local operator for the coupling of a two-component Transport model
 *        and a two-phase two-component porous-medium model under isothermal conditions.
 *
 * This model implements the coupling between a Transport model
 * and a porous-medium flow model under isothermal conditions.
 * Here the coupling conditions for the individual balance are presented:
 *
 * \todo add coupling conditions
 */
template<class TypeTag>
class TwoCTransportTwoPTwoCLocalOperator :
        public Dune::PDELab::MultiDomain::CouplingOperatorDefaultFlags,
        public Dune::PDELab::MultiDomain::NumericalJacobianCoupling<TwoCTransportTwoPTwoCLocalOperator<TypeTag>>,
        public Dune::PDELab::MultiDomain::FullCouplingPattern,
        public Dune::PDELab::InstationaryLocalOperatorDefaultMethods<double>
{
 public:
    typedef typename GET_PROP_TYPE(TypeTag, Problem) GlobalProblem;
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainCouplingLocalOperator) Implementation;

    typedef typename GET_PROP_TYPE(TypeTag, SubDomain1TypeTag) Transport2cTypeTag;
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain2TypeTag) TwoPTwoCTypeTag;

    typedef typename GET_PROP_TYPE(TwoPTwoCTypeTag, FluidSystem) FluidSystem;

    typedef typename GET_PROP_TYPE(Transport2cTypeTag, ElementVolumeVariables) ElementVolumeVariables1;
    typedef typename GET_PROP_TYPE(TwoPTwoCTypeTag, ElementVolumeVariables) ElementVolumeVariables2;

    typedef typename GET_PROP_TYPE(Transport2cTypeTag, FluxVariables) BoundaryVariables1;
    typedef typename GET_PROP_TYPE(TwoPTwoCTypeTag, FluxVariables) BoundaryVariables2;

    typedef typename GET_PROP_TYPE(Transport2cTypeTag, ElementBoundaryTypes) ElementBoundaryTypes1;
    typedef typename GET_PROP_TYPE(TwoPTwoCTypeTag, ElementBoundaryTypes) ElementBoundaryTypes2;

    typedef typename GET_PROP_TYPE(Transport2cTypeTag, BoundaryTypes) BoundaryTypes1;
    typedef typename GET_PROP_TYPE(TwoPTwoCTypeTag, BoundaryTypes) BoundaryTypes2;

    typedef typename GET_PROP_TYPE(Transport2cTypeTag, FVElementGeometry) FVElementGeometry1;
    typedef typename GET_PROP_TYPE(TwoPTwoCTypeTag, FVElementGeometry) FVElementGeometry2;

    // Multidomain Grid types
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MDGrid;
    typedef typename MDGrid::Traits::template Codim<0>::Entity MDElement;

    typedef typename GET_PROP_TYPE(Transport2cTypeTag, GridView) Transport2cGridView;
    typedef typename GET_PROP_TYPE(TwoPTwoCTypeTag, GridView) TwoPTwoCGridView;
    typedef typename Transport2cGridView::template Codim<0>::Entity SDElement1;
    typedef typename TwoPTwoCGridView::template Codim<0>::Entity SDElement2;

    typedef typename GET_PROP_TYPE(Transport2cTypeTag, Indices) Transport2cIndices;
    typedef typename GET_PROP_TYPE(TwoPTwoCTypeTag, Indices) TwoPTwoCIndices;

    enum {
        dim = MDGrid::dimension,
        dimWorld = MDGrid::dimensionworld
    };

    // Transport
    enum { numEq1 = GET_PROP_VALUE(Transport2cTypeTag, NumEq) };
    enum { nPhaseIdx1 = Transport2cIndices::phaseIdx,
           numComponents1 = Transport2cIndices::numComponents };
    // equation indices in the Transport domain
    enum { transportEqIdx1 = Transport2cIndices::transportEqIdx };
    // components indices
    enum { transportCompIdx1 = Transport2cIndices::transportCompIdx,
           phaseCompIdx1 = Transport2cIndices::phaseCompIdx };

    // POROUS MEDIUM
    enum { numEq2 = GET_PROP_VALUE(TwoPTwoCTypeTag, NumEq) };
    enum { numPhases2 = GET_PROP_VALUE(TwoPTwoCTypeTag, NumPhases) };
    // equation indices in the Darcy domain
    enum { contiWEqIdx2 = TwoPTwoCIndices::contiWEqIdx,
           contiTotalMassIdx2 = TwoPTwoCIndices::contiNEqIdx };
    // component indices
    enum { wCompIdx2 = TwoPTwoCIndices::wCompIdx,
           nCompIdx2 = TwoPTwoCIndices::nCompIdx };
    // phase indices
    enum { wPhaseIdx2 = TwoPTwoCIndices::wPhaseIdx,
           nPhaseIdx2 = TwoPTwoCIndices::nPhaseIdx };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename MDGrid::ctype CoordScalar;
    typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;

    typedef typename Transport2cGridView::template Codim<dim>::EntityPointer VertexPointer1;
    typedef typename TwoPTwoCGridView::template Codim<dim>::EntityPointer VertexPointer2;

    static const bool useMoles = GET_PROP_VALUE(Transport2cTypeTag, UseMoles);

    // multidomain flags
    static const bool doAlphaCoupling = true;
    static const bool doPatternCoupling = true;

public:
    //! \brief The constructor
    TwoCTransportTwoPTwoCLocalOperator(GlobalProblem& globalProblem)
        : globalProblem_(globalProblem)
    {
        if (GET_PROP_VALUE(Transport2cTypeTag, UseMoles) != GET_PROP_VALUE(TwoPTwoCTypeTag, UseMoles))
            DUNE_THROW(Dune::NotImplemented, "Please use the same formulation (mass/mole) in both domains.");
    }


    /*!
     * \brief Do the coupling. The unknowns are transferred from dune-multidomain.
     *        Based on them, a coupling residual is calculated and added at the
     *        respective positions in the matrix.
     *
     * \param intersectionGeometry the geometry of the intersection
     * \param lfsu1 local basis for the trial space of the Transport domain
     * \param unknowns1 the unknowns vector of the Transport element (formatted according to PDELab)
     * \param lfsv1 local basis for the test space of the Transport domain
     * \param lfsu2 local basis for the trail space of the Darcy domain
     * \param unknowns2 the unknowns vector of the Darcy element (formatted according to PDELab)
     * \param lfsv2 local basis for the test space of the Darcy domain
     * \param couplingRes1 the coupling residual from the Transport domain
     * \param couplingRes2 the coupling residual from the Darcy domain
     *
     */
    template<typename IntersectionGeom, typename LFSU1, typename LFSU2,
             typename X, typename LFSV1, typename LFSV2,typename RES>
    void alpha_coupling(const IntersectionGeom& intersectionGeometry,
                        const LFSU1& lfsu1, const X& unknowns1, const LFSV1& lfsv1,
                        const LFSU2& lfsu2, const X& unknowns2, const LFSV2& lfsv2,
                        RES& couplingRes1, RES& couplingRes2) const
    {
        const std::shared_ptr<MDElement> mdElement1
            = std::make_shared<MDElement>(intersectionGeometry.inside());
        const std::shared_ptr<MDElement> mdElement2
            = std::make_shared<MDElement>(intersectionGeometry.outside());

        // the subdomain elements
        const std::shared_ptr<SDElement1> sdElement1
            = std::make_shared<SDElement1>(globalProblem_.sdElementPointer1(*mdElement1));
        const std::shared_ptr<SDElement2> sdElement2
            = std::make_shared<SDElement2>(globalProblem_.sdElementPointer2(*mdElement2));

        // a container for the parameters on each side of the coupling interface (see below)
        CParams cParams;

        // update fvElementGeometry and the element volume variables
        updateElemVolVars(lfsu1, lfsu2,
                          unknowns1, unknowns2,
                          *sdElement1, *sdElement2,
                          cParams);

        // first element
        const int faceIdx1 = intersectionGeometry.indexInInside();
        const Dune::ReferenceElement<typename MDGrid::ctype,dim>& referenceElement1 =
            Dune::ReferenceElements<typename MDGrid::ctype,dim>::general((*mdElement1).type());
        const int numVerticesOfFace = referenceElement1.size(faceIdx1, 1, dim);

        // second element
        const int faceIdx2 = intersectionGeometry.indexInOutside();
        const Dune::ReferenceElement<typename MDGrid::ctype,dim>& referenceElement2 =
            Dune::ReferenceElements<typename MDGrid::ctype,dim>::general((*mdElement2).type());

        for (int vertexInFace = 0; vertexInFace < numVerticesOfFace; ++vertexInFace)
        {
            const int vertInElem1 = referenceElement1.subEntity(faceIdx1, 1, vertexInFace, dim);
            const int vertInElem2 = referenceElement2.subEntity(faceIdx2, 1, vertexInFace, dim);

            const int boundaryFaceIdx1 = cParams.fvGeometry1.boundaryFaceIndex(faceIdx1, vertexInFace);
            const int boundaryFaceIdx2 = cParams.fvGeometry2.boundaryFaceIndex(faceIdx2, vertexInFace);

            // obtain the boundary types
            const VertexPointer1 vPtr1 = (*sdElement1).template subEntity<dim>(vertInElem1);
            const VertexPointer2 vPtr2 = (*sdElement2).template subEntity<dim>(vertInElem2);

            globalProblem_.sdProblem1().boundaryTypes(cParams.boundaryTypes1, vPtr1);
            globalProblem_.sdProblem2().boundaryTypes(cParams.boundaryTypes2, vPtr2);

            BoundaryVariables1 boundaryVars1;
            boundaryVars1.update(globalProblem_.sdProblem1(),
                                 *sdElement1,
                                 cParams.fvGeometry1,
                                 boundaryFaceIdx1,
                                 cParams.elemVolVarsCur1,
                                 /*onBoundary=*/true);
            BoundaryVariables2 boundaryVars2;
            boundaryVars2.update(globalProblem_.sdProblem2(),
                                 *sdElement2,
                                 cParams.fvGeometry2,
                                 boundaryFaceIdx2,
                                 cParams.elemVolVarsCur2,
                                 /*onBoundary=*/true);

            asImp_()->evalCoupling(lfsu1, lfsu2,
                                   vertInElem1, vertInElem2,
                                   *sdElement1, *sdElement2,
                                   boundaryVars1, boundaryVars2,
                                   cParams,
                                   couplingRes1, couplingRes2);
        }
    }

    /*!
     * \brief Update the volume variables of the element and extract the unknowns from dune-pdelab vectors
     *        and bring them into a form which fits to dumux.
     *
     * \param lfsu1 local basis for the trial space of the Transport domain
     * \param lfsu2 local basis for the trial space of the Darcy domain
     * \param unknowns1 the unknowns vector of the Transport element (formatted according to PDELab)
     * \param unknowns2 the unknowns vector of the Darcy element (formatted according to PDELab)
     * \param sdElement1 the element in the Transport domain
     * \param sdElement2 the element in the Darcy domain
     * \param cParams a parameter container
     *
     */
    template<typename LFSU1, typename LFSU2, typename X, typename CParams>
    void updateElemVolVars(const LFSU1& lfsu1, const LFSU2& lfsu2,
                           const X& unknowns1, const X& unknowns2,
                           const SDElement1& sdElement1, const SDElement2& sdElement2,
                           CParams &cParams) const
    {
        cParams.fvGeometry1.update(globalProblem_.sdGridView1(), sdElement1);
        cParams.fvGeometry2.update(globalProblem_.sdGridView2(), sdElement2);

        const int numVertsOfElem1 = sdElement1.subEntities(dim);
        const int numVertsOfElem2 = sdElement2.subEntities(dim);

        // bring the local unknowns x1 into a form that can be passed to elemVolVarsCur.update()
        Dune::BlockVector<Dune::FieldVector<Scalar,1>> elementSol1(0.);
        Dune::BlockVector<Dune::FieldVector<Scalar,1>> elementSol2(0.);
        elementSol1.resize(unknowns1.size());
        elementSol2.resize(unknowns2.size());

        for (int idx=0; idx<numVertsOfElem1; ++idx)
        {
            for (int eqIdx1=0; eqIdx1<numEq1; ++eqIdx1)
                elementSol1[eqIdx1*numVertsOfElem1+idx] = unknowns1(lfsu1.child(eqIdx1),idx);
            for (int eqIdx2=0; eqIdx2<numEq2; ++eqIdx2)
                elementSol2[eqIdx2*numVertsOfElem2+idx] = unknowns2(lfsu2.child(eqIdx2),idx);
        }
#if HAVE_VALGRIND
        for (unsigned int i = 0; i < elementSol1.size(); i++)
            Valgrind::CheckDefined(elementSol1[i]);
        for (unsigned int i = 0; i < elementSol2.size(); i++)
            Valgrind::CheckDefined(elementSol2[i]);
#endif // HAVE_VALGRIND

        cParams.elemVolVarsPrev1.update(globalProblem_.sdProblem1(),
                                        sdElement1,
                                        cParams.fvGeometry1,
                                        true /* oldSol? */);
        cParams.elemVolVarsCur1.updatePDELab(globalProblem_.sdProblem1(),
                                             sdElement1,
                                             cParams.fvGeometry1,
                                             elementSol1);
        cParams.elemVolVarsPrev2.update(globalProblem_.sdProblem2(),
                                        sdElement2,
                                        cParams.fvGeometry2,
                                        true /* oldSol? */);
        cParams.elemVolVarsCur2.updatePDELab(globalProblem_.sdProblem2(),
                                             sdElement2,
                                             cParams.fvGeometry2,
                                             elementSol2);

        ElementBoundaryTypes1 bcTypes1;
        ElementBoundaryTypes2 bcTypes2;
        bcTypes1.update(globalProblem_.sdProblem1(), sdElement1, cParams.fvGeometry1);
        bcTypes2.update(globalProblem_.sdProblem2(), sdElement2, cParams.fvGeometry2);

        globalProblem_.localResidual1().evalPDELab(sdElement1, cParams.fvGeometry1,
                                                   cParams.elemVolVarsPrev1, cParams.elemVolVarsCur1,
                                                   bcTypes1);
        globalProblem_.localResidual2().evalPDELab(sdElement2, cParams.fvGeometry2,
                                                   cParams.elemVolVarsPrev2, cParams.elemVolVarsCur2,
                                                   bcTypes2);
    }

    /*!
     * \brief Evaluation of the coupling between the Transport (1) and Darcy (2).
     *
     * Dirichlet-like and Neumann-like conditions for the respective domain are evaluated.
     *
     * \param lfsu1 local basis for the trial space of the Transport domain
     * \param lfsu2 local basis for the trial space of the Darcy domain
     * \param vertInElem1 local vertex index in element1
     * \param vertInElem2 local vertex index in element2
     * \param sdElement1 the element in the Stokes domain
     * \param sdElement2 the element in the Darcy domain
     * \param boundaryVars1 the boundary variables at the interface of the Stokes domain
     * \param boundaryVars2 the boundary variables at the interface of the Darcy domain
     * \param cParams a parameter container
     * \param couplingRes1 the coupling residual from the Stokes domain
     * \param couplingRes2 the coupling residual from the Darcy domain
     */
    template<typename LFSU1, typename LFSU2, typename RES1, typename RES2, typename CParams>
    void evalCoupling(const LFSU1& lfsu1, const LFSU2& lfsu2,
                      const int vertInElem1, const int vertInElem2,
                      const SDElement1& sdElement1, const SDElement2& sdElement2,
                      const BoundaryVariables1& boundaryVars1, const BoundaryVariables2& boundaryVars2,
                      const CParams &cParams,
                      RES1& couplingRes1, RES2& couplingRes2) const
    {
        const GlobalPosition& globalPos1 = cParams.fvGeometry1.subContVol[vertInElem1].global;

        const GlobalPosition& bfNormal1 = boundaryVars1.face().normal;
        const Scalar density1 = useMoles ? cParams.elemVolVarsCur1[vertInElem1].molarDensity()
                                         : cParams.elemVolVarsCur1[vertInElem1].density();
        const Scalar massMoleFrac1 = useMoles ? cParams.elemVolVarsCur1[vertInElem1].moleFraction(transportCompIdx1)
                                              : cParams.elemVolVarsCur1[vertInElem1].massFraction(transportCompIdx1);

        const Scalar normalPhaseFlux1 = boundaryVars1.normalVelocity() * density1;
        const Scalar diffusiveMoleFlux1 = bfNormal1
                                          * boundaryVars1.moleFractionGrad(transportCompIdx1)
                                          * (boundaryVars1.diffusionCoeff(transportCompIdx1)
                                            + boundaryVars1.eddyDiffusivity())
                                          * boundaryVars1.molarDensity();
        Scalar diffusiveNetMassFlux = diffusiveMoleFlux1 * FluidSystem::molarMass(transportCompIdx1)
                                      - diffusiveMoleFlux1 * FluidSystem::molarMass(phaseCompIdx1);
        if (useMoles)
        {
            diffusiveNetMassFlux = 0.0;
        }

        // TOTAL MASS Balance
        // Neumann-like conditions
        if (cParams.boundaryTypes2.isCouplingNeumann(contiTotalMassIdx2))
        {
            couplingRes2.accumulate(lfsu2.child(contiTotalMassIdx2), vertInElem2,
                                    -normalPhaseFlux1 + diffusiveNetMassFlux);
        }

        // TOTAL MASS Balance
        // Dirichlet-like
        if (cParams.boundaryTypes2.isCouplingDirichlet(contiTotalMassIdx2))
        {
            couplingRes2.accumulate(lfsu2.child(contiTotalMassIdx2), vertInElem2,
                                    -cParams.elemVolVarsCur1[vertInElem1].pressure());
        }

        // COMPONENT Balance
        // Neumann-like conditions
        if (cParams.boundaryTypes1.isCouplingNeumann(transportEqIdx1))
        {
            DUNE_THROW(Dune::NotImplemented, "The coupling condition isCouplingNeumann(transportEqIdx1) is not implemented.");
        }
        if (cParams.boundaryTypes2.isCouplingNeumann(contiWEqIdx2))
        {
            if (globalProblem_.sdProblem1().isCornerPoint(globalPos1))
            {
                Scalar advectiveFlux = normalPhaseFlux1 * massMoleFrac1;
                Scalar diffusiveFlux = diffusiveMoleFlux1;
                if (!useMoles)
                {
                    diffusiveFlux *= FluidSystem::molarMass(transportCompIdx1);
                }

                couplingRes2.accumulate(lfsu2.child(contiWEqIdx2), vertInElem2,
                                        -(advectiveFlux - diffusiveFlux));
            }
            else
            {
                static_assert(GET_PROP_VALUE(Transport2cTypeTag, UseMoles) == GET_PROP_VALUE(TwoPTwoCTypeTag, UseMoles),
                              "This coupling condition is not implemented for different formulations (mass/mole) in the subdomains.");

                // the component mass flux from the stokes domain
                couplingRes2.accumulate(lfsu2.child(contiWEqIdx2), vertInElem2,
                                        globalProblem_.localResidual1().residual(vertInElem1)[transportEqIdx1]);
            }
        }

        // COMPONENT Balance
        // Dirichlet-like conditions (coupling residual is added to "real" residual)
        if (cParams.boundaryTypes1.isCouplingDirichlet(transportEqIdx1))
        {
            static_assert(numComponents1 == 2,
                          "This part of the coupling is only implemented for 2 components in the Transport domain.");

            // set residualStokes[transportEqIdx1] = x in stokesnccouplinglocalresidual.hh
            // coupling residual is added to "real" residual
            Scalar massMoleFrac = useMoles ? cParams.elemVolVarsCur2[vertInElem2].moleFraction(nPhaseIdx2, wCompIdx2)
                                           : cParams.elemVolVarsCur2[vertInElem2].massFraction(nPhaseIdx2, wCompIdx2);
            couplingRes1.accumulate(lfsu1.child(transportEqIdx1), vertInElem1,
                                    -massMoleFrac);
        }
        if (cParams.boundaryTypes2.isCouplingDirichlet(contiWEqIdx2))
        {
            DUNE_THROW(Dune::NotImplemented, "This part of the coupling is only implemented for a 2-phase system in the Darcy domain.");
        }
    }

 protected:
    GlobalProblem& globalProblem() const
    { return globalProblem_; }

    Implementation *asImp_()
    { return static_cast<Implementation *> (this); }
    const Implementation *asImp_() const
    { return static_cast<const Implementation *> (this); }

 private:
    /*!
     * \brief A struct that contains data of the FF and PM including boundary types,
     *        volume variables in both subdomains and geometric information
     */
    struct CParams
    {
        BoundaryTypes1 boundaryTypes1;
        BoundaryTypes2 boundaryTypes2;
        ElementVolumeVariables1 elemVolVarsPrev1;
        ElementVolumeVariables1 elemVolVarsCur1;
        ElementVolumeVariables2 elemVolVarsPrev2;
        ElementVolumeVariables2 elemVolVarsCur2;
        FVElementGeometry1 fvGeometry1;
        FVElementGeometry2 fvGeometry2;
    };

    GlobalProblem& globalProblem_;
};

} // end namespace Dumux

#endif // DUMUX_2CTRANSPORT_2P2C_LOCALOPERATOR_HH
