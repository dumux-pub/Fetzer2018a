// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief This local operator extends the 2ctransport2p2clocaloperator
 *        by non-isothermal conditions.
 */
#ifndef DUMUX_TWOCNITRANSPORT2P2CNILOCALOPERATOR_HH
#define DUMUX_TWOCNITRANSPORT2P2CNILOCALOPERATOR_HH

#include <dumux/multidomain/2ctransport2p2c/localoperator.hh>

namespace Dumux {

/*!
 * \ingroup TwoPTwoCNITransportTwoCNIModel
 * \brief The extension of the local operator for the coupling of a two-component Transport model
 *        and a two-phase two-component Darcy model for non-isothermal conditions.
 */
template<class TypeTag>
class TwoCNITransportTwoPTwoCNILocalOperator : public TwoCTransportTwoPTwoCLocalOperator<TypeTag>
{
public:
    typedef typename GET_PROP_TYPE(TypeTag, Problem) GlobalProblem;

    // Get the TypeTags of the subproblems
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain1TypeTag) Transport2cniTypeTag;
    typedef typename GET_PROP_TYPE(TypeTag, SubDomain2TypeTag) TwoPTwoCNITypeTag;

    typedef typename GET_PROP_TYPE(Transport2cniTypeTag, FluxVariables) BoundaryVariables1;
    typedef typename GET_PROP_TYPE(TwoPTwoCNITypeTag, FluxVariables) BoundaryVariables2;

    // Multidomain Grid types
    typedef typename GET_PROP_TYPE(TypeTag, MultiDomainGrid) MDGrid;
    typedef typename GET_PROP_TYPE(Transport2cniTypeTag, GridView) Transport2cniGridView;
    typedef typename GET_PROP_TYPE(TwoPTwoCNITypeTag, GridView) TwoPTwoCNIGridView;

    typedef typename Transport2cniGridView::template Codim<0>::Entity SDElement1;
    typedef typename TwoPTwoCNIGridView::template Codim<0>::Entity SDElement2;

    typedef typename GET_PROP_TYPE(Transport2cniTypeTag, Indices) Transport2cniIndices;
    typedef typename GET_PROP_TYPE(TwoPTwoCNITypeTag, Indices) TwoPTwoCNIIndices;

    enum {
        dim = MDGrid::dimension,
        dimWorld = MDGrid::dimensionworld
    };
    enum { energyEqIdx1 = Transport2cniIndices::energyEqIdx };
    // indices in the Darcy domain
    enum { numPhases2 = GET_PROP_VALUE(TwoPTwoCNITypeTag, NumPhases),
           energyEqIdx2 = TwoPTwoCNIIndices::energyEqIdx,
           wPhaseIdx2 = TwoPTwoCNIIndices::wPhaseIdx,
           nPhaseIdx2 = TwoPTwoCNIIndices::nPhaseIdx };
    // indices of the components
    enum { transportCompIdx1 = Transport2cniIndices::transportCompIdx,
           phaseCompIdx1 = Transport2cniIndices::phaseCompIdx };

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename MDGrid::ctype CoordScalar;
    typedef Dune::FieldVector<CoordScalar, dimWorld> GlobalPosition;

    typedef TwoCTransportTwoPTwoCLocalOperator<TypeTag> ParentType;

    //! \brief The constructor
    TwoCNITransportTwoPTwoCNILocalOperator(GlobalProblem& globalProblem)
        : ParentType(globalProblem)
    { }

    static const bool doAlphaCoupling = true;
    static const bool doPatternCoupling = true;

    //! \copydoc Dumux::TwoCTransportTwoPTwoCLocalOperator::evalCoupling12()
    template<typename LFSU1, typename LFSU2, typename RES1, typename RES2, typename CParams>
    void evalCoupling(const LFSU1& lfsu1, const LFSU2& lfsu2,
                      const int vertInElem1, const int vertInElem2,
                      const SDElement1& sdElement1, const SDElement2& sdElement2,
                      const BoundaryVariables1& boundaryVars1, const BoundaryVariables2& boundaryVars2,
                      const CParams &cParams,
                      RES1& couplingRes1, RES2& couplingRes2) const
    {
        const GlobalPosition& globalPos1 = cParams.fvGeometry1.subContVol[vertInElem1].global;

        // evaluate coupling of mass and momentum balances
        ParentType::evalCoupling(lfsu1, lfsu2,
                                 vertInElem1, vertInElem2,
                                 sdElement1, sdElement2,
                                 boundaryVars1, boundaryVars2,
                                 cParams,
                                 couplingRes1, couplingRes2);

        if (cParams.boundaryTypes2.isCouplingNeumann(energyEqIdx2))
        {
            // compute fluxes explicitly at corner points - only quarter control volume
            if (this->globalProblem().sdProblem1().isCornerPoint(globalPos1))
            {
                DUNE_THROW(Dune::NotImplemented, "The coupling handling at the corners has to be implemented");
            }
            else
            {
                // coupling via the defect the energy flux from the transport domain
                couplingRes2.accumulate(lfsu2.child(energyEqIdx2), vertInElem2,
                                        this->globalProblem().localResidual1().residual(vertInElem1)[energyEqIdx1]);
            }
        }
        if (cParams.boundaryTypes2.isCouplingDirichlet(energyEqIdx2))
        {
           DUNE_THROW(Dune::NotImplemented, "This boundary condition is not implemented");
        }


        if (cParams.boundaryTypes1.isCouplingDirichlet(energyEqIdx1))
        {
            // set residualTransport[energyIdx1] = T in transport2cnilocalresidual.hh
            couplingRes1.accumulate(lfsu1.child(energyEqIdx1), vertInElem1,
                                    -cParams.elemVolVarsCur2[vertInElem2].temperature());
        }
        if (cParams.boundaryTypes1.isCouplingNeumann(energyEqIdx1))
        {
            DUNE_THROW(Dune::NotImplemented, "This boundary condition is not implemented");
        }
    }
};

} // end namespace Dumux

#endif // DUMUX_TWOCNITRANSPORT2P2CNILOCALOPERATOR_HH
