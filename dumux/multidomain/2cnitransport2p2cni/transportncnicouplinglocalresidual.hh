// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using the coupled compositional non-isothermal Transport box model.
 */

#ifndef DUMUX_TRANSPORTNCNI_COUPLING_LOCAL_RESIDUAL_HH
#define DUMUX_TRANSPORTNCNI_COUPLING_LOCAL_RESIDUAL_HH

#include <dumux/freeflow/transportncni/localresidual.hh>
#include <dumux/freeflow/transportncni/model.hh>

namespace Dumux
{
/*!
 * \ingroup ImplicitLocalResidual
 * \ingroup TwoPTwoCNITransportTwoCNIModel
 * \brief Element-wise calculation of the Jacobian matrix for problems
 *        using the coupled compositional non-isothermal Transport box model.
 *        It is derived from the compositional non-isothermal Transport box model.
 */
template<class TypeTag>
class TransportncniCouplingLocalResidual : public TransportncniLocalResidual<TypeTag>
{
protected:
    typedef typename GET_PROP_TYPE(TypeTag, LocalResidual) Implementation;
    typedef TransportncLocalResidual<TypeTag> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Scalar) Scalar;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;

    typedef typename GET_PROP_TYPE(TypeTag, Indices) Indices;

    enum { dim = GridView::dimension,
           dimWorld = GridView::dimensionworld };
    // model properties
    enum { numEq = GET_PROP_VALUE(TypeTag, NumEq),
           numComponents = Indices::numComponents };
    enum { isBox = GET_PROP_VALUE(TypeTag, ImplicitIsBox) };
    // indices of the equations
    enum { transportEqIdx = Indices::transportEqIdx,
           energyEqIdx = Indices::energyEqIdx };
    // indices of phase and transported component
    enum { phaseIdx = Indices::phaseIdx,
           transportCompIdx = Indices::transportCompIdx,
           temperatureIdx = Indices::temperatureIdx };

    typedef typename GET_PROP_TYPE(TypeTag, VolumeVariables) VolumeVariables;

    typedef typename GridView::IntersectionIterator IntersectionIterator;

public:
    /*!
     * \brief Modified boundary treatment for the coupling of a
     *        non-isothermal Transport model
     */
    void evalBoundary_()
    {
        ParentType::evalBoundary_();

        typedef Dune::ReferenceElements<Scalar, dim> ReferenceElements;
        typedef Dune::ReferenceElement<Scalar, dim> ReferenceElement;
        const ReferenceElement &refElement = ReferenceElements::general(this->element_().geometry().type());

        // loop over vertices of the element
        for (int scvIdx = 0; scvIdx < this->fvGeometry_().numScv; scvIdx++)
        {
            // consider only SCVs on the boundary
            if (this->fvGeometry_().subContVol[scvIdx].inner && isBox)
                continue;

            // evaluate boundary conditions for the intersections of the current element
            for (const auto& intersection : Dune::intersections(this->gridView_(), this->element_()))
            {
                // handle only intersections on the boundary
                if (!intersection.boundary())
                    continue;

                if (isBox)
                {
                    // assemble the boundary for all vertices of the current face
                    const int fIdx = intersection.indexInInside();
                    const int numFaceVertices = refElement.size(fIdx, 1, dim);

                    // loop over the single vertices on the current face
                    for (int faceVertexIdx = 0; faceVertexIdx < numFaceVertices; ++faceVertexIdx)
                    {
                        // only evaluate, if we consider the same face vertex as in the outer
                        // loop over the element vertices
                        if (refElement.subEntity(fIdx, 1, faceVertexIdx, dim) != scvIdx)
                            continue;

                        const VolumeVariables &volVars = this->curVolVars_()[scvIdx];

                        // set mole or mass fraction for the transported components
                        if (numComponents != 2)
                            DUNE_THROW(Dune::NotImplemented, "This part of the coupling is only implemented for 2 components.");

                        if (this->bcTypes_(scvIdx).isCouplingDirichlet(transportEqIdx))
                        {
                            if(GET_PROP_VALUE(TypeTag, UseMoles))
                                this->residual_[scvIdx][transportEqIdx] = volVars.moleFraction(transportCompIdx);
                            else
                                this->residual_[scvIdx][transportEqIdx] = volVars.massFraction(transportCompIdx);
                            Valgrind::CheckDefined(this->residual_[scvIdx][transportEqIdx]);
                        }

                        // set temperature
                        if (this->bcTypes_(scvIdx).isCouplingDirichlet(energyEqIdx))
                        {
                            this->residual_[scvIdx][energyEqIdx] = volVars.temperature();
                            Valgrind::CheckDefined(this->residual_[scvIdx][energyEqIdx]);
                        }
                    }
                }
            }
        }
    }
};

} // Dumux

#endif // DUMUX_TRANSPORTNCNI_COUPLING_LOCAL_RESIDUAL_HH
