// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \brief Boundary types gathered on an element
 */
#ifndef DUMUX_MD_CC_ELEMENT_BOUNDARY_TYPES_HH
#define DUMUX_MD_CC_ELEMENT_BOUNDARY_TYPES_HH

#include <vector>

#include <dumux/common/valgrind.hh>

#include <dumux/implicit/cellcentered/properties.hh>

namespace Dumux
{

/*!
 * \ingroup CCModel
 * \ingroup ImplicitBoundaryTypes
 * \brief This class stores an array of BoundaryTypes objects
 */
template<class TypeTag>
class MDCCElementBoundaryTypes : public std::vector<typename GET_PROP_TYPE(TypeTag, BoundaryTypes) >
{
    typedef typename GET_PROP_TYPE(TypeTag, BoundaryTypes) BoundaryTypes;
    typedef std::vector<BoundaryTypes> ParentType;

    typedef typename GET_PROP_TYPE(TypeTag, Problem) Problem;
    typedef typename GET_PROP_TYPE(TypeTag, GridView) GridView;
    typedef typename GET_PROP_TYPE(TypeTag, FVElementGeometry) FVElementGeometry;

    typedef typename GridView::template Codim<0>::Entity Element;

public:
    /*!
     * \brief Copy constructor.
     *
     * Copying a the boundary types of an element should be explicitly
     * requested
     */
    explicit MDCCElementBoundaryTypes(const MDCCElementBoundaryTypes &v)
        : ParentType(v)
    {}

    /*!
     * \brief Default constructor.
     */
    MDCCElementBoundaryTypes()
    {
        hasDirichlet_ = false;
        hasNeumann_ = false;
        hasOutflow_ = false;
        hasCoupling_ = false;
    }

    /*!
     * \brief Update the boundary types for all vertices of an element.
     *
     * \param problem The problem object which needs to be simulated
     * \param element The DUNE Codim<0> entity for which the boundary
     *                types should be collected
     */
    void update(const Problem &problem,
                const Element &element)
    {
        Dune::dgrave << "current size of ccelembctypes vector: " << this->size() << std::endl;
        this->resize(1);

        hasDirichlet_ = false;
        hasNeumann_ = false;
        hasOutflow_ = false;
        hasCoupling_ = false;

        (*this)[0].reset();

        if (problem.model().onBoundary(element))
        {
            for (const auto& intersection : intersections(problem.gridView(), element))
            {
                if (intersection.boundary())
                {
                    problem.boundaryTypes((*this)[0], intersection);

                    hasDirichlet_ = hasDirichlet_ || (*this)[0].hasDirichlet();
                    hasNeumann_ = hasNeumann_ || (*this)[0].hasNeumann();
                    hasOutflow_ = hasOutflow_ || (*this)[0].hasOutflow();
                    hasCoupling_ = hasCoupling_ || (*this)[0].hasCoupling();
                }
            }
        }
    }

    void update(const Problem &problem,
                const Element &element,
                const FVElementGeometry &fvGeometry)
    {
        update(problem, element);
    }

    /*!
     * \brief Returns whether the element has a vertex which contains
     *        a Dirichlet value.
     */
    bool hasDirichlet() const
    { return hasDirichlet_; }

    /*!
     * \brief Returns whether the element potentially features a
     *        Neumann boundary segment.
     */
    bool hasNeumann() const
    { return hasNeumann_; }

    /*!
     * \brief Returns whether the element potentially features an
     *        outflow boundary segment.
     */
    bool hasOutflow() const
    { return hasOutflow_; }

    /*!
     * \brief Returns whether the element potentially features a
     *        coupling boundary segment.
     */
    bool hasCoupling() const
    { return hasCoupling_; }

protected:
    bool hasDirichlet_;
    bool hasNeumann_;
    bool hasOutflow_;
    bool hasCoupling_;
};

} // namespace Dumux

#endif
