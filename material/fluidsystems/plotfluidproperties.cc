// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Plot the fluid properties using the gnuplot interface
 */

#include "config.h"
#include <cstring>
#include <vector>
#include <dumux/io/gnuplotinterface.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/air.hh>
#include <dumux/material/fluidstates/compositional.hh>
#include <dumux/material/fluidsystems/h2oair.hh>

using namespace std;
////////////////////////
// the main function
////////////////////////
int main(int argc, char *argv[])
{
    bool openPlotWindow = true;
    if (argc == 2 && (strcmp(argv[1], "0") || strcmp(argv[1], "false") || strcmp(argv[1], "False")))
        openPlotWindow = false;

    //fluid types
    typedef Dumux::FluidSystems::H2OAir<double, Dumux::H2O<double>, true> Fluidsystem;
    typedef Dumux::CompositionalFluidState<double, Fluidsystem> Fluidstate;

    unsigned int numPhases = 2;
    unsigned int liquidPhase = 0;
    unsigned int gasPhase = 1;
    vector<string> phaseNames(numPhases);
    phaseNames[liquidPhase] = "liquid";
    phaseNames[gasPhase] = "gas";

    unsigned int numIntervals = 100;
    double yMin = 1e100;
    double yMax = -1e100;
    vector<double> temp(numIntervals + 1);

    vector<double> T(numIntervals + 1);
    vector<double> massFraction = {0.0, 0.0001, 0.0005, 0.001, 0.005, 0.01};
    vector<double> relativeHumidity = {0.5, 1.0};
    vector<string> massFractionStr(massFraction.size());
    vector<string> relativeHumidityStr(relativeHumidity.size());
    unsigned numGasPhaseEntries = massFraction.size() + relativeHumidity.size();
    vector<vector<vector<double>>> density(numPhases);
    vector<vector<vector<double>>> viscosity(numPhases);
    vector<vector<vector<double>>> diffCoeff(numPhases);
    vector<vector<vector<double>>> thermCond(numPhases);
    for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
    {
        unsigned numEntries = massFraction.size();
        if (phaseIdx == 1)
            numEntries = numGasPhaseEntries;

        density[phaseIdx].resize(numEntries);
        viscosity[phaseIdx].resize(numEntries);
        diffCoeff[phaseIdx].resize(numEntries);
        thermCond[phaseIdx].resize(numEntries);
        for (unsigned entryIdx = 0; entryIdx < numEntries; entryIdx++)
        {
            density[phaseIdx][entryIdx].resize(numIntervals+1);
            viscosity[phaseIdx][entryIdx].resize(numIntervals+1);
            diffCoeff[phaseIdx][entryIdx].resize(numIntervals+1);
            thermCond[phaseIdx][entryIdx].resize(numIntervals+1);
        }
    }
    double pressure = 1e5;
    double TMin = 273.15;
    double TMax = 323.15;
    double TRange = TMax - TMin;
    vector<double> densityMin(numPhases, yMin);
    vector<double> densityMax(numPhases, yMax);
    vector<double> viscosityMin(numPhases, yMin);
    vector<double> viscosityMax(numPhases, yMax);
    vector<double> diffCoeffMin(numPhases, yMin);
    vector<double> diffCoeffMax(numPhases, yMax);
    vector<double> thermCondMin(numPhases, yMin);
    vector<double> thermCondMax(numPhases, yMax);

    Fluidstate fluidstate;
    fluidstate.setPressure(0, pressure);
    fluidstate.setPressure(1, pressure);
    for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
    {
        unsigned numEntries = massFraction.size();
        if (phaseIdx == 1)
            numEntries = numGasPhaseEntries;
        for (unsigned entryIdx = 0; entryIdx < numEntries; entryIdx++)
        {
            // set mass fractions
            if (entryIdx < massFraction.size())
            {
                fluidstate.setMassFraction(phaseIdx, 1-phaseIdx, massFraction[entryIdx]);
                // convert components of massFraction vector into a vector of strings
                if (phaseIdx == 0)
                    massFractionStr[entryIdx].append(static_cast<ostringstream*>( &(ostringstream() << massFraction[entryIdx]) )->str());
            }

            // get values from component functions
            for (int i = 0; i <= numIntervals; i++)
            {
                T[i] = TMin + TRange * double(i) /double(numIntervals);
                fluidstate.setTemperature(T[i]);
                // set relative humidity
                if (entryIdx >= massFraction.size())
                {
                    fluidstate.setRelativeHumidity(fluidstate, phaseIdx, 1-phaseIdx, relativeHumidity[entryIdx-massFraction.size()]);
                    // convert components of massFraction vector into a vector of strings
                    if (phaseIdx == 1 && i == 1)
                        relativeHumidityStr[entryIdx-massFraction.size()].append(static_cast<ostringstream*>( &(ostringstream() << relativeHumidity[entryIdx-massFraction.size()]) )->str());
                }
                density[phaseIdx][entryIdx][i] = Fluidsystem::density(fluidstate, phaseIdx);
                viscosity[phaseIdx][entryIdx][i] = Fluidsystem::viscosity(fluidstate, phaseIdx);
                diffCoeff[phaseIdx][entryIdx][i] = Fluidsystem::binaryDiffusionCoefficient(fluidstate, phaseIdx, 1-phaseIdx, phaseIdx);
                thermCond[phaseIdx][entryIdx][i] = Fluidsystem::thermalConductivity(fluidstate, phaseIdx);
                densityMin[phaseIdx] = std::min(densityMin[phaseIdx], density[phaseIdx][entryIdx][i]);
                densityMax[phaseIdx] = std::max(densityMax[phaseIdx], density[phaseIdx][entryIdx][i]);
                viscosityMin[phaseIdx] = std::min(viscosityMin[phaseIdx], viscosity[phaseIdx][entryIdx][i]);
                viscosityMax[phaseIdx] = std::max(viscosityMax[phaseIdx], viscosity[phaseIdx][entryIdx][i]);
                diffCoeffMin[phaseIdx] = std::min(diffCoeffMin[phaseIdx], diffCoeff[phaseIdx][entryIdx][i]);
                diffCoeffMax[phaseIdx] = std::max(diffCoeffMax[phaseIdx], diffCoeff[phaseIdx][entryIdx][i]);
                thermCondMin[phaseIdx] = std::min(thermCondMin[phaseIdx], thermCond[phaseIdx][entryIdx][i]);
                thermCondMax[phaseIdx] = std::max(thermCondMax[phaseIdx], thermCond[phaseIdx][entryIdx][i]);
            }
        }
    }

    // gas phase density over temperature for different water vapor fractions
    for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
    {
        Dumux::GnuplotInterface<double> gnuplot(true);
        gnuplot.setOpenPlotWindow(openPlotWindow);
        gnuplot.setXRange(TMin, TMax);
        gnuplot.setYRange(densityMin[phaseIdx], densityMax[phaseIdx]);
        gnuplot.setXlabel("temperature [K]");
        gnuplot.setYlabel(phaseNames[phaseIdx] + " density [kg/m3]");
        gnuplot.setDatafileSeparator(',');
        for (unsigned entryIdx = 0; entryIdx < massFraction.size(); entryIdx++)
        {
            gnuplot.addDataSetToPlot(T, density[phaseIdx][entryIdx], "#T[K],rho[kg/m^3]",
                                     phaseNames[phaseIdx] + "_density_X=" + massFractionStr[entryIdx] + ".csv");
        }
        for (unsigned entryIdx = 0; entryIdx + massFraction.size() < numGasPhaseEntries && phaseIdx == 1; entryIdx++)
        {
            gnuplot.addDataSetToPlot(T, density[phaseIdx][entryIdx + massFraction.size()], "#T[K],rho[kg/m^3]",
                                     phaseNames[phaseIdx] + "_density_RH=" + relativeHumidityStr[entryIdx] + ".csv");
        }
        gnuplot.plot(phaseNames[phaseIdx] + "_density");
    }

    // gas phase viscosity over temperature for different water vapor fractions
    for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
    {
        Dumux::GnuplotInterface<double> gnuplot(true);
        gnuplot.setOpenPlotWindow(openPlotWindow);
        gnuplot.setXRange(TMin, TMax);
        gnuplot.setYRange(viscosityMin[phaseIdx], viscosityMax[phaseIdx]);
        gnuplot.setXlabel("temperature [K]");
        gnuplot.setYlabel(phaseNames[phaseIdx] + " viscosity [Pa s]");
        gnuplot.setDatafileSeparator(',');
        for (unsigned entryIdx = 0; entryIdx < massFraction.size(); entryIdx++)
        {
            gnuplot.addDataSetToPlot(T, viscosity[phaseIdx][entryIdx], "#T[K],mu[Pa*s]",
                                     phaseNames[phaseIdx] + "_viscosity_X=" + massFractionStr[entryIdx] + ".csv");
        }
        for (unsigned entryIdx = 0; entryIdx + massFraction.size() < numGasPhaseEntries && phaseIdx == 1; entryIdx++)
        {
            gnuplot.addDataSetToPlot(T, viscosity[phaseIdx][entryIdx + massFraction.size()], "#T[K],mu[Pa*s]",
                                     phaseNames[phaseIdx] + "_viscosity_RH=" + relativeHumidityStr[entryIdx] + ".csv");
        }
        gnuplot.plot(phaseNames[phaseIdx] + "_viscosity");
    }

    // gas phase diffCoeff over temperature for different water vapor fractions
    for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
    {
        Dumux::GnuplotInterface<double> gnuplot(true);
        gnuplot.setOpenPlotWindow(openPlotWindow);
        gnuplot.setXRange(TMin, TMax);
        gnuplot.setYRange(diffCoeffMin[phaseIdx], diffCoeffMax[phaseIdx]);
        gnuplot.setXlabel("temperature [K]");
        gnuplot.setYlabel(phaseNames[phaseIdx] + " diffusion coefficient [m^2/s]");
        gnuplot.setDatafileSeparator(',');
        for (unsigned entryIdx = 0; entryIdx<massFraction.size(); entryIdx++)
        {
            gnuplot.addDataSetToPlot(T, diffCoeff[phaseIdx][entryIdx], "#T[K],D[m^2/s]",
                                     phaseNames[phaseIdx] + "_diffCoeff_X=" + massFractionStr[entryIdx] + ".csv");
        }
        gnuplot.plot(phaseNames[phaseIdx] + "_diffCoeff");
    }

    // gas phase thermCond over temperature for different water vapor fractions
    for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
    {
        Dumux::GnuplotInterface<double> gnuplot(true);
        gnuplot.setOpenPlotWindow(openPlotWindow);
        gnuplot.setXRange(TMin, TMax);
        gnuplot.setYRange(0.9*thermCondMin[phaseIdx], 1.1*thermCondMax[phaseIdx]);
        gnuplot.setXlabel("temperature [K]");
        gnuplot.setYlabel(phaseNames[phaseIdx] + " thermal conductivity [W/(m*K)]");
        gnuplot.setDatafileSeparator(',');
        for (unsigned entryIdx = 0; entryIdx<massFraction.size(); entryIdx++)
        {
            gnuplot.addDataSetToPlot(T, thermCond[phaseIdx][entryIdx], "#T[K],lambda[W/(m*K)]",
                                     phaseNames[phaseIdx] + "_thermCond_X=" + massFractionStr[entryIdx] + ".csv");
        }
        gnuplot.plot(phaseNames[phaseIdx] + "_thermCond");
    }
}
