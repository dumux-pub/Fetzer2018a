// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Plot properties of components and fluids
 */

#include "config.h"
#include <cstring>
#include <vector>
#include <dumux/io/gnuplotinterface.hh>
#include <dumux/material/components/air.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/n2.hh>

using namespace std;
////////////////////////
// the main function
////////////////////////
int main(int argc, char *argv[])
{
    bool openPlotWindow = true;
    if (argc == 2 && (strcmp(argv[1], "0") || strcmp(argv[1], "false") || strcmp(argv[1], "False")))
        openPlotWindow = false;

    // components
    const unsigned int liquidPhaseIdx = 0;
    const unsigned int gasPhaseIdx = 1;
    bool useLiquidPhase = false;
    bool useGasPhase = false;
#if AIR
    typedef Dumux::Air<double> Component;
    useGasPhase = true;
#elif NITROGEN
    typedef Dumux::N2<double> Component;
    useGasPhase = true;
#elif WATER
    typedef Dumux::H2O<double> Component;
    useLiquidPhase = true;
    useGasPhase = true;
#endif

    unsigned int numPhases = 2;

    vector<string> phaseNames(numPhases);
    phaseNames[liquidPhaseIdx] = "liquid";
    phaseNames[gasPhaseIdx] = "gas";

    unsigned int numIntervals = 100;
    double yMin = 1e100;
    double yMax = -1e100;
    vector<double> temp(numIntervals + 1);

    vector<double> T(numIntervals + 1);
    vector<vector<double>> density(numPhases);
    vector<vector<double>> heatCapacity(numPhases);
    vector<vector<double>> viscosity(numPhases);
    for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
    {
        density[phaseIdx].resize(numIntervals+1);
        heatCapacity[phaseIdx].resize(numIntervals+1);
        viscosity[phaseIdx].resize(numIntervals+1);
    }
    double pressure = 1e5;
    double TMin = 273.15;
    double TMax = 323.15;
    double TRange = TMax - TMin;
    vector<double> densityMin(numPhases, yMin);
    vector<double> densityMax(numPhases, yMax);
    vector<double> heatCapacityMin(numPhases, yMin);
    vector<double> heatCapacityMax(numPhases, yMax);
    vector<double> viscosityMin(numPhases, yMin);
    vector<double> viscosityMax(numPhases, yMax);

    // get values from component functions
    for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
    {
        if ((phaseIdx == liquidPhaseIdx && !useLiquidPhase)
            || (phaseIdx == gasPhaseIdx && !useGasPhase))
            continue;

        for (int i = 0; i <= numIntervals; i++)
        {
            T[i] = TMin + TRange * double(i) /double(numIntervals);

            if (phaseIdx == liquidPhaseIdx && useLiquidPhase)
            {
                density[liquidPhaseIdx][i] = Component::liquidDensity(T[i], pressure);
                heatCapacity[liquidPhaseIdx][i] = Component::liquidHeatCapacity(T[i], pressure);
                viscosity[liquidPhaseIdx][i] = Component::liquidViscosity(T[i], pressure);
            }
            if (phaseIdx == gasPhaseIdx && useGasPhase)
            {
                density[gasPhaseIdx][i] = Component::gasDensity(T[i], pressure);
                heatCapacity[gasPhaseIdx][i] = Component::gasHeatCapacity(T[i], pressure);
                viscosity[gasPhaseIdx][i] = Component::gasViscosity(T[i], pressure);
            }
            densityMin[phaseIdx] = std::min(densityMin[phaseIdx], density[phaseIdx][i]);
            densityMax[phaseIdx] = std::max(densityMax[phaseIdx], density[phaseIdx][i]);
            heatCapacityMin[phaseIdx] = std::min(heatCapacityMin[phaseIdx], heatCapacity[phaseIdx][i]);
            heatCapacityMax[phaseIdx] = std::max(heatCapacityMax[phaseIdx], heatCapacity[phaseIdx][i]);
            viscosityMin[phaseIdx] = std::min(viscosityMin[phaseIdx], viscosity[phaseIdx][i]);
            viscosityMax[phaseIdx] = std::max(viscosityMax[phaseIdx], viscosity[phaseIdx][i]);
        }
    }

    // plot densities
    for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
    {
        if ((phaseIdx == liquidPhaseIdx && !useLiquidPhase)
            || (phaseIdx == gasPhaseIdx && !useGasPhase))
            continue;

        Dumux::GnuplotInterface<double> gnuplot(true);
        gnuplot.setOpenPlotWindow(openPlotWindow);
        gnuplot.setXRange(TMin, TMax);
        gnuplot.setYRange(densityMin[phaseIdx], densityMax[phaseIdx]);
        gnuplot.setXlabel("temperature [K]");
        gnuplot.setYlabel(phaseNames[phaseIdx] + " density [kg/m3]");
        gnuplot.setDatafileSeparator(',');
        gnuplot.addDataSetToPlot(T, density[phaseIdx], "#T[K],rho[kg/m^3]", phaseNames[phaseIdx] + "_density.csv");
        gnuplot.plot(phaseNames[phaseIdx] + "_density");
    }

    // plot heat capacities
    for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
    {
        if ((phaseIdx == liquidPhaseIdx && !useLiquidPhase)
            || (phaseIdx == gasPhaseIdx && !useGasPhase))
            continue;

        Dumux::GnuplotInterface<double> gnuplot(true);
        gnuplot.setOpenPlotWindow(openPlotWindow);
        gnuplot.setXRange(TMin, TMax);
        gnuplot.setYRange(heatCapacityMin[phaseIdx], heatCapacityMax[phaseIdx]);
        gnuplot.setXlabel("temperature [K]");
        gnuplot.setYlabel(phaseNames[phaseIdx] + " heat capacity [J/(kg*K)]");
        gnuplot.setDatafileSeparator(',');
        gnuplot.addDataSetToPlot(T, heatCapacity[phaseIdx], "#T[K],cp[J/(kg*K)]", phaseNames[phaseIdx] + "_heatCapacity.csv");
        gnuplot.plot(phaseNames[phaseIdx] + "_heatCapacity");
    }

    // plot viscosities
    for (unsigned int phaseIdx = 0; phaseIdx < numPhases; ++phaseIdx)
    {
        if ((phaseIdx == liquidPhaseIdx && !useLiquidPhase)
            || (phaseIdx == gasPhaseIdx && !useGasPhase))
            continue;

        Dumux::GnuplotInterface<double> gnuplot(true);
        gnuplot.setOpenPlotWindow(openPlotWindow);
        gnuplot.setXRange(TMin, TMax);
        gnuplot.setYRange(viscosityMin[phaseIdx], viscosityMax[phaseIdx]);
        gnuplot.setXlabel("temperature [K]");
        gnuplot.setYlabel(phaseNames[phaseIdx] + " viscosity [Pa*s]");
        gnuplot.setDatafileSeparator(',');
        gnuplot.addDataSetToPlot(T, viscosity[phaseIdx], "#T[K],mu[Pa*s]", phaseNames[phaseIdx] + "_viscosity.csv");
        gnuplot.plot(phaseNames[phaseIdx] + "_viscosity");
    }
}
