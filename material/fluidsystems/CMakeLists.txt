dune_symlink_to_source_files(FILES references)

# add executables and tests
add_dumux_test(plotfluidproperties plotfluidproperties plotfluidproperties.cc
               python ${dumux_INCLUDE_DIRS}/bin/testing/runtest.py
#                   --script fuzzyData
#                   --relative "1e-3"
                  --script exact
                  --command "${CMAKE_CURRENT_BINARY_DIR}/plotfluidproperties 0"
                  --files
                    # gas
                    ${CMAKE_CURRENT_SOURCE_DIR}/references/gas_density_X=0.csv
                    ${CMAKE_CURRENT_BINARY_DIR}/gas_density_X=0.csv
                    ${CMAKE_CURRENT_SOURCE_DIR}/references/gas_diffCoeff_X=0.csv
                    ${CMAKE_CURRENT_BINARY_DIR}/gas_diffCoeff_X=0.csv
                    ${CMAKE_CURRENT_SOURCE_DIR}/references/gas_thermCond_X=0.csv
                    ${CMAKE_CURRENT_BINARY_DIR}/gas_thermCond_X=0.csv
                    ${CMAKE_CURRENT_SOURCE_DIR}/references/gas_viscosity_X=0.csv
                    ${CMAKE_CURRENT_BINARY_DIR}/gas_viscosity_X=0.csv
                    # liquid
                    ${CMAKE_CURRENT_SOURCE_DIR}/references/liquid_density_X=0.csv
                    ${CMAKE_CURRENT_BINARY_DIR}/liquid_density_X=0.csv
                    ${CMAKE_CURRENT_SOURCE_DIR}/references/liquid_diffCoeff_X=0.csv
                    ${CMAKE_CURRENT_BINARY_DIR}/liquid_diffCoeff_X=0.csv
                    ${CMAKE_CURRENT_SOURCE_DIR}/references/liquid_thermCond_X=0.csv
                    ${CMAKE_CURRENT_BINARY_DIR}/liquid_thermCond_X=0.csv
                    ${CMAKE_CURRENT_SOURCE_DIR}/references/liquid_viscosity_X=0.csv
                    ${CMAKE_CURRENT_BINARY_DIR}/liquid_viscosity_X=0.csv
                    )

add_dumux_test(plot_air plot_air plotproperties.cc echo "")
target_compile_definitions(plot_air PUBLIC "AIR=1")

add_dumux_test(plot_n2 plot_n2 plotproperties.cc echo "")
target_compile_definitions(plot_n2 PUBLIC "NITROGEN=1")

add_dumux_test(plot_h2o plot_h2o plotproperties.cc echo "")
target_compile_definitions(plot_h2o PUBLIC "WATER=1")
