// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Plot the Kelvin equation for a range of capillary pressures
 */

#include "config.h"
#include <string>
#include <vector>
#include <dumux/io/gnuplotinterface.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/air.hh>
#include <dumux/material/fluidstates/compositional.hh>
#include <dumux/material/fluidsystems/h2oair.hh>

using namespace std;
////////////////////////
// the main function
////////////////////////
int main(int argc, char *argv[])
{
    bool openPlotWindow = true;
    if (argc == 2 && (strcmp(argv[1], "0") || strcmp(argv[1], "false") || strcmp(argv[1], "False")))
        openPlotWindow = false;

    //fluid types
    typedef Dumux::FluidSystems::H2OAir<double, Dumux::H2O<double>, true> Fluidsystem;
    typedef Dumux::CompositionalFluidState<double, Fluidsystem> Fluidstate;

    unsigned int liquidPhase = 0;
    unsigned int gasPhase = 1;

    unsigned int numIntervals = 100;
    vector<double> temp(numIntervals+1);

    double pressure = 1e5;
    double temperature = 293.15;
    vector<double> capillaryPressure(numIntervals+1);
    double capillaryPressureMin = 1e5;
    double capillaryPressureMax = 1e9;
    vector<double> pwSat;
    vector<double> kelvin;
    pwSat.resize(capillaryPressure.size());
    kelvin.resize(capillaryPressure.size());

    Fluidstate fluidstate;
    fluidstate.setTemperature(temperature);
    fluidstate.setPressure(gasPhase, pressure);
    fluidstate.setMassFraction(liquidPhase, liquidPhase, 1);
    fluidstate.setMassFraction(gasPhase, gasPhase, 1);
    // get values from component functions
    for (int i = 0; i < numIntervals+1; i++)
    {
        capillaryPressure[i] = std::pow(10, log10(capillaryPressureMin) + (log10(capillaryPressureMax) - log10(capillaryPressureMin)) * double(i) / double(numIntervals));
        fluidstate.setPressure(liquidPhase, pressure-capillaryPressure[i]+1e-6);
        pwSat[i] = Fluidsystem::vaporPressure(fluidstate, liquidPhase) / pressure;
        kelvin[i] = Fluidsystem::kelvinVaporPressure(fluidstate, liquidPhase, liquidPhase) / pressure;
    }

    Dumux::GnuplotInterface<double> gnuplot(true);
    gnuplot.setOpenPlotWindow(openPlotWindow);
    gnuplot.setXRange(capillaryPressureMin, capillaryPressureMax);
    gnuplot.setXlabel("Capillary pressure [Pa]");
    gnuplot.setYlabel("Equilibrium water mole fraction x^w,eq_g [-]");
    gnuplot.setDatafileSeparator(',');
    gnuplot.setOption("set log x");
    gnuplot.setOption("set arrow from 15E5, graph 0 to 15E5, graph 1 nohead");
    gnuplot.setOption("set label 'wilting pressure 15bar' at 17E5, graph 0.5 center rotate by 90");
    gnuplot.addDataSetToPlot(capillaryPressure, pwSat, "#pc[Pa],x^w,eq_g[-]", "pwSat.csv");
    gnuplot.addDataSetToPlot(capillaryPressure, kelvin, "#pc[Pa],x^w,eq_g[-]", "kelvin.csv");
    gnuplot.plot("saturatedVaporPressure");
}
