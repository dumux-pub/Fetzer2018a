// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Plot the thermal conductivity laws
 */

#include "config.h"
#include <string>
#include <vector>
#include <dumux/io/gnuplotinterface.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/air.hh>
#include <dumux/material/fluidstates/compositional.hh>
#include <dumux/material/fluidsystems/h2oair.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivityjohansen.hh>
#include <dumux/material/fluidmatrixinteractions/2p/thermalconductivitysomerton.hh>

using namespace std;
////////////////////////
// the main function
////////////////////////
int main(int argc, char *argv[])
{
    bool openPlotWindow = true;
    if (argc == 2 && (strcmp(argv[1], "0") || strcmp(argv[1], "false") || strcmp(argv[1], "False")))
        openPlotWindow = false;

    //fluid types
    typedef Dumux::FluidSystems::H2OAir<double, Dumux::H2O<double>, true> Fluidsystem;
    typedef Dumux::CompositionalFluidState<double, Fluidsystem> Fluidstate;
    typedef Dumux::ThermalConductivityJohansen<double> Johansen;
    typedef Dumux::ThermalConductivitySomerton<double> Somerton;

    unsigned int liquidPhase = 0;
    unsigned int gasPhase = 1;

    unsigned int numIntervals = 100;
    vector<double> temp(numIntervals+1);

    double pressure = 1e5;
    double temperature = 293.15;
    double lambdaSolid = 5.9;
    double porosity = 0.334;
    double rhoSolid = 2700.;

    vector<double> saturation(numIntervals+1);
    double saturationMin = 0.0;
    double saturationMax = 1.0;
    vector<double> johansen;
    vector<double> somerton;
    johansen.resize(saturation.size());
    somerton.resize(saturation.size());

    Fluidstate fluidstate;
    fluidstate.setTemperature(temperature);
    fluidstate.setPressure(gasPhase, pressure);
    fluidstate.setMassFraction(liquidPhase, liquidPhase, 1);
    fluidstate.setMassFraction(gasPhase, gasPhase, 1);
    double lambdaW = Fluidsystem::thermalConductivity(fluidstate, liquidPhase);
    double lambdaN = Fluidsystem::thermalConductivity(fluidstate, gasPhase);
    // get values from component functions
    for (int i = 0; i < numIntervals+1; i++)
    {
        saturation[i] = saturationMin + (saturationMax - saturationMin) * double(i) / double(numIntervals);
        johansen[i] = Johansen::effectiveThermalConductivity(saturation[i], lambdaW, lambdaN, lambdaSolid,
                                                             porosity, rhoSolid);
        somerton[i] = Somerton::effectiveThermalConductivity(saturation[i], lambdaW, lambdaN, lambdaSolid,
                                                             porosity, rhoSolid);
    }

    Dumux::GnuplotInterface<double> gnuplot(true);
    gnuplot.setOpenPlotWindow(openPlotWindow);
    gnuplot.setXRange(saturationMin, saturationMax);
    gnuplot.setXlabel("Liquid saturation [-]");
    gnuplot.setYlabel("Effective thermal conductivity [W/(m*K)]");
    gnuplot.setDatafileSeparator(',');
    gnuplot.addDataSetToPlot(saturation, johansen, "#Sw[-],lambda[W/(m*K)]", "johansen.csv");
    gnuplot.addDataSetToPlot(saturation, somerton, "#Sw[-],lambda[W/(m*K)]", "somerton.csv");
    gnuplot.plot("thermalConductivity");
}
