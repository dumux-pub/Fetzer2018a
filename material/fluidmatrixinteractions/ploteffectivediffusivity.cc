// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 *
 * \brief Plot the thermal conductivity laws
 */

#include "config.h"
#include <string>
#include <vector>
#include <dumux/io/gnuplotinterface.hh>
#include <dumux/material/components/h2o.hh>
#include <dumux/material/components/air.hh>
#include <dumux/material/fluidstates/compositional.hh>
#include <dumux/material/fluidsystems/h2oair.hh>
#include <dumux/material/fluidmatrixinteractions/diffusivitymillingtonquirk.hh>

using namespace std;
////////////////////////
// the main function
////////////////////////
int main(int argc, char *argv[])
{
    bool openPlotWindow = true;
    if (argc == 2 && (strcmp(argv[1], "0") || strcmp(argv[1], "false") || strcmp(argv[1], "False")))
        openPlotWindow = false;

    //fluid types
    typedef Dumux::FluidSystems::H2OAir<double, Dumux::H2O<double>, true> Fluidsystem;
    typedef Dumux::CompositionalFluidState<double, Fluidsystem> Fluidstate;
    typedef Dumux::DiffusivityMillingtonQuirk<double> Millington;

    unsigned int liquidPhase = 0;
    unsigned int gasPhase = 1;

    unsigned int numIntervals = 100;
    vector<double> temp(numIntervals+1);

    double pressure = 1e5;
    double temperature = 293.15;
    double porosity = 0.334;

    vector<double> saturation(numIntervals+1);
    double saturationMin = 0.0;
    double saturationMax = 1.0;
    vector<double> molecular;
    vector<double> millington;
    vector<double> reductionMillington;
    molecular.resize(saturation.size());
    millington.resize(saturation.size());
    reductionMillington.resize(saturation.size());

    Fluidstate fluidstate;
    fluidstate.setTemperature(temperature);
    fluidstate.setPressure(gasPhase, pressure);
    fluidstate.setMassFraction(liquidPhase, liquidPhase, 1);
    fluidstate.setMassFraction(gasPhase, gasPhase, 1);
    unsigned int phase = gasPhase;
    double diffCoeff = Fluidsystem::binaryDiffusionCoefficient(fluidstate, phase, gasPhase, liquidPhase);
    // get values from component functions
    for (int i = 0; i < numIntervals+1; i++)
    {
        saturation[i] = saturationMin + (saturationMax - saturationMin) * double(i) / double(numIntervals);
        molecular[i] = diffCoeff;
        millington[i] = Millington::effectiveDiffusivity(porosity, phase - saturation[i], diffCoeff);
        reductionMillington[i] = Millington::effectiveDiffusivity(porosity, phase - saturation[i], 1.0);
    }

    Dumux::GnuplotInterface<double> gnuplot(true);
    gnuplot.setOpenPlotWindow(openPlotWindow);
    gnuplot.setXRange(saturationMin, saturationMax);
    gnuplot.setDatafileSeparator(',');
    gnuplot.setXlabel("Liquid saturation [-]");
//     gnuplot.setYlabel("Effective diffusivity [m^2/s]");
//     gnuplot.addDataSetToPlot(saturation, molecular, "molecular.csv");
//     gnuplot.addDataSetToPlot(saturation, millington, "millington.csv");
    gnuplot.setYlabel("Gas diffusivity factor [-]");
    gnuplot.addDataSetToPlot(saturation, reductionMillington, "#Sw[-],factor[-]", "reductionMillington.csv");
    gnuplot.plot("effectiveDiffusivity");
}
