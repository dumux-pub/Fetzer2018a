reset
set angles degrees

# soil parameters
porosity=0.46
grainDiameter=0.00125
Swr=0.028
permeability=grainDiameter**2*porosity**3/(180.*(1-porosity)**2)

# fluidmatrixinteractions
sigma=71.97e-3 # surface tension in mN/m, soruce: https://en.wikipedia.org/wiki/Surface_tension#Surface_tension_of_water_and_of_seawater
contactAngle=60 # source: https://core.ac.uk/download/pdf/10193533.pdf

# initial guess for fit
n=8.0
a=0.0034

# pc-Sw Leverett
leverett(x)=sqrt(porosity/(permeability))*sigma*cos(60)*(0.364*(1.-exp(-40.*(1.-x)))+0.221*(1.-x)+0.005/(x-Swr))
# pc-Sw vanGenuchten
vanGenuchten(x,a,n)=1/a *(((x-Swr)/(1-Swr))**(-1/(1-1/n))-1)**(1/n)

# write data points for fit
yMax=-123
set print "temp.txt"
do for [x=1:10] {
  if(Swr<0.1) {
    i=x*(0.1-Swr)/10.0+Swr
    print i, leverett(i)
    if(yMax==-123) { yMax=leverett(i) }
  }
}
do for [x=10:90:10] {
  i=x/100.0
  if(i>Swr) {
    print i, leverett(i)
    if(yMax==-123) { yMax=leverett(i) }
  }
}
do for [x=90:100] {
  i=x/100.0
  if(i>Swr) { print i, leverett(i) }
}

# perform the fit
fit [0:1] vanGenuchten(x,a,n) 'temp.txt'  via a,n

# print results
set print "-"
print "\nRESULTS\n-------\n"
print "permeability = ", permeability
print "vgN          = ", n
print "vgAlpha      = ", a

set xrange[0:1]
set yrange[0:1.5*yMax]
plot leverett(x),\
    "temp.txt", \
    vanGenuchten(x,a,n)

set term pngcairo size 800,600
set output "fitVanGenuchtenToLeverett.png"
replot

!rm "temp.txt"
